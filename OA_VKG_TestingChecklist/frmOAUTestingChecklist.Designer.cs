﻿namespace OA_VKG_TestingChecklist
{
    partial class frmOAUTestingChecklist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tbCircuit2Subcooling = new System.Windows.Forms.TextBox();
            this.tbCircuit2DisTemp = new System.Windows.Forms.TextBox();
            this.tbCircuit2SDT = new System.Windows.Forms.TextBox();
            this.tbCircuit2DisPSI = new System.Windows.Forms.TextBox();
            this.tbCircuit2LiqTemp = new System.Windows.Forms.TextBox();
            this.tbCircuit2SLT = new System.Windows.Forms.TextBox();
            this.tbCircuit2LiqPSI = new System.Windows.Forms.TextBox();
            this.tbCircuit2SuctTemp = new System.Windows.Forms.TextBox();
            this.tbCircuit2SST = new System.Windows.Forms.TextBox();
            this.tbCircuit2SuctPSI = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.gbCircuit1 = new System.Windows.Forms.GroupBox();
            this.txtSuperHeat = new System.Windows.Forms.TextBox();
            this.lblSuperHeat = new System.Windows.Forms.Label();
            this.chkbCircuit1APRActive = new System.Windows.Forms.CheckBox();
            this.tbCircuit1Subcooling = new System.Windows.Forms.TextBox();
            this.tbCircuit1DisTemp = new System.Windows.Forms.TextBox();
            this.tbCircuit1SDT = new System.Windows.Forms.TextBox();
            this.tbCircuit1DisPSI = new System.Windows.Forms.TextBox();
            this.tbCircuit1LiqTemp = new System.Windows.Forms.TextBox();
            this.tbCircuit1SLT = new System.Windows.Forms.TextBox();
            this.tbCircuit1LiqPSI = new System.Windows.Forms.TextBox();
            this.tbCircuit1SuctTemp = new System.Windows.Forms.TextBox();
            this.tbCircuit1SST = new System.Windows.Forms.TextBox();
            this.tbCircuit1SuctPSI = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.gbHeat = new System.Windows.Forms.GroupBox();
            this.cbHeatSectionFuelType = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.tbHeatSizeBottom = new System.Windows.Forms.TextBox();
            this.tbHeatPartNumBottom = new System.Windows.Forms.TextBox();
            this.tbHeatSerialNumBottom = new System.Windows.Forms.TextBox();
            this.tbHeatModelNumBottom = new System.Windows.Forms.TextBox();
            this.label110 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.tbHeatRunningAmps = new System.Windows.Forms.TextBox();
            this.tbHeatRatedAmps = new System.Windows.Forms.TextBox();
            this.tbHeatSizeTop = new System.Windows.Forms.TextBox();
            this.tbHeatPONum = new System.Windows.Forms.TextBox();
            this.tbHeatItemNum = new System.Windows.Forms.TextBox();
            this.tbHeatPartNumTop = new System.Windows.Forms.TextBox();
            this.tbHeatSerialNumTop = new System.Windows.Forms.TextBox();
            this.tbHeatModelNumTop = new System.Windows.Forms.TextBox();
            this.cbHeatPosition = new System.Windows.Forms.ComboBox();
            this.cbHeatPrimary = new System.Windows.Forms.ComboBox();
            this.label35 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label125 = new System.Windows.Forms.Label();
            this.gbCircuit2 = new System.Windows.Forms.GroupBox();
            this.tbCircuit2SuperHeat = new System.Windows.Forms.TextBox();
            this.label153 = new System.Windows.Forms.Label();
            this.tbPreHeatSize = new System.Windows.Forms.TextBox();
            this.label124 = new System.Windows.Forms.Label();
            this.gbPreHeat = new System.Windows.Forms.GroupBox();
            this.tbPreHeatSerialNo = new System.Windows.Forms.TextBox();
            this.tbPreHeatModelNo = new System.Windows.Forms.TextBox();
            this.label117 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.tbPreHeatRunningAmps = new System.Windows.Forms.TextBox();
            this.tbPreHeatRatedAmps = new System.Windows.Forms.TextBox();
            this.label126 = new System.Windows.Forms.Label();
            this.tbAuditedBy = new System.Windows.Forms.TextBox();
            this.label123 = new System.Windows.Forms.Label();
            this.ButExit = new System.Windows.Forms.Button();
            this.butSave = new System.Windows.Forms.Button();
            this.tbModelNum = new System.Windows.Forms.TextBox();
            this.butPrint = new System.Windows.Forms.Button();
            this.tbTestedBy = new System.Windows.Forms.TextBox();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.tbJobName = new System.Windows.Forms.TextBox();
            this.tbSerialnum = new System.Windows.Forms.TextBox();
            this.gbERV = new System.Windows.Forms.GroupBox();
            this.tbERV_SerialNo = new System.Windows.Forms.TextBox();
            this.tbERV_ModelNo = new System.Windows.Forms.TextBox();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.chkbERVProperRotation = new System.Windows.Forms.CheckBox();
            this.tbERVRunningAmps = new System.Windows.Forms.TextBox();
            this.tbERVRatedAmps = new System.Windows.Forms.TextBox();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkbDoubleBlowerIndoor = new System.Windows.Forms.CheckBox();
            this.tbModelNumExhaust = new System.Windows.Forms.TextBox();
            this.tbModelNumIndoor = new System.Windows.Forms.TextBox();
            this.tbVoltageOutputExhaust = new System.Windows.Forms.TextBox();
            this.tbVoltageOutputIndoor = new System.Windows.Forms.TextBox();
            this.tbMinHzExhaust = new System.Windows.Forms.TextBox();
            this.tbMinHzIndoor = new System.Windows.Forms.TextBox();
            this.tbMaxHzExhaust = new System.Windows.Forms.TextBox();
            this.tbMaxHzIndoor = new System.Windows.Forms.TextBox();
            this.chkbDoubleBlowerExhaust = new System.Windows.Forms.CheckBox();
            this.dgvFilters = new System.Windows.Forms.DataGridView();
            this.FilterSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FilterStyle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Qty = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbFilters = new System.Windows.Forms.GroupBox();
            this.label60 = new System.Windows.Forms.Label();
            this.tbWheelSizeExhaust = new System.Windows.Forms.TextBox();
            this.tbRunningAmpsExhaust = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.tbWheelSizeIndoor = new System.Windows.Forms.TextBox();
            this.cbCCHeaterTested = new System.Windows.Forms.CheckBox();
            this.cbCCHeaterInstalled = new System.Windows.Forms.CheckBox();
            this.label116 = new System.Windows.Forms.Label();
            this.gbStandardEquipment = new System.Windows.Forms.GroupBox();
            this.cbRADampersTested = new System.Windows.Forms.CheckBox();
            this.cbRADampersInstalled = new System.Windows.Forms.CheckBox();
            this.label84 = new System.Windows.Forms.Label();
            this.cbAPRTested = new System.Windows.Forms.CheckBox();
            this.cbAPRInstalled = new System.Windows.Forms.CheckBox();
            this.label83 = new System.Windows.Forms.Label();
            this.cbDigitalScrollTested = new System.Windows.Forms.CheckBox();
            this.cbDigitalScrollInstalled = new System.Windows.Forms.CheckBox();
            this.label81 = new System.Windows.Forms.Label();
            this.cbOADampersTested = new System.Windows.Forms.CheckBox();
            this.cbOADampersInstalled = new System.Windows.Forms.CheckBox();
            this.cbFilterStatusTested = new System.Windows.Forms.CheckBox();
            this.cbFilterStatusInstalled = new System.Windows.Forms.CheckBox();
            this.cbFanFailureTested = new System.Windows.Forms.CheckBox();
            this.cbFanFailureInstalled = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.tbDisconnectSize = new System.Windows.Forms.TextBox();
            this.cbDisconnectType = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.gbDisconnect = new System.Windows.Forms.GroupBox();
            this.nudCircuit1Charge = new System.Windows.Forms.NumericUpDown();
            this.tbCircuit1ChargeVariance = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.gbCharge = new System.Windows.Forms.GroupBox();
            this.nudCircuit2Charge = new System.Windows.Forms.NumericUpDown();
            this.tbCircuit2ChargeVariance = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.tbRunningAmpsIndoor = new System.Windows.Forms.TextBox();
            this.nudTemperature = new System.Windows.Forms.NumericUpDown();
            this.tbVoltageL3G = new System.Windows.Forms.TextBox();
            this.tbVoltageL2G = new System.Windows.Forms.TextBox();
            this.tbVoltageL1G = new System.Windows.Forms.TextBox();
            this.tbVoltageL1L3 = new System.Windows.Forms.TextBox();
            this.tbVoltageL2L3 = new System.Windows.Forms.TextBox();
            this.tbVoltageL1L2 = new System.Windows.Forms.TextBox();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.gbBlowers = new System.Windows.Forms.GroupBox();
            this.tbRatedAmpsExhaust = new System.Windows.Forms.TextBox();
            this.tbRatedAmpsIndoor = new System.Windows.Forms.TextBox();
            this.tbHpExhaust = new System.Windows.Forms.TextBox();
            this.tbHpIndoor = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.rbVoltage575 = new System.Windows.Forms.RadioButton();
            this.rbVoltage460 = new System.Windows.Forms.RadioButton();
            this.rbVoltage208 = new System.Windows.Forms.RadioButton();
            this.nudHumidity = new System.Windows.Forms.NumericUpDown();
            this.label72 = new System.Windows.Forms.Label();
            this.tbJobNum = new System.Windows.Forms.TextBox();
            this.gbAmbientConditions = new System.Windows.Forms.GroupBox();
            this.label70 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.gbVoltage = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cbPreHeatTested = new System.Windows.Forms.CheckBox();
            this.cbPreHeatInstalled = new System.Windows.Forms.CheckBox();
            this.label109 = new System.Windows.Forms.Label();
            this.cbHotGasReheatTested = new System.Windows.Forms.CheckBox();
            this.cbHotGasReheatInstalled = new System.Windows.Forms.CheckBox();
            this.label108 = new System.Windows.Forms.Label();
            this.cbReversingValvesTested = new System.Windows.Forms.CheckBox();
            this.cbReversingValvesInstalled = new System.Windows.Forms.CheckBox();
            this.cbAuxiliaryLightsTested = new System.Windows.Forms.CheckBox();
            this.cbAuxiliaryLightsInstalled = new System.Windows.Forms.CheckBox();
            this.label106 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.cbSupplyPiezoTested = new System.Windows.Forms.CheckBox();
            this.cbSupplyPiezoInstalled = new System.Windows.Forms.CheckBox();
            this.label102 = new System.Windows.Forms.Label();
            this.cbProtoNodeTested = new System.Windows.Forms.CheckBox();
            this.cbProtoNodeInstalled = new System.Windows.Forms.CheckBox();
            this.cbExhaustPiezoTested = new System.Windows.Forms.CheckBox();
            this.cbExhaustPiezoInstalled = new System.Windows.Forms.CheckBox();
            this.cbConvenienceOutletTested = new System.Windows.Forms.CheckBox();
            this.cbConvenienceOutletInstalled = new System.Windows.Forms.CheckBox();
            this.label101 = new System.Windows.Forms.Label();
            this.cbBypassDampersTested = new System.Windows.Forms.CheckBox();
            this.cbBypassDampersInstalled = new System.Windows.Forms.CheckBox();
            this.label100 = new System.Windows.Forms.Label();
            this.cbERVTested = new System.Windows.Forms.CheckBox();
            this.cbERVInstalled = new System.Windows.Forms.CheckBox();
            this.label99 = new System.Windows.Forms.Label();
            this.cbPoweredExhaustTested = new System.Windows.Forms.CheckBox();
            this.cbPoweredExhaustInstalled = new System.Windows.Forms.CheckBox();
            this.label98 = new System.Windows.Forms.Label();
            this.cbHeaterTested = new System.Windows.Forms.CheckBox();
            this.cbHeaterInstalled = new System.Windows.Forms.CheckBox();
            this.label97 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.gbOptions = new System.Windows.Forms.GroupBox();
            this.cbGreenTrolTested = new System.Windows.Forms.CheckBox();
            this.cbGreenTrolInstalled = new System.Windows.Forms.CheckBox();
            this.lblGreenTrol = new System.Windows.Forms.Label();
            this.cbReturnSmokeDetectorTested = new System.Windows.Forms.CheckBox();
            this.cbReturnSmokeDetectorInstalled = new System.Windows.Forms.CheckBox();
            this.label86 = new System.Windows.Forms.Label();
            this.cbSupplySmokeDectectorTested = new System.Windows.Forms.CheckBox();
            this.cbSupplySmokeDectectorInstalled = new System.Windows.Forms.CheckBox();
            this.label85 = new System.Windows.Forms.Label();
            this.cbLEDServiceTested = new System.Windows.Forms.CheckBox();
            this.cbLEDServiceInstalled = new System.Windows.Forms.CheckBox();
            this.label139 = new System.Windows.Forms.Label();
            this.cbHailguardTested = new System.Windows.Forms.CheckBox();
            this.cbHailguradInstalled = new System.Windows.Forms.CheckBox();
            this.label137 = new System.Windows.Forms.Label();
            this.cbIsolationDamperTested = new System.Windows.Forms.CheckBox();
            this.cbIsolationDamperoption = new System.Windows.Forms.CheckBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.lbHeadID = new System.Windows.Forms.Label();
            this.gbStdEquipViking = new System.Windows.Forms.GroupBox();
            this.cbOADamperTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbOADamperInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label127 = new System.Windows.Forms.Label();
            this.cbFSSTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbFSSInstalledVkg = new System.Windows.Forms.CheckBox();
            this.cbFanFailSwitchTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbFanFailSwitchInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label128 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.label131 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.cbRADampTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbRADampInstalledVkg = new System.Windows.Forms.CheckBox();
            this.cbOADampTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbOADampInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label132 = new System.Windows.Forms.Label();
            this.label133 = new System.Windows.Forms.Label();
            this.gbOptionsViking = new System.Windows.Forms.GroupBox();
            this.cbHailguardVkgTested = new System.Windows.Forms.CheckBox();
            this.cbHailguardVkgInstalled = new System.Windows.Forms.CheckBox();
            this.label80 = new System.Windows.Forms.Label();
            this.cbLEDServiceVkgTested = new System.Windows.Forms.CheckBox();
            this.cbLEDServiceVKgInstalled = new System.Windows.Forms.CheckBox();
            this.label77 = new System.Windows.Forms.Label();
            this.cbPreHeatTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbPreHeatInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label135 = new System.Windows.Forms.Label();
            this.cbHGRHTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbHGRHInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label136 = new System.Windows.Forms.Label();
            this.cbAuxLightsTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbAuxLightsInstalledVkg = new System.Windows.Forms.CheckBox();
            this.cbFiltStatSwitchTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbFiltStatSwitchInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label138 = new System.Windows.Forms.Label();
            this.cbExhPiezoTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbExhPiezoInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label140 = new System.Windows.Forms.Label();
            this.cbSupplyPiezoTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbSupplyPiezoInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label141 = new System.Windows.Forms.Label();
            this.label142 = new System.Windows.Forms.Label();
            this.cbConvOutletTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbConvOutletInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label143 = new System.Windows.Forms.Label();
            this.cbBypDampTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbBypDampInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label144 = new System.Windows.Forms.Label();
            this.cbERVTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbERVInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label145 = new System.Windows.Forms.Label();
            this.cbPwrExhTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbPwrExhInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label146 = new System.Windows.Forms.Label();
            this.cbHeaterTestedVkg = new System.Windows.Forms.CheckBox();
            this.cbHeaterInstalledVkg = new System.Windows.Forms.CheckBox();
            this.label147 = new System.Windows.Forms.Label();
            this.label148 = new System.Windows.Forms.Label();
            this.label149 = new System.Windows.Forms.Label();
            this.tcTestingChecklist = new System.Windows.Forms.TabControl();
            this.tpPreChecks = new System.Windows.Forms.TabPage();
            this.pnlPreChecksContainer = new System.Windows.Forms.Panel();
            this.preChecksControl1 = new OA_VKG_TestingChecklist.UserControlObject.PreChecksControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.pnlProductCheckListContainer = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tlpOptions = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.gbMeasurePressure = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label152 = new System.Windows.Forms.Label();
            this.lblLowFire = new System.Windows.Forms.Label();
            this.lblHighFire = new System.Windows.Forms.Label();
            this.label155 = new System.Windows.Forms.Label();
            this.label156 = new System.Windows.Forms.Label();
            this.label157 = new System.Windows.Forms.Label();
            this.label158 = new System.Windows.Forms.Label();
            this.label159 = new System.Windows.Forms.Label();
            this.label160 = new System.Windows.Forms.Label();
            this.txtInletModulationPresure = new System.Windows.Forms.TextBox();
            this.txtLowFire = new System.Windows.Forms.TextBox();
            this.txtHighFire = new System.Windows.Forms.TextBox();
            this.lblRangeNG = new System.Windows.Forms.Label();
            this.label154 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.gbCompressorData = new System.Windows.Forms.GroupBox();
            this.tbAMACompressorRatedAmps6 = new System.Windows.Forms.TextBox();
            this.tbCompressor6SerialNum = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRunningAmps6 = new System.Windows.Forms.TextBox();
            this.tbCompressor6ModelNum = new System.Windows.Forms.TextBox();
            this.label120 = new System.Windows.Forms.Label();
            this.tbAMACompressorRatedAmps5 = new System.Windows.Forms.TextBox();
            this.tbCompressor5SerialNum = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRunningAmps5 = new System.Windows.Forms.TextBox();
            this.tbCompressor5ModelNum = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.tbCompressor4SerialNum = new System.Windows.Forms.TextBox();
            this.label89 = new System.Windows.Forms.Label();
            this.tbCompressor3SerialNum = new System.Windows.Forms.TextBox();
            this.label90 = new System.Windows.Forms.Label();
            this.tbAMACompressorRatedAmps4 = new System.Windows.Forms.TextBox();
            this.tbCompressor2SerialNum = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRatedAmps3 = new System.Windows.Forms.TextBox();
            this.label91 = new System.Windows.Forms.Label();
            this.tbAMACompressorRatedAmps2 = new System.Windows.Forms.TextBox();
            this.tbCompressor1SerialNum = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRatedAmps1 = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRunningAmps4 = new System.Windows.Forms.TextBox();
            this.label96 = new System.Windows.Forms.Label();
            this.tbAMACompressorRunningAmps3 = new System.Windows.Forms.TextBox();
            this.label104 = new System.Windows.Forms.Label();
            this.tbCompressor4ModelNum = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.tbCompressor3ModelNum = new System.Windows.Forms.TextBox();
            this.label150 = new System.Windows.Forms.Label();
            this.tbCompressor2ModelNum = new System.Windows.Forms.TextBox();
            this.tbCompressor1ModelNum = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRunningAmps1 = new System.Windows.Forms.TextBox();
            this.tbAMACompressorRunningAmps2 = new System.Windows.Forms.TextBox();
            this.label151 = new System.Windows.Forms.Label();
            this.gbCondensers = new System.Windows.Forms.GroupBox();
            this.tbCondenserFanRatedAmps6 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRunningAmps6 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.tbCondenserFanRatedAmps5 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRunningAmps5 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.tbCondenserFanRatedAmps4 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRatedAmps3 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRatedAmps2 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRatedAmps1 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRunningAmps4 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRunningAmps3 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRunningAmps2 = new System.Windows.Forms.TextBox();
            this.tbCondenserFanRunningAmps1 = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.gbMiscNotes = new System.Windows.Forms.GroupBox();
            this.tbMiscNotes = new System.Windows.Forms.TextBox();
            this.btnPrintProduction = new System.Windows.Forms.Button();
            this.pnlJobContainer = new System.Windows.Forms.Panel();
            this.tlpJobContainer = new System.Windows.Forms.TableLayoutPanel();
            this.butIssues = new System.Windows.Forms.Button();
            this.cbLinenum = new System.Windows.Forms.ComboBox();
            this.lblLine = new System.Windows.Forms.Label();
            this.cbHorizonType = new System.Windows.Forms.ComboBox();
            this.lblUnitType = new System.Windows.Forms.Label();
            this.pnlContainerBottom = new System.Windows.Forms.Panel();
            this.tlpButtonContainer = new System.Windows.Forms.TableLayoutPanel();
            this.btnExportAll = new System.Windows.Forms.Button();
            this.errorProviderApp = new System.Windows.Forms.ErrorProvider(this.components);
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tbCompressor1 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.gbCircuit1.SuspendLayout();
            this.gbHeat.SuspendLayout();
            this.gbCircuit2.SuspendLayout();
            this.gbPreHeat.SuspendLayout();
            this.gbERV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilters)).BeginInit();
            this.gbFilters.SuspendLayout();
            this.gbStandardEquipment.SuspendLayout();
            this.gbDisconnect.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCircuit1Charge)).BeginInit();
            this.gbCharge.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCircuit2Charge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperature)).BeginInit();
            this.gbBlowers.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHumidity)).BeginInit();
            this.gbAmbientConditions.SuspendLayout();
            this.gbVoltage.SuspendLayout();
            this.gbOptions.SuspendLayout();
            this.gbStdEquipViking.SuspendLayout();
            this.gbOptionsViking.SuspendLayout();
            this.tcTestingChecklist.SuspendLayout();
            this.tpPreChecks.SuspendLayout();
            this.pnlPreChecksContainer.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.pnlProductCheckListContainer.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tlpOptions.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.gbMeasurePressure.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.gbCompressorData.SuspendLayout();
            this.gbCondensers.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.gbMiscNotes.SuspendLayout();
            this.pnlJobContainer.SuspendLayout();
            this.tlpJobContainer.SuspendLayout();
            this.pnlContainerBottom.SuspendLayout();
            this.tlpButtonContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderApp)).BeginInit();
            this.SuspendLayout();
            // 
            // tbCircuit2Subcooling
            // 
            this.tbCircuit2Subcooling.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2Subcooling.Location = new System.Drawing.Point(232, 68);
            this.tbCircuit2Subcooling.Name = "tbCircuit2Subcooling";
            this.tbCircuit2Subcooling.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2Subcooling.TabIndex = 10;
            this.tbCircuit2Subcooling.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2DisTemp
            // 
            this.tbCircuit2DisTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2DisTemp.Location = new System.Drawing.Point(156, 125);
            this.tbCircuit2DisTemp.Name = "tbCircuit2DisTemp";
            this.tbCircuit2DisTemp.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2DisTemp.TabIndex = 9;
            this.tbCircuit2DisTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2SDT
            // 
            this.tbCircuit2SDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2SDT.Location = new System.Drawing.Point(89, 125);
            this.tbCircuit2SDT.Name = "tbCircuit2SDT";
            this.tbCircuit2SDT.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2SDT.TabIndex = 8;
            this.tbCircuit2SDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2DisPSI
            // 
            this.tbCircuit2DisPSI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2DisPSI.Location = new System.Drawing.Point(22, 125);
            this.tbCircuit2DisPSI.Name = "tbCircuit2DisPSI";
            this.tbCircuit2DisPSI.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2DisPSI.TabIndex = 7;
            this.tbCircuit2DisPSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2LiqTemp
            // 
            this.tbCircuit2LiqTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2LiqTemp.Location = new System.Drawing.Point(156, 80);
            this.tbCircuit2LiqTemp.Name = "tbCircuit2LiqTemp";
            this.tbCircuit2LiqTemp.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2LiqTemp.TabIndex = 6;
            this.tbCircuit2LiqTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2SLT
            // 
            this.tbCircuit2SLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2SLT.Location = new System.Drawing.Point(89, 80);
            this.tbCircuit2SLT.Name = "tbCircuit2SLT";
            this.tbCircuit2SLT.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2SLT.TabIndex = 5;
            this.tbCircuit2SLT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2LiqPSI
            // 
            this.tbCircuit2LiqPSI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2LiqPSI.Location = new System.Drawing.Point(22, 80);
            this.tbCircuit2LiqPSI.Name = "tbCircuit2LiqPSI";
            this.tbCircuit2LiqPSI.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2LiqPSI.TabIndex = 4;
            this.tbCircuit2LiqPSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2SuctTemp
            // 
            this.tbCircuit2SuctTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2SuctTemp.Location = new System.Drawing.Point(156, 34);
            this.tbCircuit2SuctTemp.Name = "tbCircuit2SuctTemp";
            this.tbCircuit2SuctTemp.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2SuctTemp.TabIndex = 3;
            this.tbCircuit2SuctTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2SST
            // 
            this.tbCircuit2SST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2SST.Location = new System.Drawing.Point(89, 34);
            this.tbCircuit2SST.Name = "tbCircuit2SST";
            this.tbCircuit2SST.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2SST.TabIndex = 2;
            this.tbCircuit2SST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit2SuctPSI
            // 
            this.tbCircuit2SuctPSI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2SuctPSI.Location = new System.Drawing.Point(22, 34);
            this.tbCircuit2SuctPSI.Name = "tbCircuit2SuctPSI";
            this.tbCircuit2SuctPSI.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2SuctPSI.TabIndex = 1;
            this.tbCircuit2SuctPSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(164, 109);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(52, 13);
            this.label47.TabIndex = 10;
            this.label47.Text = "Dis.Temp";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(106, 109);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(29, 13);
            this.label48.TabIndex = 9;
            this.label48.Text = "SDT";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(31, 109);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(45, 13);
            this.label49.TabIndex = 8;
            this.label49.Text = "Dis. PSI";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(163, 64);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(54, 13);
            this.label50.TabIndex = 7;
            this.label50.Text = "Liq. Temp";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(106, 64);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(27, 13);
            this.label52.TabIndex = 5;
            this.label52.Text = "SLT";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(31, 64);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(44, 13);
            this.label53.TabIndex = 4;
            this.label53.Text = "Liq. PSI";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(229, 37);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(70, 26);
            this.label54.TabIndex = 3;
            this.label54.Text = "Subcooling:\r\n(range 11-17)";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(159, 18);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(62, 13);
            this.label55.TabIndex = 2;
            this.label55.Text = "Suct. Temp";
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(106, 18);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(28, 13);
            this.label56.TabIndex = 1;
            this.label56.Text = "SST";
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(135, 171);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(46, 13);
            this.label115.TabIndex = 104;
            this.label115.Text = "Bottom";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(27, 18);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(52, 13);
            this.label57.TabIndex = 0;
            this.label57.Text = "Suct. PSI";
            // 
            // gbCircuit1
            // 
            this.gbCircuit1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCircuit1.Controls.Add(this.txtSuperHeat);
            this.gbCircuit1.Controls.Add(this.lblSuperHeat);
            this.gbCircuit1.Controls.Add(this.chkbCircuit1APRActive);
            this.gbCircuit1.Controls.Add(this.tbCircuit1Subcooling);
            this.gbCircuit1.Controls.Add(this.tbCircuit1DisTemp);
            this.gbCircuit1.Controls.Add(this.tbCircuit1SDT);
            this.gbCircuit1.Controls.Add(this.tbCircuit1DisPSI);
            this.gbCircuit1.Controls.Add(this.tbCircuit1LiqTemp);
            this.gbCircuit1.Controls.Add(this.tbCircuit1SLT);
            this.gbCircuit1.Controls.Add(this.tbCircuit1LiqPSI);
            this.gbCircuit1.Controls.Add(this.tbCircuit1SuctTemp);
            this.gbCircuit1.Controls.Add(this.tbCircuit1SST);
            this.gbCircuit1.Controls.Add(this.tbCircuit1SuctPSI);
            this.gbCircuit1.Controls.Add(this.label46);
            this.gbCircuit1.Controls.Add(this.label45);
            this.gbCircuit1.Controls.Add(this.label44);
            this.gbCircuit1.Controls.Add(this.label43);
            this.gbCircuit1.Controls.Add(this.label42);
            this.gbCircuit1.Controls.Add(this.label41);
            this.gbCircuit1.Controls.Add(this.label40);
            this.gbCircuit1.Controls.Add(this.label39);
            this.gbCircuit1.Controls.Add(this.label38);
            this.gbCircuit1.Controls.Add(this.label37);
            this.gbCircuit1.Controls.Add(this.label36);
            this.gbCircuit1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCircuit1.Location = new System.Drawing.Point(3, 247);
            this.gbCircuit1.Name = "gbCircuit1";
            this.gbCircuit1.Size = new System.Drawing.Size(325, 158);
            this.gbCircuit1.TabIndex = 14;
            this.gbCircuit1.TabStop = false;
            this.gbCircuit1.Text = "Circuit #1";
            // 
            // txtSuperHeat
            // 
            this.txtSuperHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSuperHeat.Location = new System.Drawing.Point(237, 133);
            this.txtSuperHeat.Name = "txtSuperHeat";
            this.txtSuperHeat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtSuperHeat.Size = new System.Drawing.Size(64, 20);
            this.txtSuperHeat.TabIndex = 14;
            this.txtSuperHeat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblSuperHeat
            // 
            this.lblSuperHeat.AutoSize = true;
            this.lblSuperHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSuperHeat.Location = new System.Drawing.Point(234, 104);
            this.lblSuperHeat.Name = "lblSuperHeat";
            this.lblSuperHeat.Size = new System.Drawing.Size(70, 26);
            this.lblSuperHeat.TabIndex = 13;
            this.lblSuperHeat.Text = "Super Heat:\r\n(range 10-25)";
            // 
            // chkbCircuit1APRActive
            // 
            this.chkbCircuit1APRActive.AutoSize = true;
            this.chkbCircuit1APRActive.Location = new System.Drawing.Point(254, 43);
            this.chkbCircuit1APRActive.Name = "chkbCircuit1APRActive";
            this.chkbCircuit1APRActive.Size = new System.Drawing.Size(15, 14);
            this.chkbCircuit1APRActive.TabIndex = 10;
            this.chkbCircuit1APRActive.UseVisualStyleBackColor = true;
            // 
            // tbCircuit1Subcooling
            // 
            this.tbCircuit1Subcooling.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1Subcooling.Location = new System.Drawing.Point(235, 85);
            this.tbCircuit1Subcooling.Name = "tbCircuit1Subcooling";
            this.tbCircuit1Subcooling.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1Subcooling.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1Subcooling.TabIndex = 12;
            this.tbCircuit1Subcooling.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1DisTemp
            // 
            this.tbCircuit1DisTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1DisTemp.Location = new System.Drawing.Point(156, 125);
            this.tbCircuit1DisTemp.Name = "tbCircuit1DisTemp";
            this.tbCircuit1DisTemp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1DisTemp.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1DisTemp.TabIndex = 9;
            this.tbCircuit1DisTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1SDT
            // 
            this.tbCircuit1SDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1SDT.Location = new System.Drawing.Point(89, 125);
            this.tbCircuit1SDT.Name = "tbCircuit1SDT";
            this.tbCircuit1SDT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1SDT.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1SDT.TabIndex = 8;
            this.tbCircuit1SDT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1DisPSI
            // 
            this.tbCircuit1DisPSI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1DisPSI.Location = new System.Drawing.Point(22, 125);
            this.tbCircuit1DisPSI.Name = "tbCircuit1DisPSI";
            this.tbCircuit1DisPSI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1DisPSI.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1DisPSI.TabIndex = 7;
            this.tbCircuit1DisPSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1LiqTemp
            // 
            this.tbCircuit1LiqTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1LiqTemp.Location = new System.Drawing.Point(156, 80);
            this.tbCircuit1LiqTemp.Name = "tbCircuit1LiqTemp";
            this.tbCircuit1LiqTemp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1LiqTemp.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1LiqTemp.TabIndex = 6;
            this.tbCircuit1LiqTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1SLT
            // 
            this.tbCircuit1SLT.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1SLT.Location = new System.Drawing.Point(89, 80);
            this.tbCircuit1SLT.Name = "tbCircuit1SLT";
            this.tbCircuit1SLT.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1SLT.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1SLT.TabIndex = 5;
            this.tbCircuit1SLT.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1LiqPSI
            // 
            this.tbCircuit1LiqPSI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1LiqPSI.Location = new System.Drawing.Point(22, 80);
            this.tbCircuit1LiqPSI.Name = "tbCircuit1LiqPSI";
            this.tbCircuit1LiqPSI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1LiqPSI.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1LiqPSI.TabIndex = 4;
            this.tbCircuit1LiqPSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1SuctTemp
            // 
            this.tbCircuit1SuctTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1SuctTemp.Location = new System.Drawing.Point(156, 35);
            this.tbCircuit1SuctTemp.Name = "tbCircuit1SuctTemp";
            this.tbCircuit1SuctTemp.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1SuctTemp.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1SuctTemp.TabIndex = 3;
            this.tbCircuit1SuctTemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1SST
            // 
            this.tbCircuit1SST.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1SST.Location = new System.Drawing.Point(89, 35);
            this.tbCircuit1SST.Name = "tbCircuit1SST";
            this.tbCircuit1SST.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1SST.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1SST.TabIndex = 2;
            this.tbCircuit1SST.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCircuit1SuctPSI
            // 
            this.tbCircuit1SuctPSI.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit1SuctPSI.Location = new System.Drawing.Point(22, 35);
            this.tbCircuit1SuctPSI.Name = "tbCircuit1SuctPSI";
            this.tbCircuit1SuctPSI.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit1SuctPSI.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit1SuctPSI.TabIndex = 1;
            this.tbCircuit1SuctPSI.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(163, 109);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(52, 13);
            this.label46.TabIndex = 10;
            this.label46.Text = "Dis.Temp";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(105, 109);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(29, 13);
            this.label45.TabIndex = 9;
            this.label45.Text = "SDT";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(30, 109);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(45, 13);
            this.label44.TabIndex = 8;
            this.label44.Text = "Dis. PSI";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(162, 64);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(54, 13);
            this.label43.TabIndex = 7;
            this.label43.Text = "Liq. Temp";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(234, 12);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(62, 26);
            this.label42.TabIndex = 6;
            this.label42.Text = "APR Active\r\nDuring Test";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(105, 64);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(27, 13);
            this.label41.TabIndex = 5;
            this.label41.Text = "SLT";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(30, 64);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(44, 13);
            this.label40.TabIndex = 4;
            this.label40.Text = "Liq. PSI";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(232, 56);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(70, 26);
            this.label39.TabIndex = 3;
            this.label39.Text = "Subcooling:\r\n(range 11-17)";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(158, 19);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(62, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Suct. Temp";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(105, 19);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(28, 13);
            this.label37.TabIndex = 1;
            this.label37.Text = "SST";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(26, 19);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(52, 13);
            this.label36.TabIndex = 0;
            this.label36.Text = "Suct. PSI";
            // 
            // gbHeat
            // 
            this.gbHeat.Controls.Add(this.cbHeatSectionFuelType);
            this.gbHeat.Controls.Add(this.label21);
            this.gbHeat.Controls.Add(this.label115);
            this.gbHeat.Controls.Add(this.label114);
            this.gbHeat.Controls.Add(this.tbHeatSizeBottom);
            this.gbHeat.Controls.Add(this.tbHeatPartNumBottom);
            this.gbHeat.Controls.Add(this.tbHeatSerialNumBottom);
            this.gbHeat.Controls.Add(this.tbHeatModelNumBottom);
            this.gbHeat.Controls.Add(this.label110);
            this.gbHeat.Controls.Add(this.label111);
            this.gbHeat.Controls.Add(this.label112);
            this.gbHeat.Controls.Add(this.label113);
            this.gbHeat.Controls.Add(this.tbHeatRunningAmps);
            this.gbHeat.Controls.Add(this.tbHeatRatedAmps);
            this.gbHeat.Controls.Add(this.tbHeatSizeTop);
            this.gbHeat.Controls.Add(this.tbHeatPONum);
            this.gbHeat.Controls.Add(this.tbHeatItemNum);
            this.gbHeat.Controls.Add(this.tbHeatPartNumTop);
            this.gbHeat.Controls.Add(this.tbHeatSerialNumTop);
            this.gbHeat.Controls.Add(this.tbHeatModelNumTop);
            this.gbHeat.Controls.Add(this.cbHeatPosition);
            this.gbHeat.Controls.Add(this.cbHeatPrimary);
            this.gbHeat.Controls.Add(this.label35);
            this.gbHeat.Controls.Add(this.label34);
            this.gbHeat.Controls.Add(this.label33);
            this.gbHeat.Controls.Add(this.label32);
            this.gbHeat.Controls.Add(this.label31);
            this.gbHeat.Controls.Add(this.label30);
            this.gbHeat.Controls.Add(this.label29);
            this.gbHeat.Controls.Add(this.label28);
            this.gbHeat.Controls.Add(this.label27);
            this.gbHeat.Controls.Add(this.label24);
            this.gbHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbHeat.Location = new System.Drawing.Point(3, 3);
            this.gbHeat.Name = "gbHeat";
            this.gbHeat.Padding = new System.Windows.Forms.Padding(3, 3, 8, 3);
            this.gbHeat.Size = new System.Drawing.Size(325, 238);
            this.gbHeat.TabIndex = 13;
            this.gbHeat.TabStop = false;
            this.gbHeat.Text = "Heat";
            // 
            // cbHeatSectionFuelType
            // 
            this.cbHeatSectionFuelType.FormattingEnabled = true;
            this.cbHeatSectionFuelType.Items.AddRange(new object[] {
            "NG",
            "LP"});
            this.cbHeatSectionFuelType.Location = new System.Drawing.Point(231, 16);
            this.cbHeatSectionFuelType.Name = "cbHeatSectionFuelType";
            this.cbHeatSectionFuelType.Size = new System.Drawing.Size(73, 21);
            this.cbHeatSectionFuelType.TabIndex = 2;
            this.cbHeatSectionFuelType.Text = "None";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(173, 19);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(60, 13);
            this.label21.TabIndex = 105;
            this.label21.Text = "Fuel Type: ";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(144, 63);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(29, 13);
            this.label114.TabIndex = 103;
            this.label114.Text = "Top";
            // 
            // tbHeatSizeBottom
            // 
            this.tbHeatSizeBottom.Enabled = false;
            this.tbHeatSizeBottom.Location = new System.Drawing.Point(215, 207);
            this.tbHeatSizeBottom.Name = "tbHeatSizeBottom";
            this.tbHeatSizeBottom.Size = new System.Drawing.Size(100, 20);
            this.tbHeatSizeBottom.TabIndex = 15;
            // 
            // tbHeatPartNumBottom
            // 
            this.tbHeatPartNumBottom.Enabled = false;
            this.tbHeatPartNumBottom.Location = new System.Drawing.Point(55, 207);
            this.tbHeatPartNumBottom.Name = "tbHeatPartNumBottom";
            this.tbHeatPartNumBottom.Size = new System.Drawing.Size(100, 20);
            this.tbHeatPartNumBottom.TabIndex = 14;
            // 
            // tbHeatSerialNumBottom
            // 
            this.tbHeatSerialNumBottom.Enabled = false;
            this.tbHeatSerialNumBottom.Location = new System.Drawing.Point(215, 185);
            this.tbHeatSerialNumBottom.Name = "tbHeatSerialNumBottom";
            this.tbHeatSerialNumBottom.Size = new System.Drawing.Size(100, 20);
            this.tbHeatSerialNumBottom.TabIndex = 13;
            // 
            // tbHeatModelNumBottom
            // 
            this.tbHeatModelNumBottom.Enabled = false;
            this.tbHeatModelNumBottom.Location = new System.Drawing.Point(55, 185);
            this.tbHeatModelNumBottom.Name = "tbHeatModelNumBottom";
            this.tbHeatModelNumBottom.Size = new System.Drawing.Size(100, 20);
            this.tbHeatModelNumBottom.TabIndex = 12;
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(155, 201);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(62, 26);
            this.label110.TabIndex = 98;
            this.label110.Text = "Size \r\n(MBH/kW):";
            this.label110.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(7, 210);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(39, 13);
            this.label111.TabIndex = 97;
            this.label111.Text = "Part #:";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(165, 188);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(46, 13);
            this.label112.TabIndex = 96;
            this.label112.Text = "Serial #:";
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(7, 188);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(49, 13);
            this.label113.TabIndex = 95;
            this.label113.Text = "Model #:";
            // 
            // tbHeatRunningAmps
            // 
            this.tbHeatRunningAmps.Location = new System.Drawing.Point(233, 144);
            this.tbHeatRunningAmps.Name = "tbHeatRunningAmps";
            this.tbHeatRunningAmps.Size = new System.Drawing.Size(77, 20);
            this.tbHeatRunningAmps.TabIndex = 11;
            // 
            // tbHeatRatedAmps
            // 
            this.tbHeatRatedAmps.Location = new System.Drawing.Point(71, 144);
            this.tbHeatRatedAmps.Name = "tbHeatRatedAmps";
            this.tbHeatRatedAmps.Size = new System.Drawing.Size(79, 20);
            this.tbHeatRatedAmps.TabIndex = 10;
            // 
            // tbHeatSizeTop
            // 
            this.tbHeatSizeTop.Location = new System.Drawing.Point(211, 122);
            this.tbHeatSizeTop.Name = "tbHeatSizeTop";
            this.tbHeatSizeTop.Size = new System.Drawing.Size(100, 20);
            this.tbHeatSizeTop.TabIndex = 9;
            // 
            // tbHeatPONum
            // 
            this.tbHeatPONum.Location = new System.Drawing.Point(211, 100);
            this.tbHeatPONum.Name = "tbHeatPONum";
            this.tbHeatPONum.Size = new System.Drawing.Size(100, 20);
            this.tbHeatPONum.TabIndex = 7;
            // 
            // tbHeatItemNum
            // 
            this.tbHeatItemNum.Location = new System.Drawing.Point(50, 100);
            this.tbHeatItemNum.Name = "tbHeatItemNum";
            this.tbHeatItemNum.Size = new System.Drawing.Size(100, 20);
            this.tbHeatItemNum.TabIndex = 6;
            // 
            // tbHeatPartNumTop
            // 
            this.tbHeatPartNumTop.Location = new System.Drawing.Point(50, 122);
            this.tbHeatPartNumTop.Name = "tbHeatPartNumTop";
            this.tbHeatPartNumTop.Size = new System.Drawing.Size(100, 20);
            this.tbHeatPartNumTop.TabIndex = 8;
            // 
            // tbHeatSerialNumTop
            // 
            this.tbHeatSerialNumTop.Location = new System.Drawing.Point(211, 78);
            this.tbHeatSerialNumTop.Name = "tbHeatSerialNumTop";
            this.tbHeatSerialNumTop.Size = new System.Drawing.Size(100, 20);
            this.tbHeatSerialNumTop.TabIndex = 5;
            // 
            // tbHeatModelNumTop
            // 
            this.tbHeatModelNumTop.Location = new System.Drawing.Point(50, 78);
            this.tbHeatModelNumTop.Name = "tbHeatModelNumTop";
            this.tbHeatModelNumTop.Size = new System.Drawing.Size(100, 20);
            this.tbHeatModelNumTop.TabIndex = 4;
            // 
            // cbHeatPosition
            // 
            this.cbHeatPosition.FormattingEnabled = true;
            this.cbHeatPosition.Items.AddRange(new object[] {
            "Top",
            "Top/Bottom"});
            this.cbHeatPosition.Location = new System.Drawing.Point(51, 39);
            this.cbHeatPosition.Name = "cbHeatPosition";
            this.cbHeatPosition.Size = new System.Drawing.Size(121, 21);
            this.cbHeatPosition.TabIndex = 3;
            this.cbHeatPosition.SelectedIndexChanged += new System.EventHandler(this.cbHeatPosition_SelectedIndexChanged);
            // 
            // cbHeatPrimary
            // 
            this.cbHeatPrimary.FormattingEnabled = true;
            this.cbHeatPrimary.Items.AddRange(new object[] {
            "Gas",
            "Electric",
            "None",
            "Other"});
            this.cbHeatPrimary.Location = new System.Drawing.Point(51, 16);
            this.cbHeatPrimary.Name = "cbHeatPrimary";
            this.cbHeatPrimary.Size = new System.Drawing.Size(121, 21);
            this.cbHeatPrimary.TabIndex = 1;
            this.cbHeatPrimary.Text = "None";
            this.cbHeatPrimary.SelectedIndexChanged += new System.EventHandler(this.cbHeatPrimary_SelectedIndexChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(151, 150);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(79, 13);
            this.label35.TabIndex = 9;
            this.label35.Text = "Running Amps:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(2, 149);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(68, 13);
            this.label34.TabIndex = 8;
            this.label34.Text = "Rated Amps:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(151, 118);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 26);
            this.label33.TabIndex = 7;
            this.label33.Text = "Size \r\n(MBH/kW):";
            this.label33.TextAlign = System.Drawing.ContentAlignment.BottomRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(2, 126);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(39, 13);
            this.label32.TabIndex = 6;
            this.label32.Text = "Part #:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(166, 104);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(35, 13);
            this.label31.TabIndex = 5;
            this.label31.Text = "PO #:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(161, 82);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(46, 13);
            this.label30.TabIndex = 4;
            this.label30.Text = "Serial #:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(5, 104);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(40, 13);
            this.label29.TabIndex = 3;
            this.label29.Text = "Item #:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(2, 82);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(49, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "Model #:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 42);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(47, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Position:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(3, 19);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(44, 13);
            this.label24.TabIndex = 0;
            this.label24.Text = "Primary:";
            // 
            // label125
            // 
            this.label125.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label125.Location = new System.Drawing.Point(191, 20);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(87, 13);
            this.label125.TabIndex = 1;
            this.label125.Text = "Running Amps:";
            this.label125.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbCircuit2
            // 
            this.gbCircuit2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCircuit2.Controls.Add(this.tbCircuit2SuperHeat);
            this.gbCircuit2.Controls.Add(this.label153);
            this.gbCircuit2.Controls.Add(this.tbCircuit2Subcooling);
            this.gbCircuit2.Controls.Add(this.tbCircuit2DisTemp);
            this.gbCircuit2.Controls.Add(this.tbCircuit2SDT);
            this.gbCircuit2.Controls.Add(this.tbCircuit2DisPSI);
            this.gbCircuit2.Controls.Add(this.tbCircuit2LiqTemp);
            this.gbCircuit2.Controls.Add(this.tbCircuit2SLT);
            this.gbCircuit2.Controls.Add(this.tbCircuit2LiqPSI);
            this.gbCircuit2.Controls.Add(this.tbCircuit2SuctTemp);
            this.gbCircuit2.Controls.Add(this.tbCircuit2SST);
            this.gbCircuit2.Controls.Add(this.tbCircuit2SuctPSI);
            this.gbCircuit2.Controls.Add(this.label47);
            this.gbCircuit2.Controls.Add(this.label48);
            this.gbCircuit2.Controls.Add(this.label49);
            this.gbCircuit2.Controls.Add(this.label50);
            this.gbCircuit2.Controls.Add(this.label52);
            this.gbCircuit2.Controls.Add(this.label53);
            this.gbCircuit2.Controls.Add(this.label54);
            this.gbCircuit2.Controls.Add(this.label55);
            this.gbCircuit2.Controls.Add(this.label56);
            this.gbCircuit2.Controls.Add(this.label57);
            this.gbCircuit2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCircuit2.Location = new System.Drawing.Point(3, 411);
            this.gbCircuit2.Name = "gbCircuit2";
            this.gbCircuit2.Size = new System.Drawing.Size(325, 154);
            this.gbCircuit2.TabIndex = 15;
            this.gbCircuit2.TabStop = false;
            this.gbCircuit2.Text = "Circuit #2";
            // 
            // tbCircuit2SuperHeat
            // 
            this.tbCircuit2SuperHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCircuit2SuperHeat.Location = new System.Drawing.Point(232, 118);
            this.tbCircuit2SuperHeat.Name = "tbCircuit2SuperHeat";
            this.tbCircuit2SuperHeat.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbCircuit2SuperHeat.Size = new System.Drawing.Size(64, 20);
            this.tbCircuit2SuperHeat.TabIndex = 16;
            this.tbCircuit2SuperHeat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label153
            // 
            this.label153.AutoSize = true;
            this.label153.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label153.Location = new System.Drawing.Point(229, 89);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(70, 26);
            this.label153.TabIndex = 15;
            this.label153.Text = "Super Heat:\r\n(range 10-25)";
            // 
            // tbPreHeatSize
            // 
            this.tbPreHeatSize.Location = new System.Drawing.Point(281, 60);
            this.tbPreHeatSize.Name = "tbPreHeatSize";
            this.tbPreHeatSize.Size = new System.Drawing.Size(100, 20);
            this.tbPreHeatSize.TabIndex = 5;
            // 
            // label124
            // 
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.Location = new System.Drawing.Point(191, 64);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(87, 13);
            this.label124.TabIndex = 148;
            this.label124.Text = "Size (kW):\r\n";
            this.label124.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // gbPreHeat
            // 
            this.gbPreHeat.Controls.Add(this.tbPreHeatSize);
            this.gbPreHeat.Controls.Add(this.label124);
            this.gbPreHeat.Controls.Add(this.tbPreHeatSerialNo);
            this.gbPreHeat.Controls.Add(this.tbPreHeatModelNo);
            this.gbPreHeat.Controls.Add(this.label117);
            this.gbPreHeat.Controls.Add(this.label118);
            this.gbPreHeat.Controls.Add(this.tbPreHeatRunningAmps);
            this.gbPreHeat.Controls.Add(this.tbPreHeatRatedAmps);
            this.gbPreHeat.Controls.Add(this.label125);
            this.gbPreHeat.Controls.Add(this.label126);
            this.gbPreHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPreHeat.Location = new System.Drawing.Point(477, 3);
            this.gbPreHeat.Name = "gbPreHeat";
            this.gbPreHeat.Size = new System.Drawing.Size(390, 90);
            this.gbPreHeat.TabIndex = 11;
            this.gbPreHeat.TabStop = false;
            this.gbPreHeat.Text = "Pre Heat";
            // 
            // tbPreHeatSerialNo
            // 
            this.tbPreHeatSerialNo.Location = new System.Drawing.Point(85, 38);
            this.tbPreHeatSerialNo.Name = "tbPreHeatSerialNo";
            this.tbPreHeatSerialNo.Size = new System.Drawing.Size(100, 20);
            this.tbPreHeatSerialNo.TabIndex = 2;
            // 
            // tbPreHeatModelNo
            // 
            this.tbPreHeatModelNo.Location = new System.Drawing.Point(85, 16);
            this.tbPreHeatModelNo.Name = "tbPreHeatModelNo";
            this.tbPreHeatModelNo.Size = new System.Drawing.Size(100, 20);
            this.tbPreHeatModelNo.TabIndex = 1;
            // 
            // label117
            // 
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.Location = new System.Drawing.Point(32, 41);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(50, 13);
            this.label117.TabIndex = 144;
            this.label117.Text = "Serial #:";
            this.label117.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label118
            // 
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.Location = new System.Drawing.Point(32, 20);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(50, 13);
            this.label118.TabIndex = 143;
            this.label118.Text = "Model #:";
            this.label118.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbPreHeatRunningAmps
            // 
            this.tbPreHeatRunningAmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPreHeatRunningAmps.Location = new System.Drawing.Point(281, 16);
            this.tbPreHeatRunningAmps.Name = "tbPreHeatRunningAmps";
            this.tbPreHeatRunningAmps.Size = new System.Drawing.Size(75, 20);
            this.tbPreHeatRunningAmps.TabIndex = 3;
            // 
            // tbPreHeatRatedAmps
            // 
            this.tbPreHeatRatedAmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPreHeatRatedAmps.Location = new System.Drawing.Point(281, 38);
            this.tbPreHeatRatedAmps.Name = "tbPreHeatRatedAmps";
            this.tbPreHeatRatedAmps.Size = new System.Drawing.Size(75, 20);
            this.tbPreHeatRatedAmps.TabIndex = 4;
            // 
            // label126
            // 
            this.label126.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label126.Location = new System.Drawing.Point(191, 42);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(87, 13);
            this.label126.TabIndex = 0;
            this.label126.Text = "Rated Amps:";
            this.label126.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tbAuditedBy
            // 
            this.tbAuditedBy.Location = new System.Drawing.Point(1031, 3);
            this.tbAuditedBy.Name = "tbAuditedBy";
            this.tbAuditedBy.Size = new System.Drawing.Size(135, 20);
            this.tbAuditedBy.TabIndex = 3;
            // 
            // label123
            // 
            this.label123.Location = new System.Drawing.Point(959, 0);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(66, 29);
            this.label123.TabIndex = 62;
            this.label123.Text = "Audited By:";
            this.label123.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ButExit
            // 
            this.ButExit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButExit.ForeColor = System.Drawing.Color.Red;
            this.ButExit.Location = new System.Drawing.Point(1158, 3);
            this.ButExit.Margin = new System.Windows.Forms.Padding(3, 3, 6, 3);
            this.ButExit.Name = "ButExit";
            this.ButExit.Size = new System.Drawing.Size(75, 23);
            this.ButExit.TabIndex = 3;
            this.ButExit.Text = "Exit";
            this.ButExit.UseVisualStyleBackColor = true;
            this.ButExit.Click += new System.EventHandler(this.ButExit_Click);
            // 
            // butSave
            // 
            this.butSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSave.ForeColor = System.Drawing.Color.Green;
            this.butSave.Location = new System.Drawing.Point(920, 3);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(75, 23);
            this.butSave.TabIndex = 1;
            this.butSave.Text = "Save";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // tbModelNum
            // 
            this.tlpJobContainer.SetColumnSpan(this.tbModelNum, 3);
            this.tbModelNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbModelNum.Location = new System.Drawing.Point(534, 3);
            this.tbModelNum.Name = "tbModelNum";
            this.tbModelNum.ReadOnly = true;
            this.tbModelNum.Size = new System.Drawing.Size(419, 20);
            this.tbModelNum.TabIndex = 2;
            // 
            // butPrint
            // 
            this.butPrint.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butPrint.ForeColor = System.Drawing.Color.Blue;
            this.butPrint.Location = new System.Drawing.Point(1077, 3);
            this.butPrint.Name = "butPrint";
            this.butPrint.Size = new System.Drawing.Size(75, 23);
            this.butPrint.TabIndex = 3;
            this.butPrint.Text = "Print";
            this.butPrint.UseVisualStyleBackColor = true;
            this.butPrint.Visible = false;
            // 
            // tbTestedBy
            // 
            this.tbTestedBy.Location = new System.Drawing.Point(723, 32);
            this.tbTestedBy.Name = "tbTestedBy";
            this.tbTestedBy.Size = new System.Drawing.Size(147, 20);
            this.tbTestedBy.TabIndex = 7;
            this.tbTestedBy.TextChanged += new System.EventHandler(this.tbTestedBy_TextChanged);
            // 
            // dtpDate
            // 
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDate.Location = new System.Drawing.Point(1031, 32);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(88, 20);
            this.dtpDate.TabIndex = 8;
            // 
            // tbJobName
            // 
            this.tlpJobContainer.SetColumnSpan(this.tbJobName, 2);
            this.tbJobName.Location = new System.Drawing.Point(76, 32);
            this.tbJobName.Name = "tbJobName";
            this.tbJobName.ReadOnly = true;
            this.tbJobName.Size = new System.Drawing.Size(217, 20);
            this.tbJobName.TabIndex = 4;
            // 
            // tbSerialnum
            // 
            this.tlpJobContainer.SetColumnSpan(this.tbSerialnum, 2);
            this.tbSerialnum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbSerialnum.Location = new System.Drawing.Point(299, 3);
            this.tbSerialnum.Name = "tbSerialnum";
            this.tbSerialnum.ReadOnly = true;
            this.tbSerialnum.Size = new System.Drawing.Size(147, 20);
            this.tbSerialnum.TabIndex = 1;
            // 
            // gbERV
            // 
            this.gbERV.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbERV.Controls.Add(this.tbERV_SerialNo);
            this.gbERV.Controls.Add(this.tbERV_ModelNo);
            this.gbERV.Controls.Add(this.label121);
            this.gbERV.Controls.Add(this.label122);
            this.gbERV.Controls.Add(this.chkbERVProperRotation);
            this.gbERV.Controls.Add(this.tbERVRunningAmps);
            this.gbERV.Controls.Add(this.tbERVRatedAmps);
            this.gbERV.Controls.Add(this.label65);
            this.gbERV.Controls.Add(this.label64);
            this.gbERV.Controls.Add(this.label63);
            this.gbERV.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbERV.Location = new System.Drawing.Point(3, 3);
            this.gbERV.Name = "gbERV";
            this.gbERV.Size = new System.Drawing.Size(468, 92);
            this.gbERV.TabIndex = 4;
            this.gbERV.TabStop = false;
            this.gbERV.Text = "ERV";
            // 
            // tbERV_SerialNo
            // 
            this.tbERV_SerialNo.Location = new System.Drawing.Point(63, 51);
            this.tbERV_SerialNo.Name = "tbERV_SerialNo";
            this.tbERV_SerialNo.Size = new System.Drawing.Size(100, 20);
            this.tbERV_SerialNo.TabIndex = 2;
            // 
            // tbERV_ModelNo
            // 
            this.tbERV_ModelNo.Location = new System.Drawing.Point(63, 29);
            this.tbERV_ModelNo.Name = "tbERV_ModelNo";
            this.tbERV_ModelNo.Size = new System.Drawing.Size(100, 20);
            this.tbERV_ModelNo.TabIndex = 1;
            // 
            // label121
            // 
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.Location = new System.Drawing.Point(3, 54);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(55, 12);
            this.label121.TabIndex = 144;
            this.label121.Text = "Serial #:";
            this.label121.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label122
            // 
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(6, 32);
            this.label122.Name = "label122";
            this.label122.Size = new System.Drawing.Size(52, 16);
            this.label122.TabIndex = 143;
            this.label122.Text = "Model #:";
            this.label122.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkbERVProperRotation
            // 
            this.chkbERVProperRotation.AutoSize = true;
            this.chkbERVProperRotation.Location = new System.Drawing.Point(434, 35);
            this.chkbERVProperRotation.Name = "chkbERVProperRotation";
            this.chkbERVProperRotation.Size = new System.Drawing.Size(15, 14);
            this.chkbERVProperRotation.TabIndex = 5;
            this.chkbERVProperRotation.UseVisualStyleBackColor = true;
            // 
            // tbERVRunningAmps
            // 
            this.tbERVRunningAmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbERVRunningAmps.Location = new System.Drawing.Point(254, 29);
            this.tbERVRunningAmps.Name = "tbERVRunningAmps";
            this.tbERVRunningAmps.Size = new System.Drawing.Size(75, 20);
            this.tbERVRunningAmps.TabIndex = 3;
            this.tbERVRunningAmps.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbERVRatedAmps
            // 
            this.tbERVRatedAmps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbERVRatedAmps.Location = new System.Drawing.Point(254, 51);
            this.tbERVRatedAmps.Name = "tbERVRatedAmps";
            this.tbERVRatedAmps.Size = new System.Drawing.Size(75, 20);
            this.tbERVRatedAmps.TabIndex = 4;
            this.tbERVRatedAmps.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label65
            // 
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(346, 35);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(84, 13);
            this.label65.TabIndex = 2;
            this.label65.Text = "Proper Rotation:";
            // 
            // label64
            // 
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(170, 33);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(84, 13);
            this.label64.TabIndex = 1;
            this.label64.Text = "Running Amps:";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label63
            // 
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(170, 55);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(84, 13);
            this.label63.TabIndex = 0;
            this.label63.Text = "Rated Amps:";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 29);
            this.label1.TabIndex = 28;
            this.label1.Text = "Job Number:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // chkbDoubleBlowerIndoor
            // 
            this.chkbDoubleBlowerIndoor.AutoSize = true;
            this.chkbDoubleBlowerIndoor.Location = new System.Drawing.Point(120, 204);
            this.chkbDoubleBlowerIndoor.Name = "chkbDoubleBlowerIndoor";
            this.chkbDoubleBlowerIndoor.Size = new System.Drawing.Size(15, 14);
            this.chkbDoubleBlowerIndoor.TabIndex = 17;
            this.chkbDoubleBlowerIndoor.UseVisualStyleBackColor = true;
            // 
            // tbModelNumExhaust
            // 
            this.tbModelNumExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbModelNumExhaust.Location = new System.Drawing.Point(169, 180);
            this.tbModelNumExhaust.Name = "tbModelNumExhaust";
            this.tbModelNumExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbModelNumExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbModelNumExhaust.TabIndex = 16;
            // 
            // tbModelNumIndoor
            // 
            this.tbModelNumIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbModelNumIndoor.Location = new System.Drawing.Point(94, 180);
            this.tbModelNumIndoor.Name = "tbModelNumIndoor";
            this.tbModelNumIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbModelNumIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbModelNumIndoor.TabIndex = 15;
            // 
            // tbVoltageOutputExhaust
            // 
            this.tbVoltageOutputExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVoltageOutputExhaust.Location = new System.Drawing.Point(169, 158);
            this.tbVoltageOutputExhaust.Name = "tbVoltageOutputExhaust";
            this.tbVoltageOutputExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbVoltageOutputExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbVoltageOutputExhaust.TabIndex = 14;
            this.tbVoltageOutputExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbVoltageOutputIndoor
            // 
            this.tbVoltageOutputIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVoltageOutputIndoor.Location = new System.Drawing.Point(94, 158);
            this.tbVoltageOutputIndoor.Name = "tbVoltageOutputIndoor";
            this.tbVoltageOutputIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbVoltageOutputIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbVoltageOutputIndoor.TabIndex = 13;
            this.tbVoltageOutputIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbMinHzExhaust
            // 
            this.tbMinHzExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMinHzExhaust.Location = new System.Drawing.Point(169, 136);
            this.tbMinHzExhaust.Name = "tbMinHzExhaust";
            this.tbMinHzExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbMinHzExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbMinHzExhaust.TabIndex = 12;
            this.tbMinHzExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbMinHzIndoor
            // 
            this.tbMinHzIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMinHzIndoor.Location = new System.Drawing.Point(94, 136);
            this.tbMinHzIndoor.Name = "tbMinHzIndoor";
            this.tbMinHzIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbMinHzIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbMinHzIndoor.TabIndex = 11;
            this.tbMinHzIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbMaxHzExhaust
            // 
            this.tbMaxHzExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMaxHzExhaust.Location = new System.Drawing.Point(169, 114);
            this.tbMaxHzExhaust.Name = "tbMaxHzExhaust";
            this.tbMaxHzExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbMaxHzExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbMaxHzExhaust.TabIndex = 10;
            this.tbMaxHzExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbMaxHzIndoor
            // 
            this.tbMaxHzIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMaxHzIndoor.Location = new System.Drawing.Point(94, 114);
            this.tbMaxHzIndoor.Name = "tbMaxHzIndoor";
            this.tbMaxHzIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbMaxHzIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbMaxHzIndoor.TabIndex = 9;
            this.tbMaxHzIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // chkbDoubleBlowerExhaust
            // 
            this.chkbDoubleBlowerExhaust.AutoSize = true;
            this.chkbDoubleBlowerExhaust.Location = new System.Drawing.Point(194, 204);
            this.chkbDoubleBlowerExhaust.Name = "chkbDoubleBlowerExhaust";
            this.chkbDoubleBlowerExhaust.Size = new System.Drawing.Size(15, 14);
            this.chkbDoubleBlowerExhaust.TabIndex = 18;
            this.chkbDoubleBlowerExhaust.UseVisualStyleBackColor = true;
            // 
            // dgvFilters
            // 
            this.dgvFilters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFilters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.FilterSize,
            this.FilterStyle,
            this.Qty});
            this.dgvFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvFilters.Location = new System.Drawing.Point(3, 16);
            this.dgvFilters.MultiSelect = false;
            this.dgvFilters.Name = "dgvFilters";
            this.dgvFilters.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dgvFilters.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvFilters.Size = new System.Drawing.Size(295, 21);
            this.dgvFilters.TabIndex = 0;
            // 
            // FilterSize
            // 
            this.FilterSize.HeaderText = "FilterSize";
            this.FilterSize.Name = "FilterSize";
            // 
            // FilterStyle
            // 
            this.FilterStyle.HeaderText = "FilterStyle";
            this.FilterStyle.Name = "FilterStyle";
            // 
            // Qty
            // 
            this.Qty.HeaderText = "Qty";
            this.Qty.Name = "Qty";
            // 
            // gbFilters
            // 
            this.gbFilters.Controls.Add(this.dgvFilters);
            this.gbFilters.Controls.Add(this.label60);
            this.gbFilters.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbFilters.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbFilters.Location = new System.Drawing.Point(3, 3);
            this.gbFilters.Name = "gbFilters";
            this.gbFilters.Size = new System.Drawing.Size(301, 40);
            this.gbFilters.TabIndex = 5;
            this.gbFilters.TabStop = false;
            this.gbFilters.Text = "Filters";
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(102, 72);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(76, 13);
            this.label60.TabIndex = 1;
            this.label60.Text = "Dynamic???";
            // 
            // tbWheelSizeExhaust
            // 
            this.tbWheelSizeExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWheelSizeExhaust.Location = new System.Drawing.Point(169, 92);
            this.tbWheelSizeExhaust.Name = "tbWheelSizeExhaust";
            this.tbWheelSizeExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbWheelSizeExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbWheelSizeExhaust.TabIndex = 8;
            this.tbWheelSizeExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbRunningAmpsExhaust
            // 
            this.tbRunningAmpsExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRunningAmpsExhaust.Location = new System.Drawing.Point(169, 70);
            this.tbRunningAmpsExhaust.Name = "tbRunningAmpsExhaust";
            this.tbRunningAmpsExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRunningAmpsExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbRunningAmpsExhaust.TabIndex = 6;
            this.tbRunningAmpsExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(628, 29);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(89, 27);
            this.label19.TabIndex = 40;
            this.label19.Text = "Tested By:";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbWheelSizeIndoor
            // 
            this.tbWheelSizeIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbWheelSizeIndoor.Location = new System.Drawing.Point(94, 92);
            this.tbWheelSizeIndoor.Name = "tbWheelSizeIndoor";
            this.tbWheelSizeIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbWheelSizeIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbWheelSizeIndoor.TabIndex = 7;
            this.tbWheelSizeIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // cbCCHeaterTested
            // 
            this.cbCCHeaterTested.AutoSize = true;
            this.cbCCHeaterTested.Location = new System.Drawing.Point(267, 78);
            this.cbCCHeaterTested.Name = "cbCCHeaterTested";
            this.cbCCHeaterTested.Size = new System.Drawing.Size(15, 14);
            this.cbCCHeaterTested.TabIndex = 8;
            this.cbCCHeaterTested.UseVisualStyleBackColor = true;
            // 
            // cbCCHeaterInstalled
            // 
            this.cbCCHeaterInstalled.AutoSize = true;
            this.cbCCHeaterInstalled.Location = new System.Drawing.Point(185, 78);
            this.cbCCHeaterInstalled.Name = "cbCCHeaterInstalled";
            this.cbCCHeaterInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbCCHeaterInstalled.TabIndex = 7;
            this.cbCCHeaterInstalled.UseVisualStyleBackColor = true;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(42, 78);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(115, 13);
            this.label116.TabIndex = 55;
            this.label116.Text = "Crankcase Heaters";
            // 
            // gbStandardEquipment
            // 
            this.gbStandardEquipment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStandardEquipment.Controls.Add(this.cbRADampersTested);
            this.gbStandardEquipment.Controls.Add(this.cbRADampersInstalled);
            this.gbStandardEquipment.Controls.Add(this.label84);
            this.gbStandardEquipment.Controls.Add(this.cbAPRTested);
            this.gbStandardEquipment.Controls.Add(this.cbAPRInstalled);
            this.gbStandardEquipment.Controls.Add(this.label83);
            this.gbStandardEquipment.Controls.Add(this.cbDigitalScrollTested);
            this.gbStandardEquipment.Controls.Add(this.cbDigitalScrollInstalled);
            this.gbStandardEquipment.Controls.Add(this.label81);
            this.gbStandardEquipment.Controls.Add(this.cbCCHeaterTested);
            this.gbStandardEquipment.Controls.Add(this.cbCCHeaterInstalled);
            this.gbStandardEquipment.Controls.Add(this.label116);
            this.gbStandardEquipment.Controls.Add(this.cbOADampersTested);
            this.gbStandardEquipment.Controls.Add(this.cbOADampersInstalled);
            this.gbStandardEquipment.Controls.Add(this.cbFilterStatusTested);
            this.gbStandardEquipment.Controls.Add(this.cbFilterStatusInstalled);
            this.gbStandardEquipment.Controls.Add(this.cbFanFailureTested);
            this.gbStandardEquipment.Controls.Add(this.cbFanFailureInstalled);
            this.gbStandardEquipment.Controls.Add(this.label93);
            this.gbStandardEquipment.Controls.Add(this.label92);
            this.gbStandardEquipment.Controls.Add(this.label88);
            this.gbStandardEquipment.Controls.Add(this.label87);
            this.gbStandardEquipment.Controls.Add(this.label58);
            this.gbStandardEquipment.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbStandardEquipment.Location = new System.Drawing.Point(3, 178);
            this.gbStandardEquipment.Name = "gbStandardEquipment";
            this.gbStandardEquipment.Size = new System.Drawing.Size(301, 152);
            this.gbStandardEquipment.TabIndex = 8;
            this.gbStandardEquipment.TabStop = false;
            this.gbStandardEquipment.Text = "Standard Equipment";
            // 
            // cbRADampersTested
            // 
            this.cbRADampersTested.AutoSize = true;
            this.cbRADampersTested.Location = new System.Drawing.Point(267, 124);
            this.cbRADampersTested.Name = "cbRADampersTested";
            this.cbRADampersTested.Size = new System.Drawing.Size(15, 14);
            this.cbRADampersTested.TabIndex = 14;
            this.cbRADampersTested.UseVisualStyleBackColor = true;
            // 
            // cbRADampersInstalled
            // 
            this.cbRADampersInstalled.AutoSize = true;
            this.cbRADampersInstalled.Location = new System.Drawing.Point(185, 124);
            this.cbRADampersInstalled.Name = "cbRADampersInstalled";
            this.cbRADampersInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbRADampersInstalled.TabIndex = 13;
            this.cbRADampersInstalled.UseVisualStyleBackColor = true;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(78, 124);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(77, 13);
            this.label84.TabIndex = 64;
            this.label84.Text = "RA Dampers";
            // 
            // cbAPRTested
            // 
            this.cbAPRTested.AutoSize = true;
            this.cbAPRTested.Location = new System.Drawing.Point(267, 109);
            this.cbAPRTested.Name = "cbAPRTested";
            this.cbAPRTested.Size = new System.Drawing.Size(15, 14);
            this.cbAPRTested.TabIndex = 12;
            this.cbAPRTested.UseVisualStyleBackColor = true;
            // 
            // cbAPRInstalled
            // 
            this.cbAPRInstalled.AutoSize = true;
            this.cbAPRInstalled.Location = new System.Drawing.Point(185, 109);
            this.cbAPRInstalled.Name = "cbAPRInstalled";
            this.cbAPRInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbAPRInstalled.TabIndex = 11;
            this.cbAPRInstalled.UseVisualStyleBackColor = true;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(122, 109);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(32, 13);
            this.label83.TabIndex = 61;
            this.label83.Text = "APR";
            // 
            // cbDigitalScrollTested
            // 
            this.cbDigitalScrollTested.AutoSize = true;
            this.cbDigitalScrollTested.Location = new System.Drawing.Point(267, 93);
            this.cbDigitalScrollTested.Name = "cbDigitalScrollTested";
            this.cbDigitalScrollTested.Size = new System.Drawing.Size(15, 14);
            this.cbDigitalScrollTested.TabIndex = 10;
            this.cbDigitalScrollTested.UseVisualStyleBackColor = true;
            // 
            // cbDigitalScrollInstalled
            // 
            this.cbDigitalScrollInstalled.AutoSize = true;
            this.cbDigitalScrollInstalled.Location = new System.Drawing.Point(185, 93);
            this.cbDigitalScrollInstalled.Name = "cbDigitalScrollInstalled";
            this.cbDigitalScrollInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbDigitalScrollInstalled.TabIndex = 9;
            this.cbDigitalScrollInstalled.UseVisualStyleBackColor = true;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(76, 93);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(79, 13);
            this.label81.TabIndex = 58;
            this.label81.Text = "Digital Scroll";
            // 
            // cbOADampersTested
            // 
            this.cbOADampersTested.AutoSize = true;
            this.cbOADampersTested.Location = new System.Drawing.Point(267, 63);
            this.cbOADampersTested.Name = "cbOADampersTested";
            this.cbOADampersTested.Size = new System.Drawing.Size(15, 14);
            this.cbOADampersTested.TabIndex = 6;
            this.cbOADampersTested.UseVisualStyleBackColor = true;
            // 
            // cbOADampersInstalled
            // 
            this.cbOADampersInstalled.AutoSize = true;
            this.cbOADampersInstalled.Location = new System.Drawing.Point(185, 63);
            this.cbOADampersInstalled.Name = "cbOADampersInstalled";
            this.cbOADampersInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbOADampersInstalled.TabIndex = 5;
            this.cbOADampersInstalled.UseVisualStyleBackColor = true;
            // 
            // cbFilterStatusTested
            // 
            this.cbFilterStatusTested.AutoSize = true;
            this.cbFilterStatusTested.Location = new System.Drawing.Point(267, 47);
            this.cbFilterStatusTested.Name = "cbFilterStatusTested";
            this.cbFilterStatusTested.Size = new System.Drawing.Size(15, 14);
            this.cbFilterStatusTested.TabIndex = 4;
            this.cbFilterStatusTested.UseVisualStyleBackColor = true;
            // 
            // cbFilterStatusInstalled
            // 
            this.cbFilterStatusInstalled.AutoSize = true;
            this.cbFilterStatusInstalled.Location = new System.Drawing.Point(185, 47);
            this.cbFilterStatusInstalled.Name = "cbFilterStatusInstalled";
            this.cbFilterStatusInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbFilterStatusInstalled.TabIndex = 3;
            this.cbFilterStatusInstalled.UseVisualStyleBackColor = true;
            // 
            // cbFanFailureTested
            // 
            this.cbFanFailureTested.AutoSize = true;
            this.cbFanFailureTested.Location = new System.Drawing.Point(267, 31);
            this.cbFanFailureTested.Name = "cbFanFailureTested";
            this.cbFanFailureTested.Size = new System.Drawing.Size(15, 14);
            this.cbFanFailureTested.TabIndex = 2;
            this.cbFanFailureTested.UseVisualStyleBackColor = true;
            // 
            // cbFanFailureInstalled
            // 
            this.cbFanFailureInstalled.AutoSize = true;
            this.cbFanFailureInstalled.Location = new System.Drawing.Point(185, 31);
            this.cbFanFailureInstalled.Name = "cbFanFailureInstalled";
            this.cbFanFailureInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbFanFailureInstalled.TabIndex = 1;
            this.cbFanFailureInstalled.UseVisualStyleBackColor = true;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(247, 14);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(46, 13);
            this.label93.TabIndex = 7;
            this.label93.Text = "Tested";
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(165, 14);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(55, 13);
            this.label92.TabIndex = 6;
            this.label92.Text = "Installed";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(80, 63);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(77, 13);
            this.label88.TabIndex = 2;
            this.label88.Text = "OA Dampers";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(40, 47);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(117, 13);
            this.label87.TabIndex = 1;
            this.label87.Text = "Filter Status Switch";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(45, 31);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(112, 13);
            this.label58.TabIndex = 0;
            this.label58.Text = "Fan Failure Switch";
            // 
            // tbDisconnectSize
            // 
            this.tbDisconnectSize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDisconnectSize.Location = new System.Drawing.Point(191, 24);
            this.tbDisconnectSize.Name = "tbDisconnectSize";
            this.tbDisconnectSize.Size = new System.Drawing.Size(100, 20);
            this.tbDisconnectSize.TabIndex = 2;
            // 
            // cbDisconnectType
            // 
            this.cbDisconnectType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbDisconnectType.FormattingEnabled = true;
            this.cbDisconnectType.Items.AddRange(new object[] {
            "Fused",
            "Breaker"});
            this.cbDisconnectType.Location = new System.Drawing.Point(51, 24);
            this.cbDisconnectType.Name = "cbDisconnectType";
            this.cbDisconnectType.Size = new System.Drawing.Size(121, 21);
            this.cbDisconnectType.TabIndex = 1;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(225, 9);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(27, 13);
            this.label26.TabIndex = 1;
            this.label26.Text = "Size";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(98, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(31, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "Type";
            // 
            // gbDisconnect
            // 
            this.gbDisconnect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbDisconnect.Controls.Add(this.tbDisconnectSize);
            this.gbDisconnect.Controls.Add(this.cbDisconnectType);
            this.gbDisconnect.Controls.Add(this.label26);
            this.gbDisconnect.Controls.Add(this.label25);
            this.gbDisconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDisconnect.Location = new System.Drawing.Point(3, 123);
            this.gbDisconnect.Name = "gbDisconnect";
            this.gbDisconnect.Size = new System.Drawing.Size(301, 49);
            this.gbDisconnect.TabIndex = 7;
            this.gbDisconnect.TabStop = false;
            this.gbDisconnect.Text = "Disconnect";
            // 
            // nudCircuit1Charge
            // 
            this.nudCircuit1Charge.DecimalPlaces = 2;
            this.nudCircuit1Charge.Location = new System.Drawing.Point(101, 13);
            this.nudCircuit1Charge.Name = "nudCircuit1Charge";
            this.nudCircuit1Charge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudCircuit1Charge.Size = new System.Drawing.Size(100, 20);
            this.nudCircuit1Charge.TabIndex = 1;
            // 
            // tbCircuit1ChargeVariance
            // 
            this.tbCircuit1ChargeVariance.Location = new System.Drawing.Point(222, 14);
            this.tbCircuit1ChargeVariance.Name = "tbCircuit1ChargeVariance";
            this.tbCircuit1ChargeVariance.Size = new System.Drawing.Size(62, 20);
            this.tbCircuit1ChargeVariance.TabIndex = 2;
            this.tbCircuit1ChargeVariance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(205, 17);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(14, 13);
            this.label22.TabIndex = 2;
            this.label22.Text = "±";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(46, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(55, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Circuit #1:";
            // 
            // gbCharge
            // 
            this.gbCharge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCharge.Controls.Add(this.nudCircuit2Charge);
            this.gbCharge.Controls.Add(this.tbCircuit2ChargeVariance);
            this.gbCharge.Controls.Add(this.label59);
            this.gbCharge.Controls.Add(this.label76);
            this.gbCharge.Controls.Add(this.nudCircuit1Charge);
            this.gbCharge.Controls.Add(this.tbCircuit1ChargeVariance);
            this.gbCharge.Controls.Add(this.label22);
            this.gbCharge.Controls.Add(this.label20);
            this.gbCharge.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCharge.Location = new System.Drawing.Point(3, 49);
            this.gbCharge.Name = "gbCharge";
            this.gbCharge.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.gbCharge.Size = new System.Drawing.Size(301, 68);
            this.gbCharge.TabIndex = 6;
            this.gbCharge.TabStop = false;
            this.gbCharge.Text = "Charge Data";
            // 
            // nudCircuit2Charge
            // 
            this.nudCircuit2Charge.DecimalPlaces = 2;
            this.nudCircuit2Charge.Location = new System.Drawing.Point(101, 39);
            this.nudCircuit2Charge.Name = "nudCircuit2Charge";
            this.nudCircuit2Charge.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudCircuit2Charge.Size = new System.Drawing.Size(100, 20);
            this.nudCircuit2Charge.TabIndex = 4;
            // 
            // tbCircuit2ChargeVariance
            // 
            this.tbCircuit2ChargeVariance.Location = new System.Drawing.Point(222, 40);
            this.tbCircuit2ChargeVariance.Name = "tbCircuit2ChargeVariance";
            this.tbCircuit2ChargeVariance.Size = new System.Drawing.Size(62, 20);
            this.tbCircuit2ChargeVariance.TabIndex = 5;
            this.tbCircuit2ChargeVariance.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(205, 43);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(14, 13);
            this.label59.TabIndex = 6;
            this.label59.Text = "±";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(46, 43);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(55, 13);
            this.label76.TabIndex = 3;
            this.label76.Text = "Circuit #2:";
            // 
            // tbRunningAmpsIndoor
            // 
            this.tbRunningAmpsIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRunningAmpsIndoor.Location = new System.Drawing.Point(94, 70);
            this.tbRunningAmpsIndoor.Name = "tbRunningAmpsIndoor";
            this.tbRunningAmpsIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRunningAmpsIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbRunningAmpsIndoor.TabIndex = 5;
            this.tbRunningAmpsIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // nudTemperature
            // 
            this.nudTemperature.Location = new System.Drawing.Point(85, 15);
            this.nudTemperature.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.nudTemperature.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nudTemperature.Name = "nudTemperature";
            this.nudTemperature.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudTemperature.Size = new System.Drawing.Size(100, 20);
            this.nudTemperature.TabIndex = 1;
            // 
            // tbVoltageL3G
            // 
            this.tbVoltageL3G.Location = new System.Drawing.Point(166, 84);
            this.tbVoltageL3G.Name = "tbVoltageL3G";
            this.tbVoltageL3G.Size = new System.Drawing.Size(70, 20);
            this.tbVoltageL3G.TabIndex = 9;
            this.tbVoltageL3G.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbVoltageL2G
            // 
            this.tbVoltageL2G.Location = new System.Drawing.Point(90, 84);
            this.tbVoltageL2G.Name = "tbVoltageL2G";
            this.tbVoltageL2G.Size = new System.Drawing.Size(70, 20);
            this.tbVoltageL2G.TabIndex = 8;
            this.tbVoltageL2G.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbVoltageL1G
            // 
            this.tbVoltageL1G.Location = new System.Drawing.Point(14, 84);
            this.tbVoltageL1G.Name = "tbVoltageL1G";
            this.tbVoltageL1G.Size = new System.Drawing.Size(70, 20);
            this.tbVoltageL1G.TabIndex = 7;
            this.tbVoltageL1G.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbVoltageL1L3
            // 
            this.tbVoltageL1L3.Location = new System.Drawing.Point(166, 46);
            this.tbVoltageL1L3.Name = "tbVoltageL1L3";
            this.tbVoltageL1L3.Size = new System.Drawing.Size(70, 20);
            this.tbVoltageL1L3.TabIndex = 6;
            this.tbVoltageL1L3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbVoltageL2L3
            // 
            this.tbVoltageL2L3.Location = new System.Drawing.Point(90, 46);
            this.tbVoltageL2L3.Name = "tbVoltageL2L3";
            this.tbVoltageL2L3.Size = new System.Drawing.Size(70, 20);
            this.tbVoltageL2L3.TabIndex = 5;
            this.tbVoltageL2L3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbVoltageL1L2
            // 
            this.tbVoltageL1L2.Location = new System.Drawing.Point(14, 46);
            this.tbVoltageL1L2.Name = "tbVoltageL1L2";
            this.tbVoltageL1L2.Size = new System.Drawing.Size(70, 20);
            this.tbVoltageL1L2.TabIndex = 4;
            this.tbVoltageL1L2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(181, 68);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(34, 13);
            this.label75.TabIndex = 8;
            this.label75.Text = "L3-G";
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Location = new System.Drawing.Point(107, 68);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(34, 13);
            this.label74.TabIndex = 7;
            this.label74.Text = "L2-G";
            // 
            // gbBlowers
            // 
            this.gbBlowers.Controls.Add(this.chkbDoubleBlowerExhaust);
            this.gbBlowers.Controls.Add(this.chkbDoubleBlowerIndoor);
            this.gbBlowers.Controls.Add(this.tbModelNumExhaust);
            this.gbBlowers.Controls.Add(this.tbModelNumIndoor);
            this.gbBlowers.Controls.Add(this.tbVoltageOutputExhaust);
            this.gbBlowers.Controls.Add(this.tbVoltageOutputIndoor);
            this.gbBlowers.Controls.Add(this.tbMinHzExhaust);
            this.gbBlowers.Controls.Add(this.tbMinHzIndoor);
            this.gbBlowers.Controls.Add(this.tbMaxHzExhaust);
            this.gbBlowers.Controls.Add(this.tbMaxHzIndoor);
            this.gbBlowers.Controls.Add(this.tbWheelSizeExhaust);
            this.gbBlowers.Controls.Add(this.tbWheelSizeIndoor);
            this.gbBlowers.Controls.Add(this.tbRunningAmpsExhaust);
            this.gbBlowers.Controls.Add(this.tbRunningAmpsIndoor);
            this.gbBlowers.Controls.Add(this.tbRatedAmpsExhaust);
            this.gbBlowers.Controls.Add(this.tbRatedAmpsIndoor);
            this.gbBlowers.Controls.Add(this.tbHpExhaust);
            this.gbBlowers.Controls.Add(this.tbHpIndoor);
            this.gbBlowers.Controls.Add(this.label18);
            this.gbBlowers.Controls.Add(this.label17);
            this.gbBlowers.Controls.Add(this.label16);
            this.gbBlowers.Controls.Add(this.label15);
            this.gbBlowers.Controls.Add(this.label14);
            this.gbBlowers.Controls.Add(this.label13);
            this.gbBlowers.Controls.Add(this.label12);
            this.gbBlowers.Controls.Add(this.label11);
            this.gbBlowers.Controls.Add(this.label10);
            this.gbBlowers.Controls.Add(this.label9);
            this.gbBlowers.Controls.Add(this.label8);
            this.gbBlowers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbBlowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbBlowers.Location = new System.Drawing.Point(3, 195);
            this.gbBlowers.Name = "gbBlowers";
            this.gbBlowers.Size = new System.Drawing.Size(251, 241);
            this.gbBlowers.TabIndex = 2;
            this.gbBlowers.TabStop = false;
            this.gbBlowers.Text = "Blowers";
            // 
            // tbRatedAmpsExhaust
            // 
            this.tbRatedAmpsExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRatedAmpsExhaust.Location = new System.Drawing.Point(169, 48);
            this.tbRatedAmpsExhaust.Name = "tbRatedAmpsExhaust";
            this.tbRatedAmpsExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRatedAmpsExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbRatedAmpsExhaust.TabIndex = 4;
            this.tbRatedAmpsExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbRatedAmpsIndoor
            // 
            this.tbRatedAmpsIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRatedAmpsIndoor.Location = new System.Drawing.Point(94, 48);
            this.tbRatedAmpsIndoor.Name = "tbRatedAmpsIndoor";
            this.tbRatedAmpsIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbRatedAmpsIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbRatedAmpsIndoor.TabIndex = 3;
            this.tbRatedAmpsIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbHpExhaust
            // 
            this.tbHpExhaust.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHpExhaust.Location = new System.Drawing.Point(169, 26);
            this.tbHpExhaust.Name = "tbHpExhaust";
            this.tbHpExhaust.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbHpExhaust.Size = new System.Drawing.Size(69, 20);
            this.tbHpExhaust.TabIndex = 2;
            this.tbHpExhaust.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbHpIndoor
            // 
            this.tbHpIndoor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbHpIndoor.Location = new System.Drawing.Point(94, 26);
            this.tbHpIndoor.Name = "tbHpIndoor";
            this.tbHpIndoor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tbHpIndoor.Size = new System.Drawing.Size(69, 20);
            this.tbHpIndoor.TabIndex = 1;
            this.tbHpIndoor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(14, 204);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(76, 13);
            this.label18.TabIndex = 10;
            this.label18.Text = "Double Blower";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(12, 184);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(79, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "Model Number:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(10, 162);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(81, 13);
            this.label16.TabIndex = 8;
            this.label16.Text = "Voltage Output:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(48, 140);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(43, 13);
            this.label15.TabIndex = 7;
            this.label15.Text = "Min Hz:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(45, 118);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 6;
            this.label14.Text = "Max Hz:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(27, 96);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 5;
            this.label13.Text = "Wheel Size:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(12, 74);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(79, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Running Amps:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(23, 52);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(68, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Rated Amps:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(67, 30);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Hp:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(180, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(45, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Exhaust";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(112, 10);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Indoor";
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Location = new System.Drawing.Point(32, 68);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(34, 13);
            this.label73.TabIndex = 6;
            this.label73.Text = "L1-G";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(104, 30);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(39, 13);
            this.label71.TabIndex = 4;
            this.label71.Text = "L2-L3";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(29, 30);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(39, 13);
            this.label61.TabIndex = 3;
            this.label61.Text = "L1-L2";
            // 
            // rbVoltage575
            // 
            this.rbVoltage575.AutoSize = true;
            this.rbVoltage575.Location = new System.Drawing.Point(168, 13);
            this.rbVoltage575.Name = "rbVoltage575";
            this.rbVoltage575.Size = new System.Drawing.Size(46, 17);
            this.rbVoltage575.TabIndex = 3;
            this.rbVoltage575.TabStop = true;
            this.rbVoltage575.Text = "575";
            this.rbVoltage575.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rbVoltage575.UseVisualStyleBackColor = true;
            // 
            // rbVoltage460
            // 
            this.rbVoltage460.AutoSize = true;
            this.rbVoltage460.Location = new System.Drawing.Point(105, 13);
            this.rbVoltage460.Name = "rbVoltage460";
            this.rbVoltage460.Size = new System.Drawing.Size(46, 17);
            this.rbVoltage460.TabIndex = 2;
            this.rbVoltage460.TabStop = true;
            this.rbVoltage460.Text = "460";
            this.rbVoltage460.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rbVoltage460.UseVisualStyleBackColor = true;
            // 
            // rbVoltage208
            // 
            this.rbVoltage208.AutoSize = true;
            this.rbVoltage208.Location = new System.Drawing.Point(45, 13);
            this.rbVoltage208.Name = "rbVoltage208";
            this.rbVoltage208.Size = new System.Drawing.Size(46, 17);
            this.rbVoltage208.TabIndex = 1;
            this.rbVoltage208.TabStop = true;
            this.rbVoltage208.Text = "208";
            this.rbVoltage208.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.rbVoltage208.UseVisualStyleBackColor = true;
            // 
            // nudHumidity
            // 
            this.nudHumidity.Location = new System.Drawing.Point(85, 39);
            this.nudHumidity.Name = "nudHumidity";
            this.nudHumidity.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.nudHumidity.Size = new System.Drawing.Size(100, 20);
            this.nudHumidity.TabIndex = 2;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(180, 30);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(39, 13);
            this.label72.TabIndex = 5;
            this.label72.Text = "L1-L3";
            // 
            // tbJobNum
            // 
            this.tbJobNum.Location = new System.Drawing.Point(76, 3);
            this.tbJobNum.Name = "tbJobNum";
            this.tbJobNum.ReadOnly = true;
            this.tbJobNum.Size = new System.Drawing.Size(88, 20);
            this.tbJobNum.TabIndex = 0;
            // 
            // gbAmbientConditions
            // 
            this.gbAmbientConditions.Controls.Add(this.nudHumidity);
            this.gbAmbientConditions.Controls.Add(this.nudTemperature);
            this.gbAmbientConditions.Controls.Add(this.label70);
            this.gbAmbientConditions.Controls.Add(this.label69);
            this.gbAmbientConditions.Controls.Add(this.label7);
            this.gbAmbientConditions.Controls.Add(this.label6);
            this.gbAmbientConditions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbAmbientConditions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAmbientConditions.Location = new System.Drawing.Point(3, 125);
            this.gbAmbientConditions.Name = "gbAmbientConditions";
            this.gbAmbientConditions.Size = new System.Drawing.Size(251, 64);
            this.gbAmbientConditions.TabIndex = 1;
            this.gbAmbientConditions.TabStop = false;
            this.gbAmbientConditions.Text = "Ambient Conditions";
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(191, 41);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(15, 13);
            this.label70.TabIndex = 5;
            this.label70.Text = "%";
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(191, 17);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(17, 13);
            this.label69.TabIndex = 4;
            this.label69.Text = "°F";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(10, 41);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Humidity:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(9, 17);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(70, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Temperature:";
            // 
            // gbVoltage
            // 
            this.gbVoltage.Controls.Add(this.tbVoltageL3G);
            this.gbVoltage.Controls.Add(this.tbVoltageL2G);
            this.gbVoltage.Controls.Add(this.tbVoltageL1G);
            this.gbVoltage.Controls.Add(this.tbVoltageL1L3);
            this.gbVoltage.Controls.Add(this.tbVoltageL2L3);
            this.gbVoltage.Controls.Add(this.tbVoltageL1L2);
            this.gbVoltage.Controls.Add(this.label75);
            this.gbVoltage.Controls.Add(this.label74);
            this.gbVoltage.Controls.Add(this.label73);
            this.gbVoltage.Controls.Add(this.label72);
            this.gbVoltage.Controls.Add(this.label71);
            this.gbVoltage.Controls.Add(this.label61);
            this.gbVoltage.Controls.Add(this.rbVoltage575);
            this.gbVoltage.Controls.Add(this.rbVoltage460);
            this.gbVoltage.Controls.Add(this.rbVoltage208);
            this.gbVoltage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbVoltage.Location = new System.Drawing.Point(3, 3);
            this.gbVoltage.Name = "gbVoltage";
            this.gbVoltage.Size = new System.Drawing.Size(251, 116);
            this.gbVoltage.TabIndex = 0;
            this.gbVoltage.TabStop = false;
            this.gbVoltage.Text = "Voltage";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(452, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 29);
            this.label5.TabIndex = 35;
            this.label5.Text = "Model Number";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(959, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 27);
            this.label4.TabIndex = 34;
            this.label4.Text = "Date:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(170, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 29);
            this.label3.TabIndex = 31;
            this.label3.Text = "Serial Number:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 27);
            this.label2.TabIndex = 29;
            this.label2.Text = "Job Name:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbPreHeatTested
            // 
            this.cbPreHeatTested.AutoSize = true;
            this.cbPreHeatTested.Location = new System.Drawing.Point(227, 336);
            this.cbPreHeatTested.Name = "cbPreHeatTested";
            this.cbPreHeatTested.Size = new System.Drawing.Size(15, 14);
            this.cbPreHeatTested.TabIndex = 28;
            this.cbPreHeatTested.UseVisualStyleBackColor = true;
            // 
            // cbPreHeatInstalled
            // 
            this.cbPreHeatInstalled.AutoSize = true;
            this.cbPreHeatInstalled.Location = new System.Drawing.Point(171, 334);
            this.cbPreHeatInstalled.Name = "cbPreHeatInstalled";
            this.cbPreHeatInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbPreHeatInstalled.TabIndex = 27;
            this.cbPreHeatInstalled.UseVisualStyleBackColor = true;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Location = new System.Drawing.Point(95, 334);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(57, 13);
            this.label109.TabIndex = 44;
            this.label109.Text = "Pre-Heat";
            // 
            // cbHotGasReheatTested
            // 
            this.cbHotGasReheatTested.AutoSize = true;
            this.cbHotGasReheatTested.Location = new System.Drawing.Point(227, 313);
            this.cbHotGasReheatTested.Name = "cbHotGasReheatTested";
            this.cbHotGasReheatTested.Size = new System.Drawing.Size(15, 14);
            this.cbHotGasReheatTested.TabIndex = 26;
            this.cbHotGasReheatTested.UseVisualStyleBackColor = true;
            // 
            // cbHotGasReheatInstalled
            // 
            this.cbHotGasReheatInstalled.AutoSize = true;
            this.cbHotGasReheatInstalled.Location = new System.Drawing.Point(171, 313);
            this.cbHotGasReheatInstalled.Name = "cbHotGasReheatInstalled";
            this.cbHotGasReheatInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbHotGasReheatInstalled.TabIndex = 25;
            this.cbHotGasReheatInstalled.UseVisualStyleBackColor = true;
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Location = new System.Drawing.Point(54, 313);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(98, 13);
            this.label108.TabIndex = 41;
            this.label108.Text = "Hot Gas Reheat";
            // 
            // cbReversingValvesTested
            // 
            this.cbReversingValvesTested.AutoSize = true;
            this.cbReversingValvesTested.Location = new System.Drawing.Point(227, 289);
            this.cbReversingValvesTested.Name = "cbReversingValvesTested";
            this.cbReversingValvesTested.Size = new System.Drawing.Size(15, 14);
            this.cbReversingValvesTested.TabIndex = 24;
            this.cbReversingValvesTested.UseVisualStyleBackColor = true;
            // 
            // cbReversingValvesInstalled
            // 
            this.cbReversingValvesInstalled.AutoSize = true;
            this.cbReversingValvesInstalled.Location = new System.Drawing.Point(171, 289);
            this.cbReversingValvesInstalled.Name = "cbReversingValvesInstalled";
            this.cbReversingValvesInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbReversingValvesInstalled.TabIndex = 23;
            this.cbReversingValvesInstalled.UseVisualStyleBackColor = true;
            // 
            // cbAuxiliaryLightsTested
            // 
            this.cbAuxiliaryLightsTested.AutoSize = true;
            this.cbAuxiliaryLightsTested.Location = new System.Drawing.Point(227, 265);
            this.cbAuxiliaryLightsTested.Name = "cbAuxiliaryLightsTested";
            this.cbAuxiliaryLightsTested.Size = new System.Drawing.Size(15, 14);
            this.cbAuxiliaryLightsTested.TabIndex = 22;
            this.cbAuxiliaryLightsTested.UseVisualStyleBackColor = true;
            // 
            // cbAuxiliaryLightsInstalled
            // 
            this.cbAuxiliaryLightsInstalled.AutoSize = true;
            this.cbAuxiliaryLightsInstalled.Location = new System.Drawing.Point(171, 265);
            this.cbAuxiliaryLightsInstalled.Name = "cbAuxiliaryLightsInstalled";
            this.cbAuxiliaryLightsInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbAuxiliaryLightsInstalled.TabIndex = 21;
            this.cbAuxiliaryLightsInstalled.UseVisualStyleBackColor = true;
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Location = new System.Drawing.Point(89, 265);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(62, 13);
            this.label106.TabIndex = 35;
            this.label106.Text = "UV Lights";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Location = new System.Drawing.Point(65, 219);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(87, 13);
            this.label103.TabIndex = 26;
            this.label103.Text = "Exhaust Piezo";
            // 
            // cbSupplyPiezoTested
            // 
            this.cbSupplyPiezoTested.AutoSize = true;
            this.cbSupplyPiezoTested.Location = new System.Drawing.Point(227, 195);
            this.cbSupplyPiezoTested.Name = "cbSupplyPiezoTested";
            this.cbSupplyPiezoTested.Size = new System.Drawing.Size(15, 14);
            this.cbSupplyPiezoTested.TabIndex = 16;
            this.cbSupplyPiezoTested.UseVisualStyleBackColor = true;
            // 
            // cbSupplyPiezoInstalled
            // 
            this.cbSupplyPiezoInstalled.AutoSize = true;
            this.cbSupplyPiezoInstalled.Location = new System.Drawing.Point(171, 195);
            this.cbSupplyPiezoInstalled.Name = "cbSupplyPiezoInstalled";
            this.cbSupplyPiezoInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbSupplyPiezoInstalled.TabIndex = 15;
            this.cbSupplyPiezoInstalled.UseVisualStyleBackColor = true;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Location = new System.Drawing.Point(72, 195);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(80, 13);
            this.label102.TabIndex = 23;
            this.label102.Text = "Supply Piezo";
            // 
            // cbProtoNodeTested
            // 
            this.cbProtoNodeTested.AutoSize = true;
            this.cbProtoNodeTested.Location = new System.Drawing.Point(227, 241);
            this.cbProtoNodeTested.Name = "cbProtoNodeTested";
            this.cbProtoNodeTested.Size = new System.Drawing.Size(15, 14);
            this.cbProtoNodeTested.TabIndex = 20;
            this.cbProtoNodeTested.UseVisualStyleBackColor = true;
            // 
            // cbProtoNodeInstalled
            // 
            this.cbProtoNodeInstalled.AutoSize = true;
            this.cbProtoNodeInstalled.Location = new System.Drawing.Point(171, 241);
            this.cbProtoNodeInstalled.Name = "cbProtoNodeInstalled";
            this.cbProtoNodeInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbProtoNodeInstalled.TabIndex = 19;
            this.cbProtoNodeInstalled.UseVisualStyleBackColor = true;
            // 
            // cbExhaustPiezoTested
            // 
            this.cbExhaustPiezoTested.AutoSize = true;
            this.cbExhaustPiezoTested.Location = new System.Drawing.Point(227, 219);
            this.cbExhaustPiezoTested.Name = "cbExhaustPiezoTested";
            this.cbExhaustPiezoTested.Size = new System.Drawing.Size(15, 14);
            this.cbExhaustPiezoTested.TabIndex = 18;
            this.cbExhaustPiezoTested.UseVisualStyleBackColor = true;
            // 
            // cbExhaustPiezoInstalled
            // 
            this.cbExhaustPiezoInstalled.AutoSize = true;
            this.cbExhaustPiezoInstalled.Location = new System.Drawing.Point(171, 219);
            this.cbExhaustPiezoInstalled.Name = "cbExhaustPiezoInstalled";
            this.cbExhaustPiezoInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbExhaustPiezoInstalled.TabIndex = 17;
            this.cbExhaustPiezoInstalled.UseVisualStyleBackColor = true;
            // 
            // cbConvenienceOutletTested
            // 
            this.cbConvenienceOutletTested.AutoSize = true;
            this.cbConvenienceOutletTested.Location = new System.Drawing.Point(227, 171);
            this.cbConvenienceOutletTested.Name = "cbConvenienceOutletTested";
            this.cbConvenienceOutletTested.Size = new System.Drawing.Size(15, 14);
            this.cbConvenienceOutletTested.TabIndex = 14;
            this.cbConvenienceOutletTested.UseVisualStyleBackColor = true;
            // 
            // cbConvenienceOutletInstalled
            // 
            this.cbConvenienceOutletInstalled.AutoSize = true;
            this.cbConvenienceOutletInstalled.Location = new System.Drawing.Point(171, 171);
            this.cbConvenienceOutletInstalled.Name = "cbConvenienceOutletInstalled";
            this.cbConvenienceOutletInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbConvenienceOutletInstalled.TabIndex = 13;
            this.cbConvenienceOutletInstalled.UseVisualStyleBackColor = true;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Location = new System.Drawing.Point(33, 171);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(119, 13);
            this.label101.TabIndex = 20;
            this.label101.Text = "Convenience Outlet";
            // 
            // cbBypassDampersTested
            // 
            this.cbBypassDampersTested.AutoSize = true;
            this.cbBypassDampersTested.Location = new System.Drawing.Point(227, 147);
            this.cbBypassDampersTested.Name = "cbBypassDampersTested";
            this.cbBypassDampersTested.Size = new System.Drawing.Size(15, 14);
            this.cbBypassDampersTested.TabIndex = 12;
            this.cbBypassDampersTested.UseVisualStyleBackColor = true;
            // 
            // cbBypassDampersInstalled
            // 
            this.cbBypassDampersInstalled.AutoSize = true;
            this.cbBypassDampersInstalled.Location = new System.Drawing.Point(171, 147);
            this.cbBypassDampersInstalled.Name = "cbBypassDampersInstalled";
            this.cbBypassDampersInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbBypassDampersInstalled.TabIndex = 11;
            this.cbBypassDampersInstalled.UseVisualStyleBackColor = true;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(52, 147);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(100, 13);
            this.label100.TabIndex = 17;
            this.label100.Text = "Bypass Dampers";
            // 
            // cbERVTested
            // 
            this.cbERVTested.AutoSize = true;
            this.cbERVTested.Location = new System.Drawing.Point(227, 123);
            this.cbERVTested.Name = "cbERVTested";
            this.cbERVTested.Size = new System.Drawing.Size(15, 14);
            this.cbERVTested.TabIndex = 10;
            this.cbERVTested.UseVisualStyleBackColor = true;
            // 
            // cbERVInstalled
            // 
            this.cbERVInstalled.AutoSize = true;
            this.cbERVInstalled.Location = new System.Drawing.Point(171, 123);
            this.cbERVInstalled.Name = "cbERVInstalled";
            this.cbERVInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbERVInstalled.TabIndex = 9;
            this.cbERVInstalled.UseVisualStyleBackColor = true;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(120, 123);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(32, 13);
            this.label99.TabIndex = 14;
            this.label99.Text = "ERV";
            // 
            // cbPoweredExhaustTested
            // 
            this.cbPoweredExhaustTested.AutoSize = true;
            this.cbPoweredExhaustTested.Location = new System.Drawing.Point(227, 99);
            this.cbPoweredExhaustTested.Name = "cbPoweredExhaustTested";
            this.cbPoweredExhaustTested.Size = new System.Drawing.Size(15, 14);
            this.cbPoweredExhaustTested.TabIndex = 8;
            this.cbPoweredExhaustTested.UseVisualStyleBackColor = true;
            // 
            // cbPoweredExhaustInstalled
            // 
            this.cbPoweredExhaustInstalled.AutoSize = true;
            this.cbPoweredExhaustInstalled.Location = new System.Drawing.Point(171, 99);
            this.cbPoweredExhaustInstalled.Name = "cbPoweredExhaustInstalled";
            this.cbPoweredExhaustInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbPoweredExhaustInstalled.TabIndex = 7;
            this.cbPoweredExhaustInstalled.UseVisualStyleBackColor = true;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Location = new System.Drawing.Point(47, 99);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(105, 13);
            this.label98.TabIndex = 11;
            this.label98.Text = "Powered Exhaust";
            // 
            // cbHeaterTested
            // 
            this.cbHeaterTested.AutoSize = true;
            this.cbHeaterTested.Location = new System.Drawing.Point(227, 77);
            this.cbHeaterTested.Name = "cbHeaterTested";
            this.cbHeaterTested.Size = new System.Drawing.Size(15, 14);
            this.cbHeaterTested.TabIndex = 6;
            this.cbHeaterTested.UseVisualStyleBackColor = true;
            // 
            // cbHeaterInstalled
            // 
            this.cbHeaterInstalled.AutoSize = true;
            this.cbHeaterInstalled.Location = new System.Drawing.Point(171, 77);
            this.cbHeaterInstalled.Name = "cbHeaterInstalled";
            this.cbHeaterInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbHeaterInstalled.TabIndex = 5;
            this.cbHeaterInstalled.UseVisualStyleBackColor = true;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(107, 77);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(45, 13);
            this.label97.TabIndex = 8;
            this.label97.Text = "Heater";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(206, 19);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(46, 13);
            this.label95.TabIndex = 2;
            this.label95.Text = "Tested";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(148, 19);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(55, 13);
            this.label94.TabIndex = 1;
            this.label94.Text = "Installed";
            // 
            // gbOptions
            // 
            this.gbOptions.Controls.Add(this.cbGreenTrolTested);
            this.gbOptions.Controls.Add(this.cbGreenTrolInstalled);
            this.gbOptions.Controls.Add(this.lblGreenTrol);
            this.gbOptions.Controls.Add(this.cbReturnSmokeDetectorTested);
            this.gbOptions.Controls.Add(this.cbReturnSmokeDetectorInstalled);
            this.gbOptions.Controls.Add(this.label86);
            this.gbOptions.Controls.Add(this.cbSupplySmokeDectectorTested);
            this.gbOptions.Controls.Add(this.cbSupplySmokeDectectorInstalled);
            this.gbOptions.Controls.Add(this.label85);
            this.gbOptions.Controls.Add(this.cbLEDServiceTested);
            this.gbOptions.Controls.Add(this.cbLEDServiceInstalled);
            this.gbOptions.Controls.Add(this.label139);
            this.gbOptions.Controls.Add(this.cbHailguardTested);
            this.gbOptions.Controls.Add(this.cbHailguradInstalled);
            this.gbOptions.Controls.Add(this.label137);
            this.gbOptions.Controls.Add(this.cbIsolationDamperTested);
            this.gbOptions.Controls.Add(this.cbIsolationDamperoption);
            this.gbOptions.Controls.Add(this.label23);
            this.gbOptions.Controls.Add(this.cbPreHeatTested);
            this.gbOptions.Controls.Add(this.cbPreHeatInstalled);
            this.gbOptions.Controls.Add(this.label109);
            this.gbOptions.Controls.Add(this.cbHotGasReheatTested);
            this.gbOptions.Controls.Add(this.cbHotGasReheatInstalled);
            this.gbOptions.Controls.Add(this.label108);
            this.gbOptions.Controls.Add(this.cbReversingValvesTested);
            this.gbOptions.Controls.Add(this.cbReversingValvesInstalled);
            this.gbOptions.Controls.Add(this.label107);
            this.gbOptions.Controls.Add(this.cbAuxiliaryLightsTested);
            this.gbOptions.Controls.Add(this.cbAuxiliaryLightsInstalled);
            this.gbOptions.Controls.Add(this.label106);
            this.gbOptions.Controls.Add(this.cbProtoNodeTested);
            this.gbOptions.Controls.Add(this.cbProtoNodeInstalled);
            this.gbOptions.Controls.Add(this.label105);
            this.gbOptions.Controls.Add(this.cbExhaustPiezoTested);
            this.gbOptions.Controls.Add(this.cbExhaustPiezoInstalled);
            this.gbOptions.Controls.Add(this.label103);
            this.gbOptions.Controls.Add(this.cbSupplyPiezoTested);
            this.gbOptions.Controls.Add(this.cbSupplyPiezoInstalled);
            this.gbOptions.Controls.Add(this.label102);
            this.gbOptions.Controls.Add(this.cbConvenienceOutletTested);
            this.gbOptions.Controls.Add(this.cbConvenienceOutletInstalled);
            this.gbOptions.Controls.Add(this.label101);
            this.gbOptions.Controls.Add(this.cbBypassDampersTested);
            this.gbOptions.Controls.Add(this.cbBypassDampersInstalled);
            this.gbOptions.Controls.Add(this.label100);
            this.gbOptions.Controls.Add(this.cbERVTested);
            this.gbOptions.Controls.Add(this.cbERVInstalled);
            this.gbOptions.Controls.Add(this.label99);
            this.gbOptions.Controls.Add(this.cbPoweredExhaustTested);
            this.gbOptions.Controls.Add(this.cbPoweredExhaustInstalled);
            this.gbOptions.Controls.Add(this.label98);
            this.gbOptions.Controls.Add(this.cbHeaterTested);
            this.gbOptions.Controls.Add(this.cbHeaterInstalled);
            this.gbOptions.Controls.Add(this.label97);
            this.gbOptions.Controls.Add(this.label95);
            this.gbOptions.Controls.Add(this.label94);
            this.gbOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbOptions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOptions.Location = new System.Drawing.Point(3, 3);
            this.gbOptions.Name = "gbOptions";
            this.gbOptions.Size = new System.Drawing.Size(288, 437);
            this.gbOptions.TabIndex = 12;
            this.gbOptions.TabStop = false;
            this.gbOptions.Text = "Options";
            // 
            // cbGreenTrolTested
            // 
            this.cbGreenTrolTested.AutoSize = true;
            this.cbGreenTrolTested.Location = new System.Drawing.Point(227, 419);
            this.cbGreenTrolTested.Name = "cbGreenTrolTested";
            this.cbGreenTrolTested.Size = new System.Drawing.Size(15, 14);
            this.cbGreenTrolTested.TabIndex = 36;
            this.cbGreenTrolTested.UseVisualStyleBackColor = true;
            // 
            // cbGreenTrolInstalled
            // 
            this.cbGreenTrolInstalled.AutoSize = true;
            this.cbGreenTrolInstalled.Location = new System.Drawing.Point(171, 419);
            this.cbGreenTrolInstalled.Name = "cbGreenTrolInstalled";
            this.cbGreenTrolInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbGreenTrolInstalled.TabIndex = 35;
            this.cbGreenTrolInstalled.UseVisualStyleBackColor = true;
            // 
            // lblGreenTrol
            // 
            this.lblGreenTrol.AutoSize = true;
            this.lblGreenTrol.Location = new System.Drawing.Point(85, 419);
            this.lblGreenTrol.Name = "lblGreenTrol";
            this.lblGreenTrol.Size = new System.Drawing.Size(67, 13);
            this.lblGreenTrol.TabIndex = 62;
            this.lblGreenTrol.Text = "Green Trol";
            // 
            // cbReturnSmokeDetectorTested
            // 
            this.cbReturnSmokeDetectorTested.AutoSize = true;
            this.cbReturnSmokeDetectorTested.Location = new System.Drawing.Point(227, 56);
            this.cbReturnSmokeDetectorTested.Name = "cbReturnSmokeDetectorTested";
            this.cbReturnSmokeDetectorTested.Size = new System.Drawing.Size(15, 14);
            this.cbReturnSmokeDetectorTested.TabIndex = 4;
            this.cbReturnSmokeDetectorTested.UseVisualStyleBackColor = true;
            // 
            // cbReturnSmokeDetectorInstalled
            // 
            this.cbReturnSmokeDetectorInstalled.AutoSize = true;
            this.cbReturnSmokeDetectorInstalled.Location = new System.Drawing.Point(171, 56);
            this.cbReturnSmokeDetectorInstalled.Name = "cbReturnSmokeDetectorInstalled";
            this.cbReturnSmokeDetectorInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbReturnSmokeDetectorInstalled.TabIndex = 3;
            this.cbReturnSmokeDetectorInstalled.UseVisualStyleBackColor = true;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(14, 55);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(140, 13);
            this.label86.TabIndex = 59;
            this.label86.Text = "Return Smoke Detector";
            this.label86.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbSupplySmokeDectectorTested
            // 
            this.cbSupplySmokeDectectorTested.AutoSize = true;
            this.cbSupplySmokeDectectorTested.Location = new System.Drawing.Point(227, 36);
            this.cbSupplySmokeDectectorTested.Name = "cbSupplySmokeDectectorTested";
            this.cbSupplySmokeDectectorTested.Size = new System.Drawing.Size(15, 14);
            this.cbSupplySmokeDectectorTested.TabIndex = 2;
            this.cbSupplySmokeDectectorTested.UseVisualStyleBackColor = true;
            // 
            // cbSupplySmokeDectectorInstalled
            // 
            this.cbSupplySmokeDectectorInstalled.AutoSize = true;
            this.cbSupplySmokeDectectorInstalled.Location = new System.Drawing.Point(171, 36);
            this.cbSupplySmokeDectectorInstalled.Name = "cbSupplySmokeDectectorInstalled";
            this.cbSupplySmokeDectectorInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbSupplySmokeDectectorInstalled.TabIndex = 1;
            this.cbSupplySmokeDectectorInstalled.UseVisualStyleBackColor = true;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(13, 36);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(140, 13);
            this.label85.TabIndex = 56;
            this.label85.Text = "Supply Smoke Detector";
            this.label85.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbLEDServiceTested
            // 
            this.cbLEDServiceTested.AutoSize = true;
            this.cbLEDServiceTested.Location = new System.Drawing.Point(227, 377);
            this.cbLEDServiceTested.Name = "cbLEDServiceTested";
            this.cbLEDServiceTested.Size = new System.Drawing.Size(15, 14);
            this.cbLEDServiceTested.TabIndex = 32;
            this.cbLEDServiceTested.UseVisualStyleBackColor = true;
            // 
            // cbLEDServiceInstalled
            // 
            this.cbLEDServiceInstalled.AutoSize = true;
            this.cbLEDServiceInstalled.Location = new System.Drawing.Point(171, 377);
            this.cbLEDServiceInstalled.Name = "cbLEDServiceInstalled";
            this.cbLEDServiceInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbLEDServiceInstalled.TabIndex = 31;
            this.cbLEDServiceInstalled.UseVisualStyleBackColor = true;
            // 
            // label139
            // 
            this.label139.Location = new System.Drawing.Point(30, 242);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(120, 13);
            this.label139.TabIndex = 53;
            this.label139.Text = "ProtoNode";
            this.label139.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbHailguardTested
            // 
            this.cbHailguardTested.AutoSize = true;
            this.cbHailguardTested.Location = new System.Drawing.Point(227, 400);
            this.cbHailguardTested.Name = "cbHailguardTested";
            this.cbHailguardTested.Size = new System.Drawing.Size(15, 14);
            this.cbHailguardTested.TabIndex = 34;
            this.cbHailguardTested.UseVisualStyleBackColor = true;
            // 
            // cbHailguradInstalled
            // 
            this.cbHailguradInstalled.AutoSize = true;
            this.cbHailguradInstalled.Location = new System.Drawing.Point(171, 400);
            this.cbHailguradInstalled.Name = "cbHailguradInstalled";
            this.cbHailguradInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbHailguradInstalled.TabIndex = 33;
            this.cbHailguradInstalled.UseVisualStyleBackColor = true;
            // 
            // label137
            // 
            this.label137.Location = new System.Drawing.Point(30, 290);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(120, 13);
            this.label137.TabIndex = 50;
            this.label137.Text = "Reversing Valves";
            this.label137.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbIsolationDamperTested
            // 
            this.cbIsolationDamperTested.AutoSize = true;
            this.cbIsolationDamperTested.Location = new System.Drawing.Point(227, 359);
            this.cbIsolationDamperTested.Name = "cbIsolationDamperTested";
            this.cbIsolationDamperTested.Size = new System.Drawing.Size(15, 14);
            this.cbIsolationDamperTested.TabIndex = 30;
            this.cbIsolationDamperTested.UseVisualStyleBackColor = true;
            // 
            // cbIsolationDamperoption
            // 
            this.cbIsolationDamperoption.AutoSize = true;
            this.cbIsolationDamperoption.Location = new System.Drawing.Point(171, 355);
            this.cbIsolationDamperoption.Name = "cbIsolationDamperoption";
            this.cbIsolationDamperoption.Size = new System.Drawing.Size(15, 14);
            this.cbIsolationDamperoption.TabIndex = 29;
            this.cbIsolationDamperoption.UseVisualStyleBackColor = true;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(5, 355);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(149, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Exhaust Isolation damper";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Location = new System.Drawing.Point(90, 399);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(61, 13);
            this.label107.TabIndex = 38;
            this.label107.Text = "Hailguard";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Location = new System.Drawing.Point(42, 378);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(110, 13);
            this.label105.TabIndex = 32;
            this.label105.Text = "LED Service Light";
            // 
            // lbHeadID
            // 
            this.lbHeadID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbHeadID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHeadID.Location = new System.Drawing.Point(1172, 29);
            this.lbHeadID.Name = "lbHeadID";
            this.lbHeadID.Size = new System.Drawing.Size(1, 27);
            this.lbHeadID.TabIndex = 145;
            this.lbHeadID.Text = "HeadID";
            this.lbHeadID.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbHeadID.Visible = false;
            // 
            // gbStdEquipViking
            // 
            this.gbStdEquipViking.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStdEquipViking.Controls.Add(this.cbOADamperTestedVkg);
            this.gbStdEquipViking.Controls.Add(this.cbOADamperInstalledVkg);
            this.gbStdEquipViking.Controls.Add(this.label127);
            this.gbStdEquipViking.Controls.Add(this.cbFSSTestedVkg);
            this.gbStdEquipViking.Controls.Add(this.cbFSSInstalledVkg);
            this.gbStdEquipViking.Controls.Add(this.cbFanFailSwitchTestedVkg);
            this.gbStdEquipViking.Controls.Add(this.cbFanFailSwitchInstalledVkg);
            this.gbStdEquipViking.Controls.Add(this.label128);
            this.gbStdEquipViking.Controls.Add(this.label129);
            this.gbStdEquipViking.Controls.Add(this.label131);
            this.gbStdEquipViking.Controls.Add(this.label134);
            this.gbStdEquipViking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbStdEquipViking.Location = new System.Drawing.Point(3, 336);
            this.gbStdEquipViking.Name = "gbStdEquipViking";
            this.gbStdEquipViking.Size = new System.Drawing.Size(301, 100);
            this.gbStdEquipViking.TabIndex = 9;
            this.gbStdEquipViking.TabStop = false;
            this.gbStdEquipViking.Text = "Standard Equipment Viking";
            // 
            // cbOADamperTestedVkg
            // 
            this.cbOADamperTestedVkg.AutoSize = true;
            this.cbOADamperTestedVkg.Location = new System.Drawing.Point(267, 72);
            this.cbOADamperTestedVkg.Name = "cbOADamperTestedVkg";
            this.cbOADamperTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbOADamperTestedVkg.TabIndex = 6;
            this.cbOADamperTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbOADamperInstalledVkg
            // 
            this.cbOADamperInstalledVkg.AutoSize = true;
            this.cbOADamperInstalledVkg.Location = new System.Drawing.Point(185, 72);
            this.cbOADamperInstalledVkg.Name = "cbOADamperInstalledVkg";
            this.cbOADamperInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbOADamperInstalledVkg.TabIndex = 5;
            this.cbOADamperInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label127
            // 
            this.label127.Location = new System.Drawing.Point(42, 72);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(120, 13);
            this.label127.TabIndex = 55;
            this.label127.Text = "OA Dampers";
            this.label127.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbFSSTestedVkg
            // 
            this.cbFSSTestedVkg.AutoSize = true;
            this.cbFSSTestedVkg.Location = new System.Drawing.Point(267, 52);
            this.cbFSSTestedVkg.Name = "cbFSSTestedVkg";
            this.cbFSSTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbFSSTestedVkg.TabIndex = 4;
            this.cbFSSTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbFSSInstalledVkg
            // 
            this.cbFSSInstalledVkg.AutoSize = true;
            this.cbFSSInstalledVkg.Location = new System.Drawing.Point(185, 52);
            this.cbFSSInstalledVkg.Name = "cbFSSInstalledVkg";
            this.cbFSSInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbFSSInstalledVkg.TabIndex = 3;
            this.cbFSSInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // cbFanFailSwitchTestedVkg
            // 
            this.cbFanFailSwitchTestedVkg.AutoSize = true;
            this.cbFanFailSwitchTestedVkg.Location = new System.Drawing.Point(267, 31);
            this.cbFanFailSwitchTestedVkg.Name = "cbFanFailSwitchTestedVkg";
            this.cbFanFailSwitchTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbFanFailSwitchTestedVkg.TabIndex = 2;
            this.cbFanFailSwitchTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbFanFailSwitchInstalledVkg
            // 
            this.cbFanFailSwitchInstalledVkg.AutoSize = true;
            this.cbFanFailSwitchInstalledVkg.Location = new System.Drawing.Point(185, 31);
            this.cbFanFailSwitchInstalledVkg.Name = "cbFanFailSwitchInstalledVkg";
            this.cbFanFailSwitchInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbFanFailSwitchInstalledVkg.TabIndex = 1;
            this.cbFanFailSwitchInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Location = new System.Drawing.Point(247, 11);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(46, 13);
            this.label128.TabIndex = 7;
            this.label128.Text = "Tested";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(165, 11);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(55, 13);
            this.label129.TabIndex = 6;
            this.label129.Text = "Installed";
            // 
            // label131
            // 
            this.label131.Location = new System.Drawing.Point(42, 52);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(120, 13);
            this.label131.TabIndex = 4;
            this.label131.Text = "Filter Status Switch";
            this.label131.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label134
            // 
            this.label134.Location = new System.Drawing.Point(42, 31);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(120, 13);
            this.label134.TabIndex = 0;
            this.label134.Text = "Fan Failure Switch";
            this.label134.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbRADampTestedVkg
            // 
            this.cbRADampTestedVkg.AutoSize = true;
            this.cbRADampTestedVkg.Location = new System.Drawing.Point(219, 371);
            this.cbRADampTestedVkg.Name = "cbRADampTestedVkg";
            this.cbRADampTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbRADampTestedVkg.TabIndex = 8;
            this.cbRADampTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbRADampInstalledVkg
            // 
            this.cbRADampInstalledVkg.AutoSize = true;
            this.cbRADampInstalledVkg.Location = new System.Drawing.Point(163, 371);
            this.cbRADampInstalledVkg.Name = "cbRADampInstalledVkg";
            this.cbRADampInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbRADampInstalledVkg.TabIndex = 7;
            this.cbRADampInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // cbOADampTestedVkg
            // 
            this.cbOADampTestedVkg.AutoSize = true;
            this.cbOADampTestedVkg.Location = new System.Drawing.Point(219, 348);
            this.cbOADampTestedVkg.Name = "cbOADampTestedVkg";
            this.cbOADampTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbOADampTestedVkg.TabIndex = 6;
            this.cbOADampTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbOADampInstalledVkg
            // 
            this.cbOADampInstalledVkg.AutoSize = true;
            this.cbOADampInstalledVkg.Location = new System.Drawing.Point(163, 348);
            this.cbOADampInstalledVkg.Name = "cbOADampInstalledVkg";
            this.cbOADampInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbOADampInstalledVkg.TabIndex = 5;
            this.cbOADampInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label132
            // 
            this.label132.Location = new System.Drawing.Point(23, 371);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(120, 13);
            this.label132.TabIndex = 3;
            this.label132.Text = "RA Dampers";
            this.label132.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label133
            // 
            this.label133.Location = new System.Drawing.Point(23, 348);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(120, 13);
            this.label133.TabIndex = 2;
            this.label133.Text = "OA Dampers";
            this.label133.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // gbOptionsViking
            // 
            this.gbOptionsViking.Controls.Add(this.cbHailguardVkgTested);
            this.gbOptionsViking.Controls.Add(this.cbHailguardVkgInstalled);
            this.gbOptionsViking.Controls.Add(this.label80);
            this.gbOptionsViking.Controls.Add(this.cbLEDServiceVkgTested);
            this.gbOptionsViking.Controls.Add(this.cbLEDServiceVKgInstalled);
            this.gbOptionsViking.Controls.Add(this.label77);
            this.gbOptionsViking.Controls.Add(this.cbPreHeatTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbPreHeatInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label135);
            this.gbOptionsViking.Controls.Add(this.cbHGRHTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbHGRHInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.cbRADampTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbRADampInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label136);
            this.gbOptionsViking.Controls.Add(this.cbOADampTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbOADampInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.cbAuxLightsTestedVkg);
            this.gbOptionsViking.Controls.Add(this.label132);
            this.gbOptionsViking.Controls.Add(this.cbAuxLightsInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.cbFiltStatSwitchTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbFiltStatSwitchInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label138);
            this.gbOptionsViking.Controls.Add(this.label133);
            this.gbOptionsViking.Controls.Add(this.cbExhPiezoTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbExhPiezoInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label140);
            this.gbOptionsViking.Controls.Add(this.cbSupplyPiezoTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbSupplyPiezoInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label141);
            this.gbOptionsViking.Controls.Add(this.label142);
            this.gbOptionsViking.Controls.Add(this.cbConvOutletTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbConvOutletInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label143);
            this.gbOptionsViking.Controls.Add(this.cbBypDampTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbBypDampInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label144);
            this.gbOptionsViking.Controls.Add(this.cbERVTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbERVInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label145);
            this.gbOptionsViking.Controls.Add(this.cbPwrExhTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbPwrExhInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label146);
            this.gbOptionsViking.Controls.Add(this.cbHeaterTestedVkg);
            this.gbOptionsViking.Controls.Add(this.cbHeaterInstalledVkg);
            this.gbOptionsViking.Controls.Add(this.label147);
            this.gbOptionsViking.Controls.Add(this.label148);
            this.gbOptionsViking.Controls.Add(this.label149);
            this.gbOptionsViking.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbOptionsViking.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbOptionsViking.Location = new System.Drawing.Point(3, 446);
            this.gbOptionsViking.Name = "gbOptionsViking";
            this.gbOptionsViking.Size = new System.Drawing.Size(288, 402);
            this.gbOptionsViking.TabIndex = 12;
            this.gbOptionsViking.TabStop = false;
            this.gbOptionsViking.Text = "Options Viking";
            // 
            // cbHailguardVkgTested
            // 
            this.cbHailguardVkgTested.AutoSize = true;
            this.cbHailguardVkgTested.Location = new System.Drawing.Point(219, 255);
            this.cbHailguardVkgTested.Name = "cbHailguardVkgTested";
            this.cbHailguardVkgTested.Size = new System.Drawing.Size(15, 14);
            this.cbHailguardVkgTested.TabIndex = 49;
            this.cbHailguardVkgTested.UseVisualStyleBackColor = true;
            // 
            // cbHailguardVkgInstalled
            // 
            this.cbHailguardVkgInstalled.AutoSize = true;
            this.cbHailguardVkgInstalled.Location = new System.Drawing.Point(163, 255);
            this.cbHailguardVkgInstalled.Name = "cbHailguardVkgInstalled";
            this.cbHailguardVkgInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbHailguardVkgInstalled.TabIndex = 48;
            this.cbHailguardVkgInstalled.UseVisualStyleBackColor = true;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(81, 255);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(61, 13);
            this.label80.TabIndex = 50;
            this.label80.Text = "Hailguard";
            // 
            // cbLEDServiceVkgTested
            // 
            this.cbLEDServiceVkgTested.AutoSize = true;
            this.cbLEDServiceVkgTested.Location = new System.Drawing.Point(219, 210);
            this.cbLEDServiceVkgTested.Name = "cbLEDServiceVkgTested";
            this.cbLEDServiceVkgTested.Size = new System.Drawing.Size(15, 14);
            this.cbLEDServiceVkgTested.TabIndex = 46;
            this.cbLEDServiceVkgTested.UseVisualStyleBackColor = true;
            // 
            // cbLEDServiceVKgInstalled
            // 
            this.cbLEDServiceVKgInstalled.AutoSize = true;
            this.cbLEDServiceVKgInstalled.Location = new System.Drawing.Point(163, 210);
            this.cbLEDServiceVKgInstalled.Name = "cbLEDServiceVKgInstalled";
            this.cbLEDServiceVKgInstalled.Size = new System.Drawing.Size(15, 14);
            this.cbLEDServiceVKgInstalled.TabIndex = 45;
            this.cbLEDServiceVKgInstalled.UseVisualStyleBackColor = true;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(34, 210);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(110, 13);
            this.label77.TabIndex = 47;
            this.label77.Text = "LED Service Light";
            // 
            // cbPreHeatTestedVkg
            // 
            this.cbPreHeatTestedVkg.AutoSize = true;
            this.cbPreHeatTestedVkg.Location = new System.Drawing.Point(219, 302);
            this.cbPreHeatTestedVkg.Name = "cbPreHeatTestedVkg";
            this.cbPreHeatTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbPreHeatTestedVkg.TabIndex = 30;
            this.cbPreHeatTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbPreHeatInstalledVkg
            // 
            this.cbPreHeatInstalledVkg.AutoSize = true;
            this.cbPreHeatInstalledVkg.Location = new System.Drawing.Point(163, 302);
            this.cbPreHeatInstalledVkg.Name = "cbPreHeatInstalledVkg";
            this.cbPreHeatInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbPreHeatInstalledVkg.TabIndex = 29;
            this.cbPreHeatInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label135
            // 
            this.label135.Location = new System.Drawing.Point(25, 302);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(120, 13);
            this.label135.TabIndex = 44;
            this.label135.Text = "Pre-Heat";
            this.label135.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbHGRHTestedVkg
            // 
            this.cbHGRHTestedVkg.AutoSize = true;
            this.cbHGRHTestedVkg.Location = new System.Drawing.Point(219, 279);
            this.cbHGRHTestedVkg.Name = "cbHGRHTestedVkg";
            this.cbHGRHTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbHGRHTestedVkg.TabIndex = 28;
            this.cbHGRHTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbHGRHInstalledVkg
            // 
            this.cbHGRHInstalledVkg.AutoSize = true;
            this.cbHGRHInstalledVkg.Location = new System.Drawing.Point(163, 279);
            this.cbHGRHInstalledVkg.Name = "cbHGRHInstalledVkg";
            this.cbHGRHInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbHGRHInstalledVkg.TabIndex = 27;
            this.cbHGRHInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label136
            // 
            this.label136.Location = new System.Drawing.Point(25, 279);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(120, 13);
            this.label136.TabIndex = 41;
            this.label136.Text = "Hot Gas Reheat";
            this.label136.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbAuxLightsTestedVkg
            // 
            this.cbAuxLightsTestedVkg.AutoSize = true;
            this.cbAuxLightsTestedVkg.Location = new System.Drawing.Point(219, 233);
            this.cbAuxLightsTestedVkg.Name = "cbAuxLightsTestedVkg";
            this.cbAuxLightsTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbAuxLightsTestedVkg.TabIndex = 24;
            this.cbAuxLightsTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbAuxLightsInstalledVkg
            // 
            this.cbAuxLightsInstalledVkg.AutoSize = true;
            this.cbAuxLightsInstalledVkg.Location = new System.Drawing.Point(163, 233);
            this.cbAuxLightsInstalledVkg.Name = "cbAuxLightsInstalledVkg";
            this.cbAuxLightsInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbAuxLightsInstalledVkg.TabIndex = 23;
            this.cbAuxLightsInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // cbFiltStatSwitchTestedVkg
            // 
            this.cbFiltStatSwitchTestedVkg.AutoSize = true;
            this.cbFiltStatSwitchTestedVkg.Location = new System.Drawing.Point(219, 325);
            this.cbFiltStatSwitchTestedVkg.Name = "cbFiltStatSwitchTestedVkg";
            this.cbFiltStatSwitchTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbFiltStatSwitchTestedVkg.TabIndex = 4;
            this.cbFiltStatSwitchTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbFiltStatSwitchInstalledVkg
            // 
            this.cbFiltStatSwitchInstalledVkg.AutoSize = true;
            this.cbFiltStatSwitchInstalledVkg.Location = new System.Drawing.Point(163, 325);
            this.cbFiltStatSwitchInstalledVkg.Name = "cbFiltStatSwitchInstalledVkg";
            this.cbFiltStatSwitchInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbFiltStatSwitchInstalledVkg.TabIndex = 3;
            this.cbFiltStatSwitchInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label138
            // 
            this.label138.Location = new System.Drawing.Point(25, 233);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(120, 13);
            this.label138.TabIndex = 35;
            this.label138.Text = "UV Lights";
            this.label138.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbExhPiezoTestedVkg
            // 
            this.cbExhPiezoTestedVkg.AutoSize = true;
            this.cbExhPiezoTestedVkg.Location = new System.Drawing.Point(219, 187);
            this.cbExhPiezoTestedVkg.Name = "cbExhPiezoTestedVkg";
            this.cbExhPiezoTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbExhPiezoTestedVkg.TabIndex = 18;
            this.cbExhPiezoTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbExhPiezoInstalledVkg
            // 
            this.cbExhPiezoInstalledVkg.AutoSize = true;
            this.cbExhPiezoInstalledVkg.Location = new System.Drawing.Point(163, 187);
            this.cbExhPiezoInstalledVkg.Name = "cbExhPiezoInstalledVkg";
            this.cbExhPiezoInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbExhPiezoInstalledVkg.TabIndex = 17;
            this.cbExhPiezoInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label140
            // 
            this.label140.Location = new System.Drawing.Point(25, 187);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(120, 13);
            this.label140.TabIndex = 26;
            this.label140.Text = "Exhaust Piezo";
            this.label140.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbSupplyPiezoTestedVkg
            // 
            this.cbSupplyPiezoTestedVkg.AutoSize = true;
            this.cbSupplyPiezoTestedVkg.Location = new System.Drawing.Point(219, 164);
            this.cbSupplyPiezoTestedVkg.Name = "cbSupplyPiezoTestedVkg";
            this.cbSupplyPiezoTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbSupplyPiezoTestedVkg.TabIndex = 16;
            this.cbSupplyPiezoTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbSupplyPiezoInstalledVkg
            // 
            this.cbSupplyPiezoInstalledVkg.AutoSize = true;
            this.cbSupplyPiezoInstalledVkg.Location = new System.Drawing.Point(163, 164);
            this.cbSupplyPiezoInstalledVkg.Name = "cbSupplyPiezoInstalledVkg";
            this.cbSupplyPiezoInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbSupplyPiezoInstalledVkg.TabIndex = 15;
            this.cbSupplyPiezoInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label141
            // 
            this.label141.Location = new System.Drawing.Point(25, 325);
            this.label141.Name = "label141";
            this.label141.Size = new System.Drawing.Size(120, 13);
            this.label141.TabIndex = 1;
            this.label141.Text = "Filter Status Switch";
            this.label141.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label142
            // 
            this.label142.Location = new System.Drawing.Point(23, 164);
            this.label142.Name = "label142";
            this.label142.Size = new System.Drawing.Size(120, 13);
            this.label142.TabIndex = 23;
            this.label142.Text = "Supply Piezo";
            this.label142.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbConvOutletTestedVkg
            // 
            this.cbConvOutletTestedVkg.AutoSize = true;
            this.cbConvOutletTestedVkg.Location = new System.Drawing.Point(219, 139);
            this.cbConvOutletTestedVkg.Name = "cbConvOutletTestedVkg";
            this.cbConvOutletTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbConvOutletTestedVkg.TabIndex = 14;
            this.cbConvOutletTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbConvOutletInstalledVkg
            // 
            this.cbConvOutletInstalledVkg.AutoSize = true;
            this.cbConvOutletInstalledVkg.Location = new System.Drawing.Point(163, 139);
            this.cbConvOutletInstalledVkg.Name = "cbConvOutletInstalledVkg";
            this.cbConvOutletInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbConvOutletInstalledVkg.TabIndex = 13;
            this.cbConvOutletInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label143
            // 
            this.label143.Location = new System.Drawing.Point(25, 139);
            this.label143.Name = "label143";
            this.label143.Size = new System.Drawing.Size(120, 13);
            this.label143.TabIndex = 20;
            this.label143.Text = "Convenience Outlet";
            this.label143.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbBypDampTestedVkg
            // 
            this.cbBypDampTestedVkg.AutoSize = true;
            this.cbBypDampTestedVkg.Location = new System.Drawing.Point(219, 116);
            this.cbBypDampTestedVkg.Name = "cbBypDampTestedVkg";
            this.cbBypDampTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbBypDampTestedVkg.TabIndex = 12;
            this.cbBypDampTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbBypDampInstalledVkg
            // 
            this.cbBypDampInstalledVkg.AutoSize = true;
            this.cbBypDampInstalledVkg.Location = new System.Drawing.Point(163, 116);
            this.cbBypDampInstalledVkg.Name = "cbBypDampInstalledVkg";
            this.cbBypDampInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbBypDampInstalledVkg.TabIndex = 11;
            this.cbBypDampInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label144
            // 
            this.label144.Location = new System.Drawing.Point(25, 116);
            this.label144.Name = "label144";
            this.label144.Size = new System.Drawing.Size(120, 13);
            this.label144.TabIndex = 17;
            this.label144.Text = "Bypass Dampers";
            this.label144.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbERVTestedVkg
            // 
            this.cbERVTestedVkg.AutoSize = true;
            this.cbERVTestedVkg.Location = new System.Drawing.Point(219, 93);
            this.cbERVTestedVkg.Name = "cbERVTestedVkg";
            this.cbERVTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbERVTestedVkg.TabIndex = 10;
            this.cbERVTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbERVInstalledVkg
            // 
            this.cbERVInstalledVkg.AutoSize = true;
            this.cbERVInstalledVkg.Location = new System.Drawing.Point(163, 93);
            this.cbERVInstalledVkg.Name = "cbERVInstalledVkg";
            this.cbERVInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbERVInstalledVkg.TabIndex = 9;
            this.cbERVInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label145
            // 
            this.label145.Location = new System.Drawing.Point(25, 93);
            this.label145.Name = "label145";
            this.label145.Size = new System.Drawing.Size(120, 13);
            this.label145.TabIndex = 14;
            this.label145.Text = "ERV";
            this.label145.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbPwrExhTestedVkg
            // 
            this.cbPwrExhTestedVkg.AutoSize = true;
            this.cbPwrExhTestedVkg.Location = new System.Drawing.Point(219, 70);
            this.cbPwrExhTestedVkg.Name = "cbPwrExhTestedVkg";
            this.cbPwrExhTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbPwrExhTestedVkg.TabIndex = 8;
            this.cbPwrExhTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbPwrExhInstalledVkg
            // 
            this.cbPwrExhInstalledVkg.AutoSize = true;
            this.cbPwrExhInstalledVkg.Location = new System.Drawing.Point(163, 70);
            this.cbPwrExhInstalledVkg.Name = "cbPwrExhInstalledVkg";
            this.cbPwrExhInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbPwrExhInstalledVkg.TabIndex = 7;
            this.cbPwrExhInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label146
            // 
            this.label146.Location = new System.Drawing.Point(25, 70);
            this.label146.Name = "label146";
            this.label146.Size = new System.Drawing.Size(120, 13);
            this.label146.TabIndex = 11;
            this.label146.Text = "Powered Exhaust";
            this.label146.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cbHeaterTestedVkg
            // 
            this.cbHeaterTestedVkg.AutoSize = true;
            this.cbHeaterTestedVkg.Location = new System.Drawing.Point(219, 47);
            this.cbHeaterTestedVkg.Name = "cbHeaterTestedVkg";
            this.cbHeaterTestedVkg.Size = new System.Drawing.Size(15, 14);
            this.cbHeaterTestedVkg.TabIndex = 6;
            this.cbHeaterTestedVkg.UseVisualStyleBackColor = true;
            // 
            // cbHeaterInstalledVkg
            // 
            this.cbHeaterInstalledVkg.AutoSize = true;
            this.cbHeaterInstalledVkg.Location = new System.Drawing.Point(163, 47);
            this.cbHeaterInstalledVkg.Name = "cbHeaterInstalledVkg";
            this.cbHeaterInstalledVkg.Size = new System.Drawing.Size(15, 14);
            this.cbHeaterInstalledVkg.TabIndex = 5;
            this.cbHeaterInstalledVkg.UseVisualStyleBackColor = true;
            // 
            // label147
            // 
            this.label147.Location = new System.Drawing.Point(25, 41);
            this.label147.Name = "label147";
            this.label147.Size = new System.Drawing.Size(120, 13);
            this.label147.TabIndex = 8;
            this.label147.Text = "Heater";
            this.label147.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label148
            // 
            this.label148.AutoSize = true;
            this.label148.Location = new System.Drawing.Point(202, 19);
            this.label148.Name = "label148";
            this.label148.Size = new System.Drawing.Size(46, 13);
            this.label148.TabIndex = 2;
            this.label148.Text = "Tested";
            // 
            // label149
            // 
            this.label149.AutoSize = true;
            this.label149.Location = new System.Drawing.Point(144, 19);
            this.label149.Name = "label149";
            this.label149.Size = new System.Drawing.Size(55, 13);
            this.label149.TabIndex = 1;
            this.label149.Text = "Installed";
            // 
            // tcTestingChecklist
            // 
            this.tcTestingChecklist.Controls.Add(this.tpPreChecks);
            this.tcTestingChecklist.Controls.Add(this.tabPage3);
            this.tcTestingChecklist.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tcTestingChecklist.Location = new System.Drawing.Point(0, 56);
            this.tcTestingChecklist.Name = "tcTestingChecklist";
            this.tcTestingChecklist.SelectedIndex = 0;
            this.tcTestingChecklist.Size = new System.Drawing.Size(1239, 518);
            this.tcTestingChecklist.TabIndex = 1;
            this.tcTestingChecklist.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tcTestingChecklist_Selecting);
            // 
            // tpPreChecks
            // 
            this.tpPreChecks.BackColor = System.Drawing.Color.DarkGray;
            this.tpPreChecks.Controls.Add(this.pnlPreChecksContainer);
            this.tpPreChecks.Location = new System.Drawing.Point(4, 22);
            this.tpPreChecks.Name = "tpPreChecks";
            this.tpPreChecks.Padding = new System.Windows.Forms.Padding(3);
            this.tpPreChecks.Size = new System.Drawing.Size(1231, 492);
            this.tpPreChecks.TabIndex = 0;
            this.tpPreChecks.Text = "Pre-Checks";
            // 
            // pnlPreChecksContainer
            // 
            this.pnlPreChecksContainer.Controls.Add(this.preChecksControl1);
            this.pnlPreChecksContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPreChecksContainer.Location = new System.Drawing.Point(3, 3);
            this.pnlPreChecksContainer.Name = "pnlPreChecksContainer";
            this.pnlPreChecksContainer.Size = new System.Drawing.Size(1225, 486);
            this.pnlPreChecksContainer.TabIndex = 39;
            // 
            // preChecksControl1
            // 
            this.preChecksControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.preChecksControl1.Location = new System.Drawing.Point(0, 0);
            this.preChecksControl1.Name = "preChecksControl1";
            this.preChecksControl1.Size = new System.Drawing.Size(1225, 486);
            this.preChecksControl1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.DarkGray;
            this.tabPage3.Controls.Add(this.pnlProductCheckListContainer);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(1231, 492);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Production Testing CheckList";
            // 
            // pnlProductCheckListContainer
            // 
            this.pnlProductCheckListContainer.AutoScroll = true;
            this.pnlProductCheckListContainer.Controls.Add(this.tableLayoutPanel2);
            this.pnlProductCheckListContainer.Location = new System.Drawing.Point(3, 3);
            this.pnlProductCheckListContainer.Name = "pnlProductCheckListContainer";
            this.pnlProductCheckListContainer.Size = new System.Drawing.Size(1230, 500);
            this.pnlProductCheckListContainer.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.73479F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.81007F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.80918F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.64596F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel4, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tlpOptions, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel8, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel9, 2, 3);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1213, 835);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.gbFilters, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.gbDisconnect, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.gbStandardEquipment, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.gbStdEquipViking, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.gbCharge, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(266, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 5;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.Size = new System.Drawing.Size(307, 439);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // tlpOptions
            // 
            this.tlpOptions.ColumnCount = 1;
            this.tlpOptions.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpOptions.Controls.Add(this.gbOptions, 0, 0);
            this.tlpOptions.Controls.Add(this.gbOptionsViking, 0, 1);
            this.tlpOptions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpOptions.Location = new System.Drawing.Point(579, 3);
            this.tlpOptions.Name = "tlpOptions";
            this.tlpOptions.RowCount = 2;
            this.tlpOptions.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpOptions.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpOptions.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpOptions.Size = new System.Drawing.Size(294, 439);
            this.tlpOptions.TabIndex = 4;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.gbHeat, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.gbCircuit1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.gbCircuit2, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.gbMeasurePressure, 0, 3);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(879, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel2.SetRowSpan(this.tableLayoutPanel6, 3);
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.Size = new System.Drawing.Size(331, 722);
            this.tableLayoutPanel6.TabIndex = 5;
            // 
            // gbMeasurePressure
            // 
            this.gbMeasurePressure.Controls.Add(this.tableLayoutPanel1);
            this.gbMeasurePressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMeasurePressure.Location = new System.Drawing.Point(3, 571);
            this.gbMeasurePressure.Name = "gbMeasurePressure";
            this.gbMeasurePressure.Size = new System.Drawing.Size(325, 148);
            this.gbMeasurePressure.TabIndex = 16;
            this.gbMeasurePressure.TabStop = false;
            this.gbMeasurePressure.Text = "Measured Pressure";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.label152, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblLowFire, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblHighFire, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label155, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label156, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label157, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label158, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.label159, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.label160, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtInletModulationPresure, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtLowFire, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtHighFire, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblRangeNG, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label154, 3, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(319, 129);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label152
            // 
            this.label152.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label152.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label152.Location = new System.Drawing.Point(3, 13);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(100, 31);
            this.label152.TabIndex = 0;
            this.label152.Text = "Inlet Modulation pressure";
            this.label152.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLowFire
            // 
            this.lblLowFire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLowFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLowFire.Location = new System.Drawing.Point(3, 44);
            this.lblLowFire.Name = "lblLowFire";
            this.lblLowFire.Size = new System.Drawing.Size(100, 26);
            this.lblLowFire.TabIndex = 1;
            this.lblLowFire.Text = "Low Fire";
            this.lblLowFire.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblHighFire
            // 
            this.lblHighFire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHighFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHighFire.Location = new System.Drawing.Point(3, 70);
            this.lblHighFire.Name = "lblHighFire";
            this.lblHighFire.Size = new System.Drawing.Size(100, 26);
            this.lblHighFire.TabIndex = 2;
            this.lblHighFire.Text = "High Fire";
            this.lblHighFire.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label155
            // 
            this.label155.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label155.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label155.Location = new System.Drawing.Point(165, 13);
            this.label155.Name = "label155";
            this.label155.Size = new System.Drawing.Size(76, 31);
            this.label155.TabIndex = 3;
            this.label155.Text = "5\"H2O";
            this.label155.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label156
            // 
            this.label156.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label156.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label156.Location = new System.Drawing.Point(165, 44);
            this.label156.Name = "label156";
            this.label156.Size = new System.Drawing.Size(76, 26);
            this.label156.TabIndex = 4;
            this.label156.Text = "0.4\"H2O";
            this.label156.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label157
            // 
            this.label157.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label157.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label157.Location = new System.Drawing.Point(165, 70);
            this.label157.Name = "label157";
            this.label157.Size = new System.Drawing.Size(76, 26);
            this.label157.TabIndex = 5;
            this.label157.Text = "3.5\"H2O";
            this.label157.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label158
            // 
            this.label158.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label158.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label158.Location = new System.Drawing.Point(247, 13);
            this.label158.Name = "label158";
            this.label158.Size = new System.Drawing.Size(69, 31);
            this.label158.TabIndex = 6;
            this.label158.Text = "10.5\"H2O";
            this.label158.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label159
            // 
            this.label159.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label159.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label159.Location = new System.Drawing.Point(247, 44);
            this.label159.Name = "label159";
            this.label159.Size = new System.Drawing.Size(69, 26);
            this.label159.TabIndex = 7;
            this.label159.Text = "1.2\"H2O";
            this.label159.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label160
            // 
            this.label160.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label160.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label160.Location = new System.Drawing.Point(247, 70);
            this.label160.Name = "label160";
            this.label160.Size = new System.Drawing.Size(69, 26);
            this.label160.TabIndex = 8;
            this.label160.Text = "8\"H2O";
            this.label160.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInletModulationPresure
            // 
            this.txtInletModulationPresure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInletModulationPresure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInletModulationPresure.Location = new System.Drawing.Point(109, 16);
            this.txtInletModulationPresure.Name = "txtInletModulationPresure";
            this.txtInletModulationPresure.Size = new System.Drawing.Size(50, 20);
            this.txtInletModulationPresure.TabIndex = 9;
            // 
            // txtLowFire
            // 
            this.txtLowFire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLowFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLowFire.Location = new System.Drawing.Point(109, 47);
            this.txtLowFire.Name = "txtLowFire";
            this.txtLowFire.Size = new System.Drawing.Size(50, 20);
            this.txtLowFire.TabIndex = 10;
            // 
            // txtHighFire
            // 
            this.txtHighFire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtHighFire.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtHighFire.Location = new System.Drawing.Point(109, 73);
            this.txtHighFire.Name = "txtHighFire";
            this.txtHighFire.Size = new System.Drawing.Size(50, 20);
            this.txtHighFire.TabIndex = 11;
            // 
            // lblRangeNG
            // 
            this.lblRangeNG.AutoSize = true;
            this.lblRangeNG.Location = new System.Drawing.Point(165, 0);
            this.lblRangeNG.Name = "lblRangeNG";
            this.lblRangeNG.Size = new System.Drawing.Size(76, 13);
            this.lblRangeNG.TabIndex = 12;
            this.lblRangeNG.Text = "Ranges  NG";
            // 
            // label154
            // 
            this.label154.AutoSize = true;
            this.label154.Location = new System.Drawing.Point(247, 0);
            this.label154.Name = "label154";
            this.label154.Size = new System.Drawing.Size(69, 13);
            this.label154.TabIndex = 13;
            this.label154.Text = "Ranges LP";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel7.AutoSize = true;
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel7, 3);
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68F));
            this.tableLayoutPanel7.Controls.Add(this.gbCompressorData, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.gbCondensers, 0, 0);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 448);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(870, 173);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // gbCompressorData
            // 
            this.gbCompressorData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRatedAmps6);
            this.gbCompressorData.Controls.Add(this.tbCompressor6SerialNum);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRunningAmps6);
            this.gbCompressorData.Controls.Add(this.tbCompressor6ModelNum);
            this.gbCompressorData.Controls.Add(this.label120);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRatedAmps5);
            this.gbCompressorData.Controls.Add(this.tbCompressor5SerialNum);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRunningAmps5);
            this.gbCompressorData.Controls.Add(this.tbCompressor5ModelNum);
            this.gbCompressorData.Controls.Add(this.label119);
            this.gbCompressorData.Controls.Add(this.tbCompressor4SerialNum);
            this.gbCompressorData.Controls.Add(this.label89);
            this.gbCompressorData.Controls.Add(this.tbCompressor3SerialNum);
            this.gbCompressorData.Controls.Add(this.label90);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRatedAmps4);
            this.gbCompressorData.Controls.Add(this.tbCompressor2SerialNum);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRatedAmps3);
            this.gbCompressorData.Controls.Add(this.label91);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRatedAmps2);
            this.gbCompressorData.Controls.Add(this.tbCompressor1SerialNum);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRatedAmps1);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRunningAmps4);
            this.gbCompressorData.Controls.Add(this.label96);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRunningAmps3);
            this.gbCompressorData.Controls.Add(this.label104);
            this.gbCompressorData.Controls.Add(this.tbCompressor4ModelNum);
            this.gbCompressorData.Controls.Add(this.label130);
            this.gbCompressorData.Controls.Add(this.tbCompressor3ModelNum);
            this.gbCompressorData.Controls.Add(this.label150);
            this.gbCompressorData.Controls.Add(this.tbCompressor2ModelNum);
            this.gbCompressorData.Controls.Add(this.tbCompressor1ModelNum);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRunningAmps1);
            this.gbCompressorData.Controls.Add(this.tbAMACompressorRunningAmps2);
            this.gbCompressorData.Controls.Add(this.label151);
            this.gbCompressorData.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCompressorData.Location = new System.Drawing.Point(281, 3);
            this.gbCompressorData.Name = "gbCompressorData";
            this.gbCompressorData.Size = new System.Drawing.Size(586, 167);
            this.gbCompressorData.TabIndex = 10;
            this.gbCompressorData.TabStop = false;
            this.gbCompressorData.Text = "Compressor Data";
            // 
            // tbAMACompressorRatedAmps6
            // 
            this.tbAMACompressorRatedAmps6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRatedAmps6.Location = new System.Drawing.Point(499, 136);
            this.tbAMACompressorRatedAmps6.Name = "tbAMACompressorRatedAmps6";
            this.tbAMACompressorRatedAmps6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRatedAmps6.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRatedAmps6.TabIndex = 24;
            // 
            // tbCompressor6SerialNum
            // 
            this.tbCompressor6SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor6SerialNum.Location = new System.Drawing.Point(265, 136);
            this.tbCompressor6SerialNum.Name = "tbCompressor6SerialNum";
            this.tbCompressor6SerialNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor6SerialNum.TabIndex = 22;
            // 
            // tbAMACompressorRunningAmps6
            // 
            this.tbAMACompressorRunningAmps6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRunningAmps6.Location = new System.Drawing.Point(423, 136);
            this.tbAMACompressorRunningAmps6.Name = "tbAMACompressorRunningAmps6";
            this.tbAMACompressorRunningAmps6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRunningAmps6.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRunningAmps6.TabIndex = 23;
            // 
            // tbCompressor6ModelNum
            // 
            this.tbCompressor6ModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor6ModelNum.Location = new System.Drawing.Point(107, 136);
            this.tbCompressor6ModelNum.Name = "tbCompressor6ModelNum";
            this.tbCompressor6ModelNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor6ModelNum.TabIndex = 21;
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label120.Location = new System.Drawing.Point(25, 140);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(78, 13);
            this.label120.TabIndex = 135;
            this.label120.Text = "Compressor #6";
            // 
            // tbAMACompressorRatedAmps5
            // 
            this.tbAMACompressorRatedAmps5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRatedAmps5.Location = new System.Drawing.Point(499, 114);
            this.tbAMACompressorRatedAmps5.Name = "tbAMACompressorRatedAmps5";
            this.tbAMACompressorRatedAmps5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRatedAmps5.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRatedAmps5.TabIndex = 20;
            // 
            // tbCompressor5SerialNum
            // 
            this.tbCompressor5SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor5SerialNum.Location = new System.Drawing.Point(265, 114);
            this.tbCompressor5SerialNum.Name = "tbCompressor5SerialNum";
            this.tbCompressor5SerialNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor5SerialNum.TabIndex = 18;
            // 
            // tbAMACompressorRunningAmps5
            // 
            this.tbAMACompressorRunningAmps5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRunningAmps5.Location = new System.Drawing.Point(423, 114);
            this.tbAMACompressorRunningAmps5.Name = "tbAMACompressorRunningAmps5";
            this.tbAMACompressorRunningAmps5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRunningAmps5.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRunningAmps5.TabIndex = 19;
            // 
            // tbCompressor5ModelNum
            // 
            this.tbCompressor5ModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor5ModelNum.Location = new System.Drawing.Point(107, 114);
            this.tbCompressor5ModelNum.Name = "tbCompressor5ModelNum";
            this.tbCompressor5ModelNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor5ModelNum.TabIndex = 17;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(25, 118);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(78, 13);
            this.label119.TabIndex = 132;
            this.label119.Text = "Compressor #5";
            // 
            // tbCompressor4SerialNum
            // 
            this.tbCompressor4SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor4SerialNum.Location = new System.Drawing.Point(265, 92);
            this.tbCompressor4SerialNum.Name = "tbCompressor4SerialNum";
            this.tbCompressor4SerialNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor4SerialNum.TabIndex = 14;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(318, 11);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(43, 13);
            this.label89.TabIndex = 3;
            this.label89.Text = "Serial #";
            // 
            // tbCompressor3SerialNum
            // 
            this.tbCompressor3SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor3SerialNum.Location = new System.Drawing.Point(265, 70);
            this.tbCompressor3SerialNum.Name = "tbCompressor3SerialNum";
            this.tbCompressor3SerialNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor3SerialNum.TabIndex = 10;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(156, 11);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(46, 13);
            this.label90.TabIndex = 2;
            this.label90.Text = "Model #";
            // 
            // tbAMACompressorRatedAmps4
            // 
            this.tbAMACompressorRatedAmps4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRatedAmps4.Location = new System.Drawing.Point(499, 92);
            this.tbAMACompressorRatedAmps4.Name = "tbAMACompressorRatedAmps4";
            this.tbAMACompressorRatedAmps4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRatedAmps4.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRatedAmps4.TabIndex = 16;
            // 
            // tbCompressor2SerialNum
            // 
            this.tbCompressor2SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor2SerialNum.Location = new System.Drawing.Point(265, 48);
            this.tbCompressor2SerialNum.Name = "tbCompressor2SerialNum";
            this.tbCompressor2SerialNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor2SerialNum.TabIndex = 6;
            // 
            // tbAMACompressorRatedAmps3
            // 
            this.tbAMACompressorRatedAmps3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRatedAmps3.Location = new System.Drawing.Point(499, 70);
            this.tbAMACompressorRatedAmps3.Name = "tbAMACompressorRatedAmps3";
            this.tbAMACompressorRatedAmps3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRatedAmps3.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRatedAmps3.TabIndex = 12;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(25, 30);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(78, 13);
            this.label91.TabIndex = 27;
            this.label91.Text = "Compressor #1";
            // 
            // tbAMACompressorRatedAmps2
            // 
            this.tbAMACompressorRatedAmps2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRatedAmps2.Location = new System.Drawing.Point(499, 48);
            this.tbAMACompressorRatedAmps2.Name = "tbAMACompressorRatedAmps2";
            this.tbAMACompressorRatedAmps2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRatedAmps2.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRatedAmps2.TabIndex = 8;
            // 
            // tbCompressor1SerialNum
            // 
            this.tbCompressor1SerialNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor1SerialNum.Location = new System.Drawing.Point(265, 26);
            this.tbCompressor1SerialNum.Name = "tbCompressor1SerialNum";
            this.tbCompressor1SerialNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor1SerialNum.TabIndex = 2;
            // 
            // tbAMACompressorRatedAmps1
            // 
            this.tbAMACompressorRatedAmps1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRatedAmps1.Location = new System.Drawing.Point(499, 26);
            this.tbAMACompressorRatedAmps1.Name = "tbAMACompressorRatedAmps1";
            this.tbAMACompressorRatedAmps1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRatedAmps1.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRatedAmps1.TabIndex = 4;
            // 
            // tbAMACompressorRunningAmps4
            // 
            this.tbAMACompressorRunningAmps4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRunningAmps4.Location = new System.Drawing.Point(423, 92);
            this.tbAMACompressorRunningAmps4.Name = "tbAMACompressorRunningAmps4";
            this.tbAMACompressorRunningAmps4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRunningAmps4.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRunningAmps4.TabIndex = 15;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(25, 52);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(78, 13);
            this.label96.TabIndex = 28;
            this.label96.Text = "Compressor #2";
            // 
            // tbAMACompressorRunningAmps3
            // 
            this.tbAMACompressorRunningAmps3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRunningAmps3.Location = new System.Drawing.Point(423, 70);
            this.tbAMACompressorRunningAmps3.Name = "tbAMACompressorRunningAmps3";
            this.tbAMACompressorRunningAmps3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRunningAmps3.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRunningAmps3.TabIndex = 11;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(421, 10);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(76, 13);
            this.label104.TabIndex = 18;
            this.label104.Text = "Running Amps";
            // 
            // tbCompressor4ModelNum
            // 
            this.tbCompressor4ModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor4ModelNum.Location = new System.Drawing.Point(107, 92);
            this.tbCompressor4ModelNum.Name = "tbCompressor4ModelNum";
            this.tbCompressor4ModelNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor4ModelNum.TabIndex = 13;
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label130.Location = new System.Drawing.Point(25, 74);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(78, 13);
            this.label130.TabIndex = 29;
            this.label130.Text = "Compressor #3";
            // 
            // tbCompressor3ModelNum
            // 
            this.tbCompressor3ModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor3ModelNum.Location = new System.Drawing.Point(107, 70);
            this.tbCompressor3ModelNum.Name = "tbCompressor3ModelNum";
            this.tbCompressor3ModelNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor3ModelNum.TabIndex = 9;
            // 
            // label150
            // 
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.Location = new System.Drawing.Point(25, 96);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(78, 13);
            this.label150.TabIndex = 30;
            this.label150.Text = "Compressor #4";
            // 
            // tbCompressor2ModelNum
            // 
            this.tbCompressor2ModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor2ModelNum.Location = new System.Drawing.Point(107, 48);
            this.tbCompressor2ModelNum.Name = "tbCompressor2ModelNum";
            this.tbCompressor2ModelNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor2ModelNum.TabIndex = 5;
            // 
            // tbCompressor1ModelNum
            // 
            this.tbCompressor1ModelNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor1ModelNum.Location = new System.Drawing.Point(107, 26);
            this.tbCompressor1ModelNum.Name = "tbCompressor1ModelNum";
            this.tbCompressor1ModelNum.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor1ModelNum.TabIndex = 1;
            // 
            // tbAMACompressorRunningAmps1
            // 
            this.tbAMACompressorRunningAmps1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRunningAmps1.Location = new System.Drawing.Point(423, 26);
            this.tbAMACompressorRunningAmps1.Name = "tbAMACompressorRunningAmps1";
            this.tbAMACompressorRunningAmps1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRunningAmps1.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRunningAmps1.TabIndex = 3;
            // 
            // tbAMACompressorRunningAmps2
            // 
            this.tbAMACompressorRunningAmps2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAMACompressorRunningAmps2.Location = new System.Drawing.Point(423, 48);
            this.tbAMACompressorRunningAmps2.Name = "tbAMACompressorRunningAmps2";
            this.tbAMACompressorRunningAmps2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tbAMACompressorRunningAmps2.Size = new System.Drawing.Size(70, 20);
            this.tbAMACompressorRunningAmps2.TabIndex = 7;
            // 
            // label151
            // 
            this.label151.AutoSize = true;
            this.label151.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label151.Location = new System.Drawing.Point(501, 10);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(65, 13);
            this.label151.TabIndex = 1;
            this.label151.Text = "Rated Amps";
            // 
            // gbCondensers
            // 
            this.gbCondensers.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbCondensers.Controls.Add(this.tbCondenserFanRatedAmps6);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRunningAmps6);
            this.gbCondensers.Controls.Add(this.label51);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRatedAmps5);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRunningAmps5);
            this.gbCondensers.Controls.Add(this.label62);
            this.gbCondensers.Controls.Add(this.label66);
            this.gbCondensers.Controls.Add(this.label67);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRatedAmps4);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRatedAmps3);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRatedAmps2);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRatedAmps1);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRunningAmps4);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRunningAmps3);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRunningAmps2);
            this.gbCondensers.Controls.Add(this.tbCondenserFanRunningAmps1);
            this.gbCondensers.Controls.Add(this.label68);
            this.gbCondensers.Controls.Add(this.label78);
            this.gbCondensers.Controls.Add(this.label79);
            this.gbCondensers.Controls.Add(this.label82);
            this.gbCondensers.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCondensers.Location = new System.Drawing.Point(3, 3);
            this.gbCondensers.Name = "gbCondensers";
            this.gbCondensers.Size = new System.Drawing.Size(272, 167);
            this.gbCondensers.TabIndex = 3;
            this.gbCondensers.TabStop = false;
            this.gbCondensers.Text = "Condenser Motor Amps";
            // 
            // tbCondenserFanRatedAmps6
            // 
            this.tbCondenserFanRatedAmps6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRatedAmps6.Location = new System.Drawing.Point(200, 139);
            this.tbCondenserFanRatedAmps6.Name = "tbCondenserFanRatedAmps6";
            this.tbCondenserFanRatedAmps6.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRatedAmps6.TabIndex = 12;
            this.tbCondenserFanRatedAmps6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRunningAmps6
            // 
            this.tbCondenserFanRunningAmps6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRunningAmps6.Location = new System.Drawing.Point(125, 139);
            this.tbCondenserFanRunningAmps6.Name = "tbCondenserFanRunningAmps6";
            this.tbCondenserFanRunningAmps6.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRunningAmps6.TabIndex = 11;
            this.tbCondenserFanRunningAmps6.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(12, 143);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(95, 13);
            this.label51.TabIndex = 26;
            this.label51.Text = "Condenser Fan #6";
            // 
            // tbCondenserFanRatedAmps5
            // 
            this.tbCondenserFanRatedAmps5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRatedAmps5.Location = new System.Drawing.Point(200, 117);
            this.tbCondenserFanRatedAmps5.Name = "tbCondenserFanRatedAmps5";
            this.tbCondenserFanRatedAmps5.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRatedAmps5.TabIndex = 10;
            this.tbCondenserFanRatedAmps5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRunningAmps5
            // 
            this.tbCondenserFanRunningAmps5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRunningAmps5.Location = new System.Drawing.Point(125, 117);
            this.tbCondenserFanRunningAmps5.Name = "tbCondenserFanRunningAmps5";
            this.tbCondenserFanRunningAmps5.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRunningAmps5.TabIndex = 9;
            this.tbCondenserFanRunningAmps5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(12, 121);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(95, 13);
            this.label62.TabIndex = 23;
            this.label62.Text = "Condenser Fan #5";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(126, 13);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(76, 13);
            this.label66.TabIndex = 22;
            this.label66.Text = "Running Amps";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(205, 13);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(65, 13);
            this.label67.TabIndex = 21;
            this.label67.Text = "Rated Amps";
            // 
            // tbCondenserFanRatedAmps4
            // 
            this.tbCondenserFanRatedAmps4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRatedAmps4.Location = new System.Drawing.Point(200, 95);
            this.tbCondenserFanRatedAmps4.Name = "tbCondenserFanRatedAmps4";
            this.tbCondenserFanRatedAmps4.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRatedAmps4.TabIndex = 8;
            this.tbCondenserFanRatedAmps4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRatedAmps3
            // 
            this.tbCondenserFanRatedAmps3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRatedAmps3.Location = new System.Drawing.Point(200, 73);
            this.tbCondenserFanRatedAmps3.Name = "tbCondenserFanRatedAmps3";
            this.tbCondenserFanRatedAmps3.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRatedAmps3.TabIndex = 6;
            this.tbCondenserFanRatedAmps3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRatedAmps2
            // 
            this.tbCondenserFanRatedAmps2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRatedAmps2.Location = new System.Drawing.Point(200, 51);
            this.tbCondenserFanRatedAmps2.Name = "tbCondenserFanRatedAmps2";
            this.tbCondenserFanRatedAmps2.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRatedAmps2.TabIndex = 4;
            this.tbCondenserFanRatedAmps2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRatedAmps1
            // 
            this.tbCondenserFanRatedAmps1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRatedAmps1.Location = new System.Drawing.Point(200, 29);
            this.tbCondenserFanRatedAmps1.Name = "tbCondenserFanRatedAmps1";
            this.tbCondenserFanRatedAmps1.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRatedAmps1.TabIndex = 2;
            this.tbCondenserFanRatedAmps1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRunningAmps4
            // 
            this.tbCondenserFanRunningAmps4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRunningAmps4.Location = new System.Drawing.Point(125, 95);
            this.tbCondenserFanRunningAmps4.Name = "tbCondenserFanRunningAmps4";
            this.tbCondenserFanRunningAmps4.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRunningAmps4.TabIndex = 7;
            this.tbCondenserFanRunningAmps4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRunningAmps3
            // 
            this.tbCondenserFanRunningAmps3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRunningAmps3.Location = new System.Drawing.Point(125, 73);
            this.tbCondenserFanRunningAmps3.Name = "tbCondenserFanRunningAmps3";
            this.tbCondenserFanRunningAmps3.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRunningAmps3.TabIndex = 5;
            this.tbCondenserFanRunningAmps3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRunningAmps2
            // 
            this.tbCondenserFanRunningAmps2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRunningAmps2.Location = new System.Drawing.Point(125, 51);
            this.tbCondenserFanRunningAmps2.Name = "tbCondenserFanRunningAmps2";
            this.tbCondenserFanRunningAmps2.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRunningAmps2.TabIndex = 3;
            this.tbCondenserFanRunningAmps2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // tbCondenserFanRunningAmps1
            // 
            this.tbCondenserFanRunningAmps1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCondenserFanRunningAmps1.Location = new System.Drawing.Point(125, 29);
            this.tbCondenserFanRunningAmps1.Name = "tbCondenserFanRunningAmps1";
            this.tbCondenserFanRunningAmps1.Size = new System.Drawing.Size(69, 20);
            this.tbCondenserFanRunningAmps1.TabIndex = 1;
            this.tbCondenserFanRunningAmps1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(12, 99);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(95, 13);
            this.label68.TabIndex = 9;
            this.label68.Text = "Condenser Fan #4";
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(12, 77);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(95, 13);
            this.label78.TabIndex = 8;
            this.label78.Text = "Condenser Fan #3";
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(12, 55);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(95, 13);
            this.label79.TabIndex = 7;
            this.label79.Text = "Condenser Fan #2";
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(12, 33);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(95, 13);
            this.label82.TabIndex = 6;
            this.label82.Text = "Condenser Fan #1";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel8, 3);
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.54546F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.45454F));
            this.tableLayoutPanel8.Controls.Add(this.gbERV, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.gbPreHeat, 1, 0);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 627);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel8.Size = new System.Drawing.Size(870, 98);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.gbAmbientConditions, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.gbBlowers, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.gbVoltage, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(257, 439);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel9, 2);
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.Controls.Add(this.gbMiscNotes, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.btnPrintProduction, 1, 0);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(579, 731);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(631, 89);
            this.tableLayoutPanel9.TabIndex = 6;
            // 
            // gbMiscNotes
            // 
            this.gbMiscNotes.Controls.Add(this.tbMiscNotes);
            this.gbMiscNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMiscNotes.Location = new System.Drawing.Point(3, 3);
            this.gbMiscNotes.Name = "gbMiscNotes";
            this.gbMiscNotes.Size = new System.Drawing.Size(544, 83);
            this.gbMiscNotes.TabIndex = 17;
            this.gbMiscNotes.TabStop = false;
            this.gbMiscNotes.Text = "Misc. Notes";
            // 
            // tbMiscNotes
            // 
            this.tbMiscNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbMiscNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMiscNotes.Location = new System.Drawing.Point(3, 16);
            this.tbMiscNotes.Multiline = true;
            this.tbMiscNotes.Name = "tbMiscNotes";
            this.tbMiscNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbMiscNotes.Size = new System.Drawing.Size(538, 64);
            this.tbMiscNotes.TabIndex = 0;
            // 
            // btnPrintProduction
            // 
            this.btnPrintProduction.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrintProduction.ForeColor = System.Drawing.Color.Black;
            this.btnPrintProduction.Location = new System.Drawing.Point(553, 25);
            this.btnPrintProduction.Margin = new System.Windows.Forms.Padding(3, 25, 3, 3);
            this.btnPrintProduction.Name = "btnPrintProduction";
            this.btnPrintProduction.Size = new System.Drawing.Size(75, 44);
            this.btnPrintProduction.TabIndex = 18;
            this.btnPrintProduction.Text = "Print";
            this.btnPrintProduction.UseVisualStyleBackColor = true;
            this.btnPrintProduction.Visible = false;
            this.btnPrintProduction.Click += new System.EventHandler(this.btnPrintProduction_Click);
            // 
            // pnlJobContainer
            // 
            this.pnlJobContainer.Controls.Add(this.tlpJobContainer);
            this.pnlJobContainer.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlJobContainer.Location = new System.Drawing.Point(0, 0);
            this.pnlJobContainer.Name = "pnlJobContainer";
            this.pnlJobContainer.Size = new System.Drawing.Size(1239, 56);
            this.pnlJobContainer.TabIndex = 0;
            // 
            // tlpJobContainer
            // 
            this.tlpJobContainer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tlpJobContainer.ColumnCount = 13;
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3F));
            this.tlpJobContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpJobContainer.Controls.Add(this.label1, 0, 0);
            this.tlpJobContainer.Controls.Add(this.label2, 0, 1);
            this.tlpJobContainer.Controls.Add(this.tbJobName, 1, 1);
            this.tlpJobContainer.Controls.Add(this.tbJobNum, 1, 0);
            this.tlpJobContainer.Controls.Add(this.butIssues, 12, 0);
            this.tlpJobContainer.Controls.Add(this.label3, 2, 0);
            this.tlpJobContainer.Controls.Add(this.tbAuditedBy, 10, 0);
            this.tlpJobContainer.Controls.Add(this.dtpDate, 10, 1);
            this.tlpJobContainer.Controls.Add(this.label123, 9, 0);
            this.tlpJobContainer.Controls.Add(this.label4, 9, 1);
            this.tlpJobContainer.Controls.Add(this.label19, 7, 1);
            this.tlpJobContainer.Controls.Add(this.tbModelNum, 6, 0);
            this.tlpJobContainer.Controls.Add(this.label5, 5, 0);
            this.tlpJobContainer.Controls.Add(this.cbLinenum, 6, 1);
            this.tlpJobContainer.Controls.Add(this.lbHeadID, 11, 1);
            this.tlpJobContainer.Controls.Add(this.lblLine, 5, 1);
            this.tlpJobContainer.Controls.Add(this.cbHorizonType, 4, 1);
            this.tlpJobContainer.Controls.Add(this.lblUnitType, 3, 1);
            this.tlpJobContainer.Controls.Add(this.tbTestedBy, 8, 1);
            this.tlpJobContainer.Controls.Add(this.tbSerialnum, 3, 0);
            this.tlpJobContainer.Location = new System.Drawing.Point(0, 0);
            this.tlpJobContainer.Name = "tlpJobContainer";
            this.tlpJobContainer.RowCount = 2;
            this.tlpJobContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 53.06123F));
            this.tlpJobContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 46.93877F));
            this.tlpJobContainer.Size = new System.Drawing.Size(1239, 56);
            this.tlpJobContainer.TabIndex = 0;
            // 
            // butIssues
            // 
            this.butIssues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.butIssues.Enabled = false;
            this.butIssues.Location = new System.Drawing.Point(1176, 3);
            this.butIssues.Name = "butIssues";
            this.tlpJobContainer.SetRowSpan(this.butIssues, 2);
            this.butIssues.Size = new System.Drawing.Size(60, 50);
            this.butIssues.TabIndex = 9;
            this.butIssues.Text = "Issues";
            this.butIssues.UseVisualStyleBackColor = true;
            this.butIssues.Click += new System.EventHandler(this.butIssues_Click);
            // 
            // cbLinenum
            // 
            this.cbLinenum.FormattingEnabled = true;
            this.cbLinenum.Location = new System.Drawing.Point(534, 32);
            this.cbLinenum.Name = "cbLinenum";
            this.cbLinenum.Size = new System.Drawing.Size(88, 21);
            this.cbLinenum.TabIndex = 6;
            this.cbLinenum.SelectedIndexChanged += new System.EventHandler(this.cbLinenum_SelectedIndexChanged);
            // 
            // lblLine
            // 
            this.lblLine.AutoSize = true;
            this.lblLine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLine.Location = new System.Drawing.Point(452, 29);
            this.lblLine.Name = "lblLine";
            this.lblLine.Size = new System.Drawing.Size(76, 27);
            this.lblLine.TabIndex = 147;
            this.lblLine.Text = "Line";
            this.lblLine.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbHorizonType
            // 
            this.cbHorizonType.FormattingEnabled = true;
            this.cbHorizonType.Location = new System.Drawing.Point(358, 32);
            this.cbHorizonType.Name = "cbHorizonType";
            this.cbHorizonType.Size = new System.Drawing.Size(88, 21);
            this.cbHorizonType.TabIndex = 5;
            this.cbHorizonType.SelectedIndexChanged += new System.EventHandler(this.cbHorizonType_SelectedIndexChanged);
            // 
            // lblUnitType
            // 
            this.lblUnitType.AutoSize = true;
            this.lblUnitType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnitType.Location = new System.Drawing.Point(299, 29);
            this.lblUnitType.Name = "lblUnitType";
            this.lblUnitType.Size = new System.Drawing.Size(53, 27);
            this.lblUnitType.TabIndex = 146;
            this.lblUnitType.Text = "Unit Type";
            this.lblUnitType.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlContainerBottom
            // 
            this.pnlContainerBottom.Controls.Add(this.tlpButtonContainer);
            this.pnlContainerBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlContainerBottom.Location = new System.Drawing.Point(0, 574);
            this.pnlContainerBottom.Name = "pnlContainerBottom";
            this.pnlContainerBottom.Size = new System.Drawing.Size(1239, 35);
            this.pnlContainerBottom.TabIndex = 2;
            // 
            // tlpButtonContainer
            // 
            this.tlpButtonContainer.ColumnCount = 5;
            this.tlpButtonContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtonContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtonContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtonContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtonContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpButtonContainer.Controls.Add(this.btnExportAll, 2, 0);
            this.tlpButtonContainer.Controls.Add(this.ButExit, 4, 0);
            this.tlpButtonContainer.Controls.Add(this.butSave, 1, 0);
            this.tlpButtonContainer.Controls.Add(this.butPrint, 3, 0);
            this.tlpButtonContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtonContainer.Location = new System.Drawing.Point(0, 0);
            this.tlpButtonContainer.Name = "tlpButtonContainer";
            this.tlpButtonContainer.RowCount = 1;
            this.tlpButtonContainer.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtonContainer.Size = new System.Drawing.Size(1239, 35);
            this.tlpButtonContainer.TabIndex = 0;
            // 
            // btnExportAll
            // 
            this.btnExportAll.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExportAll.ForeColor = System.Drawing.Color.Blue;
            this.btnExportAll.Location = new System.Drawing.Point(1001, 3);
            this.btnExportAll.Name = "btnExportAll";
            this.btnExportAll.Size = new System.Drawing.Size(70, 23);
            this.btnExportAll.TabIndex = 2;
            this.btnExportAll.Text = "Print All";
            this.btnExportAll.UseVisualStyleBackColor = true;
            this.btnExportAll.Visible = false;
            this.btnExportAll.Click += new System.EventHandler(this.btnExportAll_Click_1);
            // 
            // errorProviderApp
            // 
            this.errorProviderApp.ContainerControl = this;
            // 
            // textBox6
            // 
            this.textBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(107, 26);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(152, 20);
            this.textBox6.TabIndex = 1;
            // 
            // tbCompressor1
            // 
            this.tbCompressor1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCompressor1.Location = new System.Drawing.Point(107, 26);
            this.tbCompressor1.Name = "tbCompressor1";
            this.tbCompressor1.Size = new System.Drawing.Size(152, 20);
            this.tbCompressor1.TabIndex = 1;
            // 
            // textBox3
            // 
            this.textBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(265, 26);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(152, 20);
            this.textBox3.TabIndex = 2;
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(423, 26);
            this.textBox7.Name = "textBox7";
            this.textBox7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox7.Size = new System.Drawing.Size(70, 20);
            this.textBox7.TabIndex = 3;
            // 
            // textBox4
            // 
            this.textBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox4.Location = new System.Drawing.Point(499, 26);
            this.textBox4.Name = "textBox4";
            this.textBox4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox4.Size = new System.Drawing.Size(70, 20);
            this.textBox4.TabIndex = 4;
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(107, 48);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(152, 20);
            this.textBox5.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(265, 48);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 20);
            this.textBox1.TabIndex = 6;
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(423, 48);
            this.textBox8.Name = "textBox8";
            this.textBox8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox8.Size = new System.Drawing.Size(70, 20);
            this.textBox8.TabIndex = 7;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(499, 48);
            this.textBox2.Name = "textBox2";
            this.textBox2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.textBox2.Size = new System.Drawing.Size(70, 20);
            this.textBox2.TabIndex = 8;
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(227, 56);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(15, 14);
            this.checkBox13.TabIndex = 58;
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(171, 56);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(15, 14);
            this.checkBox14.TabIndex = 57;
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Location = new System.Drawing.Point(227, 36);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(15, 14);
            this.checkBox11.TabIndex = 55;
            this.checkBox11.UseVisualStyleBackColor = true;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(171, 36);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(15, 14);
            this.checkBox12.TabIndex = 54;
            this.checkBox12.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(267, 109);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(15, 14);
            this.checkBox7.TabIndex = 60;
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(185, 109);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(15, 14);
            this.checkBox8.TabIndex = 59;
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(267, 93);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(15, 14);
            this.checkBox5.TabIndex = 57;
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(185, 93);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(15, 14);
            this.checkBox6.TabIndex = 56;
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(267, 124);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(15, 14);
            this.checkBox9.TabIndex = 63;
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(185, 124);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(15, 14);
            this.checkBox10.TabIndex = 62;
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // frmOAUTestingChecklist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.Disable;
            this.BackColor = System.Drawing.Color.DarkGray;
            this.ClientSize = new System.Drawing.Size(1239, 609);
            this.Controls.Add(this.tcTestingChecklist);
            this.Controls.Add(this.pnlContainerBottom);
            this.Controls.Add(this.pnlJobContainer);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.MinimizeBox = false;
            this.Name = "frmOAUTestingChecklist";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmOAUTestingChecklist_Load);
            this.Resize += new System.EventHandler(this.frmOAUTestingChecklist_Resize);
            this.gbCircuit1.ResumeLayout(false);
            this.gbCircuit1.PerformLayout();
            this.gbHeat.ResumeLayout(false);
            this.gbHeat.PerformLayout();
            this.gbCircuit2.ResumeLayout(false);
            this.gbCircuit2.PerformLayout();
            this.gbPreHeat.ResumeLayout(false);
            this.gbPreHeat.PerformLayout();
            this.gbERV.ResumeLayout(false);
            this.gbERV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFilters)).EndInit();
            this.gbFilters.ResumeLayout(false);
            this.gbFilters.PerformLayout();
            this.gbStandardEquipment.ResumeLayout(false);
            this.gbStandardEquipment.PerformLayout();
            this.gbDisconnect.ResumeLayout(false);
            this.gbDisconnect.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCircuit1Charge)).EndInit();
            this.gbCharge.ResumeLayout(false);
            this.gbCharge.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudCircuit2Charge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudTemperature)).EndInit();
            this.gbBlowers.ResumeLayout(false);
            this.gbBlowers.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHumidity)).EndInit();
            this.gbAmbientConditions.ResumeLayout(false);
            this.gbAmbientConditions.PerformLayout();
            this.gbVoltage.ResumeLayout(false);
            this.gbVoltage.PerformLayout();
            this.gbOptions.ResumeLayout(false);
            this.gbOptions.PerformLayout();
            this.gbStdEquipViking.ResumeLayout(false);
            this.gbStdEquipViking.PerformLayout();
            this.gbOptionsViking.ResumeLayout(false);
            this.gbOptionsViking.PerformLayout();
            this.tcTestingChecklist.ResumeLayout(false);
            this.tpPreChecks.ResumeLayout(false);
            this.pnlPreChecksContainer.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.pnlProductCheckListContainer.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tlpOptions.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.gbMeasurePressure.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.gbCompressorData.ResumeLayout(false);
            this.gbCompressorData.PerformLayout();
            this.gbCondensers.ResumeLayout(false);
            this.gbCondensers.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.gbMiscNotes.ResumeLayout(false);
            this.gbMiscNotes.PerformLayout();
            this.pnlJobContainer.ResumeLayout(false);
            this.tlpJobContainer.ResumeLayout(false);
            this.tlpJobContainer.PerformLayout();
            this.pnlContainerBottom.ResumeLayout(false);
            this.tlpButtonContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderApp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.GroupBox gbCircuit1;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox gbHeat;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label125;
        public System.Windows.Forms.GroupBox gbCircuit2;
        private System.Windows.Forms.Label label124;
        public System.Windows.Forms.GroupBox gbPreHeat;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Button ButExit;
        private System.Windows.Forms.Button butPrint;
        public System.Windows.Forms.TextBox tbJobName;
        public System.Windows.Forms.TextBox tbSerialnum;
        public System.Windows.Forms.GroupBox gbERV;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbFilters;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        public System.Windows.Forms.GroupBox gbDisconnect;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.GroupBox gbCharge;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.GroupBox gbBlowers;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.GroupBox gbAmbientConditions;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gbVoltage;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        public System.Windows.Forms.TextBox tbAuditedBy;
        public System.Windows.Forms.TextBox tbModelNum;
        public System.Windows.Forms.TextBox tbTestedBy;
        public System.Windows.Forms.DateTimePicker dtpDate;
        public System.Windows.Forms.TextBox tbJobNum;
        public System.Windows.Forms.NumericUpDown nudTemperature;
        public System.Windows.Forms.NumericUpDown nudHumidity;
        public System.Windows.Forms.NumericUpDown nudCircuit1Charge;
        public System.Windows.Forms.TextBox tbCircuit1ChargeVariance;
        public System.Windows.Forms.TextBox tbDisconnectSize;
        public System.Windows.Forms.ComboBox cbDisconnectType;
        public System.Windows.Forms.TextBox tbERV_SerialNo;
        public System.Windows.Forms.TextBox tbERV_ModelNo;
        public System.Windows.Forms.CheckBox chkbERVProperRotation;
        public System.Windows.Forms.TextBox tbERVRunningAmps;
        public System.Windows.Forms.TextBox tbERVRatedAmps;
        public System.Windows.Forms.TextBox tbModelNumExhaust;
        public System.Windows.Forms.TextBox tbModelNumIndoor;
        public System.Windows.Forms.TextBox tbVoltageOutputExhaust;
        public System.Windows.Forms.TextBox tbVoltageOutputIndoor;
        public System.Windows.Forms.TextBox tbMinHzExhaust;
        public System.Windows.Forms.TextBox tbMinHzIndoor;
        public System.Windows.Forms.TextBox tbMaxHzExhaust;
        public System.Windows.Forms.TextBox tbMaxHzIndoor;
        public System.Windows.Forms.TextBox tbWheelSizeExhaust;
        public System.Windows.Forms.TextBox tbRunningAmpsExhaust;
        public System.Windows.Forms.TextBox tbWheelSizeIndoor;
        public System.Windows.Forms.TextBox tbRunningAmpsIndoor;
        public System.Windows.Forms.TextBox tbRatedAmpsExhaust;
        public System.Windows.Forms.TextBox tbRatedAmpsIndoor;
        public System.Windows.Forms.TextBox tbHpExhaust;
        public System.Windows.Forms.TextBox tbHpIndoor;
        public System.Windows.Forms.CheckBox chkbDoubleBlowerIndoor;
        public System.Windows.Forms.CheckBox chkbDoubleBlowerExhaust;
        public System.Windows.Forms.DataGridView dgvFilters;
        public System.Windows.Forms.CheckBox cbCCHeaterTested;
        public System.Windows.Forms.CheckBox cbCCHeaterInstalled;
        public System.Windows.Forms.CheckBox cbOADampersTested;
        public System.Windows.Forms.CheckBox cbOADampersInstalled;
        public System.Windows.Forms.CheckBox cbFilterStatusTested;
        public System.Windows.Forms.CheckBox cbFilterStatusInstalled;
        public System.Windows.Forms.CheckBox cbFanFailureTested;
        public System.Windows.Forms.CheckBox cbFanFailureInstalled;
        public System.Windows.Forms.CheckBox cbPreHeatTested;
        public System.Windows.Forms.CheckBox cbPreHeatInstalled;
        public System.Windows.Forms.CheckBox cbHotGasReheatTested;
        public System.Windows.Forms.CheckBox cbHotGasReheatInstalled;
        public System.Windows.Forms.CheckBox cbReversingValvesTested;
        public System.Windows.Forms.CheckBox cbReversingValvesInstalled;
        public System.Windows.Forms.CheckBox cbAuxiliaryLightsTested;
        public System.Windows.Forms.CheckBox cbAuxiliaryLightsInstalled;
        public System.Windows.Forms.CheckBox cbSupplyPiezoTested;
        public System.Windows.Forms.CheckBox cbSupplyPiezoInstalled;
        public System.Windows.Forms.CheckBox cbProtoNodeTested;
        public System.Windows.Forms.CheckBox cbProtoNodeInstalled;
        public System.Windows.Forms.CheckBox cbExhaustPiezoTested;
        public System.Windows.Forms.CheckBox cbExhaustPiezoInstalled;
        public System.Windows.Forms.CheckBox cbConvenienceOutletTested;
        public System.Windows.Forms.CheckBox cbConvenienceOutletInstalled;
        public System.Windows.Forms.CheckBox cbBypassDampersTested;
        public System.Windows.Forms.CheckBox cbBypassDampersInstalled;
        public System.Windows.Forms.CheckBox cbERVTested;
        public System.Windows.Forms.CheckBox cbERVInstalled;
        public System.Windows.Forms.CheckBox cbPoweredExhaustTested;
        public System.Windows.Forms.CheckBox cbPoweredExhaustInstalled;
        public System.Windows.Forms.CheckBox cbHeaterTested;
        public System.Windows.Forms.CheckBox cbHeaterInstalled;
        public System.Windows.Forms.TextBox tbCircuit2Subcooling;
        public System.Windows.Forms.TextBox tbCircuit2DisTemp;
        public System.Windows.Forms.TextBox tbCircuit2SDT;
        public System.Windows.Forms.TextBox tbCircuit2DisPSI;
        public System.Windows.Forms.TextBox tbCircuit2LiqTemp;
        public System.Windows.Forms.TextBox tbCircuit2SLT;
        public System.Windows.Forms.TextBox tbCircuit2LiqPSI;
        public System.Windows.Forms.TextBox tbCircuit2SuctTemp;
        public System.Windows.Forms.TextBox tbCircuit2SST;
        public System.Windows.Forms.TextBox tbCircuit2SuctPSI;
        public System.Windows.Forms.CheckBox chkbCircuit1APRActive;
        public System.Windows.Forms.TextBox tbCircuit1Subcooling;
        public System.Windows.Forms.TextBox tbCircuit1DisTemp;
        public System.Windows.Forms.TextBox tbCircuit1SDT;
        public System.Windows.Forms.TextBox tbCircuit1DisPSI;
        public System.Windows.Forms.TextBox tbCircuit1LiqTemp;
        public System.Windows.Forms.TextBox tbCircuit1SLT;
        public System.Windows.Forms.TextBox tbCircuit1LiqPSI;
        public System.Windows.Forms.TextBox tbCircuit1SuctTemp;
        public System.Windows.Forms.TextBox tbCircuit1SST;
        public System.Windows.Forms.TextBox tbCircuit1SuctPSI;
        public System.Windows.Forms.TextBox tbHeatSizeBottom;
        public System.Windows.Forms.TextBox tbHeatPartNumBottom;
        public System.Windows.Forms.TextBox tbHeatSerialNumBottom;
        public System.Windows.Forms.TextBox tbHeatModelNumBottom;
        public System.Windows.Forms.TextBox tbHeatRunningAmps;
        public System.Windows.Forms.TextBox tbHeatRatedAmps;
        public System.Windows.Forms.TextBox tbHeatSizeTop;
        public System.Windows.Forms.TextBox tbHeatPONum;
        public System.Windows.Forms.TextBox tbHeatItemNum;
        public System.Windows.Forms.TextBox tbHeatPartNumTop;
        public System.Windows.Forms.TextBox tbHeatSerialNumTop;
        public System.Windows.Forms.TextBox tbHeatModelNumTop;
        public System.Windows.Forms.ComboBox cbHeatPosition;
        public System.Windows.Forms.ComboBox cbHeatPrimary;
        public System.Windows.Forms.TextBox tbPreHeatSize;
        public System.Windows.Forms.TextBox tbPreHeatSerialNo;
        public System.Windows.Forms.TextBox tbPreHeatModelNo;
        public System.Windows.Forms.TextBox tbPreHeatRunningAmps;
        public System.Windows.Forms.TextBox tbPreHeatRatedAmps;
        public System.Windows.Forms.Label lbHeadID;
        public System.Windows.Forms.TextBox tbVoltageL3G;
        public System.Windows.Forms.TextBox tbVoltageL2G;
        public System.Windows.Forms.TextBox tbVoltageL1G;
        public System.Windows.Forms.TextBox tbVoltageL1L3;
        public System.Windows.Forms.TextBox tbVoltageL2L3;
        public System.Windows.Forms.TextBox tbVoltageL1L2;
        public System.Windows.Forms.RadioButton rbVoltage575;
        public System.Windows.Forms.RadioButton rbVoltage460;
        public System.Windows.Forms.RadioButton rbVoltage208;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.Label label141;
        private System.Windows.Forms.Label label142;
        private System.Windows.Forms.Label label143;
        private System.Windows.Forms.Label label144;
        private System.Windows.Forms.Label label145;
        private System.Windows.Forms.Label label146;
        private System.Windows.Forms.Label label147;
        private System.Windows.Forms.Label label148;
        private System.Windows.Forms.Label label149;
        public System.Windows.Forms.CheckBox cbOADamperTestedVkg;
        public System.Windows.Forms.CheckBox cbOADamperInstalledVkg;
        public System.Windows.Forms.CheckBox cbFSSTestedVkg;
        public System.Windows.Forms.CheckBox cbFSSInstalledVkg;
        public System.Windows.Forms.CheckBox cbRADampTestedVkg;
        public System.Windows.Forms.CheckBox cbRADampInstalledVkg;
        public System.Windows.Forms.CheckBox cbOADampTestedVkg;
        public System.Windows.Forms.CheckBox cbOADampInstalledVkg;
        public System.Windows.Forms.CheckBox cbFanFailSwitchTestedVkg;
        public System.Windows.Forms.CheckBox cbFanFailSwitchInstalledVkg;
        public System.Windows.Forms.CheckBox cbPreHeatTestedVkg;
        public System.Windows.Forms.CheckBox cbPreHeatInstalledVkg;
        public System.Windows.Forms.CheckBox cbHGRHTestedVkg;
        public System.Windows.Forms.CheckBox cbHGRHInstalledVkg;
        public System.Windows.Forms.CheckBox cbAuxLightsTestedVkg;
        public System.Windows.Forms.CheckBox cbAuxLightsInstalledVkg;
        public System.Windows.Forms.CheckBox cbFiltStatSwitchTestedVkg;
        public System.Windows.Forms.CheckBox cbFiltStatSwitchInstalledVkg;
        public System.Windows.Forms.CheckBox cbExhPiezoTestedVkg;
        public System.Windows.Forms.CheckBox cbExhPiezoInstalledVkg;
        public System.Windows.Forms.CheckBox cbSupplyPiezoTestedVkg;
        public System.Windows.Forms.CheckBox cbSupplyPiezoInstalledVkg;
        public System.Windows.Forms.CheckBox cbConvOutletTestedVkg;
        public System.Windows.Forms.CheckBox cbConvOutletInstalledVkg;
        public System.Windows.Forms.CheckBox cbBypDampTestedVkg;
        public System.Windows.Forms.CheckBox cbBypDampInstalledVkg;
        public System.Windows.Forms.CheckBox cbERVTestedVkg;
        public System.Windows.Forms.CheckBox cbERVInstalledVkg;
        public System.Windows.Forms.CheckBox cbPwrExhTestedVkg;
        public System.Windows.Forms.CheckBox cbPwrExhInstalledVkg;
        public System.Windows.Forms.CheckBox cbHeaterTestedVkg;
        public System.Windows.Forms.CheckBox cbHeaterInstalledVkg;
        public System.Windows.Forms.GroupBox gbStdEquipViking;
        public System.Windows.Forms.GroupBox gbOptionsViking;
        public System.Windows.Forms.GroupBox gbStandardEquipment;
        public System.Windows.Forms.GroupBox gbOptions;
        public System.Windows.Forms.Button butSave;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilterSize;
        private System.Windows.Forms.DataGridViewTextBoxColumn FilterStyle;
        private System.Windows.Forms.DataGridViewTextBoxColumn Qty;
        private System.Windows.Forms.TabControl tcTestingChecklist;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Panel pnlJobContainer;
        private System.Windows.Forms.Panel pnlContainerBottom;
        private System.Windows.Forms.ErrorProvider errorProviderApp;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label105;
        public System.Windows.Forms.ComboBox cbHeatSectionFuelType;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TableLayoutPanel tlpJobContainer;
        private System.Windows.Forms.Label lblUnitType;
        private System.Windows.Forms.Label lblLine;
        public System.Windows.Forms.Button butIssues;
        private System.Windows.Forms.TabPage tpPreChecks;
        private System.Windows.Forms.Panel pnlPreChecksContainer;
        public System.Windows.Forms.ComboBox cbHorizonType;
        public System.Windows.Forms.ComboBox cbLinenum;
        private System.Windows.Forms.CheckBox cbIsolationDamperTested;
        private System.Windows.Forms.CheckBox cbIsolationDamperoption;
        private System.Windows.Forms.Label label23;
        public System.Windows.Forms.NumericUpDown nudCircuit2Charge;
        public System.Windows.Forms.TextBox tbCircuit2ChargeVariance;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label76;
        public System.Windows.Forms.CheckBox cbLEDServiceTested;
        public System.Windows.Forms.CheckBox cbLEDServiceInstalled;
        private System.Windows.Forms.Label label139;
        public System.Windows.Forms.CheckBox cbHailguardTested;
        public System.Windows.Forms.CheckBox cbHailguradInstalled;
        private System.Windows.Forms.Label label137;
        public System.Windows.Forms.CheckBox cbLEDServiceVkgTested;
        public System.Windows.Forms.CheckBox cbLEDServiceVKgInstalled;
        private System.Windows.Forms.Label label77;
        public System.Windows.Forms.CheckBox cbHailguardVkgTested;
        public System.Windows.Forms.CheckBox cbHailguardVkgInstalled;
        private System.Windows.Forms.Label label80;
        public System.Windows.Forms.CheckBox cbRADampersTested;
        public System.Windows.Forms.CheckBox cbRADampersInstalled;
        private System.Windows.Forms.Label label84;
        public System.Windows.Forms.CheckBox cbAPRTested;
        public System.Windows.Forms.CheckBox cbAPRInstalled;
        private System.Windows.Forms.Label label83;
        public System.Windows.Forms.CheckBox cbDigitalScrollTested;
        public System.Windows.Forms.CheckBox cbDigitalScrollInstalled;
        private System.Windows.Forms.Label label81;
        public System.Windows.Forms.GroupBox gbCondensers;
        public System.Windows.Forms.TextBox tbCondenserFanRatedAmps6;
        public System.Windows.Forms.TextBox tbCondenserFanRunningAmps6;
        private System.Windows.Forms.Label label51;
        public System.Windows.Forms.TextBox tbCondenserFanRatedAmps5;
        public System.Windows.Forms.TextBox tbCondenserFanRunningAmps5;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        public System.Windows.Forms.TextBox tbCondenserFanRatedAmps4;
        public System.Windows.Forms.TextBox tbCondenserFanRatedAmps3;
        public System.Windows.Forms.TextBox tbCondenserFanRatedAmps2;
        public System.Windows.Forms.TextBox tbCondenserFanRatedAmps1;
        public System.Windows.Forms.TextBox tbCondenserFanRunningAmps4;
        public System.Windows.Forms.TextBox tbCondenserFanRunningAmps3;
        public System.Windows.Forms.TextBox tbCondenserFanRunningAmps2;
        public System.Windows.Forms.TextBox tbCondenserFanRunningAmps1;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label label82;
        public System.Windows.Forms.GroupBox gbCompressorData;
        public System.Windows.Forms.TextBox tbAMACompressorRatedAmps6;
        public System.Windows.Forms.TextBox tbCompressor6SerialNum;
        public System.Windows.Forms.TextBox tbAMACompressorRunningAmps6;
        public System.Windows.Forms.TextBox tbCompressor6ModelNum;
        private System.Windows.Forms.Label label120;
        public System.Windows.Forms.TextBox tbAMACompressorRatedAmps5;
        public System.Windows.Forms.TextBox tbCompressor5SerialNum;
        public System.Windows.Forms.TextBox tbAMACompressorRunningAmps5;
        public System.Windows.Forms.TextBox tbCompressor5ModelNum;
        private System.Windows.Forms.Label label119;
        public System.Windows.Forms.TextBox tbCompressor4SerialNum;
        private System.Windows.Forms.Label label89;
        public System.Windows.Forms.TextBox tbCompressor3SerialNum;
        private System.Windows.Forms.Label label90;
        public System.Windows.Forms.TextBox tbAMACompressorRatedAmps4;
        public System.Windows.Forms.TextBox tbCompressor2SerialNum;
        public System.Windows.Forms.TextBox tbAMACompressorRatedAmps3;
        private System.Windows.Forms.Label label91;
        public System.Windows.Forms.TextBox tbAMACompressorRatedAmps2;
        public System.Windows.Forms.TextBox tbCompressor1SerialNum;
        public System.Windows.Forms.TextBox tbAMACompressorRatedAmps1;
        public System.Windows.Forms.TextBox tbAMACompressorRunningAmps4;
        private System.Windows.Forms.Label label96;
        public System.Windows.Forms.TextBox tbAMACompressorRunningAmps3;
        private System.Windows.Forms.Label label104;
        public System.Windows.Forms.TextBox tbCompressor4ModelNum;
        private System.Windows.Forms.Label label130;
        public System.Windows.Forms.TextBox tbCompressor3ModelNum;
        private System.Windows.Forms.Label label150;
        public System.Windows.Forms.TextBox tbCompressor2ModelNum;
        public System.Windows.Forms.TextBox tbCompressor1ModelNum;
        public System.Windows.Forms.TextBox tbAMACompressorRunningAmps1;
        public System.Windows.Forms.TextBox tbAMACompressorRunningAmps2;
        private System.Windows.Forms.Label label151;
        public System.Windows.Forms.CheckBox cbReturnSmokeDetectorTested;
        public System.Windows.Forms.CheckBox cbReturnSmokeDetectorInstalled;
        private System.Windows.Forms.Label label86;
        public System.Windows.Forms.CheckBox cbSupplySmokeDectectorTested;
        public System.Windows.Forms.CheckBox cbSupplySmokeDectectorInstalled;
        private System.Windows.Forms.Label label85;
        public System.Windows.Forms.TextBox textBox6;
        public System.Windows.Forms.TextBox tbCompressor1;
        public System.Windows.Forms.TextBox textBox3;
        public System.Windows.Forms.TextBox textBox7;
        public System.Windows.Forms.TextBox textBox4;
        public System.Windows.Forms.TextBox textBox5;
        public System.Windows.Forms.TextBox textBox1;
        public System.Windows.Forms.TextBox textBox8;
        public System.Windows.Forms.TextBox textBox2;
        public System.Windows.Forms.CheckBox checkBox13;
        public System.Windows.Forms.CheckBox checkBox14;
        public System.Windows.Forms.CheckBox checkBox11;
        public System.Windows.Forms.CheckBox checkBox12;
        public System.Windows.Forms.CheckBox checkBox7;
        public System.Windows.Forms.CheckBox checkBox8;
        public System.Windows.Forms.CheckBox checkBox5;
        public System.Windows.Forms.CheckBox checkBox6;
        public System.Windows.Forms.CheckBox checkBox9;
        public System.Windows.Forms.CheckBox checkBox10;
        public System.Windows.Forms.GroupBox gbMeasurePressure;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label lblLowFire;
        private System.Windows.Forms.Label lblHighFire;
        private System.Windows.Forms.Label label155;
        private System.Windows.Forms.Label label156;
        private System.Windows.Forms.Label label157;
        private System.Windows.Forms.Label label158;
        private System.Windows.Forms.Label label159;
        private System.Windows.Forms.Label label160;
        public System.Windows.Forms.TextBox txtInletModulationPresure;
        public System.Windows.Forms.TextBox txtLowFire;
        public System.Windows.Forms.TextBox txtHighFire;
        public System.Windows.Forms.Button btnPrintProduction;
        private System.Windows.Forms.TableLayoutPanel tlpButtonContainer;
        private System.Windows.Forms.Button btnExportAll;
        private System.Windows.Forms.Label lblRangeNG;
        private System.Windows.Forms.Label label154;
        private System.Windows.Forms.Panel pnlProductCheckListContainer;
        public System.Windows.Forms.CheckBox cbGreenTrolTested;
        public System.Windows.Forms.CheckBox cbGreenTrolInstalled;
        private System.Windows.Forms.Label lblGreenTrol;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tlpOptions;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.TextBox txtSuperHeat;
        public System.Windows.Forms.Label lblSuperHeat;
        public System.Windows.Forms.TextBox tbCircuit2SuperHeat;
        private System.Windows.Forms.Label label153;
        public System.Windows.Forms.GroupBox gbMiscNotes;
        public System.Windows.Forms.TextBox tbMiscNotes;
        private UserControlObject.PreChecksControl preChecksControl1;
    }
}