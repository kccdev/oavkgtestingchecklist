﻿namespace OA_VKG_TestingChecklist
{
    partial class frmIssues
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tbIssueImpactTimeMinutes = new System.Windows.Forms.TextBox();
            this.lbID = new System.Windows.Forms.Label();
            this.lbHeadID = new System.Windows.Forms.Label();
            this.dgvIssues = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.cbIssueType = new System.Windows.Forms.ComboBox();
            this.lbJobNum = new System.Windows.Forms.Label();
            this.butClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.cbHorizonType = new System.Windows.Forms.ComboBox();
            this.cbLinenum = new System.Windows.Forms.ComboBox();
            this.butSubmitIssue = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.tbIssueDescription = new System.Windows.Forms.TextBox();
            this.tbIssueSource = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssues)).BeginInit();
            this.SuspendLayout();
            // 
            // tbIssueImpactTimeMinutes
            // 
            this.tbIssueImpactTimeMinutes.Location = new System.Drawing.Point(920, 250);
            this.tbIssueImpactTimeMinutes.Name = "tbIssueImpactTimeMinutes";
            this.tbIssueImpactTimeMinutes.Size = new System.Drawing.Size(68, 20);
            this.tbIssueImpactTimeMinutes.TabIndex = 5;
            // 
            // lbID
            // 
            this.lbID.AutoSize = true;
            this.lbID.Location = new System.Drawing.Point(14, 411);
            this.lbID.Name = "lbID";
            this.lbID.Size = new System.Drawing.Size(18, 13);
            this.lbID.TabIndex = 46;
            this.lbID.Text = "ID";
            this.lbID.Visible = false;
            // 
            // lbHeadID
            // 
            this.lbHeadID.AutoSize = true;
            this.lbHeadID.Location = new System.Drawing.Point(14, 395);
            this.lbHeadID.Name = "lbHeadID";
            this.lbHeadID.Size = new System.Drawing.Size(44, 13);
            this.lbHeadID.TabIndex = 44;
            this.lbHeadID.Text = "HeadID";
            this.lbHeadID.Visible = false;
            // 
            // dgvIssues
            // 
            this.dgvIssues.AllowUserToAddRows = false;
            this.dgvIssues.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.Gainsboro;
            this.dgvIssues.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvIssues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvIssues.Location = new System.Drawing.Point(11, 47);
            this.dgvIssues.MultiSelect = false;
            this.dgvIssues.Name = "dgvIssues";
            this.dgvIssues.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvIssues.Size = new System.Drawing.Size(1102, 169);
            this.dgvIssues.TabIndex = 1;
            this.dgvIssues.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvIssues_CellMouseDoubleClick);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(917, 233);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(169, 13);
            this.label7.TabIndex = 41;
            this.label7.Text = "Issue Impact Time (Total Minutes):";
            // 
            // cbIssueType
            // 
            this.cbIssueType.DropDownWidth = 225;
            this.cbIssueType.FormattingEnabled = true;
            this.cbIssueType.Items.AddRange(new object[] {
            "Assembly",
            "Electric-Board",
            "Electric-Line",
            "Engineering",
            "Purchased-Part",
            "Mechanical",
            "Refrigeration",
            "Sub-Assembly",
            "Sub-Assembly Aux Box",
            "Sub-Assembly Condenser Fan",
            "Sub-Assembly Transformers/Breakers"});
            this.cbIssueType.Location = new System.Drawing.Point(582, 250);
            this.cbIssueType.Name = "cbIssueType";
            this.cbIssueType.Size = new System.Drawing.Size(157, 21);
            this.cbIssueType.TabIndex = 4;
            // 
            // lbJobNum
            // 
            this.lbJobNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbJobNum.Location = new System.Drawing.Point(474, 15);
            this.lbJobNum.Name = "lbJobNum";
            this.lbJobNum.Size = new System.Drawing.Size(180, 20);
            this.lbJobNum.TabIndex = 42;
            this.lbJobNum.Text = "JobNum";
            this.lbJobNum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // butClose
            // 
            this.butClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butClose.ForeColor = System.Drawing.Color.Red;
            this.butClose.Location = new System.Drawing.Point(576, 395);
            this.butClose.Name = "butClose";
            this.butClose.Size = new System.Drawing.Size(75, 23);
            this.butClose.TabIndex = 9;
            this.butClose.Text = "Close";
            this.butClose.UseVisualStyleBackColor = true;
            this.butClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(585, 231);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(62, 13);
            this.label2.TabIndex = 31;
            this.label2.Text = "Issue Type:";
            // 
            // cbHorizonType
            // 
            this.cbHorizonType.FormattingEnabled = true;
            this.cbHorizonType.Location = new System.Drawing.Point(317, 250);
            this.cbHorizonType.Name = "cbHorizonType";
            this.cbHorizonType.Size = new System.Drawing.Size(121, 21);
            this.cbHorizonType.TabIndex = 3;
            // 
            // cbLinenum
            // 
            this.cbLinenum.FormattingEnabled = true;
            this.cbLinenum.Location = new System.Drawing.Point(11, 250);
            this.cbLinenum.Name = "cbLinenum";
            this.cbLinenum.Size = new System.Drawing.Size(121, 21);
            this.cbLinenum.TabIndex = 2;
            // 
            // butSubmitIssue
            // 
            this.butSubmitIssue.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSubmitIssue.ForeColor = System.Drawing.Color.Green;
            this.butSubmitIssue.Location = new System.Drawing.Point(476, 395);
            this.butSubmitIssue.Name = "butSubmitIssue";
            this.butSubmitIssue.Size = new System.Drawing.Size(75, 23);
            this.butSubmitIssue.TabIndex = 8;
            this.butSubmitIssue.Text = "Submit";
            this.butSubmitIssue.UseVisualStyleBackColor = true;
            this.butSubmitIssue.Click += new System.EventHandler(this.butSubmitIssue_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(579, 287);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(102, 13);
            this.label6.TabIndex = 40;
            this.label6.Text = "Description of issue:";
            // 
            // tbIssueDescription
            // 
            this.tbIssueDescription.Location = new System.Drawing.Point(576, 303);
            this.tbIssueDescription.Multiline = true;
            this.tbIssueDescription.Name = "tbIssueDescription";
            this.tbIssueDescription.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbIssueDescription.Size = new System.Drawing.Size(540, 75);
            this.tbIssueDescription.TabIndex = 7;
            // 
            // tbIssueSource
            // 
            this.tbIssueSource.Location = new System.Drawing.Point(11, 303);
            this.tbIssueSource.Multiline = true;
            this.tbIssueSource.Name = "tbIssueSource";
            this.tbIssueSource.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbIssueSource.Size = new System.Drawing.Size(540, 75);
            this.tbIssueSource.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(14, 287);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(129, 13);
            this.label5.TabIndex = 37;
            this.label5.Text = "Issue source identified as:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(320, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 34;
            this.label3.Text = "Unit Type:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 234);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 29;
            this.label1.Text = "Line #:";
            // 
            // frmIssues
            // 
            this.ClientSize = new System.Drawing.Size(1126, 439);
            this.Controls.Add(this.tbIssueImpactTimeMinutes);
            this.Controls.Add(this.lbID);
            this.Controls.Add(this.lbHeadID);
            this.Controls.Add(this.dgvIssues);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cbIssueType);
            this.Controls.Add(this.lbJobNum);
            this.Controls.Add(this.butClose);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbHorizonType);
            this.Controls.Add(this.cbLinenum);
            this.Controls.Add(this.butSubmitIssue);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.tbIssueDescription);
            this.Controls.Add(this.tbIssueSource);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Name = "frmIssues";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmIssues_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvIssues)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox tbIssueImpactTimeMinutes;
        public System.Windows.Forms.Label lbID;
        public System.Windows.Forms.Label lbHeadID;
        public System.Windows.Forms.DataGridView dgvIssues;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.ComboBox cbIssueType;
        public System.Windows.Forms.Label lbJobNum;
        private System.Windows.Forms.Button butClose;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.ComboBox cbHorizonType;
        public System.Windows.Forms.ComboBox cbLinenum;
        private System.Windows.Forms.Button butSubmitIssue;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox tbIssueDescription;
        public System.Windows.Forms.TextBox tbIssueSource;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
    }
}
