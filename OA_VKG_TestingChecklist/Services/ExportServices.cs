﻿using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Kernel.Colors;
using iText.Layout.Properties;
using iText.Kernel.Geom;
using iText.Layout.Borders;
using System;

namespace OA_VKG_TestingChecklist.Services
{
    class ExportServices
    {
        #region "Export Methods"
        public void ExportPreChecks(MainBO mainBO, string exportFilePath)
        {
            PdfWriter writer = new PdfWriter(exportFilePath);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf, PageSize.A4.Rotate());

            PreChecksControlsTable(mainBO, document);
            document.Close();
        }

        public void ExportPrePowerChecks(MainBO mainBO, string exportFilePath)
        {
            PdfWriter writer = new PdfWriter(exportFilePath);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf, PageSize.A4.Rotate());

            PrePowerChecksControlsTable(mainBO, document);
            document.Close();
        }

        public void ExportOAUTestingChecklist(MainBO mainBO, string exportFilePath)
        {
            PdfWriter writer = new PdfWriter(exportFilePath);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf, PageSize.A4.Rotate());

            OAUTestingChecklistControlsTable(mainBO, document);
            document.Close();
        }

        public void ExportTestingChecklist(MainBO mainBO, string exportFilePath)
        {
            PdfWriter writer = new PdfWriter(exportFilePath);
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf, PageSize.A4.Rotate());

            PreChecksControlsTable(mainBO, document);
            document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
            OAUTestingChecklistControlsTable(mainBO, document);
            document.Close();
        }
        #endregion

        #region "Public Methods"
        public void PreChecksControlsTable(MainBO mainBO, Document document)
        {
            {//document top table
                Table headerTable = new Table(6, false);

                Cell cellJobNum = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Job Number")).SetBold();
                Cell cellJobNumValue = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph(mainBO.JobNum));
                Cell cellJobName = new Cell(1, 1).SetWidth(90)
                   .Add(new Paragraph("Job Name")).SetBold();
                Cell cellJobNameValue = new Cell(1, 1).SetWidth(130)
                  .Add(new Paragraph(mainBO.Name));
                Cell cellModelNum = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Model Number")).SetBold();
                Cell cellModelNumValue = new Cell(1, 1).SetWidth(280)
                  .Add(new Paragraph(mainBO.Modelnum));

                Cell cellUnitType = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Unit Type")).SetBold();
                Cell cellUnitTypeValue = new Cell(1, 1).SetWidth(120).SetHeight(20)
                  .Add(new Paragraph(mainBO.HorizonType));
                Cell cellLine = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Line")).SetBold();
                Cell cellLineValue = new Cell(1, 1).SetWidth(130).SetHeight(20)
                  .Add(new Paragraph(mainBO.Line));
                Cell cellSerialNum = new Cell(1, 1).SetWidth(90)
                .Add(new Paragraph("Serial Number")).SetBold();
                Cell cellSerialNumValue = new Cell(1, 1).SetWidth(280).SetHeight(20)
                  .Add(new Paragraph(mainBO.Serialnum));

                Cell cellTestedBy = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Tested By")).SetBold();
                Cell cellTestedByValue = new Cell(1, 1).SetWidth(120).SetHeight(20)
                  .Add(new Paragraph(mainBO.TestedBy));
                Cell cellAuditedBy = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Audited By")).SetBold();
                Cell cellAuditedByValue = new Cell(1, 1).SetWidth(130).SetHeight(20)
                  .Add(new Paragraph(mainBO.AuditedBy));
                Cell cellDate = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Date")).SetBold();
                Cell cellDateValue = new Cell(1, 1).SetWidth(280).SetHeight(20)
                  .Add(new Paragraph(mainBO.DateTested.ToString()));

                headerTable.AddCell(cellJobNum);
                headerTable.AddCell(cellJobNumValue);
                headerTable.AddCell(cellJobName);
                headerTable.AddCell(cellJobNameValue);
                headerTable.AddCell(cellModelNum);
                headerTable.AddCell(cellModelNumValue);

                headerTable.AddCell(cellUnitType);
                headerTable.AddCell(cellUnitTypeValue);
                headerTable.AddCell(cellLine);
                headerTable.AddCell(cellLineValue);
                headerTable.AddCell(cellSerialNum);
                headerTable.AddCell(cellSerialNumValue);

                headerTable.AddCell(cellTestedBy);
                headerTable.AddCell(cellTestedByValue);
                headerTable.AddCell(cellAuditedBy);
                headerTable.AddCell(cellAuditedByValue);
                headerTable.AddCell(cellDate);
                headerTable.AddCell(cellDateValue);

                document.Add(headerTable);
            }
            Color headerColor = WebColors.GetRGBColor("#A6A6A6");

            //paragraphs
            Paragraph header = new Paragraph("Product Pre-Testing Checks").SetBold()
            .SetTextAlignment(TextAlignment.CENTER)
            .SetFontSize(20);

            document.Add(header);

            Paragraph SubHeader1 = new Paragraph("Please complete the Pre-Test Checklist for every unit that comes off the assembly line.")
            .SetTextAlignment(TextAlignment.CENTER);
            document.Add(SubHeader1);

            Paragraph SubHeader2 = new Paragraph("Enter a ‘P’ for Pass, ‘F’ for Fail, or ‘N’ for does not apply.")
            .SetTextAlignment(TextAlignment.CENTER);
            document.Add(SubHeader2);

            Paragraph SubHeader3 = new Paragraph("For any Failures reported, please document in the Misc Notes section what was incorrect.")
            .SetTextAlignment(TextAlignment.CENTER);
            document.Add(SubHeader3);

            {
                Table tb_2 = new Table(3, false).SetMarginTop(20);

                Table tb_1 = new Table(1, false);

                Table table1 = new Table(2, false);

                Cell cell11 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("PRE-CHECKS")).SetBold().SetBackgroundColor(headerColor);
                Cell cell12 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Verify submittal unit match"));
                Cell cell13 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Verify_sub_un_match));
                Cell cell14 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("unit built in compliance with CCS"));
                Cell cell15 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Unit_built_compl_CCS));

                table1.AddCell(cell11);
                table1.AddCell(cell12);
                table1.AddCell(cell13);
                table1.AddCell(cell14);
                table1.AddCell(cell15);

                Table table7 = new Table(2, false).SetMarginTop(10);

                Cell cell71 = new Cell(1, 2).SetWidth(400)
                     .Add(new Paragraph("VISUAL INSPECTION")).SetBold().SetBackgroundColor(headerColor);
                Cell cell72 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Inspect unit for items left in unit (debris)"));
                Cell cell73 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Inspect_unit_items));
                Cell cell74 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Power wires to all drives"));
                Cell cell75 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Power_wire_driver));
                Cell cell76 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Line & Load are terminated correctly"));
                Cell cell77 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Line_Load_term_Correctly));
                Cell cell78 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("All terminations made with proper connectors"));
                Cell cell79 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Vis_term_prop_connector));

                table7.AddCell(cell71);
                table7.AddCell(cell72);
                table7.AddCell(cell73);
                table7.AddCell(cell74);
                table7.AddCell(cell75);
                table7.AddCell(cell76);
                table7.AddCell(cell77);
                table7.AddCell(cell78);
                table7.AddCell(cell79);

                Table table2 = new Table(2, false).SetMarginTop(10);

                Cell cell21 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("VISUAL INSPECTION: COMPRESSORS & CRANKCASE")).SetBold().SetBackgroundColor(headerColor);
                Cell cell22 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Verify plug harness are correct"));
                Cell cell23 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Verify_plug_harn_correct));
                Cell cell24 = new Cell(1, 1).SetWidth(400)
                .Add(new Paragraph("Crankcase is correct voltage"));
                Cell cell25 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.PreChecklist.Crankcase_correct_volt));

                table2.AddCell(cell21);
                table2.AddCell(cell22);
                table2.AddCell(cell23);
                table2.AddCell(cell24);
                table2.AddCell(cell25);

                Cell cell1 = new Cell(1, 1).Add(table1).SetWidth(400).SetBorder(Border.NO_BORDER);
                Cell cell2 = new Cell(1, 1).Add(table7).SetWidth(400).SetBorder(Border.NO_BORDER);
                Cell cell3 = new Cell(1, 1).Add(table2).SetWidth(400).SetBorder(Border.NO_BORDER);

                tb_1.AddCell(cell1);
                tb_1.AddCell(cell2);
                tb_1.AddCell(cell3);
            
                Table table8 = new Table(2, false);

                Cell cell81 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("PRE-POWER CHECKS")).SetBold().SetBackgroundColor(headerColor);
                Cell cell82 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Power Whip is not overly tight and has some strain relief for test"));
                Cell cell83 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Power_whip_over_tight));
                Cell cell84 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Correct size Whip for the unit"));
                Cell cell85 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Correct_size_whip_unit));
                Cell cell86 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Connect power leads to power whip. For grounding purposes only. No power to unit"));
                Cell cell87 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Co_Power_Ground_purp));
                Cell cell88 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Check Continuity to ground at every high voltage termination"));
                Cell cell89 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Cont_High_volt_term));
                Cell cell810 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Check Continunity between phases on all fan motors"));
                Cell cell811 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Check_Phases_fan_motor));
                Cell cell812 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Remove all 3 high voltage connectors from the CoreSense sensor"));
                Cell cell813 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Remove_volt_Sen_Sensor));
                Cell cell814 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Hi-Pot the unit (DO NOT HI POT ROSENBERG  FANS)"));
                Cell cell815 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Hi_pot_unit_fans));

                table8.AddCell(cell81);
                table8.AddCell(cell82);
                table8.AddCell(cell83);
                table8.AddCell(cell84);
                table8.AddCell(cell85);
                table8.AddCell(cell86);
                table8.AddCell(cell87);
                table8.AddCell(cell88);
                table8.AddCell(cell89);
                table8.AddCell(cell810);
                table8.AddCell(cell811);
                table8.AddCell(cell812);
                table8.AddCell(cell813);
                table8.AddCell(cell814);
                table8.AddCell(cell815);
               
                Cell cell4 = new Cell(1, 1).Add(tb_1).SetWidth(400).SetBorder(Border.NO_BORDER);
                Cell cell5 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell cell6 = new Cell(1, 1).Add(table8).SetWidth(400).SetBorder(Border.NO_BORDER);

                tb_2.AddCell(cell4);
                tb_2.AddCell(cell5);
                tb_2.AddCell(cell6);

                document.Add(tb_2);
            }

            {
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));
                Table tb_5 = new Table(3, false).SetMarginTop(20);
                Table tb_3 = new Table(1, false);

                Table table4 = new Table(2, false);

                Cell cell41 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("ELECTRIC HEAT")).SetBold().SetBackgroundColor(headerColor);
                Cell cell42 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Check power terminations on both sides of the terminal block"));
                Cell cell43 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.OAUTPrePower.Check_Power_term_block));
                Cell cell44 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Inspect elements of heater for debris/damage"));
                Cell cell45 = new Cell(1, 1).SetPaddingRight(20)
                .Add(new Paragraph(mainBO.OAUTPrePower.Inspect_element_heater));

                table4.AddCell(cell41);
                table4.AddCell(cell42);
                table4.AddCell(cell43);
                table4.AddCell(cell44);
                table4.AddCell(cell45);

                Table table6 = new Table(2, false).SetMarginTop(10);

                Cell cell61 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("GAS LEAK TEST")).SetBold().SetBackgroundColor(headerColor);
                Cell cell62 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Ensure field gas port plug is present"));
                Cell cell63 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.En_field_gas_port));
                Cell cell64 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Do not continue if Iniitals or Plugs are Missing"));
                Cell cell65 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.In_Plug_are_Missing));
                Cell cell66 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Turn off gas once leak test is complete & passes inspection"));
                Cell cell67 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Turn_off_gas_inspection));

                table6.AddCell(cell61);
                table6.AddCell(cell62);
                table6.AddCell(cell63);
                table6.AddCell(cell64);
                table6.AddCell(cell65);
                table6.AddCell(cell66);
                table6.AddCell(cell67);

                Cell cell7 = new Cell(1, 1).Add(table4).SetWidth(400).SetBorder(Border.NO_BORDER);
                Cell cell8 = new Cell(1, 1).Add(table6).SetWidth(400).SetBorder(Border.NO_BORDER);

                tb_3.AddCell(cell7);
                tb_3.AddCell(cell8);

                Table tb_4 = new Table(1, false);

                Table table5 = new Table(2, false);

                Cell cell51 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("GAS HEAT")).SetBold().SetBackgroundColor(headerColor);
                Cell cell52 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Check high voltage from the transformer is landed correctly on heater control board"));
                Cell cell53 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Check_heater_cont_board));
                Cell cell54 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Correct voltage written on transformer"));
                Cell cell55 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Co_volt_writtn_transform));

                table5.AddCell(cell51);
                table5.AddCell(cell52);
                table5.AddCell(cell53);
                table5.AddCell(cell54);
                table5.AddCell(cell55);

                Table table3 = new Table(2, false).SetMarginTop(10);

                Cell cell31 = new Cell(1, 2).SetWidth(400)
                 .Add(new Paragraph("PRE-POWER FINAL CHECKS")).SetBold().SetBackgroundColor(headerColor);
                Cell cell32 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Set phase monitor"));
                Cell cell33 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Set_Phase_Monitor));
                Cell cell34 = new Cell(1, 1).SetWidth(400)
                 .Add(new Paragraph("Unlock - correct voltage disconnect"));
                Cell cell35 = new Cell(1, 1).SetPaddingRight(20)
                 .Add(new Paragraph(mainBO.OAUTPrePower.Un_Correct_volt_disconn));

                table3.AddCell(cell31);
                table3.AddCell(cell32);
                table3.AddCell(cell33);
                table3.AddCell(cell34);
                table3.AddCell(cell35);

                Cell cell9 = new Cell(1, 1).Add(table5).SetWidth(400).SetBorder(Border.NO_BORDER);
                Cell cell110 = new Cell(1, 1).Add(table3).SetWidth(400).SetBorder(Border.NO_BORDER);

                tb_4.AddCell(cell9);
                tb_4.AddCell(cell110);

                Cell cell111 = new Cell(1, 1).Add(tb_3).SetWidth(400).SetBorder(Border.NO_BORDER);
                Cell cell112 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell cell113 = new Cell(1, 1).Add(tb_4).SetWidth(400).SetBorder(Border.NO_BORDER);

                tb_5.AddCell(cell111);
                tb_5.AddCell(cell112);
                tb_5.AddCell(cell113);

                document.Add(tb_5);
            }

        }
        
        public void PrePowerChecksControlsTable(MainBO mainBO, Document document)
        {
            {//document top table
                Table headerTable = new Table(6, false);

                Cell cellJobNum = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Job Number")).SetBold();
                Cell cellJobNumValue = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph(mainBO.JobNum));
                Cell cellJobName = new Cell(1, 1).SetWidth(90)
                   .Add(new Paragraph("Job Name")).SetBold();
                Cell cellJobNameValue = new Cell(1, 1).SetWidth(130)
                  .Add(new Paragraph(mainBO.Name));
                Cell cellModelNum = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Model Number")).SetBold();
                Cell cellModelNumValue = new Cell(1, 1).SetWidth(280)
                  .Add(new Paragraph(mainBO.Modelnum));

                Cell cellUnitType = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Unit Type")).SetBold();
                Cell cellUnitTypeValue = new Cell(1, 1).SetWidth(120).SetHeight(20)
                  .Add(new Paragraph(mainBO.HorizonType));
                Cell cellLine = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Line")).SetBold();
                Cell cellLineValue = new Cell(1, 1).SetWidth(130).SetHeight(20)
                  .Add(new Paragraph(mainBO.Line));
                Cell cellSerialNum = new Cell(1, 1).SetWidth(90)
                .Add(new Paragraph("Serial Number")).SetBold();
                Cell cellSerialNumValue = new Cell(1, 1).SetWidth(280).SetHeight(20)
                  .Add(new Paragraph(mainBO.Serialnum));

                Cell cellTestedBy = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Tested By")).SetBold();
                Cell cellTestedByValue = new Cell(1, 1).SetWidth(120).SetHeight(20)
                  .Add(new Paragraph(mainBO.TestedBy));
                Cell cellAuditedBy = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Audited By")).SetBold();
                Cell cellAuditedByValue = new Cell(1, 1).SetWidth(130).SetHeight(20)
                  .Add(new Paragraph(mainBO.AuditedBy));
                Cell cellDate = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Date")).SetBold();
                Cell cellDateValue = new Cell(1, 1).SetWidth(280).SetHeight(20)
                  .Add(new Paragraph(mainBO.DateTested.ToString()));

                headerTable.AddCell(cellJobNum);
                headerTable.AddCell(cellJobNumValue);
                headerTable.AddCell(cellJobName);
                headerTable.AddCell(cellJobNameValue);
                headerTable.AddCell(cellModelNum);
                headerTable.AddCell(cellModelNumValue);

                headerTable.AddCell(cellUnitType);
                headerTable.AddCell(cellUnitTypeValue);
                headerTable.AddCell(cellLine);
                headerTable.AddCell(cellLineValue);
                headerTable.AddCell(cellSerialNum);
                headerTable.AddCell(cellSerialNumValue);

                headerTable.AddCell(cellTestedBy);
                headerTable.AddCell(cellTestedByValue);
                headerTable.AddCell(cellAuditedBy);
                headerTable.AddCell(cellAuditedByValue);
                headerTable.AddCell(cellDate);
                headerTable.AddCell(cellDateValue);

                document.Add(headerTable);
            }
            Color headerColor = WebColors.GetRGBColor("#A6A6A6");

            //paragraphs
            Paragraph header = new Paragraph("Product Pre-Power Checks").SetBold()
            .SetTextAlignment(TextAlignment.CENTER)
            .SetFontSize(20);

            document.Add(header);

            Paragraph SubHeader1 = new Paragraph("Please complete the Pre-Test Checklist for every unit that comes off the assembly line.")
            .SetTextAlignment(TextAlignment.CENTER);
            document.Add(SubHeader1);

            Paragraph SubHeader2 = new Paragraph("Enter a ‘P’ for Pass, ‘F’ for Fail, or ‘N’ for does not apply.")
            .SetTextAlignment(TextAlignment.CENTER);
            document.Add(SubHeader2);

            Paragraph SubHeader3 = new Paragraph("For any Failures reported, please document in the Misc Notes section what was incorrect.")
            .SetTextAlignment(TextAlignment.CENTER);
            document.Add(SubHeader3);

            Table tb_3 = new Table(3, false).SetMarginTop(20);

            Table table7 = new Table(2, false);

            Cell cell81 = new Cell(1, 2).SetWidth(400)
             .Add(new Paragraph("ELECTRIC HEAT")).SetBold().SetBackgroundColor(headerColor);
            Cell cell82 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Check power terminations on both sides of the terminal block"));
            Cell cell83 = new Cell(1, 1).SetPaddingRight(20)
            .Add(new Paragraph(mainBO.OAUTPrePower.Check_Power_term_block));
            Cell cell84 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Inspect elements of heater for debris/damage"));
            Cell cell85 = new Cell(1, 1).SetPaddingRight(20)
            .Add(new Paragraph(mainBO.OAUTPrePower.Inspect_element_heater));

            table7.AddCell(cell81);
            table7.AddCell(cell82);
            table7.AddCell(cell83);
            table7.AddCell(cell84);
            table7.AddCell(cell85);

            Table table8 = new Table(2, false);


            Cell cell91 = new Cell(1, 2).SetWidth(400)
             .Add(new Paragraph("GAS HEAT")).SetBold().SetBackgroundColor(headerColor);
            Cell cell92 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Check high voltage from the transformer is landed correctly on heater control board"));
            Cell cell93 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Check_heater_cont_board));
            Cell cell94 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Correct voltage written on transformer"));
            Cell cell95 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Co_volt_writtn_transform));

            table8.AddCell(cell91);
            table8.AddCell(cell92);
            table8.AddCell(cell93);
            table8.AddCell(cell94);
            table8.AddCell(cell95);

            Cell cell7 = new Cell(1, 1).Add(table7).SetWidth(400).SetBorder(Border.NO_BORDER);
            Cell cell8 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
            Cell cell9 = new Cell(1, 1).Add(table8).SetWidth(400).SetBorder(Border.NO_BORDER);

            tb_3.AddCell(cell7);
            tb_3.AddCell(cell8);
            tb_3.AddCell(cell9);
            document.Add(tb_3);

            Table tb_4 = new Table(3, false).SetMarginTop(20);

            Table table9 = new Table(2, false);


            Cell cell101 = new Cell(1, 2).SetWidth(400)
             .Add(new Paragraph("GAS LEAK TEST")).SetBold().SetBackgroundColor(headerColor);
            Cell cell102 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Connect gas to manifold"));
            Cell cell103 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Connect_gas_manifold));
            Cell cell104 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Unlock Gas valves"));
            Cell cell105 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Unlock_gas_valves));
            Cell cell106 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Turn on gas"));
            Cell cell107 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Turn_on_gas));
            Cell cell108 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Ensure field gas port plug is present"));
            Cell cell109 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.En_field_gas_port));
            Cell cell1010 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Do not continue if Iniitals or Plugs are Missing"));
            Cell cell1011 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.In_Plug_are_Missing));
            Cell cell1012 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Turn off gas once leak test is complete & passes inspection"));
            Cell cell1013 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Turn_off_gas_inspection));


            table9.AddCell(cell101);
            table9.AddCell(cell102);
            table9.AddCell(cell103);
            table9.AddCell(cell104);
            table9.AddCell(cell105);
            table9.AddCell(cell106);
            table9.AddCell(cell107);
            table9.AddCell(cell108);
            table9.AddCell(cell109);
            table9.AddCell(cell1010);
            table9.AddCell(cell1011);
            table9.AddCell(cell1012);
            table9.AddCell(cell1013);


            Table table10 = new Table(2, false);


            Cell cell01 = new Cell(1, 2).SetWidth(400)
             .Add(new Paragraph("PRE-POWER FINAL CHECKS")).SetBold().SetBackgroundColor(headerColor);
            Cell cell02 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Back out all screws on air-proving switches (IFFS, FSS)"));
            Cell cell03 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Bac_Air_proving_switches));
            Cell cell04 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Install TD7"));
            Cell cell05 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Install_TD7));
            Cell cell06 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Program UC600"));
            Cell cell07 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Program_UC600));
            Cell cell08 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Set phase monitor"));
            Cell cell09 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Set_Phase_Monitor));
            Cell cell010 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Unlock disconnect key box"));
            Cell cell011 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Unlock_disconnect_k_box));
            Cell cell012 = new Cell(1, 1).SetWidth(400)
             .Add(new Paragraph("Unlock - correct voltage disconnect"));
            Cell cell013 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Un_Correct_volt_disconn));


            table10.AddCell(cell01);
            table10.AddCell(cell02);
            table10.AddCell(cell03);
            table10.AddCell(cell04);
            table10.AddCell(cell05);
            table10.AddCell(cell06);
            table10.AddCell(cell07);
            table10.AddCell(cell08);
            table10.AddCell(cell09);
            table10.AddCell(cell010);
            table10.AddCell(cell011);
            table10.AddCell(cell012);
            table10.AddCell(cell013);

            Cell cell10 = new Cell(1, 1).Add(table9).SetWidth(400).SetBorder(Border.NO_BORDER);
            Cell cell111 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
            //.Add(new Paragraph("Verify submittal unit match"));
            Cell cell112 = new Cell(1, 1).Add(table10).SetWidth(400).SetBorder(Border.NO_BORDER);

            tb_4.AddCell(cell10);
            tb_4.AddCell(cell111);
            tb_4.AddCell(cell112);

            document.Add(tb_4);

            Table table6 = new Table(2, false).SetMarginTop(50).SetWidth(780);


            Cell cell71 = new Cell(1, 2)
             .Add(new Paragraph("PRE-POWER CHECKS")).SetBold().SetBackgroundColor(headerColor);
            Cell cell72 = new Cell(1, 1)
             .Add(new Paragraph("Power Whip is not overly tight and has some strain relief for test"));
            Cell cell73 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Power_whip_over_tight));
            Cell cell74 = new Cell(1, 1)
             .Add(new Paragraph("Correct size Whip for the unit"));
            Cell cell75 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Correct_size_whip_unit));
            Cell cell76 = new Cell(1, 1)
             .Add(new Paragraph("Attach guages to unit (ie: Manometers & Temp clamps"));
            Cell cell77 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Attach_guages_unit));
            Cell cell78 = new Cell(1, 1)
             .Add(new Paragraph("Connect power leads to power whip. For grounding purposes only. No power to unit"));
            Cell cell79 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Co_Power_Ground_purp));
            Cell cell710 = new Cell(1, 1)
             .Add(new Paragraph("Check Continuity to ground at every high voltage termination"));
            Cell cell711 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Cont_High_volt_term));
            Cell cell712 = new Cell(1, 1)
             .Add(new Paragraph("Check Continunity between phases on all fan motors"));
            Cell cell713 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Check_Phases_fan_motor));
            Cell cell714 = new Cell(1, 1)
             .Add(new Paragraph("Remove all 3 high voltage connectors from the CoreSense sensor"));
            Cell cell715 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Remove_volt_Sen_Sensor));
            Cell cell716 = new Cell(1, 1)
             .Add(new Paragraph("Hi-Pot the unit (DO NOT HI POT ROSENBERG  FANS)"));
            Cell cell717 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Hi_pot_unit_fans));
            Cell cell718 = new Cell(1, 1)
             .Add(new Paragraph("Inspect ARC flash PPE for damage (rips, tears and holes)"));
            Cell cell719 = new Cell(1, 1).SetPaddingRight(20)
             .Add(new Paragraph(mainBO.OAUTPrePower.Inspect_ARC_flash));


            table6.AddCell(cell71);
            table6.AddCell(cell72);
            table6.AddCell(cell73);
            table6.AddCell(cell74);
            table6.AddCell(cell75);
            table6.AddCell(cell76);
            table6.AddCell(cell77);
            table6.AddCell(cell78);
            table6.AddCell(cell79);
            table6.AddCell(cell710);
            table6.AddCell(cell711);
            table6.AddCell(cell712);
            table6.AddCell(cell713);
            table6.AddCell(cell714);
            table6.AddCell(cell715);
            table6.AddCell(cell716);
            table6.AddCell(cell717);
            table6.AddCell(cell718);
            table6.AddCell(cell719);

            document.Add(table6);

        }

        public void OAUTestingChecklistControlsTable(MainBO mainBO, Document document)
        {

            {//document top table
                Table headerTable = new Table(6, false);

                Cell cellJobNum = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Job Number")).SetBold();
                Cell cellJobNumValue = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph(mainBO.JobNum));
                Cell cellJobName = new Cell(1, 1).SetWidth(90)
                   .Add(new Paragraph("Job Name")).SetBold();
                Cell cellJobNameValue = new Cell(1, 1).SetWidth(130)
                  .Add(new Paragraph(mainBO.Name));
                Cell cellModelNum = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Model Number")).SetBold();
                Cell cellModelNumValue = new Cell(1, 1).SetWidth(280)
                  .Add(new Paragraph(mainBO.Modelnum));

                Cell cellUnitType = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Unit Type")).SetBold();
                Cell cellUnitTypeValue = new Cell(1, 1).SetWidth(120).SetHeight(20)
                  .Add(new Paragraph(mainBO.HorizonType));
                Cell cellLine = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Line")).SetBold();
                Cell cellLineValue = new Cell(1, 1).SetWidth(130).SetHeight(20)
                  .Add(new Paragraph(mainBO.Line));
                Cell cellSerialNum = new Cell(1, 1).SetWidth(90)
                .Add(new Paragraph("Serial Number")).SetBold();
                Cell cellSerialNumValue = new Cell(1, 1).SetWidth(280).SetHeight(20)
                  .Add(new Paragraph(mainBO.Serialnum));

                Cell cellTestedBy = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Tested By")).SetBold();
                Cell cellTestedByValue = new Cell(1, 1).SetWidth(120).SetHeight(20)
                  .Add(new Paragraph(mainBO.TestedBy));
                Cell cellAuditedBy = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Audited By")).SetBold();
                Cell cellAuditedByValue = new Cell(1, 1).SetWidth(130).SetHeight(20)
                  .Add(new Paragraph(mainBO.AuditedBy));
                Cell cellDate = new Cell(1, 1).SetWidth(90)
                  .Add(new Paragraph("Date")).SetBold();
                Cell cellDateValue = new Cell(1, 1).SetWidth(280).SetHeight(20)
                  .Add(new Paragraph(mainBO.DateTested.ToString()));

                headerTable.AddCell(cellJobNum);
                headerTable.AddCell(cellJobNumValue);
                headerTable.AddCell(cellJobName);
                headerTable.AddCell(cellJobNameValue);
                headerTable.AddCell(cellModelNum);
                headerTable.AddCell(cellModelNumValue);

                headerTable.AddCell(cellUnitType);
                headerTable.AddCell(cellUnitTypeValue);
                headerTable.AddCell(cellLine);
                headerTable.AddCell(cellLineValue);
                headerTable.AddCell(cellSerialNum);
                headerTable.AddCell(cellSerialNumValue);

                headerTable.AddCell(cellTestedBy);
                headerTable.AddCell(cellTestedByValue);
                headerTable.AddCell(cellAuditedBy);
                headerTable.AddCell(cellAuditedByValue);
                headerTable.AddCell(cellDate);
                headerTable.AddCell(cellDateValue);

                document.Add(headerTable);
            }

            Paragraph header = new Paragraph("Production Testing Checklist").SetBold()
            .SetTextAlignment(TextAlignment.CENTER).SetFontSize(20);

            document.Add(header);

            Color headerColor = WebColors.GetRGBColor("#A6A6A6");
            Color color = WebColors.GetRGBColor("#FFC7B3");

            { //tb_1
                Table tb_1 = new Table(5, false).SetMarginTop(20);

                //voltage table
                Table table1 = new Table(3, false);
                
                Cell cellVoltage = new Cell(1, 3).SetWidth(270)
                 .Add(new Paragraph("Voltage")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell Voltage_R1_C1 = new Cell(1, 2).SetWidth(180)
                      .Add(new Paragraph("Voltage")).SetBold();
                Cell Voltage_R1_C2 = new Cell(1, 1).SetWidth(90)
                      .Add(new Paragraph(mainBO.Voltages.Voltage));

                Cell Voltage_R2_C1 = new Cell(1, 1).SetWidth(90).SetBackgroundColor(color)
                 .Add(new Paragraph("L1-L2"));
                Cell Voltage_R2_C2 = new Cell(1, 1).SetWidth(90).SetBackgroundColor(color)
                 .Add(new Paragraph("L2-L3"));
                Cell Voltage_R2_C3 = new Cell(1, 1).SetWidth(90).SetBackgroundColor(color)
                 .Add(new Paragraph("L1-L3"));

                 Cell Voltage_R3_C1 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                     .Add(new Paragraph(mainBO.Voltages.VoltageL1L2));
                 Cell Voltage_R3_C2 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                     .Add(new Paragraph(mainBO.Voltages.VoltageL2L3));
                 Cell Voltage_R3_C3 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                     .Add(new Paragraph(mainBO.Voltages.VoltageL1L3));

                Cell Voltage_R4_C1 = new Cell(1, 1).SetWidth(90).SetBackgroundColor(color)
                 .Add(new Paragraph("L1-G"));
                Cell Voltage_R4_C2 = new Cell(1, 1).SetWidth(90).SetBackgroundColor(color)
                 .Add(new Paragraph("L3-G"));
                Cell Voltage_R4_C3 = new Cell(1, 1).SetWidth(90).SetBackgroundColor(color)
                 .Add(new Paragraph("L3-G"));

                 Cell Voltage_R5_C1 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                     .Add(new Paragraph(mainBO.Voltages.VoltageL1G));
                 Cell Voltage_R5_C2 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                     .Add(new Paragraph(mainBO.Voltages.VoltageL2G));
                 Cell Voltage_R5_C3 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                     .Add(new Paragraph(mainBO.Voltages.VoltageL3G));

                table1.AddCell(cellVoltage);
                table1.AddCell(Voltage_R1_C1);
                table1.AddCell(Voltage_R1_C2);
                table1.AddCell(Voltage_R2_C1);
                table1.AddCell(Voltage_R2_C2);
                table1.AddCell(Voltage_R2_C3);
                table1.AddCell(Voltage_R3_C1);
                table1.AddCell(Voltage_R3_C2);
                table1.AddCell(Voltage_R3_C3);
                table1.AddCell(Voltage_R4_C1);
                table1.AddCell(Voltage_R4_C2);
                table1.AddCell(Voltage_R4_C3);
                table1.AddCell(Voltage_R5_C1);
                table1.AddCell(Voltage_R5_C2);
                 table1.AddCell(Voltage_R5_C3);

                Table filterChargeTable = new Table(1, false);
                //filters table

                Table table2 = new Table(3, false);

                Cell cellFilters = new Cell(1, 3).SetWidth(270)
                 .Add(new Paragraph("Filters")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell Filters_R1_C1 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Filter Size")).SetBackgroundColor(color);
                Cell Filters_R1_C2 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Filter Style")).SetBackgroundColor(color);
                Cell Filters_R1_C3 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Qty")).SetBackgroundColor(color);

                table2.AddCell(cellFilters);
                table2.AddCell(Filters_R1_C1);
                table2.AddCell(Filters_R1_C2);
                table2.AddCell(Filters_R1_C3);

                if(mainBO.Filters.Count == 0)
                {
                    Cell Filters_R2_C1 = new Cell(1, 1).SetWidth(90).SetHeight(20);
                    Cell Filters_R2_C2 = new Cell(1, 1).SetWidth(90).SetHeight(20);
                    Cell Filters_R2_C3 = new Cell(1, 1).SetWidth(90).SetHeight(20);

                    table2.AddCell(Filters_R2_C1);
                    table2.AddCell(Filters_R2_C2);
                    table2.AddCell(Filters_R2_C3);
                }
                else
                {
                    foreach (var filter in mainBO.Filters)
                    {
                        Cell Filters_R2_C1 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                           .Add(new Paragraph(filter.FiltersSize));
                        Cell Filters_R2_C2 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                            .Add(new Paragraph(filter.FiltersStyle));
                        Cell Filters_R2_C3 = new Cell(1, 1).SetWidth(90).SetHeight(20)
                            .Add(new Paragraph(filter.Qnty.ToString()));

                        table2.AddCell(Filters_R2_C1);
                        table2.AddCell(Filters_R2_C2);
                        table2.AddCell(Filters_R2_C3);
                    }
                }
                

                //Charge data table

                Table table3 = new Table(3, false).SetMarginTop(20);

                Cell cellChargeData = new Cell(1, 3).SetWidth(270)
                 .Add(new Paragraph("Charge data")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell ChargeData_R1_C1 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Circuit #1")).SetBackgroundColor(color);
                Cell ChargeData_R1_C2 = new Cell(1, 1).SetWidth(90)
                    .Add(new Paragraph(mainBO.Charge_Circuit_1.ToString()));
                Cell ChargeData_R1_C3 = new Cell(1, 1).SetWidth(90)
                    .Add(new Paragraph(mainBO.Charge_Circuit_1_Var.ToString()));

                table3.AddCell(cellChargeData);
                table3.AddCell(ChargeData_R1_C1);
                table3.AddCell(ChargeData_R1_C2);
                table3.AddCell(ChargeData_R1_C3);

                if (mainBO.ProductType.ToString() == "Horizon")
                {
                    Cell ChargeData_R2_C1 = new Cell(1, 1).SetWidth(90)
                     .Add(new Paragraph("Circuit #2")).SetBackgroundColor(color);
                    Cell ChargeData_R2_C2 = new Cell(1, 1).SetWidth(90)
                        .Add(new Paragraph(mainBO.Charge_Circuit_2.ToString()));
                    Cell ChargeData_R2_C3 = new Cell(1, 1).SetWidth(90)
                        .Add(new Paragraph(mainBO.Charge_Circuit_2_Var.ToString()));

                    table3.AddCell(ChargeData_R2_C1);
                    table3.AddCell(ChargeData_R2_C2);
                    table3.AddCell(ChargeData_R2_C3);
                }

                Cell cell1 = new Cell(1, 1).Add(table2).SetWidth(270).SetBorder(Border.NO_BORDER);
                Cell cell2 = new Cell(1, 1).Add(table3).SetWidth(270).SetBorder(Border.NO_BORDER);

                filterChargeTable.AddCell(cell1);
                filterChargeTable.AddCell(cell2);

                Table ambientDisconnectedTable = new Table(1, false);

                //Ambient Conditions
                Table table4 = new Table(2, false);

                Cell cellAmbientConditions = new Cell(1, 4).SetWidth(270)
                 .Add(new Paragraph("Ambient Conditions")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell Temperature_R1_C1 = new Cell(1, 1).SetWidth(135)
                 .Add(new Paragraph("Temperature")).SetBackgroundColor(color);
                Cell Temperature_R1_C2 = new Cell(1, 1).SetWidth(135)
                    .Add(new Paragraph(mainBO.AC_Temp.ToString()));

                Cell Temperature_R2_C1 = new Cell(1, 1).SetWidth(135)
                 .Add(new Paragraph("humidity")).SetBackgroundColor(color);
                Cell Temperature_R2_C2 = new Cell(1, 1).SetWidth(135)
                    .Add(new Paragraph(mainBO.AC_Humidity.ToString()));

                table4.AddCell(cellAmbientConditions);
                table4.AddCell(Temperature_R1_C1);
                table4.AddCell(Temperature_R1_C2);
                table4.AddCell(Temperature_R2_C1);
                table4.AddCell(Temperature_R2_C2);

                //Disconnected table
                Table table5 = new Table(2, false).SetMarginTop(20);

                Cell cellDisconnected = new Cell(1, 2).SetWidth(270)
                 .Add(new Paragraph("Disconnected")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell Disconnected_R1_C1 = new Cell(1, 1).SetWidth(135)
                 .Add(new Paragraph("Type")).SetBackgroundColor(color);
                Cell Disconnected_R1_C2 = new Cell(1, 1).SetWidth(135)
                    .Add(new Paragraph(mainBO.Disconnect_Type));

                Cell Disconnected_R2_C1 = new Cell(1, 1).SetWidth(135)
                 .Add(new Paragraph("Size")).SetBackgroundColor(color);
                Cell Disconnected_R2_C2 = new Cell(1, 1).SetWidth(135)
                    .Add(new Paragraph(mainBO.Disconnect_Size));

                table5.AddCell(cellDisconnected);
                table5.AddCell(Disconnected_R1_C1);
                table5.AddCell(Disconnected_R1_C2);
                table5.AddCell(Disconnected_R2_C1);
                table5.AddCell(Disconnected_R2_C2);

                Cell cell3 = new Cell(1, 1).Add(table4).SetWidth(270).SetBorder(Border.NO_BORDER);
                Cell cell4 = new Cell(1, 1).Add(table5).SetWidth(270).SetBorder(Border.NO_BORDER);

                ambientDisconnectedTable.AddCell(cell3);
                ambientDisconnectedTable.AddCell(cell4);

                Cell celltb_1_1 = new Cell(1, 1).Add(table1).SetWidth(270).SetBorder(Border.NO_BORDER);
                Cell celltb_1_2 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell celltb_1_3 = new Cell(1, 1).Add(filterChargeTable).SetWidth(270).SetBorder(Border.NO_BORDER);
                Cell celltb_1_4 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell celltb_1_5 = new Cell(1, 1).Add(ambientDisconnectedTable).SetWidth(270).SetBorder(Border.NO_BORDER);

                tb_1.AddCell(celltb_1_1);
                tb_1.AddCell(celltb_1_2);
                tb_1.AddCell(celltb_1_3);
                tb_1.AddCell(celltb_1_4);
                tb_1.AddCell(celltb_1_5);
                document.Add(tb_1);
            }

            {//tb_2

                Table tb_2 = new Table(3, false).SetMarginTop(20);

                Table table6 = new Table(3, false);

                Cell cellCondenserMotor = new Cell(1, 5).SetWidth(315)
                 .Add(new Paragraph("Condenser Motor Amps")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell CondenserMotor_R1_C1 = new Cell(1, 1).SetWidth(115).SetBackgroundColor(color);
                Cell CondenserMotor_R1_C2 = new Cell(1, 1).SetWidth(100)
                 .Add(new Paragraph("Running Amps")).SetBackgroundColor(color);
                Cell CondenserMotor_R1_C3 = new Cell(1, 1).SetWidth(100)
                 .Add(new Paragraph("Rated Amps")).SetBackgroundColor(color);

                table6.AddCell(cellCondenserMotor);
                table6.AddCell(CondenserMotor_R1_C1);
                table6.AddCell(CondenserMotor_R1_C2);
                table6.AddCell(CondenserMotor_R1_C3);

                foreach (var condenser in mainBO.Condensers)
                {
                    Cell CondenserMotor_R2_C1 = new Cell(1, 1).SetWidth(115)
                       .Add(new Paragraph(condenser.CondenserNum.ToString()));
                    Cell CondenserMotor_R2_C2 = new Cell(1, 1).SetWidth(100)
                        .Add(new Paragraph(condenser.ConenserdRunningAmps.ToString())).SetTextAlignment(TextAlignment.RIGHT);
                    Cell CondenserMotor_R2_C3 = new Cell(1, 1).SetWidth(100)
                        .Add(new Paragraph(condenser.CondenserRatedAmps.ToString())).SetTextAlignment(TextAlignment.RIGHT);

                    table6.AddCell(CondenserMotor_R2_C1);
                    table6.AddCell(CondenserMotor_R2_C2);
                    table6.AddCell(CondenserMotor_R2_C3);
                }

                Table table7 = new Table(5, false);

                Cell cellCompressorData = new Cell(1, 5).SetWidth(515)
                 .Add(new Paragraph("Compressor Data")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell CompressorData_R1_C1 = new Cell(1, 1).SetWidth(135).SetBackgroundColor(color);
                Cell CompressorData_R1_C2 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Model#")).SetBackgroundColor(color);
                Cell CompressorData_R1_C3 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Serial#")).SetBackgroundColor(color);
                Cell CompressorData_R1_C4 = new Cell(1, 1).SetWidth(100)
                 .Add(new Paragraph("Running Amps")).SetBackgroundColor(color);
                Cell CompressorData_R1_C5 = new Cell(1, 1).SetWidth(100)
                 .Add(new Paragraph("Rated Amps")).SetBackgroundColor(color);

                table7.AddCell(cellCompressorData);
                table7.AddCell(CompressorData_R1_C1);
                table7.AddCell(CompressorData_R1_C2);
                table7.AddCell(CompressorData_R1_C3);
                table7.AddCell(CompressorData_R1_C4);
                table7.AddCell(CompressorData_R1_C5);

                foreach (var compressor in mainBO.Compressors)
                {
                    Cell CompressorData_R2_C1 = new Cell(1, 1).SetWidth(135)
                         .Add(new Paragraph(compressor.CompressorNum.ToString()));
                    Cell CompressorData_R2_C2 = new Cell(1, 1).SetWidth(90)
                        .Add(new Paragraph(compressor.CompressorModelNum)).SetTextAlignment(TextAlignment.RIGHT);
                    Cell CompressorData_R2_C3 = new Cell(1, 1).SetWidth(90)
                        .Add(new Paragraph(compressor.CompressorSerialNum)).SetTextAlignment(TextAlignment.RIGHT);
                    Cell CompressorData_R2_C4 = new Cell(1, 1).SetWidth(100)
                        .Add(new Paragraph(compressor.CompressorRunningAmps.ToString())).SetTextAlignment(TextAlignment.RIGHT);
                    Cell CompressorData_R2_C5 = new Cell(1, 1).SetWidth(100)
                        .Add(new Paragraph(compressor.CompressorRatedAmps.ToString())).SetTextAlignment(TextAlignment.RIGHT);

                    table7.AddCell(CompressorData_R2_C1);
                    table7.AddCell(CompressorData_R2_C2);
                    table7.AddCell(CompressorData_R2_C3);
                    table7.AddCell(CompressorData_R2_C4);
                    table7.AddCell(CompressorData_R2_C5);
                }

                Cell cell16 = new Cell(1, 1).Add(table6).SetWidth(315).SetBorder(Border.NO_BORDER);
                Cell cell17 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell cell18 = new Cell(1, 1).Add(table7).SetWidth(515).SetBorder(Border.NO_BORDER);

                tb_2.AddCell(cell16);
                tb_2.AddCell(cell17);
                tb_2.AddCell(cell18);

                document.Add(tb_2);
            }

            {  //tb_3
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));

                Table tb_3 = new Table(3, false);

                Table table8 = new Table(6, false);

                Cell cellERV = new Cell(1, 6).SetWidth(500)
                 .Add(new Paragraph("ERV")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell ERVR_1_C1 = new Cell(1, 1).SetWidth(80)
                 .Add(new Paragraph("Model #")).SetBackgroundColor(color);
                Cell ERVR_1_C2 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.ERV_ModelNo));
                Cell ERVR_1_C4 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Running Amps")).SetBackgroundColor(color);
                Cell ERVR_1_C5 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.ERV_Running_Amp));
                Cell ERVR_1_C7 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Proper Rotation")).SetBackgroundColor(color);
                Cell ERVR_1_C8 = new Cell(1, 1).SetWidth(60)
                    .Add(new Paragraph(mainBO.ERV_Proper_Rotation.ToString()));

                Cell ERVR_2_C1 = new Cell(1, 1).SetWidth(80)
                 .Add(new Paragraph("Serial #")).SetBackgroundColor(color);
                Cell ERVR_2_C2 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.ERV_SerialNo));
                Cell ERVR_2_C4 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Rated Amps")).SetBackgroundColor(color);
                Cell ERVR_2_C5 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.ERV_Rated_Amp));
                Cell ERVR_2_C6 = new Cell(1, 2).SetWidth(170);

                table8.AddCell(cellERV);
                table8.AddCell(ERVR_1_C1);
                table8.AddCell(ERVR_1_C2);
                table8.AddCell(ERVR_1_C4);
                table8.AddCell(ERVR_1_C5);
                table8.AddCell(ERVR_1_C7);
                table8.AddCell(ERVR_1_C8);
                table8.AddCell(ERVR_2_C1);
                table8.AddCell(ERVR_2_C2);
                table8.AddCell(ERVR_2_C4);
                table8.AddCell(ERVR_2_C5);
                table8.AddCell(ERVR_2_C6);

                Table table9 = new Table(4, false);

                Cell cellPreHeat = new Cell(1, 4).SetWidth(330)
                 .Add(new Paragraph("Pre Heat")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell PreHeat_R1_C1 = new Cell(1, 1).SetWidth(80)
                 .Add(new Paragraph("Model #")).SetBackgroundColor(color);
                Cell PreHeat_R1_C2 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.Heat_model_num_PreHeat));
                Cell PreHeat_R1_C4 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Running Amps")).SetBackgroundColor(color);
                Cell PreHeat_R1_C5 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.Heat_RunningAmp_PreHeat));

                Cell PreHeat_R2_C1 = new Cell(1, 1).SetWidth(80)
                 .Add(new Paragraph("Serial #")).SetBackgroundColor(color);
                Cell PreHeat_R2_C2 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO._Heat_Serial_PreHeat));
                Cell PreHeat_R2_C4 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Rated Amps")).SetBackgroundColor(color);
                Cell PreHeat_R2_C5 = new Cell(1, 1).SetWidth(80)
                    .Add(new Paragraph(mainBO.Heat_RatedAmp_PreHeat));

                Cell PreHeat_R3_C1 = new Cell(1, 2).SetWidth(160);
                Cell PreHeat_R3_C2 = new Cell(1, 1).SetWidth(90)
                 .Add(new Paragraph("Size(kW)")).SetBackgroundColor(color);
                Cell PreHeat_R3_C3 = new Cell(1, 3).SetWidth(80)
                    .Add(new Paragraph(mainBO.Heat_Size_PreHeat));

                table9.AddCell(cellPreHeat);
                table9.AddCell(PreHeat_R1_C1);
                table9.AddCell(PreHeat_R1_C2);
                table9.AddCell(PreHeat_R1_C4);
                table9.AddCell(PreHeat_R1_C5);
                table9.AddCell(PreHeat_R2_C1);
                table9.AddCell(PreHeat_R2_C2);
                table9.AddCell(PreHeat_R2_C4);
                table9.AddCell(PreHeat_R2_C5);
                table9.AddCell(PreHeat_R3_C1);
                table9.AddCell(PreHeat_R3_C2);
                table9.AddCell(PreHeat_R3_C3);

                Cell cell9 = new Cell(1, 1).Add(table9).SetWidth(330).SetBorder(Border.NO_BORDER);
                Cell cell10 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell cell11 = new Cell(1, 1).Add(table8).SetWidth(500).SetBorder(Border.NO_BORDER);

                tb_3.AddCell(cell9);
                tb_3.AddCell(cell10);
                tb_3.AddCell(cell11);

                document.Add(tb_3);
            }

            {//tb_4
                Table tb_4 = new Table(3, false).SetMarginTop(20);

                Table heatMeasuredPressure = new Table(1, false);

                Table heatTable = new Table(4, false);

                Cell cellHeat = new Cell(1, 4).SetWidth(390)
                 .Add(new Paragraph("Heat")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell HeatR_1_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Primary")).SetBackgroundColor(color);
                Cell HeatR_1_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Primary));
                Cell HeatR_1_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Fuel Type")).SetBackgroundColor(color);
                Cell HeatR_1_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_FuelType));

                Cell HeatR_2_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Position")).SetBackgroundColor(color);
                Cell HeatR_2_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.TopBottom));
                Cell HeatR_2_C3 = new Cell(1, 3).SetWidth(110);

                Cell cellHeatTop = new Cell(1, 4).SetWidth(390)
                 .Add(new Paragraph("Top")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell HeatTopR_1_C1 = new Cell(1, 1).SetWidth(110)
                   .Add(new Paragraph("Model #")).SetBackgroundColor(color);
                Cell HeatTopR_1_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_model_num_Top));
                Cell HeatTopR_1_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Serial #")).SetBackgroundColor(color);
                Cell HeatTopR_1_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Serial_Top));

                Cell HeatTopR_2_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Item #")).SetBackgroundColor(color);
                Cell HeatTopR_2_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Item_num));
                Cell HeatTopR_2_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("PO #")).SetBackgroundColor(color);
                Cell HeatTopR_2_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_PO_num));

                Cell HeatTopR_3_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Part #")).SetBackgroundColor(color);
                Cell HeatTopR_3_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Partnum_Top));
                Cell HeatTopR_3_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Size(MBH/kW)")).SetBackgroundColor(color);
                Cell HeatTopR_3_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Size_Top));

                Cell HeatTopR_4_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Rated Amps")).SetBackgroundColor(color);
                Cell HeatTopR_4_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Rated_Amp));
                Cell HeatTopR_4_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Running Amps")).SetBackgroundColor(color);
                Cell HeatTopR_4_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Running_Amp));

                Cell cellHeatBottom = new Cell(1, 4).SetWidth(390)
                 .Add(new Paragraph("Bottom")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell HeatBottomR_1_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Model #")).SetBackgroundColor(color);
                Cell HeatBottomR_1_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_model_num_Bottom));
                Cell HeatBottomR_1_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Serial #")).SetBackgroundColor(color);
                Cell HeatBottomR_1_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Serial_Bottom));

                Cell HeatBottomR_2_C1 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Part #")).SetBackgroundColor(color);
                Cell HeatBottomR_2_C2 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Partnum_Bottom));
                Cell HeatBottomR_2_C3 = new Cell(1, 1).SetWidth(110)
                 .Add(new Paragraph("Size(MBH/kW)")).SetBackgroundColor(color);
                Cell HeatBottomR_2_C4 = new Cell(1, 1).SetWidth(85)
                    .Add(new Paragraph(mainBO.Heat_Size_Bottom));

                heatTable.AddCell(cellHeat);
                heatTable.AddCell(HeatR_1_C1);
                heatTable.AddCell(HeatR_1_C2);
                heatTable.AddCell(HeatR_1_C3);
                heatTable.AddCell(HeatR_1_C4);
                heatTable.AddCell(HeatR_2_C1);
                heatTable.AddCell(HeatR_2_C2);
                heatTable.AddCell(HeatR_2_C3);
                heatTable.AddCell(cellHeatTop);
                heatTable.AddCell(HeatTopR_1_C1);
                heatTable.AddCell(HeatTopR_1_C2);
                heatTable.AddCell(HeatTopR_1_C3);
                heatTable.AddCell(HeatTopR_1_C4);
                heatTable.AddCell(HeatTopR_2_C1);
                heatTable.AddCell(HeatTopR_2_C2);
                heatTable.AddCell(HeatTopR_2_C3);
                heatTable.AddCell(HeatTopR_2_C4);
                heatTable.AddCell(HeatTopR_3_C1);
                heatTable.AddCell(HeatTopR_3_C2);
                heatTable.AddCell(HeatTopR_3_C3);
                heatTable.AddCell(HeatTopR_3_C4);
                heatTable.AddCell(HeatTopR_4_C1);
                heatTable.AddCell(HeatTopR_4_C2);
                heatTable.AddCell(HeatTopR_4_C3);
                heatTable.AddCell(HeatTopR_4_C4);
                heatTable.AddCell(cellHeatBottom);
                heatTable.AddCell(HeatBottomR_1_C1);
                heatTable.AddCell(HeatBottomR_1_C2);
                heatTable.AddCell(HeatBottomR_1_C3);
                heatTable.AddCell(HeatBottomR_1_C4);
                heatTable.AddCell(HeatBottomR_2_C1);
                heatTable.AddCell(HeatBottomR_2_C2);
                heatTable.AddCell(HeatBottomR_2_C3);
                heatTable.AddCell(HeatBottomR_2_C4);

                Table measuredPressureTable = new Table(3, false).SetMarginTop(20);

                Cell cellMeasurePress = new Cell(1, 3).SetWidth(390)
                 .Add(new Paragraph("Measured Pressure")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell measurePress_R1_C1 = new Cell(1, 1).SetWidth(130)
                 .Add(new Paragraph("Inlet")).SetBackgroundColor(color);
                Cell measurePress_R1_C2 = new Cell(1, 1).SetWidth(130)
                 .Add(new Paragraph("Low Fire")).SetBackgroundColor(color);
                Cell measurePress_R1_C3 = new Cell(1, 1).SetWidth(130)
                 .Add(new Paragraph("High Fire")).SetBackgroundColor(color);
                
                Cell measurePress_R2_C1 = new Cell(1, 1).SetWidth(130).SetHeight(20)
                      .Add(new Paragraph(mainBO.InletModulationPressure.ToString()));
                Cell measurePress_R2_C2 = new Cell(1, 1).SetWidth(130).SetHeight(20)
                      .Add(new Paragraph(mainBO.LowFire.ToString()));
                Cell measurePress_R2_C3 = new Cell(1, 1).SetWidth(130).SetHeight(20)
                     .Add(new Paragraph(mainBO.HighFire.ToString()));

                measuredPressureTable.AddCell(cellMeasurePress);
                measuredPressureTable.AddCell(measurePress_R1_C1);
                measuredPressureTable.AddCell(measurePress_R1_C2);
                measuredPressureTable.AddCell(measurePress_R1_C3);
                measuredPressureTable.AddCell(measurePress_R2_C1);
                measuredPressureTable.AddCell(measurePress_R2_C2);
                measuredPressureTable.AddCell(measurePress_R2_C3);

                Cell cellHM1 = new Cell(1, 1).Add(heatTable).SetWidth(390).SetBorder(Border.NO_BORDER);
                Cell cellHM2 = new Cell(1, 1).Add(measuredPressureTable).SetWidth(390).SetBorder(Border.NO_BORDER);

                heatMeasuredPressure.AddCell(cellHM1);
                if (mainBO.ProductType.ToString() == "Horizon")
                {
                    heatMeasuredPressure.AddCell(cellHM2);
                }

                //circuit #1 table

                Table circuitsTable = new Table(1, false);

                Table circuit1Table = new Table(4, false);

                Cell cell1circuit1 = new Cell(1, 4).SetWidth(440)
                 .Add(new Paragraph("Circuit #1")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell circuit1_R1_C1 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Suct. PSI")).SetBackgroundColor(color);
                Cell circuit1_R1_C2 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("SST")).SetBackgroundColor(color);
                Cell circuit1_R1_C3 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Suct. Temp")).SetBackgroundColor(color);
                Cell circuit1_R1_C4 = new Cell(1, 1).SetWidth(155)
                  .Add(new Paragraph("APR Active During Test")).SetBackgroundColor(color);


                Cell circuit1_R2_C1 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                      .Add(new Paragraph(mainBO.Circuit1.SuctPSIVal));
                Cell circuit1_R2_C2 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                      .Add(new Paragraph(mainBO.Circuit1.SSTVal));
                Cell circuit1_R2_C3 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                     .Add(new Paragraph(mainBO.Circuit1.SuctTempVal));
                Cell circuit1_R2_C4 = new Cell(1, 1).SetWidth(155).SetHeight(20)
                   .Add(new Paragraph(mainBO.APRActive.ToString())).SetTextAlignment(TextAlignment.CENTER);


                Cell circuit1_R3_C1 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Liq. PSI")).SetBackgroundColor(color);
                Cell circuit1_R3_C2 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("SLT")).SetBackgroundColor(color);
                Cell circuit1_R3_C3 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Liq. Temp")).SetBackgroundColor(color);
                Cell circuit1_R3_C4 = new Cell(1, 1).SetWidth(155)
                 .Add(new Paragraph("Subcooling(Range 11-17)")).SetBackgroundColor(color);

                Cell circuit1_R3_C5 = new Cell(1, 1).SetWidth(155)
                 .Add(new Paragraph("Subcooling(Range 5-15)")).SetBackgroundColor(color);

                Cell circuit1_R4_C1 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.LiqPSIVal));
                Cell circuit1_R4_C2 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.SLT));
                Cell circuit1_R4_C3 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.LiqTempVal));
                Cell circuit1_R4_C4 = new Cell(1, 1).SetWidth(155).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.Subcooling));

                Cell circuit1_R5_C1 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Dis. PSI")).SetBackgroundColor(color);
                Cell circuit1_R5_C2 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("SDT")).SetBackgroundColor(color);
                Cell circuit1_R5_C3 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Dis. Temp")).SetBackgroundColor(color);
                Cell circuit1_R5_C4 = new Cell(1, 1).SetWidth(155)
                 .Add(new Paragraph("Super Heat(Range 10-25)")).SetBackgroundColor(color);

                Cell circuit1_R5_C5 = new Cell(1, 1).SetWidth(155)
                 .Add(new Paragraph("Super Heat(Range 10-20)")).SetBackgroundColor(color);

                Cell circuit1_R6_C1 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.DisPSIVal));
                Cell circuit1_R6_C2 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.SDTVal));
                Cell circuit1_R6_C3 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.DisTempVal));
                Cell circuit1_R6_C4 = new Cell(1, 1).SetWidth(155).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit1.SuperHeat));

                circuit1Table.AddCell(cell1circuit1);
                circuit1Table.AddCell(circuit1_R1_C1);
                circuit1Table.AddCell(circuit1_R1_C2);
                circuit1Table.AddCell(circuit1_R1_C3);
                circuit1Table.AddCell(circuit1_R1_C4);
                circuit1Table.AddCell(circuit1_R2_C1);
                circuit1Table.AddCell(circuit1_R2_C2);
                circuit1Table.AddCell(circuit1_R2_C3);
                circuit1Table.AddCell(circuit1_R2_C4);
                circuit1Table.AddCell(circuit1_R3_C1);
                circuit1Table.AddCell(circuit1_R3_C2);
                circuit1Table.AddCell(circuit1_R3_C3);
                if (mainBO.ProductType.ToString() == "Horizon")
                {
                    circuit1Table.AddCell(circuit1_R3_C4);
                }
                else if(mainBO.ProductType.ToString() == "MixedAir")
                {
                    circuit1Table.AddCell(circuit1_R3_C5);
                }

                circuit1Table.AddCell(circuit1_R4_C1);
                circuit1Table.AddCell(circuit1_R4_C2);
                circuit1Table.AddCell(circuit1_R4_C3);
                circuit1Table.AddCell(circuit1_R4_C4);
                circuit1Table.AddCell(circuit1_R5_C1);
                circuit1Table.AddCell(circuit1_R5_C2);
                circuit1Table.AddCell(circuit1_R5_C3);
                if (mainBO.ProductType.ToString() == "Horizon")
                {
                    circuit1Table.AddCell(circuit1_R5_C4);
                }
                else if (mainBO.ProductType.ToString() == "MixedAir")
                {
                    circuit1Table.AddCell(circuit1_R5_C5);
                }
                
                circuit1Table.AddCell(circuit1_R6_C1);
                circuit1Table.AddCell(circuit1_R6_C2);
                circuit1Table.AddCell(circuit1_R6_C3);
                circuit1Table.AddCell(circuit1_R6_C4);

                //circuit #2 table

                Table circuit2Table = new Table(4, false).SetMarginTop(20);

                Cell cellcircuit2 = new Cell(1, 4).SetWidth(440)
                 .Add(new Paragraph("Circuit #2")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell circuit2_R1_C1 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Suct. PSI")).SetBackgroundColor(color);
                Cell circuit2_R1_C2 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("SST")).SetBackgroundColor(color);
                Cell circuit2_R1_C3 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Suct. Temp")).SetBackgroundColor(color);
                Cell circuit2_R1_C4 = new Cell(2, 1).SetWidth(155);

                Cell circuit2_R2_C1 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.SuctPSIVal));
                Cell circuit2_R2_C2 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.SSTVal));
                Cell circuit2_R2_C3 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.SuctTempVal));

                Cell circuit2_R3_C1 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Liq. PSI")).SetBackgroundColor(color);
                Cell circuit2_R3_C2 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("SLT")).SetBackgroundColor(color);
                Cell circuit2_R3_C3 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Liq. Temp")).SetBackgroundColor(color);
                Cell circuit2_R3_C4 = new Cell(1, 1).SetWidth(155)
                 .Add(new Paragraph("Subcooling(Range 11-17)")).SetBackgroundColor(color);


                Cell circuit2_R4_C1 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.LiqPSIVal));
                Cell circuit2_R4_C2 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.SLT));
                Cell circuit2_R4_C3 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.LiqTempVal));
                Cell circuit2_R4_C4 = new Cell(1, 1).SetWidth(155).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.Subcooling));


                Cell circuit2_R5_C1 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Dis. PSI")).SetBackgroundColor(color);
                Cell circuit2_R5_C2 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("SDT")).SetBackgroundColor(color);
                Cell circuit2_R5_C3 = new Cell(1, 1).SetWidth(95)
                 .Add(new Paragraph("Dis. Temp")).SetBackgroundColor(color);
                Cell circuit2_R5_C4 = new Cell(1, 1).SetWidth(155)
                 .Add(new Paragraph("Super Heat(Range 10-25)")).SetBackgroundColor(color);

                Cell circuit2_R6_C1 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.DisPSIVal));
                Cell circuit2_R6_C2 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.SDTVal));
                Cell circuit2_R6_C3 = new Cell(1, 1).SetWidth(95).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.DisTempVal));
                Cell circuit2_R6_C4 = new Cell(1, 1).SetWidth(155).SetHeight(20)
                    .Add(new Paragraph(mainBO.Circuit2.SuperHeat));

                circuit2Table.AddCell(cellcircuit2);
                circuit2Table.AddCell(circuit2_R1_C1);
                circuit2Table.AddCell(circuit2_R1_C2);
                circuit2Table.AddCell(circuit2_R1_C3);
                circuit2Table.AddCell(circuit2_R1_C4);
                circuit2Table.AddCell(circuit2_R2_C1);
                circuit2Table.AddCell(circuit2_R2_C2);
                circuit2Table.AddCell(circuit2_R2_C3);
                circuit2Table.AddCell(circuit2_R3_C1);
                circuit2Table.AddCell(circuit2_R3_C2);
                circuit2Table.AddCell(circuit2_R3_C3);
                circuit2Table.AddCell(circuit2_R3_C4);
                circuit2Table.AddCell(circuit2_R4_C1);
                circuit2Table.AddCell(circuit2_R4_C2);
                circuit2Table.AddCell(circuit2_R4_C3);
                circuit2Table.AddCell(circuit2_R4_C4);
                circuit2Table.AddCell(circuit2_R5_C1);
                circuit2Table.AddCell(circuit2_R5_C2);
                circuit2Table.AddCell(circuit2_R5_C3);
                circuit2Table.AddCell(circuit2_R5_C4);
                circuit2Table.AddCell(circuit2_R6_C1);
                circuit2Table.AddCell(circuit2_R6_C2);
                circuit2Table.AddCell(circuit2_R6_C3);
                circuit2Table.AddCell(circuit2_R6_C4);

                Cell cell12 = new Cell(1, 1).Add(circuit1Table).SetWidth(440).SetBorder(Border.NO_BORDER);
                Cell cell13 = new Cell(1, 1).Add(circuit2Table).SetWidth(440).SetBorder(Border.NO_BORDER);
                Cell cellCM = new Cell(1, 1).Add(measuredPressureTable).SetWidth(440).SetBorder(Border.NO_BORDER);

                circuitsTable.AddCell(cell12);
                if (mainBO.ProductType.ToString() == "Horizon")
                {
                    circuitsTable.AddCell(cell13);
                }
                else if(mainBO.ProductType.ToString() == "MixedAir")
                {
                    circuitsTable.AddCell(cellCM);
                }

                Cell cell14 = new Cell(1, 1).Add(circuitsTable).SetWidth(440).SetBorder(Border.NO_BORDER);
                Cell cell15 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell cell16 = new Cell(1, 1).Add(heatMeasuredPressure).SetWidth(390).SetBorder(Border.NO_BORDER);

                tb_4.AddCell(cell14);
                tb_4.AddCell(cell15);
                tb_4.AddCell(cell16);

                document.Add(tb_4);
            }

            {//tb_5
                document.Add(new AreaBreak(AreaBreakType.NEXT_PAGE));

                Table tb_5 = new Table(3, false);

                Table blowerStandardEquipmentTable = new Table(1, false);

                Table blowerTable = new Table(3, false);

                Cell cellBlower = new Cell(1, 3).SetWidth(415)
                 .Add(new Paragraph("Blowers")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell BlowersR_1_C1 = new Cell(1, 1).SetWidth(175).SetBackgroundColor(color);
                Cell BlowersR_1_C3 = new Cell(1, 1).SetWidth(120)
                 .Add(new Paragraph("Indoor")).SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(color);
                Cell BlowersR_1_C5 = new Cell(1, 1).SetWidth(120)
                 .Add(new Paragraph("Exhaust")).SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(color);

                blowerTable.AddCell(cellBlower);
                blowerTable.AddCell(BlowersR_1_C1);
                blowerTable.AddCell(BlowersR_1_C3);
                blowerTable.AddCell(BlowersR_1_C5);

                foreach (var blower in mainBO.Blowers)
                {
                    Cell BlowersR_2_C1 = new Cell(1, 1).SetWidth(175)
                        .Add(new Paragraph(blower.BlowerProperty));
                    Cell BlowersR_2_C3 = new Cell(1, 1).SetWidth(120)
                        .Add(new Paragraph(blower.IndoorVal.ToString())).SetTextAlignment(TextAlignment.RIGHT);
                    Cell BlowersR_2_C5 = new Cell(1, 1).SetWidth(120)
                        .Add(new Paragraph(blower.ExhaustVal.ToString())).SetTextAlignment(TextAlignment.RIGHT);

                    blowerTable.AddCell(BlowersR_2_C1);
                    blowerTable.AddCell(BlowersR_2_C3);
                    blowerTable.AddCell(BlowersR_2_C5);
                }

                Cell BlowersR_9_C1 = new Cell(1, 1).SetWidth(175)
                 .Add(new Paragraph("Model number"));
                Cell BlowersR_9_C3 = new Cell(1, 1).SetWidth(120)
                    .Add(new Paragraph(mainBO.IndoorBlowerModel)).SetTextAlignment(TextAlignment.RIGHT);
                Cell BlowersR_9_C5 = new Cell(1, 1).SetWidth(120)
                    .Add(new Paragraph(mainBO.ExhaustBlowerModel)).SetTextAlignment(TextAlignment.RIGHT);

                Cell BlowersR_10_C1 = new Cell(1, 1).SetWidth(175)
                 .Add(new Paragraph("Double Blower"));
                Cell BlowersR_10_C3 = new Cell(1, 1).SetWidth(120)
                    .Add(new Paragraph(mainBO.IndoorBlowerType.ToString())).SetTextAlignment(TextAlignment.CENTER);
                Cell BlowersR_10_C5 = new Cell(1, 1).SetWidth(120)
                    .Add(new Paragraph(mainBO.ExhaustBlowerType.ToString())).SetTextAlignment(TextAlignment.CENTER);

                blowerTable.AddCell(BlowersR_9_C1);
                blowerTable.AddCell(BlowersR_9_C3);
                blowerTable.AddCell(BlowersR_9_C5);
                blowerTable.AddCell(BlowersR_10_C1);
                blowerTable.AddCell(BlowersR_10_C3);
                blowerTable.AddCell(BlowersR_10_C5);


                Table standardEquipmentTable = new Table(3, false).SetMarginTop(18);
                Cell cellStandardEquipment = new Cell(1, 3).SetWidth(415)
                .Add(new Paragraph("Standard Equipment")).SetBold().SetBackgroundColor(headerColor)
                .SetTextAlignment(TextAlignment.CENTER);

                Cell StandardEquipment_R1_C1 = new Cell(1, 1).SetWidth(175).SetBackgroundColor(color);
                Cell StandardEquipment_R1_C2 = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph("Installed")).SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(color);
                Cell StandardEquipment_R1_C4 = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph("Tested")).SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(color);

                standardEquipmentTable.AddCell(cellStandardEquipment);
                standardEquipmentTable.AddCell(StandardEquipment_R1_C1);
                standardEquipmentTable.AddCell(StandardEquipment_R1_C2);
                standardEquipmentTable.AddCell(StandardEquipment_R1_C4);

                foreach (var standardEquip in mainBO.StandardEquipments)
                {
                    Cell StandardEquipment_R2_C1 = new Cell(1, 1).SetWidth(175)
                         .Add(new Paragraph(standardEquip.Optionlabel));
                    Cell StandardEquipment_R2_C2 = new Cell(1, 1).SetWidth(120)
                         .Add(new Paragraph((standardEquip.InstalledType == true) ? "Yes" : "No")).SetTextAlignment(TextAlignment.CENTER);
                    Cell StandardEquipment_R2_C4 = new Cell(1, 1).SetWidth(120)
                        .Add(new Paragraph((standardEquip.TestedType == true) ? "Yes" : "No")).SetTextAlignment(TextAlignment.CENTER);
                    standardEquipmentTable.AddCell(StandardEquipment_R2_C1);
                    standardEquipmentTable.AddCell(StandardEquipment_R2_C2);
                    standardEquipmentTable.AddCell(StandardEquipment_R2_C4);
                }

                Cell cell17 = new Cell(1, 1).Add(blowerTable).SetWidth(415).SetBorder(Border.NO_BORDER);
                Cell cell18 = new Cell(1, 1).Add(standardEquipmentTable).SetWidth(415).SetBorder(Border.NO_BORDER);

                blowerStandardEquipmentTable.AddCell(cell17);
                blowerStandardEquipmentTable.AddCell(cell18);

                Table optionsTable = new Table(3, false);

                Cell celloptions = new Cell(1, 3).SetWidth(415)
                 .Add(new Paragraph("Options")).SetBold().SetBackgroundColor(headerColor)
                 .SetTextAlignment(TextAlignment.CENTER);

                Cell options_R1_C1 = new Cell(1, 1).SetWidth(175).SetBackgroundColor(color);
                Cell options_R1_C2 = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph("Installed")).SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(color);
                Cell options_R1_C4 = new Cell(1, 1).SetWidth(120)
                  .Add(new Paragraph("Tested")).SetTextAlignment(TextAlignment.CENTER).SetBackgroundColor(color);

                optionsTable.AddCell(celloptions);
                optionsTable.AddCell(options_R1_C1);
                optionsTable.AddCell(options_R1_C2);
                optionsTable.AddCell(options_R1_C4);

                foreach (var option in mainBO.Options)
                {
                    Cell options_R2_C1 = new Cell(1, 1).SetWidth(175)
                        .Add(new Paragraph(option.Optionlabel));
                    Cell options_R2_C2 = new Cell(1, 1).SetWidth(120)
                        .Add(new Paragraph((option.InstalledType == true) ? "Yes" : "No")).SetTextAlignment(TextAlignment.CENTER);
                    Cell options_R2_C4 = new Cell(1, 1).SetWidth(120)
                        .Add(new Paragraph((option.TestedType == true) ? "Yes" : "No")).SetTextAlignment(TextAlignment.CENTER);
                    optionsTable.AddCell(options_R2_C1);
                    optionsTable.AddCell(options_R2_C2);
                    optionsTable.AddCell(options_R2_C4);
                }

                Cell cell19 = new Cell(1, 1).Add(blowerStandardEquipmentTable).SetWidth(415).SetBorder(Border.NO_BORDER);
                Cell cell20 = new Cell(1, 1).SetPaddingRight(20).SetBorder(Border.NO_BORDER);
                Cell cell21 = new Cell(1, 1).Add(optionsTable).SetWidth(415).SetBorder(Border.NO_BORDER);
                
                tb_5.AddCell(cell19);
                tb_5.AddCell(cell20);
                tb_5.AddCell(cell21);

                document.Add(tb_5);

            }

        }
        #endregion
       
    }
}
