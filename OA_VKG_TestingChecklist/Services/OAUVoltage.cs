﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.Services
{
    class OAUVoltage
    {
        public string Voltage { get; set; }
        public string VoltageL1L2 { get; set; }
        public string VoltageL2L3 { get; set; }
        public string VoltageL1L3 { get; set; }
        public string VoltageL1G { get; set; }
        public string VoltageL2G { get; set; }
        public string VoltageL3G { get; set; }
    }
}
