﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.Services
{
    class OAUTPrePowerCheckslist
    {
        private string _Power_whip_over_tight;
        public string Power_whip_over_tight
        {
            get
            {
                return this._Power_whip_over_tight;
            }
            set
            {
                this._Power_whip_over_tight = value;
            }
        }

        private string _Correct_size_whip_unit;
        public string Correct_size_whip_unit
        {
            get
            {
                return this._Correct_size_whip_unit;
            }
            set
            {
                this._Correct_size_whip_unit = value;
            }
        }

        private string _Attach_guages_unit;
        public string Attach_guages_unit
        {
            get
            {
                return this._Attach_guages_unit;
            }
            set
            {
                this._Attach_guages_unit = value;
            }
        }

        private string _Co_Power_Ground_purp;
        public string Co_Power_Ground_purp
        {
            get
            {
                return this._Co_Power_Ground_purp;
            }
            set
            {
                this._Co_Power_Ground_purp = value;
            }
        }

        private string _Cont_High_volt_term;
        public string Cont_High_volt_term
        {
            get
            {
                return this._Cont_High_volt_term;
            }
            set
            {
                this._Cont_High_volt_term = value;
            }
        }

        private string _Check_Phases_fan_motor;
        public string Check_Phases_fan_motor
        {
            get
            {
                return this._Check_Phases_fan_motor;
            }
            set
            {
                this._Check_Phases_fan_motor = value;
            }
        }

        private string _Remove_volt_Sen_Sensor;
        public string Remove_volt_Sen_Sensor
        {
            get
            {
                return this._Remove_volt_Sen_Sensor;
            }
            set
            {
                this._Remove_volt_Sen_Sensor = value;
            }
        }

        private string _Hi_pot_unit_fans;
        public string Hi_pot_unit_fans
        {
            get
            {
                return this._Hi_pot_unit_fans;
            }
            set
            {
                this._Hi_pot_unit_fans = value;
            }
        }

        private string _Inspect_ARC_flash;
        public string Inspect_ARC_flash
        {
            get
            {
                return this._Inspect_ARC_flash;
            }
            set
            {
                this._Inspect_ARC_flash = value;
            }
        }

        private string _Check_Power_term_block;
        public string Check_Power_term_block
        {
            get
            {
                return this._Check_Power_term_block;
            }
            set
            {
                this._Check_Power_term_block = value;
            }
        }

        private string _Inspect_element_heater;
        public string Inspect_element_heater
        {
            get
            {
                return this._Inspect_element_heater;
            }
            set
            {
                this._Inspect_element_heater = value;
            }
        }

        private string _Check_heater_cont_board;
        public string Check_heater_cont_board
        {
            get
            {
                return this._Check_heater_cont_board;
            }
            set
            {
                this._Check_heater_cont_board = value;
            }
        }

        private string _Co_volt_writtn_transform;
        public string Co_volt_writtn_transform
        {
            get
            {
                return this._Co_volt_writtn_transform;
            }
            set
            {
                this._Co_volt_writtn_transform = value;
            }
        }

        private string _Connect_gas_manifold;
        public string Connect_gas_manifold
        {
            get
            {
                return this._Connect_gas_manifold;
            }
            set
            {
                this._Connect_gas_manifold = value;
            }
        }

        private string _Unlock_gas_valves;
        public string Unlock_gas_valves
        {
            get
            {
                return this._Unlock_gas_valves;
            }
            set
            {
                this._Unlock_gas_valves = value;
            }
        }

        private string _Turn_on_gas;
        public string Turn_on_gas
        {
            get
            {
                return this._Turn_on_gas;
            }
            set
            {
                this._Turn_on_gas = value;
            }
        }

        private string _En_field_gas_port;
        public string En_field_gas_port
        {
            get
            {
                return this._En_field_gas_port;
            }
            set
            {
                this._En_field_gas_port = value;
            }
        }

        private string _In_Plug_are_Missing;
        public string In_Plug_are_Missing
        {
            get
            {
                return this._In_Plug_are_Missing;
            }
            set
            {
                this._In_Plug_are_Missing = value;
            }
        }

        private string _Turn_off_gas_inspection;
        public string Turn_off_gas_inspection
        {
            get
            {
                return this._Turn_off_gas_inspection;
            }
            set
            {
                this._Turn_off_gas_inspection = value;
            }
        }

        private string _Bac_Air_proving_switches;
        public string Bac_Air_proving_switches
        {
            get
            {
                return this._Bac_Air_proving_switches;
            }
            set
            {
                this._Bac_Air_proving_switches = value;
            }
        }

        private string _Install_TD7;
        public string Install_TD7
        {
            get
            {
                return this._Install_TD7;
            }
            set
            {
                this._Install_TD7 = value;
            }
        }

        private string _Program_UC600;
        public string Program_UC600
        {
            get
            {
                return this._Program_UC600;
            }
            set
            {
                this._Program_UC600 = value;
            }
        }

        private string _Set_Phase_Monitor;
        public string Set_Phase_Monitor
        {
            get
            {
                return this._Set_Phase_Monitor;
            }
            set
            {
                this._Set_Phase_Monitor = value;
            }
        }

        private string _Unlock_disconnect_k_box;
        public string Unlock_disconnect_k_box
        {
            get
            {
                return this._Unlock_disconnect_k_box;
            }
            set
            {
                this._Unlock_disconnect_k_box = value;
            }
        }

        private string _Un_Correct_volt_disconn;
        public string Un_Correct_volt_disconn
        {
            get
            {
                return this._Un_Correct_volt_disconn;
            }
            set
            {
                this._Un_Correct_volt_disconn = value;
            }
        }
        private string _PrePowerMiscNotes;
        public string PrePowerMiscNotes
        {
            get
            {
                return this._PrePowerMiscNotes;
            }
            set
            {

                this._PrePowerMiscNotes = value;

            }
        }

    }
}
