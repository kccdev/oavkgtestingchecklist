﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using OA_VKG_TestingChecklist.UserControlObject;

namespace OA_VKG_TestingChecklist
{
    class MainBO : MainDTO
    {        
        #region "Get Methods"   
        public DataTable GetOA_VKG_JobNumbers(string unitType)
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

               
                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetOA_VKG_JobList", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@UnitType", unitType);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }    


        public DataTable GetBlowerData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetBlowerData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCondenserData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetCondenserData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCompressorData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetCompressorData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetMeasurePressureData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetMeasurePressure", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        public DataTable GetPreChecksOption()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    String query = "Select * from OAUT_TC_PreChecksOption";
                    using (SqlCommand cmd = new SqlCommand(query, connection))
                    {
                        cmd.CommandText = query;
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
         catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
            public DataTable GetFilterData()
            {
                DataTable dt = new DataTable();

                try
                {
                    var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                    using (var connection = new SqlConnection(connectionString))
                    {
                        using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetFilterData", connection))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@HeadID", HeadID);
                            using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                            {
                                if (da.SelectCommand.Connection.State != ConnectionState.Open)
                                {
                                    da.Fill(dt);
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return dt;
            }
        

        public DataTable GetPreChecksData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetPreChecks", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetPrePowerChecksData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetPrePowerChecks", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }


        public DataTable GetHeatData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetHeatData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetOptionsData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetOptionsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetVoltageData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetVoltageData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        public DataTable GetCircuitData()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetCircuitData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }
        #endregion

        #region "Insert Methods"
        public string InsertJobHead()
        {
            string HeadID_Str = "-1";

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertJobHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@Serialnum", Serialnum);
                        cmd.Parameters.AddWithValue("@Modelnum", Modelnum);
                        cmd.Parameters.AddWithValue("@DateTested", DateTested);
                        cmd.Parameters.AddWithValue("@HorizonType", HorizonType);
                        cmd.Parameters.AddWithValue("@Line", Line);
                        cmd.Parameters.AddWithValue("@TestedBy", TestedBy);
                        cmd.Parameters.AddWithValue("@AC_Temp", AC_Temp);
                        cmd.Parameters.AddWithValue("@AC_Humidity", AC_Humidity);
                        cmd.Parameters.AddWithValue("@Disconnect_Type", Disconnect_Type);
                        cmd.Parameters.AddWithValue("@Disconnect_Size", Disconnect_Size);
                        cmd.Parameters.AddWithValue("@Charge_Circuit_1", Charge_Circuit_1);
                        cmd.Parameters.AddWithValue("@Charge_Circuit_2", Charge_Circuit_2);
                        cmd.Parameters.AddWithValue("@Charge_Circuit_1_var", Charge_Circuit_1_Var);
                        cmd.Parameters.AddWithValue("@Charge_Circuit_2_var", Charge_Circuit_2_Var);
                        cmd.Parameters.AddWithValue("@ERV_RatedAmp", ERV_Rated_Amp);
                        cmd.Parameters.AddWithValue("@ERV_RunningAmp", ERV_Running_Amp);
                        cmd.Parameters.AddWithValue("@ERV_Proper_Rotation", ERV_Proper_Rotation);
                        cmd.Parameters.AddWithValue("@MiscNotes", MiscNotes);
                        cmd.Parameters.AddWithValue("@ERV_ModelNo", ERV_ModelNo);
                        cmd.Parameters.AddWithValue("@ERV_SerialNo", ERV_SerialNo);
                        cmd.Parameters.Add("@ID", SqlDbType.BigInt);
                        cmd.Parameters["@ID"].Direction = ParameterDirection.Output;
                        connection.Open();
                        cmd.ExecuteNonQuery();
                        HeadID_Str = cmd.Parameters["@ID"].Value.ToString();                        
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return HeadID_Str;
        }

        public void InsertBlowerData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertBlowerData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@BlowerType", BlowerType);
                        cmd.Parameters.AddWithValue("@HP", BlwHP);
                        cmd.Parameters.AddWithValue("@RatedAmp", BlwRatedAmp);
                        cmd.Parameters.AddWithValue("@RunningAmp", BlwRunningAmp);
                        cmd.Parameters.AddWithValue("@WheelSize", BlwWheelSize);
                        cmd.Parameters.AddWithValue("@MaxHz", BlwMaxHz);
                        cmd.Parameters.AddWithValue("@MinHz", BlwMinHz);
                        cmd.Parameters.AddWithValue("@VoltageOutput", BlwVoltageOutput);
                        cmd.Parameters.AddWithValue("@ModelNum", BlwModelNum);
                        cmd.Parameters.AddWithValue("@SerialNum", BlwSerialnum);
                        cmd.Parameters.AddWithValue("@DoubleBlower", BlwDoubleBlower);                      

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public void InsertCircuitData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertCircuitData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@CircuitNum", CircuitNum);
                        cmd.Parameters.AddWithValue("@CircuitValueType", CircuitValueType);
                        cmd.Parameters.AddWithValue("@CircuitValue", CircuitValue);
                       
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertMeasurePressure()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertMeasurePressure", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        cmd.Parameters.AddWithValue("@InletModulationPressure", InletModulationPressure);
                        cmd.Parameters.AddWithValue("@LowFire", LowFire);
                        cmd.Parameters.AddWithValue("@HighFire", HighFire);
                       

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertCompressorData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertCompressorData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@CompressorNum", CompNum);
                        cmd.Parameters.AddWithValue("@ModelNum", CompModelNum);
                        cmd.Parameters.AddWithValue("@SerialNum", CompSerialNum);
                        cmd.Parameters.AddWithValue("@RatedAmps", CompRatedAmps);
                        cmd.Parameters.AddWithValue("@RunningAmps", CompRunningAmps);

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertCondenserData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertCondenserData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@CondenserNum", CondNum);
                        cmd.Parameters.AddWithValue("@ModelNum", CondModelNum);
                        cmd.Parameters.AddWithValue("@SerialNum", CondSerialNum);
                        cmd.Parameters.AddWithValue("@RatedAmps", CondRatedAmps);
                        cmd.Parameters.AddWithValue("@RunningAmps", CondRunningAmps);

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertFilterData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertFilterData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@FilterSize", FilterSize);
                        cmd.Parameters.AddWithValue("@FilterStyle", FilterStyle);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@PartNum", FilterPartNum);

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertPreChecksData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertPreChecksData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@Verify_sub_un_match", PreChecklist.Verify_sub_un_match);
                        cmd.Parameters.AddWithValue("@Unit_built_compl_CCS", PreChecklist.Unit_built_compl_CCS);
                        cmd.Parameters.AddWithValue("@Inspect_unit_items", PreChecklist.Inspect_unit_items);
                       

                        cmd.Parameters.AddWithValue("@Power_wire_driver", PreChecklist.Power_wire_driver);
                        cmd.Parameters.AddWithValue("@Line_Load_term_Correctly", PreChecklist.Line_Load_term_Correctly);
                        cmd.Parameters.AddWithValue("@Vis_term_prop_connector", PreChecklist.Vis_term_prop_connector);
                        

                        cmd.Parameters.AddWithValue("@Verify_plug_harn_correct", PreChecklist.Verify_plug_harn_correct);
                       
                        cmd.Parameters.AddWithValue("@Crankcase_correct_volt", PreChecklist.Crankcase_correct_volt);

                        cmd.Parameters.AddWithValue("@Misc_notes", PreChecklist.PreChecksMiscNotes);
                        cmd.Parameters.AddWithValue("@Power_whip_over_tight", PreChecklist.Power_whip_over_tight);
                        cmd.Parameters.AddWithValue("@Correct_size_whip_unit", PreChecklist.Correct_size_whip_unit);
                        cmd.Parameters.AddWithValue("@Co_Power_Ground_purp", PreChecklist.Co_Power_Ground_purp);

                        cmd.Parameters.AddWithValue("@Cont_High_volt_term", PreChecklist.Cont_High_volt_term);
                        cmd.Parameters.AddWithValue("@Check_Phases_fan_motor", PreChecklist.Check_Phases_fan_motor);
                        cmd.Parameters.AddWithValue("@Remove_volt_Sen_Sensor", PreChecklist.Remove_volt_Sen_Sensor);
                        cmd.Parameters.AddWithValue("@Hi_pot_unit_fans", PreChecklist.Hi_pot_unit_fans);

                        cmd.Parameters.AddWithValue("@Check_Power_term_block", PreChecklist.Check_Power_term_block);
                        cmd.Parameters.AddWithValue("@Inspect_element_heater", PreChecklist.Inspect_element_heater);
                        cmd.Parameters.AddWithValue("@Check_heater_cont_board", PreChecklist.Check_heater_cont_board);
                        cmd.Parameters.AddWithValue("@Co_volt_writtn_transform", PreChecklist.Co_volt_writtn_transform);
                        cmd.Parameters.AddWithValue("@En_field_gas_port", PreChecklist.En_field_gas_port);
                        cmd.Parameters.AddWithValue("@In_Plug_are_Missing", PreChecklist.In_Plug_are_Missing);
                        cmd.Parameters.AddWithValue("@Turn_off_gas_inspection", PreChecklist.Turn_off_gas_inspection);
                        cmd.Parameters.AddWithValue("@Set_Phase_Monitor", PreChecklist.Set_Phase_Monitor);
                        cmd.Parameters.AddWithValue("@Un_Correct_volt_disconn", PreChecklist.Un_Correct_volt_disconn);

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void InsertPrePowerChecksData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertPrePowerChecksData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        cmd.Parameters.AddWithValue("@Power_whip_over_tight", OAUTPrePower.Power_whip_over_tight);
                        cmd.Parameters.AddWithValue("@Correct_size_whip_unit", OAUTPrePower.Correct_size_whip_unit);
                        cmd.Parameters.AddWithValue("@Co_Power_Ground_purp", OAUTPrePower.Co_Power_Ground_purp);

                        cmd.Parameters.AddWithValue("@Cont_High_volt_term", OAUTPrePower.Cont_High_volt_term);
                        cmd.Parameters.AddWithValue("@Check_Phases_fan_motor", OAUTPrePower.Check_Phases_fan_motor);
                        cmd.Parameters.AddWithValue("@Remove_volt_Sen_Sensor", OAUTPrePower.Remove_volt_Sen_Sensor);
                        cmd.Parameters.AddWithValue("@Hi_pot_unit_fans", OAUTPrePower.Hi_pot_unit_fans);

                        cmd.Parameters.AddWithValue("@Check_Power_term_block", OAUTPrePower.Check_Power_term_block);
                        cmd.Parameters.AddWithValue("@Inspect_element_heater", OAUTPrePower.Inspect_element_heater);
                        cmd.Parameters.AddWithValue("@Check_heater_cont_board", OAUTPrePower.Check_heater_cont_board);
                        cmd.Parameters.AddWithValue("@Co_volt_writtn_transform", OAUTPrePower.Co_volt_writtn_transform);
                        cmd.Parameters.AddWithValue("@En_field_gas_port", OAUTPrePower.En_field_gas_port);
                        cmd.Parameters.AddWithValue("@In_Plug_are_Missing", OAUTPrePower.In_Plug_are_Missing);
                        cmd.Parameters.AddWithValue("@Turn_off_gas_inspection", OAUTPrePower.Turn_off_gas_inspection);
                        cmd.Parameters.AddWithValue("@Set_Phase_Monitor", OAUTPrePower.Set_Phase_Monitor);
                        cmd.Parameters.AddWithValue("@Un_Correct_volt_disconn", OAUTPrePower.Un_Correct_volt_disconn);
                        cmd.Parameters.AddWithValue("@Misc_notes", OAUTPrePower.PrePowerMiscNotes);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public void InsertHeatData()
        {
            
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertHeatData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@Heat_Primary", Heat_Primary);
                        cmd.Parameters.AddWithValue("@Heat_Item_num", Heat_Item_num);
                        cmd.Parameters.AddWithValue("@Heat_PO_num", Heat_PO_num);
                        cmd.Parameters.AddWithValue("@Heat_model_num_Top", Heat_model_num_Top);

                        cmd.Parameters.AddWithValue("@Heat_Serial_Top", Heat_Serial_Top);
                        cmd.Parameters.AddWithValue("@Heat_Partnum_Top", Heat_Partnum_Top);
                        cmd.Parameters.AddWithValue("@Heat_Size_Top", Heat_Size_Top);
                        cmd.Parameters.AddWithValue("@Heat_Rated_Amp", Heat_Rated_Amp);
                        cmd.Parameters.AddWithValue("@Heat_Running_Amp", Heat_Running_Amp);

                        cmd.Parameters.AddWithValue("@TopBottom", TopBottom);
                        cmd.Parameters.AddWithValue("@Heat_model_num_Bottom", Heat_model_num_Bottom);
                        cmd.Parameters.AddWithValue("@Heat_Serial_Bottom", Heat_Serial_Bottom);
                        cmd.Parameters.AddWithValue("@Heat_Partnum_Bottom", Heat_Partnum_Bottom);
                        cmd.Parameters.AddWithValue("@Heat_Size_Bottom", Heat_Size_Bottom);

                        cmd.Parameters.AddWithValue("@Heat_model_num_PreHeat", Heat_model_num_PreHeat);
                        cmd.Parameters.AddWithValue("@Heat_Serial_PreHeat", Heat_Serial_PreHeat);
                        cmd.Parameters.AddWithValue("@Heat_RatedAmp_PreHeat", Heat_RatedAmp_PreHeat);
                        cmd.Parameters.AddWithValue("@Heat_RunningAmp_PreHeat", Heat_RunningAmp_PreHeat);
                        cmd.Parameters.AddWithValue("@Heat_Size_PreHeat", Heat_Size_PreHeat);
                        cmd.Parameters.AddWithValue("@Heat_FuelType", Heat_FuelType);
                      
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertOptionsData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertOptionsData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@OptionName", OptionName);
                        cmd.Parameters.AddWithValue("@OptionInstalled", OptionInstalled);
                        cmd.Parameters.AddWithValue("@OptionTested", OptionTested);
                       
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void InsertVoltageData()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertVoltageData", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        cmd.Parameters.AddWithValue("@Voltage", Voltage);
                        cmd.Parameters.AddWithValue("@Voltage_Value", Voltage_Value);
                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public void DeleteChecklistByHeadID()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_DeleteChecklistByHeadID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@Head_ID", HeadID);
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public DataTable SearchJobHead()
        {
            DataTable dt = new DataTable();

            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetOA_VKG_SearchJobHead", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        #region Issues SQL

        public DataTable GetJobIssues()
        {
            DataTable dt_Results = new DataTable();
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_GetJobIssues", connection))
                    {
                        cmd.CommandTimeout = 120;
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);                         
                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            if (da.SelectCommand.Connection.State != ConnectionState.Open)
                            {
                                da.Fill(dt_Results);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt_Results;
        }

        public void InsertIssues()
        {
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_InsertIssues", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);
                        cmd.Parameters.AddWithValue("@JobNum", JobNum);
                        cmd.Parameters.AddWithValue("@IssueTypeCode", IssueTypeCode);
                        cmd.Parameters.AddWithValue("@IssueSource", IssueSource);
                        cmd.Parameters.AddWithValue("@IssueDescription", IssueDescription);                        
                        cmd.Parameters.AddWithValue("@Notified", Notified);
                        cmd.Parameters.AddWithValue("@ImpactTime", ImpactTime);
                        cmd.Parameters.AddWithValue("@IssueCodes", IssueCodes);

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int UpdateIssues()
        {
            int errorCode = 0;
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_UpdateIssues", connection))
                    {                        
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.Parameters.AddWithValue("@IssueTypeCode", IssueTypeCode);
                        cmd.Parameters.AddWithValue("@IssueSource", IssueSource);
                        cmd.Parameters.AddWithValue("@IssueDescription", IssueDescription);                       
                        cmd.Parameters.AddWithValue("@Notified", Notified);
                        cmd.Parameters.AddWithValue("@ImpactTime", ImpactTime);
                        cmd.Parameters.AddWithValue("@IssueCodes", IssueCodes);

                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = -1;
                throw ex;
            }
            return errorCode;
        }

        public int UpdateIssuesHeadID()
        {
            int errorCode = 0;
            try
            {
                var connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString;

                using (var connection = new SqlConnection(connectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("KCC.OAUT_TC_UpdateIssuesHeadID", connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.Parameters.AddWithValue("@HeadID", HeadID);                        
                        connection.Open();
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                errorCode = -1;
                throw ex;
            }
            return errorCode;
        }        
        #endregion

    }
}
