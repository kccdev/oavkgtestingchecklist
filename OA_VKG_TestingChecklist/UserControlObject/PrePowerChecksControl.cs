﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OA_VKG_TestingChecklist.Services;
using OA_VKG_TestingChecklist.Extensions;
using OA_VKG_TestingChecklist.Common;

namespace OA_VKG_TestingChecklist.UserControlObject
{
    public partial class PrePowerChecksControl : UserControl
    {
        #region "Data Members"
        MainBO objMain = new MainBO();
        bool IsValidated = true;
        #endregion
       
        public PrePowerChecksControl()
        {
            InitializeComponent();
        }
        #region "Public Methods"
        public void ConfigureAndPopulateControls()
        {
            DataTable dtPreChecks = objMain.GetPreChecksOption();

            if (dtPreChecks.Rows.Count > 0)
            {

                this.FindControls(typeof(ComboBox)).ForEach(c =>
                {
                    ComboBox cmb = c as ComboBox;
                    cmb.ValueMember = "OptionText";
                    cmb.DisplayMember = "OptionText";

                    cmb.DataSource = new BindingSource()
                    {
                        DataSource = dtPreChecks
                    };
                    cmb.SelectedIndex = -1;
                });
                cmbHeaterControlBoardPrePowerChecks.SelectedIndex = - 1;
                cmbVoltageOnTransformerPrePowerChecks.SelectedIndex = - 1;
            
                cmbFieldGasPortPrePowerChecks.SelectedIndex = -1;
                cmbPlugMissingPrePowerChecks.SelectedIndex = -1;
                cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = -1;
            }
        }
        public string ValidateConditions()
        {
            StringBuilder errors = new StringBuilder();
            IsValidated = true;
            try
            {
                foreach (Control control in tlpElectricHeat.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;
                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderPrePowerChecks.SetError(combo, "Required!");
                            IsValidated = false;
                        }
                        else
                        {
                            errorProviderPrePowerChecks.SetError(combo, "");
                        }
                    }
                }

                foreach (Control control in tlpPrePowerChecks.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;

                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderPrePowerChecks.SetError(combo, "Required!");
                            IsValidated = false;

                            //return;
                        }
                        else
                        {
                            errorProviderPrePowerChecks.SetError(combo, "");
                        }
                    }
                }

                foreach (Control control in tlpPrePowerFinalSteps.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;

                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderPrePowerChecks.SetError(combo, "Required!");
                            IsValidated = false;

                            //return;
                        }
                        else
                        {
                            errorProviderPrePowerChecks.SetError(combo, "");
                        }
                    }
                }

                foreach (Control control in tlpGasHeat.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;
                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderPrePowerChecks.SetError(combo, "Required!");
                            IsValidated = false;
                        }
                        else
                        {
                            errorProviderPrePowerChecks.SetError(combo, "");
                        }
                    }
                }
                if (!IsValidated)
                {
                    errors.AppendLine("PrePowerChecks:");
                    errors.AppendLine("You must fill the all mandatory fields before saving!");
                }
                else
                {
                    //return "";
                    if (cmbHeaterControlBoardPrePowerChecks.Text.Trim() != null && (cmbHeaterControlBoardPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbHeaterControlBoardPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Fail) || cmbVoltageOnTransformerPrePowerChecks.Text.Trim() != null && (cmbVoltageOnTransformerPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbVoltageOnTransformerPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Fail))
                    {
                        foreach (Control control in tlpGasLeakTest.Controls)
                        {
                            if (control is ComboBox)
                            {
                                ComboBox combo = control as ComboBox;
                                if (string.IsNullOrWhiteSpace(combo.Text))
                                {
                                    combo.Focus();
                                    errorProviderPrePowerChecks.SetError(combo, "Required!");
                                    IsValidated = false;
                                }
                                else
                                {
                                    errorProviderPrePowerChecks.SetError(combo, "");
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (Control control in tlpGasLeakTest.Controls)
                        {
                            if (control is ComboBox)
                            {
                                ComboBox combo = control as ComboBox;
                                if (string.IsNullOrWhiteSpace(combo.Text))
                                {
                                    combo.Focus();
                                    errorProviderPrePowerChecks.SetError(combo, "");

                                }
                                else
                                {
                                    errorProviderPrePowerChecks.SetError(combo, "");
                                }
                            }
                        }
                    }
                    if (!IsValidated)
                    {
                        errors.AppendLine("PrePowerChecks:");
                        errors.AppendLine("You must complete GAS LEAK TEST checks before saving.");
                    }
                    else
                    {
                        errors.Clear();
                    }
                }
                return errors.ToString();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        internal void SetDependencies(MainBO objMain)
        {
            this.objMain = objMain;
        }

        public void LoadPrePowerData(int headID)
        {
            if (headID > 0)
            {
                DataTable dataTable = objMain.GetPrePowerChecksData();
                if (dataTable.Rows.Count > 0)
                {
                    DataRow drPreChecks = dataTable.Rows[0];
                    cmbPowerWhipPrePowerChecks.Text = drPreChecks["Power_whip_over_tight"].ToString();
                    cmbCorrectSizeWhipPrePowerChecks.Text = drPreChecks["Correct_size_whip_unit"].ToString();
                    cmbConnectPowerPrePowerChecks.Text = drPreChecks["Co_Power_Ground_purp"].ToString();
                    cmbCheckContinuityPrePower.Text = drPreChecks["Cont_High_volt_term"].ToString();
                    cmbFanMotorsPrePowerChecks.Text = drPreChecks["Check_Phases_fan_motor"].ToString();
                    cmbCoreSenseSensorPrePowerChecks.Text = drPreChecks["Remove_volt_Sen_Sensor"].ToString();
                    cmbHiPotECMPrePowerChecks.Text = drPreChecks["Hi_pot_unit_fans"].ToString();
                    cmbCheckPowerTerminationPrePowerChecks.Text = drPreChecks["Check_Power_term_block"].ToString();
                    cmbInspectElementsPrePowerChecks.Text = drPreChecks["Inspect_element_heater"].ToString();
                    cmbHeaterControlBoardPrePowerChecks.Text = drPreChecks["Check_heater_cont_board"].ToString();
                    cmbVoltageOnTransformerPrePowerChecks.Text = drPreChecks["Co_volt_writtn_transform"].ToString();

                    cmbFieldGasPortPrePowerChecks.Text = drPreChecks["En_field_gas_port"].ToString();

                    cmbPlugMissingPrePowerChecks.Text = drPreChecks["In_Plug_are_Missing"].ToString();
                    cmbTurnOffGasLeakTestPrePowerChecks.Text = drPreChecks["Turn_off_gas_inspection"].ToString();

                    cmbSetPhaseMonitorPrePowerChecks.Text = drPreChecks["Set_Phase_Monitor"].ToString();
                    cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Text = drPreChecks["Un_Correct_volt_disconn"].ToString();
                    tbPrePowerMiscNotes.Text = drPreChecks["Misc_Notes"].ToString();



                }
            }
        }

        public void BindPrePowerChecksDataAtPrintEvent()
        {
            objMain.OAUTPrePower = new OAUTPrePowerCheckslist();
            objMain.OAUTPrePower.Power_whip_over_tight = cmbPowerWhipPrePowerChecks.Text;
            objMain.OAUTPrePower.Correct_size_whip_unit = cmbCorrectSizeWhipPrePowerChecks.Text;
           
            objMain.OAUTPrePower.Co_Power_Ground_purp = cmbConnectPowerPrePowerChecks.Text;
            objMain.OAUTPrePower.Cont_High_volt_term = cmbCheckContinuityPrePower.Text;
            objMain.OAUTPrePower.Check_Phases_fan_motor = cmbFanMotorsPrePowerChecks.Text;
            objMain.OAUTPrePower.Remove_volt_Sen_Sensor = cmbCoreSenseSensorPrePowerChecks.Text;
            objMain.OAUTPrePower.Hi_pot_unit_fans = cmbHiPotECMPrePowerChecks.Text;
          
            objMain.OAUTPrePower.Check_Power_term_block = cmbCheckPowerTerminationPrePowerChecks.Text;
            objMain.OAUTPrePower.Inspect_element_heater = cmbInspectElementsPrePowerChecks.Text;
            objMain.OAUTPrePower.Check_heater_cont_board = cmbHeaterControlBoardPrePowerChecks.Text;
            objMain.OAUTPrePower.Co_volt_writtn_transform = cmbVoltageOnTransformerPrePowerChecks.Text;
           
            objMain.OAUTPrePower.En_field_gas_port = cmbFieldGasPortPrePowerChecks.Text;
            objMain.OAUTPrePower.In_Plug_are_Missing = cmbPlugMissingPrePowerChecks.Text;
            objMain.OAUTPrePower.Turn_off_gas_inspection = cmbTurnOffGasLeakTestPrePowerChecks.Text;
           
            objMain.OAUTPrePower.Set_Phase_Monitor = cmbSetPhaseMonitorPrePowerChecks.Text;
           
            objMain.OAUTPrePower.Un_Correct_volt_disconn = cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Text;
        }


        //public void BindPreChecksDataFormToModel()
        //{
        //    objMain.OAUTPrePower = new OAUTPrePowerCheckslist();
        //    objMain.OAUTPrePower.Power_whip_over_tight = cmbPowerWhipPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Correct_size_whip_unit = cmbCorrectSizeWhipPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Co_Power_Ground_purp = cmbConnectPowerPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Cont_High_volt_term = cmbCheckContinuityPrePower.Text;
        //    objMain.OAUTPrePower.Check_Phases_fan_motor = cmbFanMotorsPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Remove_volt_Sen_Sensor = cmbCoreSenseSensorPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Hi_pot_unit_fans = cmbHiPotECMPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Check_Power_term_block = cmbCheckPowerTerminationPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Inspect_element_heater = cmbInspectElementsPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Check_heater_cont_board = cmbHeaterControlBoardPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Co_volt_writtn_transform = cmbVoltageOnTransformerPrePowerChecks.Text;
        //    objMain.OAUTPrePower.En_field_gas_port = cmbFieldGasPortPrePowerChecks.Text;
        //    objMain.OAUTPrePower.In_Plug_are_Missing = cmbPlugMissingPrePowerChecks.Text;
        //    objMain.OAUTPrePower.Turn_off_gas_inspection = cmbTurnOffGasLeakTestPrePowerChecks.Text;

        //    objMain.OAUTPrePower.Set_Phase_Monitor = cmbSetPhaseMonitorPrePowerChecks.Text;
          //  objMain.OAUTPrePower.Un_Correct_volt_disconn = cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Text;
        //    objMain.OAUTPrePower.PrePowerMiscNotes = tbPrePowerMiscNotes.Text;

        //    objMain.InsertPrePowerChecksData();

        //}
        #endregion
        #region "Private Methods"
        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 'P' || e.KeyChar == 'N' || e.KeyChar == 'F' || e.KeyChar == 8 ||
            e.KeyChar == 'p' || e.KeyChar == 'n' || e.KeyChar == 'f')
            {
                if (e.KeyChar == 'p' || e.KeyChar == 'n' || e.KeyChar == 'f')
                {
                    e.KeyChar -= (char)32;
                }

            }
            else
            {
                e.Handled = true;

            }
        }
        #endregion
        #region "Control Events"
        private void cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

            if (cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null && (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbCheckPowerTerminationPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbInspectElementsPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                //cmbConnectGasToManifoldPrePowerChecks.SelectedIndex = -1;
                //cmbUnlockPrePowerChecksGasValves.SelectedIndex = -1;
                //cmbTurnOnGasPrePowerChecks.SelectedIndex = -1;
                //cmbFieldGasPortPrePowerChecks.SelectedIndex = -1;
                //cmbPlugMissingPrePowerChecks.SelectedIndex = -1;
                //cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = -1;
            }
            else if(cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
                ) && cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null && 
                (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
                ))
            {
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;

                //cmbInspectElementsPrePowerChecks.ResetText();
                //cmbCheckPowerTerminationPrePowerChecks.ResetText();
                //cmbConnectGasToManifoldPrePowerChecks.ResetText();
                //cmbUnlockPrePowerChecksGasValves.ResetText();
                //cmbTurnOnGasPrePowerChecks.ResetText();
                //cmbFieldGasPortPrePowerChecks.ResetText();
                //cmbPlugMissingPrePowerChecks.ResetText();
                //cmbTurnOffGasLeakTestPrePowerChecks.ResetText();
            }
            else
            {
                cmbFieldGasPortPrePowerChecks.ResetText();
                cmbPlugMissingPrePowerChecks.ResetText();
                cmbTurnOffGasLeakTestPrePowerChecks.ResetText();
            }
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);


        }

        private void cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

            if (cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null && (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbCheckPowerTerminationPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbInspectElementsPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                //cmbConnectGasToManifoldPrePowerChecks.SelectedIndex = -1;
                //cmbUnlockPrePowerChecksGasValves.SelectedIndex = -1;
                //cmbTurnOnGasPrePowerChecks.SelectedIndex = -1;
                //cmbFieldGasPortPrePowerChecks.SelectedIndex = -1;
                //cmbPlugMissingPrePowerChecks.SelectedIndex = -1;
                //cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = -1;
            }
            else if(cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null && 
                (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
                ) && cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && 
                (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
               ))
            {
                //cmbCheckPowerTerminationPrePowerChecks.ResetText();
                //cmbInspectElementsPrePowerChecks.ResetText();
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
            }
            else {
                cmbFieldGasPortPrePowerChecks.ResetText();
                cmbPlugMissingPrePowerChecks.ResetText();
                cmbTurnOffGasLeakTestPrePowerChecks.ResetText();
            }
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

        }

        private void cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

            if (cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbCheckPowerTerminationPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbInspectElementsPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbHeaterControlBoardPrePowerChecks.SelectedIndex=cmbHeaterControlBoardPrePowerChecks.Items.Count-1;
                cmbVoltageOnTransformerPrePowerChecks.SelectedIndex = cmbVoltageOnTransformerPrePowerChecks.Items.Count - 1;
                cmbFieldGasPortPrePowerChecks.SelectedIndex = cmbFieldGasPortPrePowerChecks.Items.Count - 1;
                cmbPlugMissingPrePowerChecks.SelectedIndex = cmbPlugMissingPrePowerChecks.Items.Count - 1;
                cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = cmbTurnOffGasLeakTestPrePowerChecks.Items.Count - 1;

            }
            else if(cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
                ) &&
                cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
                ))
            {
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
            }
            else
            {
            }
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
           this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);
        }

        private void cmbInspectElementsPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);

            if (cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbInspectElementsPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbCheckPowerTerminationPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbHeaterControlBoardPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbVoltageOnTransformerPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbFieldGasPortPrePowerChecks.SelectedIndex = cmbFieldGasPortPrePowerChecks.Items.Count - 1;
                cmbPlugMissingPrePowerChecks.SelectedIndex = cmbPlugMissingPrePowerChecks.Items.Count - 1;
                cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = cmbTurnOffGasLeakTestPrePowerChecks.Items.Count - 1;

            }
            else if(cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
               ) && cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
                ))
            {
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
            }
            else
            {
            }
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
        }
        private void btnPrintPrePowerChecks_Click(object sender, EventArgs e)
        {
            BindPrePowerChecksDataAtPrintEvent();
            ExportServices exportServices = new ExportServices();
            try
            {
                string reportsPath = $"{Application.StartupPath}\\Reports"; string filePath = $"{Application.StartupPath}\\Reports\\PrePowerChecks_{Guid.NewGuid().ToString()}.pdf";
                if (!System.IO.Directory.Exists(reportsPath))
                {
                    System.IO.Directory.CreateDirectory(reportsPath);
                }
                exportServices.ExportPrePowerChecks(objMain, filePath);
                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        #endregion




    }
}
