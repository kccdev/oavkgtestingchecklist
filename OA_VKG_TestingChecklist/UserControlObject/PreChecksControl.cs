﻿using OA_VKG_TestingChecklist.Common;
using OA_VKG_TestingChecklist.Extensions;
using OA_VKG_TestingChecklist.Services;
using System;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace OA_VKG_TestingChecklist.UserControlObject
{
    public partial class PreChecksControl : UserControl
    {
        #region "Data Members"
        MainBO objMain = new MainBO();
        bool IsValidated = true;
        #endregion

        public PreChecksControl()
        {
            InitializeComponent();

        }
        #region "Public Methods"
        public void ConfigureAndPopulateControls()
        {      

            DataTable dtPreChecks = objMain.GetPreChecksOption();

            if (dtPreChecks.Rows.Count > 0)
            {

                this.FindControls(typeof(ComboBox)).ForEach(c =>
                {
                    ComboBox cmb = c as ComboBox;
                    cmb.ValueMember ="OptionText";
                    cmb.DisplayMember ="OptionText";

                    cmb.DataSource = new BindingSource()
                    {
                        DataSource = dtPreChecks
                    };
                    cmb.SelectedIndex = -1;
                });

                cmbHeaterControlBoardPrePowerChecks.SelectedIndex = -1;
                cmbVoltageOnTransformerPrePowerChecks.SelectedIndex = -1;

                cmbFieldGasPortPrePowerChecks.SelectedIndex = -1;
                cmbPlugMissingPrePowerChecks.SelectedIndex = -1;
                cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = -1;
            }
        }

        internal void SetDependencies(MainBO objMain)
        {
            this.objMain = objMain;
        }

        public void LoadData(int headID)
        {
            if (headID > 0)
            {
                DataTable dataTable = objMain.GetPreChecksData();
                if (dataTable.Rows.Count > 0)
                {
                    DataRow drPreChecks = dataTable.Rows[0];
                    cmbSubmittalUnitMatch.Text = drPreChecks["Verify_sub_un_match"].ToString();
                    cmbUnitBuiltCCS.Text = drPreChecks["Unit_built_compl_CCS"].ToString();
                    cmbInspectUnit.Text = drPreChecks["Inspect_unit_items"].ToString();
                    cmbPowerWires.Text = drPreChecks["Power_wire_driver"].ToString();
                    cmbLineLoad.Text = drPreChecks["Line_Load_term_Correctly"].ToString();
                    cmbTerminatioMode.Text = drPreChecks["Vis_term_prop_connector"].ToString();
                    cmbVerifyPlugHarness.Text = drPreChecks["Verify_plug_harn_correct"].ToString();
                    cmbCrankcaseVoltage.Text = drPreChecks["Crankcase_correct_volt"].ToString();
                    tbPreChecksMiscNotes.Text = drPreChecks["Misc_notes"].ToString();

                    cmbPowerWhipPrePowerChecks.Text = drPreChecks["Power_whip_over_tight"].ToString();
                    cmbCorrectSizeWhipPrePowerChecks.Text = drPreChecks["Correct_size_whip_unit"].ToString();
                    cmbConnectPowerPrePowerChecks.Text = drPreChecks["Co_Power_Ground_purp"].ToString();
                    cmbCheckContinuityPrePower.Text = drPreChecks["Cont_High_volt_term"].ToString();
                    cmbFanMotorsPrePowerChecks.Text = drPreChecks["Check_Phases_fan_motor"].ToString();
                    cmbCoreSenseSensorPrePowerChecks.Text = drPreChecks["Remove_volt_Sen_Sensor"].ToString();
                    cmbHiPotECMPrePowerChecks.Text = drPreChecks["Hi_pot_unit_fans"].ToString();
                    cmbCheckPowerTerminationPrePowerChecks.Text = drPreChecks["Check_Power_term_block"].ToString();
                    cmbInspectElementsPrePowerChecks.Text = drPreChecks["Inspect_element_heater"].ToString();
                    cmbHeaterControlBoardPrePowerChecks.Text = drPreChecks["Check_heater_cont_board"].ToString();
                    cmbVoltageOnTransformerPrePowerChecks.Text = drPreChecks["Co_volt_writtn_transform"].ToString();

                    cmbFieldGasPortPrePowerChecks.Text = drPreChecks["En_field_gas_port"].ToString();

                    cmbPlugMissingPrePowerChecks.Text = drPreChecks["In_Plug_are_Missing"].ToString();
                    cmbTurnOffGasLeakTestPrePowerChecks.Text = drPreChecks["Turn_off_gas_inspection"].ToString();

                    cmbSetPhaseMonitorPrePowerChecks.Text = drPreChecks["Set_Phase_Monitor"].ToString();
                    cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Text = drPreChecks["Un_Correct_volt_disconn"].ToString();

                }
            }
        }

        public void BindPreChecksDataAtPrintEvent()
        {
            objMain.PreChecklist = new OAUPreChecklist();
            objMain.PreChecklist.Verify_sub_un_match = cmbSubmittalUnitMatch.Text;
            objMain.PreChecklist.Unit_built_compl_CCS = cmbUnitBuiltCCS.Text;
            objMain.PreChecklist.Inspect_unit_items = cmbInspectUnit.Text;
            objMain.PreChecklist.Power_wire_driver = cmbPowerWires.Text;
            objMain.PreChecklist.Line_Load_term_Correctly = cmbLineLoad.Text;
            objMain.PreChecklist.Vis_term_prop_connector = cmbTerminatioMode.Text;
            objMain.PreChecklist.Verify_plug_harn_correct = cmbVerifyPlugHarness.Text;
            objMain.PreChecklist.Crankcase_correct_volt = cmbCrankcaseVoltage.Text;

            objMain.OAUTPrePower = new OAUTPrePowerCheckslist();
            objMain.OAUTPrePower.Power_whip_over_tight = cmbPowerWhipPrePowerChecks.Text;
            objMain.OAUTPrePower.Correct_size_whip_unit = cmbCorrectSizeWhipPrePowerChecks.Text;

            objMain.OAUTPrePower.Co_Power_Ground_purp = cmbConnectPowerPrePowerChecks.Text;
            objMain.OAUTPrePower.Cont_High_volt_term = cmbCheckContinuityPrePower.Text;
            objMain.OAUTPrePower.Check_Phases_fan_motor = cmbFanMotorsPrePowerChecks.Text;
            objMain.OAUTPrePower.Remove_volt_Sen_Sensor = cmbCoreSenseSensorPrePowerChecks.Text;
            objMain.OAUTPrePower.Hi_pot_unit_fans = cmbHiPotECMPrePowerChecks.Text;

            objMain.OAUTPrePower.Check_Power_term_block = cmbCheckPowerTerminationPrePowerChecks.Text;
            objMain.OAUTPrePower.Inspect_element_heater = cmbInspectElementsPrePowerChecks.Text;
            objMain.OAUTPrePower.Check_heater_cont_board = cmbHeaterControlBoardPrePowerChecks.Text;
            objMain.OAUTPrePower.Co_volt_writtn_transform = cmbVoltageOnTransformerPrePowerChecks.Text;

            objMain.OAUTPrePower.En_field_gas_port = cmbFieldGasPortPrePowerChecks.Text;
            objMain.OAUTPrePower.In_Plug_are_Missing = cmbPlugMissingPrePowerChecks.Text;
            objMain.OAUTPrePower.Turn_off_gas_inspection = cmbTurnOffGasLeakTestPrePowerChecks.Text;

            objMain.OAUTPrePower.Set_Phase_Monitor = cmbSetPhaseMonitorPrePowerChecks.Text;

            objMain.OAUTPrePower.Un_Correct_volt_disconn = cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Text;
        }

        public void BindPreChecksDataFormToModel()
        {
            try
            {
                objMain.PreChecklist = new OAUPreChecklist();
                objMain.PreChecklist.Verify_sub_un_match = cmbSubmittalUnitMatch.Text;
                objMain.PreChecklist.Unit_built_compl_CCS = cmbUnitBuiltCCS.Text;
                objMain.PreChecklist.Inspect_unit_items = cmbInspectUnit.Text;
                objMain.PreChecklist.Power_wire_driver = cmbPowerWires.Text;
                objMain.PreChecklist.Line_Load_term_Correctly = cmbLineLoad.Text;
                objMain.PreChecklist.Vis_term_prop_connector = cmbTerminatioMode.Text;
                objMain.PreChecklist.Verify_plug_harn_correct = cmbVerifyPlugHarness.Text;
                objMain.PreChecklist.Crankcase_correct_volt = cmbCrankcaseVoltage.Text;
                objMain.PreChecklist.PreChecksMiscNotes = tbPreChecksMiscNotes.Text;
                objMain.PreChecklist.Power_whip_over_tight = cmbPowerWhipPrePowerChecks.Text;
                objMain.PreChecklist.Correct_size_whip_unit = cmbCorrectSizeWhipPrePowerChecks.Text;
                objMain.PreChecklist.Co_Power_Ground_purp = cmbConnectPowerPrePowerChecks.Text;
                objMain.PreChecklist.Cont_High_volt_term = cmbCheckContinuityPrePower.Text;
                objMain.PreChecklist.Check_Phases_fan_motor = cmbFanMotorsPrePowerChecks.Text;
                objMain.PreChecklist.Remove_volt_Sen_Sensor = cmbCoreSenseSensorPrePowerChecks.Text;
                objMain.PreChecklist.Hi_pot_unit_fans = cmbHiPotECMPrePowerChecks.Text;
                objMain.PreChecklist.Check_Power_term_block = cmbCheckPowerTerminationPrePowerChecks.Text;
                objMain.PreChecklist.Inspect_element_heater = cmbInspectElementsPrePowerChecks.Text;
                objMain.PreChecklist.Check_heater_cont_board = cmbHeaterControlBoardPrePowerChecks.Text;
                objMain.PreChecklist.Co_volt_writtn_transform = cmbVoltageOnTransformerPrePowerChecks.Text;
                objMain.PreChecklist.En_field_gas_port = cmbFieldGasPortPrePowerChecks.Text;
                objMain.PreChecklist.In_Plug_are_Missing = cmbPlugMissingPrePowerChecks.Text;
                objMain.PreChecklist.Turn_off_gas_inspection = cmbTurnOffGasLeakTestPrePowerChecks.Text;

                objMain.PreChecklist.Set_Phase_Monitor = cmbSetPhaseMonitorPrePowerChecks.Text;
                objMain.PreChecklist.Un_Correct_volt_disconn = cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Text;

                objMain.InsertPreChecksData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_PreChecks - " + ex);
            }
        }
        public String ValidateCondtions()
        {
            StringBuilder errors =new StringBuilder();
            IsValidated = true;
            try
            {
                this.FindControls(typeof(ComboBox)).ForEach(c =>
                {
                    ComboBox cmb = c as ComboBox;
                    if (string.IsNullOrWhiteSpace(cmb.Text))
                    {
                        cmb.Focus();
                        errorProviderApp.SetError(cmb, "Required!");
                        IsValidated = false;

                        //return;
                    }
                    else
                    {
                        errorProviderApp.SetError(cmb, "");
                    }
                });
                foreach (Control control in tlpElectricHeat.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;
                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderApp.SetError(combo, "Required!");
                            IsValidated = false;
                        }
                        else
                        {
                            errorProviderApp.SetError(combo, "");
                        }
                    }
                }

                foreach (Control control in tlpPrePowerChecks.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;

                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderApp.SetError(combo, "Required!");
                            IsValidated = false;

                            //return;
                        }
                        else
                        {
                            errorProviderApp.SetError(combo, "");
                        }
                    }
                }

                foreach (Control control in tlpPrePowerFinalSteps.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;

                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderApp.SetError(combo, "Required!");
                            IsValidated = false;

                            //return;
                        }
                        else
                        {
                            errorProviderApp.SetError(combo, "");
                        }
                    }
                }

                foreach (Control control in tlpGasHeat.Controls)
                {
                    if (control is ComboBox)
                    {
                        ComboBox combo = control as ComboBox;
                        if (string.IsNullOrWhiteSpace(combo.Text))
                        {
                            combo.Focus();
                            errorProviderApp.SetError(combo, "Required!");
                            IsValidated = false;
                        }
                        else
                        {
                            errorProviderApp.SetError(combo, "");
                        }
                    }
                }
                if (!IsValidated)
                {
                    errors.AppendLine("PrePowerChecks:");
                    errors.AppendLine("You must fill the all mandatory fields before saving!");
                }
                else
                {
                    //return "";
                    if (cmbHeaterControlBoardPrePowerChecks.Text.Trim() != null && (cmbHeaterControlBoardPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbHeaterControlBoardPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Fail) || cmbVoltageOnTransformerPrePowerChecks.Text.Trim() != null && (cmbVoltageOnTransformerPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbVoltageOnTransformerPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Fail))
                    {
                        foreach (Control control in tlpGasLeakTest.Controls)
                        {
                            if (control is ComboBox)
                            {
                                ComboBox combo = control as ComboBox;
                                if (string.IsNullOrWhiteSpace(combo.Text))
                                {
                                    combo.Focus();
                                    errorProviderApp.SetError(combo, "Required!");
                                    IsValidated = false;
                                }
                                else
                                {
                                    errorProviderApp.SetError(combo, "");
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (Control control in tlpGasLeakTest.Controls)
                        {
                            if (control is ComboBox)
                            {
                                ComboBox combo = control as ComboBox;
                                if (string.IsNullOrWhiteSpace(combo.Text))
                                {
                                    combo.Focus();
                                    errorProviderApp.SetError(combo, "");

                                }
                                else
                                {
                                    errorProviderApp.SetError(combo, "");
                                }
                            }
                        }
                    }
                    if (!IsValidated)
                    {
                        errors.AppendLine("PreChecks:");
                        errors.AppendLine("You must fill the all mandatory fields before saving!");
                    }
                    else
                    {
                        errors.Clear();
                    }
                }
                    return errors.ToString();
            }
            
            catch (Exception e)
            {
                return "";
            }
        }
        #endregion
        #region "Events"
        private void ComboBox_KeyPress(object sender, KeyPressEventArgs e)
        {          
            
                if (e.KeyChar == 'P' || e.KeyChar == 'N' || e.KeyChar == 'F' || e.KeyChar == 8 ||
                e.KeyChar == 'p' || e.KeyChar == 'n' || e.KeyChar == 'f')
                {
                    if(e.KeyChar == 'p' || e.KeyChar == 'n' || e.KeyChar == 'f')
                    {
                        e.KeyChar -= (char)32;
                    }

                }
                else
                {
                e.Handled = true;
                
                }
        }

       

        private void btnPrintPreChecks_Click(object sender, EventArgs e)
        {
            BindPreChecksDataAtPrintEvent();
            ExportServices exportServices = new ExportServices();
            try
            {
                string reportsPath = $"{Application.StartupPath}\\Reports";
                string filePath = $"{Application.StartupPath}\\Reports\\PreChecks_{Guid.NewGuid().ToString()}.pdf";
                if (!System.IO.Directory.Exists(reportsPath))
                {
                    System.IO.Directory.CreateDirectory(reportsPath);
                }
                exportServices.ExportPreChecks(objMain, filePath);
                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void PreChecksControl_Load(object sender, EventArgs e)
        {
            errorProviderApp.ContainerControl = this;
        }

        #endregion

        private void cmbSetPhaseMonitorPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void tlpPrePowerChecks_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

            if (cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null && (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbCheckPowerTerminationPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbInspectElementsPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                //cmbConnectGasToManifoldPrePowerChecks.SelectedIndex = -1;
                //cmbUnlockPrePowerChecksGasValves.SelectedIndex = -1;
                //cmbTurnOnGasPrePowerChecks.SelectedIndex = -1;
                //cmbFieldGasPortPrePowerChecks.SelectedIndex = -1;
                //cmbPlugMissingPrePowerChecks.SelectedIndex = -1;
                //cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = -1;
            }
            else if (cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
                ) && cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null &&
                (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
                ))
            {
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;

                //cmbInspectElementsPrePowerChecks.ResetText();
                //cmbCheckPowerTerminationPrePowerChecks.ResetText();
                //cmbConnectGasToManifoldPrePowerChecks.ResetText();
                //cmbUnlockPrePowerChecksGasValves.ResetText();
                //cmbTurnOnGasPrePowerChecks.ResetText();
                //cmbFieldGasPortPrePowerChecks.ResetText();
                //cmbPlugMissingPrePowerChecks.ResetText();
                //cmbTurnOffGasLeakTestPrePowerChecks.ResetText();
            }
            else
            {
                cmbFieldGasPortPrePowerChecks.ResetText();
                cmbPlugMissingPrePowerChecks.ResetText();
                cmbTurnOffGasLeakTestPrePowerChecks.ResetText();
            }
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);


        }

        private void cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

            if (cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null && (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbHeaterControlBoardPrePowerChecks.SelectedItem != null && (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Pass
                || cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbCheckPowerTerminationPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbInspectElementsPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                //cmbConnectGasToManifoldPrePowerChecks.SelectedIndex = -1;
                //cmbUnlockPrePowerChecksGasValves.SelectedIndex = -1;
                //cmbTurnOnGasPrePowerChecks.SelectedIndex = -1;
                //cmbFieldGasPortPrePowerChecks.SelectedIndex = -1;
                //cmbPlugMissingPrePowerChecks.SelectedIndex = -1;
                //cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = -1;
            }
            else if (cmbVoltageOnTransformerPrePowerChecks.SelectedItem != null &&
                (cmbVoltageOnTransformerPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
                ) && cmbHeaterControlBoardPrePowerChecks.SelectedItem != null &&
                (cmbHeaterControlBoardPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.NotApplication
               ))
            {
                //cmbCheckPowerTerminationPrePowerChecks.ResetText();
                //cmbInspectElementsPrePowerChecks.ResetText();
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
            }
            else
            {
                cmbFieldGasPortPrePowerChecks.ResetText();
                cmbPlugMissingPrePowerChecks.ResetText();
                cmbTurnOffGasLeakTestPrePowerChecks.ResetText();
            }
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

        }

        private void cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);

            if (cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbCheckPowerTerminationPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbInspectElementsPrePowerChecks.Text.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbHeaterControlBoardPrePowerChecks.SelectedIndex = cmbHeaterControlBoardPrePowerChecks.Items.Count - 1;
                cmbVoltageOnTransformerPrePowerChecks.SelectedIndex = cmbVoltageOnTransformerPrePowerChecks.Items.Count - 1;
                cmbFieldGasPortPrePowerChecks.SelectedIndex = cmbFieldGasPortPrePowerChecks.Items.Count - 1;
                cmbPlugMissingPrePowerChecks.SelectedIndex = cmbPlugMissingPrePowerChecks.Items.Count - 1;
                cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = cmbTurnOffGasLeakTestPrePowerChecks.Items.Count - 1;

            }
            else if (cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
                ) &&
                cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
                ))
            {
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
            }
            else
            {
            }
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbInspectElementsPrePowerChecks_SelectedIndexChanged);
        }

        private void cmbInspectElementsPrePowerChecks_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged -= new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);

            if (cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbInspectElementsPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail) || cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.Pass
                || cmbCheckPowerTerminationPrePowerChecks.SelectedValue.ToString().Trim() == PreCheckDropdownText.Fail))
            {
                cmbHeaterControlBoardPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbVoltageOnTransformerPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbFieldGasPortPrePowerChecks.SelectedIndex = cmbFieldGasPortPrePowerChecks.Items.Count - 1;
                cmbPlugMissingPrePowerChecks.SelectedIndex = cmbPlugMissingPrePowerChecks.Items.Count - 1;
                cmbTurnOffGasLeakTestPrePowerChecks.SelectedIndex = cmbTurnOffGasLeakTestPrePowerChecks.Items.Count - 1;

            }
            else if (cmbInspectElementsPrePowerChecks.SelectedItem != null && (cmbInspectElementsPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
               ) && cmbCheckPowerTerminationPrePowerChecks.SelectedItem != null && (cmbCheckPowerTerminationPrePowerChecks.Text.Trim() == PreCheckDropdownText.NotApplication
                ))
            {
                cmbFieldGasPortPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbPlugMissingPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
                cmbTurnOffGasLeakTestPrePowerChecks.Text = PreCheckDropdownText.NotApplication;
            }
            else
            {
            }
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new EventHandler(cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
        }

        private void gbMiscNotes_Enter(object sender, EventArgs e)
        {

        }

        private void gbPreChecks_Enter(object sender, EventArgs e)
        {

        }
    }
}



