﻿namespace OA_VKG_TestingChecklist.UserControlObject
{
    partial class PrePowerChecksControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.gbMiscNotes = new System.Windows.Forms.GroupBox();
            this.tbPrePowerMiscNotes = new System.Windows.Forms.TextBox();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnPrintPrePowerChecks = new System.Windows.Forms.Button();
            this.gbPrePowerFinalSteps = new System.Windows.Forms.GroupBox();
            this.tlpPrePowerFinalSteps = new System.Windows.Forms.TableLayoutPanel();
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbSetPhaseMonitorPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblSetPhaseMonitor = new System.Windows.Forms.Label();
            this.lblUnlockCorrectVoltageDisconnect = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gbPrePowerChecks = new System.Windows.Forms.GroupBox();
            this.tlpPrePowerChecks = new System.Windows.Forms.TableLayoutPanel();
            this.cmbCoreSenseSensorPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblConnectPowerHeads = new System.Windows.Forms.Label();
            this.lblRemoveAll3HighPrePowerChecks = new System.Windows.Forms.Label();
            this.cmbPowerWhipPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblContinuityHighVoltageTermination = new System.Windows.Forms.Label();
            this.cmbHiPotECMPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbFanMotorsPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbCorrectSizeWhipPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblAllFanMotorsPrePowerChecks = new System.Windows.Forms.Label();
            this.cmbConnectPowerPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblCheckContinuityPrePowerChecks = new System.Windows.Forms.Label();
            this.cmbCheckContinuityPrePower = new System.Windows.Forms.ComboBox();
            this.lblHiPotUnitPrePowerChecks = new System.Windows.Forms.Label();
            this.lblNoPowerUnitPrePowerChecks = new System.Windows.Forms.Label();
            this.tlpProductonPretestingChecks = new System.Windows.Forms.TableLayoutPanel();
            this.lblProductPreTesting = new System.Windows.Forms.Label();
            this.lblPleaseCompletePretestChecklist = new System.Windows.Forms.Label();
            this.lblEnteraPAndF = new System.Windows.Forms.Label();
            this.label130 = new System.Windows.Forms.Label();
            this.errorProviderPrePowerChecks = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlGasLeakTest = new System.Windows.Forms.Panel();
            this.pnlPowerChecks = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbElectricHeat = new System.Windows.Forms.GroupBox();
            this.tlpElectricHeat = new System.Windows.Forms.TableLayoutPanel();
            this.lblInspectElementHeaterElectricHeat = new System.Windows.Forms.Label();
            this.lblCheckPowerTerminationElectricHeat = new System.Windows.Forms.Label();
            this.cmbCheckPowerTerminationPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbInspectElementsPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.gbGasHeat = new System.Windows.Forms.GroupBox();
            this.tlpGasHeat = new System.Windows.Forms.TableLayoutPanel();
            this.lblCorrectVoltageGasHeat = new System.Windows.Forms.Label();
            this.lblCheckHighVoltageElectricHeat = new System.Windows.Forms.Label();
            this.cmbHeaterControlBoardPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbVoltageOnTransformerPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.gbGasLeakTest = new System.Windows.Forms.GroupBox();
            this.tlpGasLeakTest = new System.Windows.Forms.TableLayoutPanel();
            this.cmbTurnOffGasLeakTestPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblTurnOffGasLeakTest = new System.Windows.Forms.Label();
            this.lblEnsureFieldGasLeakTest = new System.Windows.Forms.Label();
            this.cmbFieldGasPortPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbPlugMissingPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblPlugsAreMissingLeakTest = new System.Windows.Forms.Label();
            this.pnlElectricHeat = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel4.SuspendLayout();
            this.gbMiscNotes.SuspendLayout();
            this.panel7.SuspendLayout();
            this.gbPrePowerFinalSteps.SuspendLayout();
            this.tlpPrePowerFinalSteps.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.gbPrePowerChecks.SuspendLayout();
            this.tlpPrePowerChecks.SuspendLayout();
            this.tlpProductonPretestingChecks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPrePowerChecks)).BeginInit();
            this.pnlPowerChecks.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbElectricHeat.SuspendLayout();
            this.tlpElectricHeat.SuspendLayout();
            this.panel5.SuspendLayout();
            this.gbGasHeat.SuspendLayout();
            this.tlpGasHeat.SuspendLayout();
            this.panel6.SuspendLayout();
            this.gbGasLeakTest.SuspendLayout();
            this.tlpGasLeakTest.SuspendLayout();
            this.pnlElectricHeat.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel4.Controls.Add(this.pnlPowerChecks, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel4, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.pnlElectricHeat, 2, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 64);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1226, 432);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.gbMiscNotes);
            this.panel4.Controls.Add(this.panel7);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(921, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(302, 426);
            this.panel4.TabIndex = 3;
            // 
            // gbMiscNotes
            // 
            this.gbMiscNotes.Controls.Add(this.tbPrePowerMiscNotes);
            this.gbMiscNotes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbMiscNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMiscNotes.Location = new System.Drawing.Point(0, 304);
            this.gbMiscNotes.Name = "gbMiscNotes";
            this.gbMiscNotes.Size = new System.Drawing.Size(302, 88);
            this.gbMiscNotes.TabIndex = 2;
            this.gbMiscNotes.TabStop = false;
            this.gbMiscNotes.Text = "Misc. Notes";
            // 
            // tbPrePowerMiscNotes
            // 
            this.tbPrePowerMiscNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPrePowerMiscNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPrePowerMiscNotes.Location = new System.Drawing.Point(3, 16);
            this.tbPrePowerMiscNotes.Multiline = true;
            this.tbPrePowerMiscNotes.Name = "tbPrePowerMiscNotes";
            this.tbPrePowerMiscNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbPrePowerMiscNotes.Size = new System.Drawing.Size(296, 69);
            this.tbPrePowerMiscNotes.TabIndex = 0;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.btnPrintPrePowerChecks);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel7.Location = new System.Drawing.Point(0, 392);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(302, 34);
            this.panel7.TabIndex = 2;
            // 
            // btnPrintPrePowerChecks
            // 
            this.btnPrintPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrintPrePowerChecks.Location = new System.Drawing.Point(227, 0);
            this.btnPrintPrePowerChecks.Name = "btnPrintPrePowerChecks";
            this.btnPrintPrePowerChecks.Size = new System.Drawing.Size(75, 34);
            this.btnPrintPrePowerChecks.TabIndex = 0;
            this.btnPrintPrePowerChecks.Text = "Print";
            this.btnPrintPrePowerChecks.UseVisualStyleBackColor = true;
            this.btnPrintPrePowerChecks.Click += new System.EventHandler(this.btnPrintPrePowerChecks_Click);
            // 
            // gbPrePowerFinalSteps
            // 
            this.gbPrePowerFinalSteps.Controls.Add(this.tlpPrePowerFinalSteps);
            this.gbPrePowerFinalSteps.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPrePowerFinalSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPrePowerFinalSteps.Location = new System.Drawing.Point(0, 0);
            this.gbPrePowerFinalSteps.Name = "gbPrePowerFinalSteps";
            this.gbPrePowerFinalSteps.Size = new System.Drawing.Size(300, 75);
            this.gbPrePowerFinalSteps.TabIndex = 1;
            this.gbPrePowerFinalSteps.TabStop = false;
            this.gbPrePowerFinalSteps.Text = "PRE-POWER FINAL CHECKS";
            // 
            // tlpPrePowerFinalSteps
            // 
            this.tlpPrePowerFinalSteps.ColumnCount = 2;
            this.tlpPrePowerFinalSteps.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrePowerFinalSteps.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpPrePowerFinalSteps.Controls.Add(this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks, 1, 5);
            this.tlpPrePowerFinalSteps.Controls.Add(this.cmbSetPhaseMonitorPrePowerChecks, 1, 3);
            this.tlpPrePowerFinalSteps.Controls.Add(this.lblSetPhaseMonitor, 0, 3);
            this.tlpPrePowerFinalSteps.Controls.Add(this.lblUnlockCorrectVoltageDisconnect, 0, 5);
            this.tlpPrePowerFinalSteps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPrePowerFinalSteps.Location = new System.Drawing.Point(3, 16);
            this.tlpPrePowerFinalSteps.Name = "tlpPrePowerFinalSteps";
            this.tlpPrePowerFinalSteps.RowCount = 6;
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.Size = new System.Drawing.Size(294, 56);
            this.tlpPrePowerFinalSteps.TabIndex = 14;
            // 
            // cmbUnlockCorrectVoltageDisconnectPrePowerChecks
            // 
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.FormattingEnabled = true;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.IntegralHeight = false;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Location = new System.Drawing.Point(219, 30);
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.MaxLength = 1;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Name = "cmbUnlockCorrectVoltageDisconnectPrePowerChecks";
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.TabIndex = 7;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbSetPhaseMonitorPrePowerChecks
            // 
            this.cmbSetPhaseMonitorPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSetPhaseMonitorPrePowerChecks.FormattingEnabled = true;
            this.cmbSetPhaseMonitorPrePowerChecks.Location = new System.Drawing.Point(219, 3);
            this.cmbSetPhaseMonitorPrePowerChecks.MaxLength = 1;
            this.cmbSetPhaseMonitorPrePowerChecks.Name = "cmbSetPhaseMonitorPrePowerChecks";
            this.cmbSetPhaseMonitorPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbSetPhaseMonitorPrePowerChecks.TabIndex = 5;
            this.cmbSetPhaseMonitorPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSetPhaseMonitor
            // 
            this.lblSetPhaseMonitor.AutoSize = true;
            this.lblSetPhaseMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSetPhaseMonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetPhaseMonitor.Location = new System.Drawing.Point(3, 0);
            this.lblSetPhaseMonitor.Name = "lblSetPhaseMonitor";
            this.lblSetPhaseMonitor.Size = new System.Drawing.Size(210, 27);
            this.lblSetPhaseMonitor.TabIndex = 15;
            this.lblSetPhaseMonitor.Text = "Set phase monitor";
            this.lblSetPhaseMonitor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnlockCorrectVoltageDisconnect
            // 
            this.lblUnlockCorrectVoltageDisconnect.AutoSize = true;
            this.lblUnlockCorrectVoltageDisconnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnlockCorrectVoltageDisconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnlockCorrectVoltageDisconnect.Location = new System.Drawing.Point(3, 27);
            this.lblUnlockCorrectVoltageDisconnect.Name = "lblUnlockCorrectVoltageDisconnect";
            this.lblUnlockCorrectVoltageDisconnect.Size = new System.Drawing.Size(210, 29);
            this.lblUnlockCorrectVoltageDisconnect.TabIndex = 0;
            this.lblUnlockCorrectVoltageDisconnect.Text = "Unlock - correct voltage disconnect";
            this.lblUnlockCorrectVoltageDisconnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(309, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 426);
            this.panel1.TabIndex = 1;
            this.panel1.TabStop = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.pnlGasLeakTest);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(300, 426);
            this.panel3.TabIndex = 1;
            // 
            // gbPrePowerChecks
            // 
            this.gbPrePowerChecks.Controls.Add(this.tlpPrePowerChecks);
            this.gbPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPrePowerChecks.Location = new System.Drawing.Point(0, 0);
            this.gbPrePowerChecks.Name = "gbPrePowerChecks";
            this.gbPrePowerChecks.Size = new System.Drawing.Size(300, 210);
            this.gbPrePowerChecks.TabIndex = 0;
            this.gbPrePowerChecks.TabStop = false;
            this.gbPrePowerChecks.Text = "PRE-POWER CHECKS";
            // 
            // tlpPrePowerChecks
            // 
            this.tlpPrePowerChecks.ColumnCount = 2;
            this.tlpPrePowerChecks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrePowerChecks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpPrePowerChecks.Controls.Add(this.cmbCoreSenseSensorPrePowerChecks, 1, 6);
            this.tlpPrePowerChecks.Controls.Add(this.lblConnectPowerHeads, 0, 0);
            this.tlpPrePowerChecks.Controls.Add(this.lblRemoveAll3HighPrePowerChecks, 0, 6);
            this.tlpPrePowerChecks.Controls.Add(this.cmbPowerWhipPrePowerChecks, 1, 0);
            this.tlpPrePowerChecks.Controls.Add(this.lblContinuityHighVoltageTermination, 0, 1);
            this.tlpPrePowerChecks.Controls.Add(this.cmbHiPotECMPrePowerChecks, 1, 7);
            this.tlpPrePowerChecks.Controls.Add(this.cmbFanMotorsPrePowerChecks, 1, 5);
            this.tlpPrePowerChecks.Controls.Add(this.cmbCorrectSizeWhipPrePowerChecks, 1, 1);
            this.tlpPrePowerChecks.Controls.Add(this.lblAllFanMotorsPrePowerChecks, 0, 5);
            this.tlpPrePowerChecks.Controls.Add(this.cmbConnectPowerPrePowerChecks, 1, 3);
            this.tlpPrePowerChecks.Controls.Add(this.lblCheckContinuityPrePowerChecks, 0, 4);
            this.tlpPrePowerChecks.Controls.Add(this.cmbCheckContinuityPrePower, 1, 4);
            this.tlpPrePowerChecks.Controls.Add(this.lblHiPotUnitPrePowerChecks, 0, 7);
            this.tlpPrePowerChecks.Controls.Add(this.lblNoPowerUnitPrePowerChecks, 0, 3);
            this.tlpPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPrePowerChecks.Location = new System.Drawing.Point(3, 16);
            this.tlpPrePowerChecks.Name = "tlpPrePowerChecks";
            this.tlpPrePowerChecks.RowCount = 9;
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrePowerChecks.Size = new System.Drawing.Size(294, 191);
            this.tlpPrePowerChecks.TabIndex = 17;
            // 
            // cmbCoreSenseSensorPrePowerChecks
            // 
            this.cmbCoreSenseSensorPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCoreSenseSensorPrePowerChecks.FormattingEnabled = true;
            this.cmbCoreSenseSensorPrePowerChecks.Location = new System.Drawing.Point(231, 138);
            this.cmbCoreSenseSensorPrePowerChecks.MaxLength = 1;
            this.cmbCoreSenseSensorPrePowerChecks.Name = "cmbCoreSenseSensorPrePowerChecks";
            this.cmbCoreSenseSensorPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbCoreSenseSensorPrePowerChecks.TabIndex = 6;
            this.cmbCoreSenseSensorPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblConnectPowerHeads
            // 
            this.lblConnectPowerHeads.AutoSize = true;
            this.lblConnectPowerHeads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnectPowerHeads.Location = new System.Drawing.Point(3, 0);
            this.lblConnectPowerHeads.Name = "lblConnectPowerHeads";
            this.lblConnectPowerHeads.Size = new System.Drawing.Size(219, 26);
            this.lblConnectPowerHeads.TabIndex = 1;
            this.lblConnectPowerHeads.Text = "Power Whip is not overly tight and has some strain relief for test";
            // 
            // lblRemoveAll3HighPrePowerChecks
            // 
            this.lblRemoveAll3HighPrePowerChecks.AutoSize = true;
            this.lblRemoveAll3HighPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRemoveAll3HighPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemoveAll3HighPrePowerChecks.Location = new System.Drawing.Point(3, 135);
            this.lblRemoveAll3HighPrePowerChecks.Name = "lblRemoveAll3HighPrePowerChecks";
            this.lblRemoveAll3HighPrePowerChecks.Size = new System.Drawing.Size(222, 27);
            this.lblRemoveAll3HighPrePowerChecks.TabIndex = 15;
            this.lblRemoveAll3HighPrePowerChecks.Text = "Remove all 3 high voltage connectors from the CoreSense sensor";
            // 
            // cmbPowerWhipPrePowerChecks
            // 
            this.cmbPowerWhipPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPowerWhipPrePowerChecks.FormattingEnabled = true;
            this.cmbPowerWhipPrePowerChecks.Location = new System.Drawing.Point(231, 3);
            this.cmbPowerWhipPrePowerChecks.MaxLength = 1;
            this.cmbPowerWhipPrePowerChecks.Name = "cmbPowerWhipPrePowerChecks";
            this.cmbPowerWhipPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbPowerWhipPrePowerChecks.TabIndex = 0;
            this.cmbPowerWhipPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblContinuityHighVoltageTermination
            // 
            this.lblContinuityHighVoltageTermination.AutoSize = true;
            this.lblContinuityHighVoltageTermination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblContinuityHighVoltageTermination.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContinuityHighVoltageTermination.Location = new System.Drawing.Point(3, 27);
            this.lblContinuityHighVoltageTermination.Name = "lblContinuityHighVoltageTermination";
            this.lblContinuityHighVoltageTermination.Size = new System.Drawing.Size(222, 27);
            this.lblContinuityHighVoltageTermination.TabIndex = 4;
            this.lblContinuityHighVoltageTermination.Text = "Correct size Whip for the unit";
            this.lblContinuityHighVoltageTermination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbHiPotECMPrePowerChecks
            // 
            this.cmbHiPotECMPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHiPotECMPrePowerChecks.FormattingEnabled = true;
            this.cmbHiPotECMPrePowerChecks.Location = new System.Drawing.Point(231, 165);
            this.cmbHiPotECMPrePowerChecks.MaxLength = 1;
            this.cmbHiPotECMPrePowerChecks.Name = "cmbHiPotECMPrePowerChecks";
            this.cmbHiPotECMPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbHiPotECMPrePowerChecks.TabIndex = 7;
            this.cmbHiPotECMPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbFanMotorsPrePowerChecks
            // 
            this.cmbFanMotorsPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFanMotorsPrePowerChecks.FormattingEnabled = true;
            this.cmbFanMotorsPrePowerChecks.Location = new System.Drawing.Point(231, 111);
            this.cmbFanMotorsPrePowerChecks.MaxLength = 1;
            this.cmbFanMotorsPrePowerChecks.Name = "cmbFanMotorsPrePowerChecks";
            this.cmbFanMotorsPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbFanMotorsPrePowerChecks.TabIndex = 5;
            this.cmbFanMotorsPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbCorrectSizeWhipPrePowerChecks
            // 
            this.cmbCorrectSizeWhipPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCorrectSizeWhipPrePowerChecks.FormattingEnabled = true;
            this.cmbCorrectSizeWhipPrePowerChecks.Location = new System.Drawing.Point(231, 30);
            this.cmbCorrectSizeWhipPrePowerChecks.MaxLength = 1;
            this.cmbCorrectSizeWhipPrePowerChecks.Name = "cmbCorrectSizeWhipPrePowerChecks";
            this.cmbCorrectSizeWhipPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbCorrectSizeWhipPrePowerChecks.TabIndex = 1;
            this.cmbCorrectSizeWhipPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblAllFanMotorsPrePowerChecks
            // 
            this.lblAllFanMotorsPrePowerChecks.AutoSize = true;
            this.lblAllFanMotorsPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAllFanMotorsPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllFanMotorsPrePowerChecks.Location = new System.Drawing.Point(3, 108);
            this.lblAllFanMotorsPrePowerChecks.Name = "lblAllFanMotorsPrePowerChecks";
            this.lblAllFanMotorsPrePowerChecks.Size = new System.Drawing.Size(222, 27);
            this.lblAllFanMotorsPrePowerChecks.TabIndex = 13;
            this.lblAllFanMotorsPrePowerChecks.Text = "Check Continunity between phases on all fan motors";
            // 
            // cmbConnectPowerPrePowerChecks
            // 
            this.cmbConnectPowerPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConnectPowerPrePowerChecks.FormattingEnabled = true;
            this.cmbConnectPowerPrePowerChecks.Location = new System.Drawing.Point(231, 57);
            this.cmbConnectPowerPrePowerChecks.MaxLength = 1;
            this.cmbConnectPowerPrePowerChecks.Name = "cmbConnectPowerPrePowerChecks";
            this.cmbConnectPowerPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbConnectPowerPrePowerChecks.TabIndex = 3;
            this.cmbConnectPowerPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCheckContinuityPrePowerChecks
            // 
            this.lblCheckContinuityPrePowerChecks.AutoSize = true;
            this.lblCheckContinuityPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckContinuityPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckContinuityPrePowerChecks.Location = new System.Drawing.Point(3, 81);
            this.lblCheckContinuityPrePowerChecks.Name = "lblCheckContinuityPrePowerChecks";
            this.lblCheckContinuityPrePowerChecks.Size = new System.Drawing.Size(222, 27);
            this.lblCheckContinuityPrePowerChecks.TabIndex = 0;
            this.lblCheckContinuityPrePowerChecks.Text = "Check Continuity to ground at every high voltage termination";
            // 
            // cmbCheckContinuityPrePower
            // 
            this.cmbCheckContinuityPrePower.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCheckContinuityPrePower.FormattingEnabled = true;
            this.cmbCheckContinuityPrePower.Location = new System.Drawing.Point(231, 84);
            this.cmbCheckContinuityPrePower.MaxLength = 1;
            this.cmbCheckContinuityPrePower.Name = "cmbCheckContinuityPrePower";
            this.cmbCheckContinuityPrePower.Size = new System.Drawing.Size(60, 21);
            this.cmbCheckContinuityPrePower.TabIndex = 4;
            this.cmbCheckContinuityPrePower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblHiPotUnitPrePowerChecks
            // 
            this.lblHiPotUnitPrePowerChecks.AutoSize = true;
            this.lblHiPotUnitPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHiPotUnitPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHiPotUnitPrePowerChecks.Location = new System.Drawing.Point(3, 162);
            this.lblHiPotUnitPrePowerChecks.Name = "lblHiPotUnitPrePowerChecks";
            this.lblHiPotUnitPrePowerChecks.Size = new System.Drawing.Size(222, 27);
            this.lblHiPotUnitPrePowerChecks.TabIndex = 17;
            this.lblHiPotUnitPrePowerChecks.Text = "Hi-Pot the unit (DO NOT HI POT ROSENBERG  FANS)";
            // 
            // lblNoPowerUnitPrePowerChecks
            // 
            this.lblNoPowerUnitPrePowerChecks.AutoSize = true;
            this.lblNoPowerUnitPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoPowerUnitPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoPowerUnitPrePowerChecks.Location = new System.Drawing.Point(3, 54);
            this.lblNoPowerUnitPrePowerChecks.Name = "lblNoPowerUnitPrePowerChecks";
            this.lblNoPowerUnitPrePowerChecks.Size = new System.Drawing.Size(222, 27);
            this.lblNoPowerUnitPrePowerChecks.TabIndex = 2;
            this.lblNoPowerUnitPrePowerChecks.Text = "Connect power leads to power whip. For grounding purposes only. No power to unit";
            // 
            // tlpProductonPretestingChecks
            // 
            this.tlpProductonPretestingChecks.ColumnCount = 1;
            this.tlpProductonPretestingChecks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpProductonPretestingChecks.Controls.Add(this.lblProductPreTesting, 0, 0);
            this.tlpProductonPretestingChecks.Controls.Add(this.lblPleaseCompletePretestChecklist, 0, 1);
            this.tlpProductonPretestingChecks.Controls.Add(this.lblEnteraPAndF, 0, 2);
            this.tlpProductonPretestingChecks.Controls.Add(this.label130, 0, 3);
            this.tlpProductonPretestingChecks.Dock = System.Windows.Forms.DockStyle.Top;
            this.tlpProductonPretestingChecks.Location = new System.Drawing.Point(0, 0);
            this.tlpProductonPretestingChecks.Name = "tlpProductonPretestingChecks";
            this.tlpProductonPretestingChecks.RowCount = 4;
            this.tlpProductonPretestingChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpProductonPretestingChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tlpProductonPretestingChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tlpProductonPretestingChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tlpProductonPretestingChecks.Size = new System.Drawing.Size(1226, 64);
            this.tlpProductonPretestingChecks.TabIndex = 0;
            // 
            // lblProductPreTesting
            // 
            this.lblProductPreTesting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblProductPreTesting.AutoSize = true;
            this.lblProductPreTesting.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProductPreTesting.Location = new System.Drawing.Point(510, 0);
            this.lblProductPreTesting.Name = "lblProductPreTesting";
            this.lblProductPreTesting.Size = new System.Drawing.Size(206, 21);
            this.lblProductPreTesting.TabIndex = 3;
            this.lblProductPreTesting.Text = "Product Pre-Testing Checks";
            // 
            // lblPleaseCompletePretestChecklist
            // 
            this.lblPleaseCompletePretestChecklist.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblPleaseCompletePretestChecklist.AutoSize = true;
            this.lblPleaseCompletePretestChecklist.Location = new System.Drawing.Point(405, 21);
            this.lblPleaseCompletePretestChecklist.Name = "lblPleaseCompletePretestChecklist";
            this.lblPleaseCompletePretestChecklist.Size = new System.Drawing.Size(415, 14);
            this.lblPleaseCompletePretestChecklist.TabIndex = 1;
            this.lblPleaseCompletePretestChecklist.Text = "Please complete the Pre-Test Checklist for every unit that comes off the assembly" +
    " line. ";
            // 
            // lblEnteraPAndF
            // 
            this.lblEnteraPAndF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.lblEnteraPAndF.AutoSize = true;
            this.lblEnteraPAndF.Location = new System.Drawing.Point(472, 35);
            this.lblEnteraPAndF.Name = "lblEnteraPAndF";
            this.lblEnteraPAndF.Size = new System.Drawing.Size(281, 14);
            this.lblEnteraPAndF.TabIndex = 2;
            this.lblEnteraPAndF.Text = " Enter a ‘P’ for Pass, ‘F’ for Fail, or ‘N’ for does not apply.  ";
            // 
            // label130
            // 
            this.label130.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(395, 49);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(436, 15);
            this.label130.TabIndex = 3;
            this.label130.Text = "For any Failures reported, please document in the Misc Notes section what was inc" +
    "orrect.   ";
            // 
            // errorProviderPrePowerChecks
            // 
            this.errorProviderPrePowerChecks.ContainerControl = this;
            // 
            // pnlGasLeakTest
            // 
            this.pnlGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGasLeakTest.Location = new System.Drawing.Point(0, 0);
            this.pnlGasLeakTest.Name = "pnlGasLeakTest";
            this.pnlGasLeakTest.Size = new System.Drawing.Size(300, 426);
            this.pnlGasLeakTest.TabIndex = 3;
            // 
            // pnlPowerChecks
            // 
            this.pnlPowerChecks.Controls.Add(this.panel2);
            this.pnlPowerChecks.Controls.Add(this.gbPrePowerChecks);
            this.pnlPowerChecks.Location = new System.Drawing.Point(3, 3);
            this.pnlPowerChecks.Name = "pnlPowerChecks";
            this.pnlPowerChecks.Size = new System.Drawing.Size(300, 205);
            this.pnlPowerChecks.TabIndex = 3;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbPrePowerFinalSteps);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 210);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(300, 0);
            this.panel2.TabIndex = 1;
            // 
            // gbElectricHeat
            // 
            this.gbElectricHeat.Controls.Add(this.tlpElectricHeat);
            this.gbElectricHeat.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbElectricHeat.Location = new System.Drawing.Point(0, 0);
            this.gbElectricHeat.Name = "gbElectricHeat";
            this.gbElectricHeat.Size = new System.Drawing.Size(300, 75);
            this.gbElectricHeat.TabIndex = 0;
            this.gbElectricHeat.TabStop = false;
            this.gbElectricHeat.Text = "ELECTRIC HEAT";
            // 
            // tlpElectricHeat
            // 
            this.tlpElectricHeat.ColumnCount = 2;
            this.tlpElectricHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpElectricHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpElectricHeat.Controls.Add(this.cmbInspectElementsPrePowerChecks, 1, 1);
            this.tlpElectricHeat.Controls.Add(this.cmbCheckPowerTerminationPrePowerChecks, 1, 0);
            this.tlpElectricHeat.Controls.Add(this.lblCheckPowerTerminationElectricHeat, 0, 0);
            this.tlpElectricHeat.Controls.Add(this.lblInspectElementHeaterElectricHeat, 0, 1);
            this.tlpElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpElectricHeat.Location = new System.Drawing.Point(3, 16);
            this.tlpElectricHeat.Name = "tlpElectricHeat";
            this.tlpElectricHeat.RowCount = 2;
            this.tlpElectricHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpElectricHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpElectricHeat.Size = new System.Drawing.Size(294, 56);
            this.tlpElectricHeat.TabIndex = 0;
            // 
            // lblInspectElementHeaterElectricHeat
            // 
            this.lblInspectElementHeaterElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInspectElementHeaterElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectElementHeaterElectricHeat.Location = new System.Drawing.Point(3, 27);
            this.lblInspectElementHeaterElectricHeat.Name = "lblInspectElementHeaterElectricHeat";
            this.lblInspectElementHeaterElectricHeat.Size = new System.Drawing.Size(210, 29);
            this.lblInspectElementHeaterElectricHeat.TabIndex = 3;
            this.lblInspectElementHeaterElectricHeat.Text = "Inspect elements of heater for debris/damage";
            this.lblInspectElementHeaterElectricHeat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCheckPowerTerminationElectricHeat
            // 
            this.lblCheckPowerTerminationElectricHeat.AutoSize = true;
            this.lblCheckPowerTerminationElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckPowerTerminationElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckPowerTerminationElectricHeat.Location = new System.Drawing.Point(3, 0);
            this.lblCheckPowerTerminationElectricHeat.Name = "lblCheckPowerTerminationElectricHeat";
            this.lblCheckPowerTerminationElectricHeat.Size = new System.Drawing.Size(210, 27);
            this.lblCheckPowerTerminationElectricHeat.TabIndex = 2;
            this.lblCheckPowerTerminationElectricHeat.Text = "Check power terminations on both sides of the terminal block";
            this.lblCheckPowerTerminationElectricHeat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbCheckPowerTerminationPrePowerChecks
            // 
            this.cmbCheckPowerTerminationPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCheckPowerTerminationPrePowerChecks.FormattingEnabled = true;
            this.cmbCheckPowerTerminationPrePowerChecks.Location = new System.Drawing.Point(219, 3);
            this.cmbCheckPowerTerminationPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbCheckPowerTerminationPrePowerChecks.MaxLength = 1;
            this.cmbCheckPowerTerminationPrePowerChecks.Name = "cmbCheckPowerTerminationPrePowerChecks";
            this.cmbCheckPowerTerminationPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbCheckPowerTerminationPrePowerChecks.TabIndex = 0;
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbInspectElementsPrePowerChecks
            // 
            this.cmbInspectElementsPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbInspectElementsPrePowerChecks.FormattingEnabled = true;
            this.cmbInspectElementsPrePowerChecks.Location = new System.Drawing.Point(219, 30);
            this.cmbInspectElementsPrePowerChecks.MaxLength = 1;
            this.cmbInspectElementsPrePowerChecks.Name = "cmbInspectElementsPrePowerChecks";
            this.cmbInspectElementsPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbInspectElementsPrePowerChecks.TabIndex = 1;
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbInspectElementsPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Controls.Add(this.gbGasHeat);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 75);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(300, 351);
            this.panel5.TabIndex = 1;
            // 
            // gbGasHeat
            // 
            this.gbGasHeat.Controls.Add(this.tlpGasHeat);
            this.gbGasHeat.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbGasHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGasHeat.Location = new System.Drawing.Point(0, 0);
            this.gbGasHeat.Name = "gbGasHeat";
            this.gbGasHeat.Size = new System.Drawing.Size(300, 75);
            this.gbGasHeat.TabIndex = 0;
            this.gbGasHeat.TabStop = false;
            this.gbGasHeat.Text = "GAS HEAT";
            // 
            // tlpGasHeat
            // 
            this.tlpGasHeat.AutoSize = true;
            this.tlpGasHeat.ColumnCount = 2;
            this.tlpGasHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpGasHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpGasHeat.Controls.Add(this.cmbVoltageOnTransformerPrePowerChecks, 1, 1);
            this.tlpGasHeat.Controls.Add(this.cmbHeaterControlBoardPrePowerChecks, 1, 0);
            this.tlpGasHeat.Controls.Add(this.lblCheckHighVoltageElectricHeat, 0, 0);
            this.tlpGasHeat.Controls.Add(this.lblCorrectVoltageGasHeat, 0, 1);
            this.tlpGasHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpGasHeat.Location = new System.Drawing.Point(3, 16);
            this.tlpGasHeat.Name = "tlpGasHeat";
            this.tlpGasHeat.RowCount = 2;
            this.tlpGasHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasHeat.Size = new System.Drawing.Size(294, 56);
            this.tlpGasHeat.TabIndex = 0;
            // 
            // lblCorrectVoltageGasHeat
            // 
            this.lblCorrectVoltageGasHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorrectVoltageGasHeat.Location = new System.Drawing.Point(3, 27);
            this.lblCorrectVoltageGasHeat.Name = "lblCorrectVoltageGasHeat";
            this.lblCorrectVoltageGasHeat.Size = new System.Drawing.Size(199, 23);
            this.lblCorrectVoltageGasHeat.TabIndex = 0;
            this.lblCorrectVoltageGasHeat.Text = "Correct voltage written on transformer";
            this.lblCorrectVoltageGasHeat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblCheckHighVoltageElectricHeat
            // 
            this.lblCheckHighVoltageElectricHeat.AutoSize = true;
            this.lblCheckHighVoltageElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckHighVoltageElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckHighVoltageElectricHeat.Location = new System.Drawing.Point(3, 0);
            this.lblCheckHighVoltageElectricHeat.Name = "lblCheckHighVoltageElectricHeat";
            this.lblCheckHighVoltageElectricHeat.Size = new System.Drawing.Size(210, 27);
            this.lblCheckHighVoltageElectricHeat.TabIndex = 1;
            this.lblCheckHighVoltageElectricHeat.Text = "Check high voltage from the transformer is landed correctly on heater control boa" +
    "rd";
            // 
            // cmbHeaterControlBoardPrePowerChecks
            // 
            this.cmbHeaterControlBoardPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHeaterControlBoardPrePowerChecks.FormattingEnabled = true;
            this.cmbHeaterControlBoardPrePowerChecks.Location = new System.Drawing.Point(219, 3);
            this.cmbHeaterControlBoardPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbHeaterControlBoardPrePowerChecks.MaxLength = 1;
            this.cmbHeaterControlBoardPrePowerChecks.Name = "cmbHeaterControlBoardPrePowerChecks";
            this.cmbHeaterControlBoardPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbHeaterControlBoardPrePowerChecks.TabIndex = 2;
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbHeaterControlBoardPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbVoltageOnTransformerPrePowerChecks
            // 
            this.cmbVoltageOnTransformerPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVoltageOnTransformerPrePowerChecks.FormattingEnabled = true;
            this.cmbVoltageOnTransformerPrePowerChecks.Location = new System.Drawing.Point(219, 30);
            this.cmbVoltageOnTransformerPrePowerChecks.MaxLength = 1;
            this.cmbVoltageOnTransformerPrePowerChecks.Name = "cmbVoltageOnTransformerPrePowerChecks";
            this.cmbVoltageOnTransformerPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbVoltageOnTransformerPrePowerChecks.TabIndex = 3;
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.gbGasLeakTest);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 75);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(300, 276);
            this.panel6.TabIndex = 1;
            // 
            // gbGasLeakTest
            // 
            this.gbGasLeakTest.Controls.Add(this.tlpGasLeakTest);
            this.gbGasLeakTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbGasLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGasLeakTest.Location = new System.Drawing.Point(0, 0);
            this.gbGasLeakTest.Name = "gbGasLeakTest";
            this.gbGasLeakTest.Size = new System.Drawing.Size(300, 102);
            this.gbGasLeakTest.TabIndex = 2;
            this.gbGasLeakTest.TabStop = false;
            this.gbGasLeakTest.Text = "GAS LEAK TEST";
            // 
            // tlpGasLeakTest
            // 
            this.tlpGasLeakTest.ColumnCount = 2;
            this.tlpGasLeakTest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpGasLeakTest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpGasLeakTest.Controls.Add(this.lblPlugsAreMissingLeakTest, 0, 4);
            this.tlpGasLeakTest.Controls.Add(this.cmbPlugMissingPrePowerChecks, 1, 4);
            this.tlpGasLeakTest.Controls.Add(this.cmbFieldGasPortPrePowerChecks, 1, 3);
            this.tlpGasLeakTest.Controls.Add(this.lblEnsureFieldGasLeakTest, 0, 3);
            this.tlpGasLeakTest.Controls.Add(this.lblTurnOffGasLeakTest, 0, 5);
            this.tlpGasLeakTest.Controls.Add(this.cmbTurnOffGasLeakTestPrePowerChecks, 1, 5);
            this.tlpGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpGasLeakTest.Location = new System.Drawing.Point(3, 16);
            this.tlpGasLeakTest.Name = "tlpGasLeakTest";
            this.tlpGasLeakTest.RowCount = 6;
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.Size = new System.Drawing.Size(294, 83);
            this.tlpGasLeakTest.TabIndex = 11;
            // 
            // cmbTurnOffGasLeakTestPrePowerChecks
            // 
            this.cmbTurnOffGasLeakTestPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTurnOffGasLeakTestPrePowerChecks.FormattingEnabled = true;
            this.cmbTurnOffGasLeakTestPrePowerChecks.Location = new System.Drawing.Point(219, 57);
            this.cmbTurnOffGasLeakTestPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbTurnOffGasLeakTestPrePowerChecks.MaxLength = 1;
            this.cmbTurnOffGasLeakTestPrePowerChecks.Name = "cmbTurnOffGasLeakTestPrePowerChecks";
            this.cmbTurnOffGasLeakTestPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbTurnOffGasLeakTestPrePowerChecks.TabIndex = 5;
            this.cmbTurnOffGasLeakTestPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblTurnOffGasLeakTest
            // 
            this.lblTurnOffGasLeakTest.AutoSize = true;
            this.lblTurnOffGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTurnOffGasLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnOffGasLeakTest.Location = new System.Drawing.Point(3, 54);
            this.lblTurnOffGasLeakTest.Name = "lblTurnOffGasLeakTest";
            this.lblTurnOffGasLeakTest.Size = new System.Drawing.Size(210, 29);
            this.lblTurnOffGasLeakTest.TabIndex = 13;
            this.lblTurnOffGasLeakTest.Text = "Turn off gas once leak test is complete & passes inspection";
            this.lblTurnOffGasLeakTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblEnsureFieldGasLeakTest
            // 
            this.lblEnsureFieldGasLeakTest.AutoSize = true;
            this.lblEnsureFieldGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEnsureFieldGasLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnsureFieldGasLeakTest.Location = new System.Drawing.Point(3, 0);
            this.lblEnsureFieldGasLeakTest.Name = "lblEnsureFieldGasLeakTest";
            this.lblEnsureFieldGasLeakTest.Size = new System.Drawing.Size(210, 27);
            this.lblEnsureFieldGasLeakTest.TabIndex = 12;
            this.lblEnsureFieldGasLeakTest.Text = "Ensure field gas port plug is present";
            this.lblEnsureFieldGasLeakTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbFieldGasPortPrePowerChecks
            // 
            this.cmbFieldGasPortPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFieldGasPortPrePowerChecks.FormattingEnabled = true;
            this.cmbFieldGasPortPrePowerChecks.Location = new System.Drawing.Point(219, 3);
            this.cmbFieldGasPortPrePowerChecks.MaxLength = 1;
            this.cmbFieldGasPortPrePowerChecks.Name = "cmbFieldGasPortPrePowerChecks";
            this.cmbFieldGasPortPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbFieldGasPortPrePowerChecks.TabIndex = 3;
            this.cmbFieldGasPortPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbPlugMissingPrePowerChecks
            // 
            this.cmbPlugMissingPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPlugMissingPrePowerChecks.FormattingEnabled = true;
            this.cmbPlugMissingPrePowerChecks.Location = new System.Drawing.Point(219, 30);
            this.cmbPlugMissingPrePowerChecks.MaxLength = 1;
            this.cmbPlugMissingPrePowerChecks.Name = "cmbPlugMissingPrePowerChecks";
            this.cmbPlugMissingPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbPlugMissingPrePowerChecks.TabIndex = 4;
            this.cmbPlugMissingPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblPlugsAreMissingLeakTest
            // 
            this.lblPlugsAreMissingLeakTest.AutoSize = true;
            this.lblPlugsAreMissingLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPlugsAreMissingLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlugsAreMissingLeakTest.Location = new System.Drawing.Point(3, 27);
            this.lblPlugsAreMissingLeakTest.Name = "lblPlugsAreMissingLeakTest";
            this.lblPlugsAreMissingLeakTest.Size = new System.Drawing.Size(210, 27);
            this.lblPlugsAreMissingLeakTest.TabIndex = 1;
            this.lblPlugsAreMissingLeakTest.Text = "Do not continue if Iniitals or Plugs are Missing";
            this.lblPlugsAreMissingLeakTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlElectricHeat
            // 
            this.pnlElectricHeat.Controls.Add(this.panel5);
            this.pnlElectricHeat.Controls.Add(this.gbElectricHeat);
            this.pnlElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlElectricHeat.Location = new System.Drawing.Point(615, 3);
            this.pnlElectricHeat.Name = "pnlElectricHeat";
            this.pnlElectricHeat.Size = new System.Drawing.Size(300, 426);
            this.pnlElectricHeat.TabIndex = 4;
            // 
            // PrePowerChecksControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.tlpProductonPretestingChecks);
            this.Name = "PrePowerChecksControl";
            this.Size = new System.Drawing.Size(1226, 496);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.gbMiscNotes.ResumeLayout(false);
            this.gbMiscNotes.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.gbPrePowerFinalSteps.ResumeLayout(false);
            this.tlpPrePowerFinalSteps.ResumeLayout(false);
            this.tlpPrePowerFinalSteps.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.gbPrePowerChecks.ResumeLayout(false);
            this.tlpPrePowerChecks.ResumeLayout(false);
            this.tlpPrePowerChecks.PerformLayout();
            this.tlpProductonPretestingChecks.ResumeLayout(false);
            this.tlpProductonPretestingChecks.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderPrePowerChecks)).EndInit();
            this.pnlPowerChecks.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.gbElectricHeat.ResumeLayout(false);
            this.tlpElectricHeat.ResumeLayout(false);
            this.tlpElectricHeat.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.gbGasHeat.ResumeLayout(false);
            this.gbGasHeat.PerformLayout();
            this.tlpGasHeat.ResumeLayout(false);
            this.tlpGasHeat.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.gbGasLeakTest.ResumeLayout(false);
            this.tlpGasLeakTest.ResumeLayout(false);
            this.tlpGasLeakTest.PerformLayout();
            this.pnlElectricHeat.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnPrintPrePowerChecks;
        private System.Windows.Forms.GroupBox gbPrePowerFinalSteps;
        private System.Windows.Forms.TableLayoutPanel tlpPrePowerFinalSteps;
        private System.Windows.Forms.ComboBox cmbUnlockCorrectVoltageDisconnectPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbSetPhaseMonitorPrePowerChecks;
        private System.Windows.Forms.Label lblSetPhaseMonitor;
        private System.Windows.Forms.Label lblUnlockCorrectVoltageDisconnect;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox gbPrePowerChecks;
        private System.Windows.Forms.TableLayoutPanel tlpPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCoreSenseSensorPrePowerChecks;
        private System.Windows.Forms.Label lblConnectPowerHeads;
        private System.Windows.Forms.Label lblRemoveAll3HighPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbPowerWhipPrePowerChecks;
        private System.Windows.Forms.Label lblContinuityHighVoltageTermination;
        private System.Windows.Forms.ComboBox cmbHiPotECMPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbFanMotorsPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCorrectSizeWhipPrePowerChecks;
        private System.Windows.Forms.Label lblAllFanMotorsPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbConnectPowerPrePowerChecks;
        private System.Windows.Forms.Label lblCheckContinuityPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCheckContinuityPrePower;
        private System.Windows.Forms.Label lblHiPotUnitPrePowerChecks;
        private System.Windows.Forms.Label lblNoPowerUnitPrePowerChecks;
        private System.Windows.Forms.TableLayoutPanel tlpProductonPretestingChecks;
        private System.Windows.Forms.Label lblProductPreTesting;
        private System.Windows.Forms.Label lblPleaseCompletePretestChecklist;
        private System.Windows.Forms.Label lblEnteraPAndF;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.GroupBox gbMiscNotes;
        public System.Windows.Forms.TextBox tbPrePowerMiscNotes;
        private System.Windows.Forms.ErrorProvider errorProviderPrePowerChecks;
        private System.Windows.Forms.Panel pnlGasLeakTest;
        private System.Windows.Forms.Panel pnlPowerChecks;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlElectricHeat;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.GroupBox gbGasLeakTest;
        private System.Windows.Forms.TableLayoutPanel tlpGasLeakTest;
        private System.Windows.Forms.Label lblPlugsAreMissingLeakTest;
        private System.Windows.Forms.ComboBox cmbPlugMissingPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbFieldGasPortPrePowerChecks;
        private System.Windows.Forms.Label lblEnsureFieldGasLeakTest;
        private System.Windows.Forms.Label lblTurnOffGasLeakTest;
        private System.Windows.Forms.ComboBox cmbTurnOffGasLeakTestPrePowerChecks;
        private System.Windows.Forms.GroupBox gbGasHeat;
        private System.Windows.Forms.TableLayoutPanel tlpGasHeat;
        private System.Windows.Forms.ComboBox cmbVoltageOnTransformerPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbHeaterControlBoardPrePowerChecks;
        private System.Windows.Forms.Label lblCheckHighVoltageElectricHeat;
        private System.Windows.Forms.Label lblCorrectVoltageGasHeat;
        private System.Windows.Forms.GroupBox gbElectricHeat;
        private System.Windows.Forms.TableLayoutPanel tlpElectricHeat;
        private System.Windows.Forms.ComboBox cmbInspectElementsPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCheckPowerTerminationPrePowerChecks;
        private System.Windows.Forms.Label lblCheckPowerTerminationElectricHeat;
        private System.Windows.Forms.Label lblInspectElementHeaterElectricHeat;
    }
}
