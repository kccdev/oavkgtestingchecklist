﻿namespace OA_VKG_TestingChecklist.UserControlObject
{
    partial class PreChecksControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tlpPreChecks = new System.Windows.Forms.TableLayoutPanel();
            this.label150 = new System.Windows.Forms.Label();
            this.label151 = new System.Windows.Forms.Label();
            this.label152 = new System.Windows.Forms.Label();
            this.label153 = new System.Windows.Forms.Label();
            this.tlpgrpControls = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pnlVisualInspection = new System.Windows.Forms.Panel();
            this.pnlCrankcase = new System.Windows.Forms.Panel();
            this.gbVisualInspectionCompressorsCrankcase = new System.Windows.Forms.GroupBox();
            this.tlpVisualInspectionCompressorsCrankcase = new System.Windows.Forms.TableLayoutPanel();
            this.cmbCrankcaseVoltage = new System.Windows.Forms.ComboBox();
            this.cmbVerifyPlugHarness = new System.Windows.Forms.ComboBox();
            this.lblCrankcaseCorrectVoltage = new System.Windows.Forms.Label();
            this.lblVerifyPlugHarnessCorrect = new System.Windows.Forms.Label();
            this.gbVisualInspection = new System.Windows.Forms.GroupBox();
            this.tlpVisualInspection = new System.Windows.Forms.TableLayoutPanel();
            this.cmbInspectUnit = new System.Windows.Forms.ComboBox();
            this.cmbPowerWires = new System.Windows.Forms.ComboBox();
            this.cmbLineLoad = new System.Windows.Forms.ComboBox();
            this.cmbTerminatioMode = new System.Windows.Forms.ComboBox();
            this.lblInspectUnitItemsLeftUnit = new System.Windows.Forms.Label();
            this.lblPowerWiresAllDrives = new System.Windows.Forms.Label();
            this.lblLineLoadCorrectly = new System.Windows.Forms.Label();
            this.lblAllTerminationsProperConnectors = new System.Windows.Forms.Label();
            this.gbPreChecks = new System.Windows.Forms.GroupBox();
            this.tlpPreChecksContainer = new System.Windows.Forms.TableLayoutPanel();
            this.cmbSubmittalUnitMatch = new System.Windows.Forms.ComboBox();
            this.cmbUnitBuiltCCS = new System.Windows.Forms.ComboBox();
            this.lblVerifySubmittalUnitMatch = new System.Windows.Forms.Label();
            this.lblUnitBuiltComplianceCCS = new System.Windows.Forms.Label();
            this.pnlPowerChecks = new System.Windows.Forms.Panel();
            this.pnlElectricHeat = new System.Windows.Forms.Panel();
            this.gbElectricHeat = new System.Windows.Forms.GroupBox();
            this.tlpElectricHeat = new System.Windows.Forms.TableLayoutPanel();
            this.cmbInspectElementsPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbCheckPowerTerminationPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblCheckPowerTerminationElectricHeat = new System.Windows.Forms.Label();
            this.lblInspectElementHeaterElectricHeat = new System.Windows.Forms.Label();
            this.gbPrePowerChecks = new System.Windows.Forms.GroupBox();
            this.tlpPrePowerChecks = new System.Windows.Forms.TableLayoutPanel();
            this.cmbCoreSenseSensorPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblConnectPowerHeads = new System.Windows.Forms.Label();
            this.lblRemoveAll3HighPrePowerChecks = new System.Windows.Forms.Label();
            this.cmbPowerWhipPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblContinuityHighVoltageTermination = new System.Windows.Forms.Label();
            this.cmbHiPotECMPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbFanMotorsPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbCorrectSizeWhipPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblAllFanMotorsPrePowerChecks = new System.Windows.Forms.Label();
            this.cmbConnectPowerPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblCheckContinuityPrePowerChecks = new System.Windows.Forms.Label();
            this.cmbCheckContinuityPrePower = new System.Windows.Forms.ComboBox();
            this.lblHiPotUnitPrePowerChecks = new System.Windows.Forms.Label();
            this.lblNoPowerUnitPrePowerChecks = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbPrePowerFinalSteps = new System.Windows.Forms.GroupBox();
            this.tlpPrePowerFinalSteps = new System.Windows.Forms.TableLayoutPanel();
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbSetPhaseMonitorPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblSetPhaseMonitor = new System.Windows.Forms.Label();
            this.lblUnlockCorrectVoltageDisconnect = new System.Windows.Forms.Label();
            this.gbGasLeakTest = new System.Windows.Forms.GroupBox();
            this.tlpGasLeakTest = new System.Windows.Forms.TableLayoutPanel();
            this.lblPlugsAreMissingLeakTest = new System.Windows.Forms.Label();
            this.cmbPlugMissingPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbFieldGasPortPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblEnsureFieldGasLeakTest = new System.Windows.Forms.Label();
            this.lblTurnOffGasLeakTest = new System.Windows.Forms.Label();
            this.cmbTurnOffGasLeakTestPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.gbGasHeat = new System.Windows.Forms.GroupBox();
            this.tlpGasHeat = new System.Windows.Forms.TableLayoutPanel();
            this.cmbVoltageOnTransformerPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.cmbHeaterControlBoardPrePowerChecks = new System.Windows.Forms.ComboBox();
            this.lblCheckHighVoltageElectricHeat = new System.Windows.Forms.Label();
            this.lblCorrectVoltageGasHeat = new System.Windows.Forms.Label();
            this.gbMiscNotes = new System.Windows.Forms.GroupBox();
            this.tbPreChecksMiscNotes = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnPrintPreChecks = new System.Windows.Forms.Button();
            this.errorProviderApp = new System.Windows.Forms.ErrorProvider(this.components);
            this.tlpPreChecks.SuspendLayout();
            this.tlpgrpControls.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlVisualInspection.SuspendLayout();
            this.pnlCrankcase.SuspendLayout();
            this.gbVisualInspectionCompressorsCrankcase.SuspendLayout();
            this.tlpVisualInspectionCompressorsCrankcase.SuspendLayout();
            this.gbVisualInspection.SuspendLayout();
            this.tlpVisualInspection.SuspendLayout();
            this.gbPreChecks.SuspendLayout();
            this.tlpPreChecksContainer.SuspendLayout();
            this.pnlPowerChecks.SuspendLayout();
            this.pnlElectricHeat.SuspendLayout();
            this.gbElectricHeat.SuspendLayout();
            this.tlpElectricHeat.SuspendLayout();
            this.gbPrePowerChecks.SuspendLayout();
            this.tlpPrePowerChecks.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbPrePowerFinalSteps.SuspendLayout();
            this.tlpPrePowerFinalSteps.SuspendLayout();
            this.gbGasLeakTest.SuspendLayout();
            this.tlpGasLeakTest.SuspendLayout();
            this.gbGasHeat.SuspendLayout();
            this.tlpGasHeat.SuspendLayout();
            this.gbMiscNotes.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderApp)).BeginInit();
            this.SuspendLayout();
            // 
            // tlpPreChecks
            // 
            this.tlpPreChecks.ColumnCount = 1;
            this.tlpPreChecks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPreChecks.Controls.Add(this.label150, 0, 0);
            this.tlpPreChecks.Controls.Add(this.label151, 0, 1);
            this.tlpPreChecks.Controls.Add(this.label152, 0, 2);
            this.tlpPreChecks.Controls.Add(this.label153, 0, 3);
            this.tlpPreChecks.Dock = System.Windows.Forms.DockStyle.Top;
            this.tlpPreChecks.Location = new System.Drawing.Point(0, 0);
            this.tlpPreChecks.Name = "tlpPreChecks";
            this.tlpPreChecks.RowCount = 4;
            this.tlpPreChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tlpPreChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tlpPreChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tlpPreChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tlpPreChecks.Size = new System.Drawing.Size(1250, 64);
            this.tlpPreChecks.TabIndex = 149;
            // 
            // label150
            // 
            this.label150.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label150.AutoSize = true;
            this.label150.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label150.Location = new System.Drawing.Point(522, 0);
            this.label150.Name = "label150";
            this.label150.Size = new System.Drawing.Size(206, 21);
            this.label150.TabIndex = 0;
            this.label150.Text = "Product Pre-Testing Checks";
            // 
            // label151
            // 
            this.label151.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label151.AutoSize = true;
            this.label151.Location = new System.Drawing.Point(417, 21);
            this.label151.Name = "label151";
            this.label151.Size = new System.Drawing.Size(415, 14);
            this.label151.TabIndex = 1;
            this.label151.Text = "Please complete the Pre-Test Checklist for every unit that comes off the assembly" +
    " line. ";
            // 
            // label152
            // 
            this.label152.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label152.AutoSize = true;
            this.label152.Location = new System.Drawing.Point(484, 35);
            this.label152.Name = "label152";
            this.label152.Size = new System.Drawing.Size(281, 14);
            this.label152.TabIndex = 2;
            this.label152.Text = " Enter a ‘P’ for Pass, ‘F’ for Fail, or ‘N’ for does not apply.  ";
            // 
            // label153
            // 
            this.label153.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label153.AutoSize = true;
            this.label153.Location = new System.Drawing.Point(407, 49);
            this.label153.Name = "label153";
            this.label153.Size = new System.Drawing.Size(436, 15);
            this.label153.TabIndex = 3;
            this.label153.Text = "For any Failures reported, please document in the Misc Notes section what was inc" +
    "orrect.   ";
            // 
            // tlpgrpControls
            // 
            this.tlpgrpControls.ColumnCount = 3;
            this.tlpgrpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tlpgrpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlpgrpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tlpgrpControls.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpgrpControls.Controls.Add(this.panel4, 0, 0);
            this.tlpgrpControls.Controls.Add(this.pnlPowerChecks, 1, 0);
            this.tlpgrpControls.Controls.Add(this.panel5, 2, 0);
            this.tlpgrpControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpgrpControls.Location = new System.Drawing.Point(0, 64);
            this.tlpgrpControls.Name = "tlpgrpControls";
            this.tlpgrpControls.RowCount = 1;
            this.tlpgrpControls.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpgrpControls.Size = new System.Drawing.Size(1250, 600);
            this.tlpgrpControls.TabIndex = 150;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.pnlVisualInspection);
            this.panel4.Controls.Add(this.gbPreChecks);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(369, 594);
            this.panel4.TabIndex = 0;
            // 
            // pnlVisualInspection
            // 
            this.pnlVisualInspection.Controls.Add(this.pnlCrankcase);
            this.pnlVisualInspection.Controls.Add(this.gbVisualInspection);
            this.pnlVisualInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlVisualInspection.Location = new System.Drawing.Point(0, 75);
            this.pnlVisualInspection.Name = "pnlVisualInspection";
            this.pnlVisualInspection.Size = new System.Drawing.Size(369, 519);
            this.pnlVisualInspection.TabIndex = 3;
            // 
            // pnlCrankcase
            // 
            this.pnlCrankcase.Controls.Add(this.gbVisualInspectionCompressorsCrankcase);
            this.pnlCrankcase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCrankcase.Location = new System.Drawing.Point(0, 128);
            this.pnlCrankcase.Name = "pnlCrankcase";
            this.pnlCrankcase.Size = new System.Drawing.Size(369, 391);
            this.pnlCrankcase.TabIndex = 3;
            // 
            // gbVisualInspectionCompressorsCrankcase
            // 
            this.gbVisualInspectionCompressorsCrankcase.Controls.Add(this.tlpVisualInspectionCompressorsCrankcase);
            this.gbVisualInspectionCompressorsCrankcase.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbVisualInspectionCompressorsCrankcase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbVisualInspectionCompressorsCrankcase.Location = new System.Drawing.Point(0, 0);
            this.gbVisualInspectionCompressorsCrankcase.Name = "gbVisualInspectionCompressorsCrankcase";
            this.gbVisualInspectionCompressorsCrankcase.Size = new System.Drawing.Size(369, 92);
            this.gbVisualInspectionCompressorsCrankcase.TabIndex = 2;
            this.gbVisualInspectionCompressorsCrankcase.TabStop = false;
            this.gbVisualInspectionCompressorsCrankcase.Text = "VISUAL INSPECTION: COMPRESSORS CRANKCASE";
            // 
            // tlpVisualInspectionCompressorsCrankcase
            // 
            this.tlpVisualInspectionCompressorsCrankcase.ColumnCount = 2;
            this.tlpVisualInspectionCompressorsCrankcase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVisualInspectionCompressorsCrankcase.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpVisualInspectionCompressorsCrankcase.Controls.Add(this.cmbCrankcaseVoltage, 1, 5);
            this.tlpVisualInspectionCompressorsCrankcase.Controls.Add(this.cmbVerifyPlugHarness, 1, 1);
            this.tlpVisualInspectionCompressorsCrankcase.Controls.Add(this.lblCrankcaseCorrectVoltage, 0, 5);
            this.tlpVisualInspectionCompressorsCrankcase.Controls.Add(this.lblVerifyPlugHarnessCorrect, 0, 1);
            this.tlpVisualInspectionCompressorsCrankcase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpVisualInspectionCompressorsCrankcase.Location = new System.Drawing.Point(3, 16);
            this.tlpVisualInspectionCompressorsCrankcase.Name = "tlpVisualInspectionCompressorsCrankcase";
            this.tlpVisualInspectionCompressorsCrankcase.RowCount = 6;
            this.tlpVisualInspectionCompressorsCrankcase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspectionCompressorsCrankcase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspectionCompressorsCrankcase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspectionCompressorsCrankcase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspectionCompressorsCrankcase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspectionCompressorsCrankcase.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspectionCompressorsCrankcase.Size = new System.Drawing.Size(363, 73);
            this.tlpVisualInspectionCompressorsCrankcase.TabIndex = 0;
            // 
            // cmbCrankcaseVoltage
            // 
            this.cmbCrankcaseVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCrankcaseVoltage.FormattingEnabled = true;
            this.cmbCrankcaseVoltage.Location = new System.Drawing.Point(288, 30);
            this.cmbCrankcaseVoltage.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbCrankcaseVoltage.MaxLength = 1;
            this.cmbCrankcaseVoltage.Name = "cmbCrankcaseVoltage";
            this.cmbCrankcaseVoltage.Size = new System.Drawing.Size(60, 21);
            this.cmbCrankcaseVoltage.TabIndex = 2;
            this.cmbCrankcaseVoltage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbVerifyPlugHarness
            // 
            this.cmbVerifyPlugHarness.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVerifyPlugHarness.FormattingEnabled = true;
            this.cmbVerifyPlugHarness.Location = new System.Drawing.Point(288, 3);
            this.cmbVerifyPlugHarness.MaxLength = 1;
            this.cmbVerifyPlugHarness.Name = "cmbVerifyPlugHarness";
            this.cmbVerifyPlugHarness.Size = new System.Drawing.Size(60, 21);
            this.cmbVerifyPlugHarness.TabIndex = 1;
            this.cmbVerifyPlugHarness.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCrankcaseCorrectVoltage
            // 
            this.lblCrankcaseCorrectVoltage.AutoSize = true;
            this.lblCrankcaseCorrectVoltage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCrankcaseCorrectVoltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCrankcaseCorrectVoltage.Location = new System.Drawing.Point(3, 27);
            this.lblCrankcaseCorrectVoltage.Name = "lblCrankcaseCorrectVoltage";
            this.lblCrankcaseCorrectVoltage.Size = new System.Drawing.Size(279, 46);
            this.lblCrankcaseCorrectVoltage.TabIndex = 41;
            this.lblCrankcaseCorrectVoltage.Text = "Crankcase is correct voltage";
            this.lblCrankcaseCorrectVoltage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblVerifyPlugHarnessCorrect
            // 
            this.lblVerifyPlugHarnessCorrect.AutoSize = true;
            this.lblVerifyPlugHarnessCorrect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVerifyPlugHarnessCorrect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerifyPlugHarnessCorrect.Location = new System.Drawing.Point(3, 0);
            this.lblVerifyPlugHarnessCorrect.Name = "lblVerifyPlugHarnessCorrect";
            this.lblVerifyPlugHarnessCorrect.Size = new System.Drawing.Size(279, 27);
            this.lblVerifyPlugHarnessCorrect.TabIndex = 37;
            this.lblVerifyPlugHarnessCorrect.Text = "Verify plug harness are correct";
            this.lblVerifyPlugHarnessCorrect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbVisualInspection
            // 
            this.gbVisualInspection.Controls.Add(this.tlpVisualInspection);
            this.gbVisualInspection.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbVisualInspection.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbVisualInspection.Location = new System.Drawing.Point(0, 0);
            this.gbVisualInspection.Name = "gbVisualInspection";
            this.gbVisualInspection.Size = new System.Drawing.Size(369, 128);
            this.gbVisualInspection.TabIndex = 1;
            this.gbVisualInspection.TabStop = false;
            this.gbVisualInspection.Text = "VISUAL INSPECTION";
            // 
            // tlpVisualInspection
            // 
            this.tlpVisualInspection.ColumnCount = 2;
            this.tlpVisualInspection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpVisualInspection.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpVisualInspection.Controls.Add(this.cmbInspectUnit, 1, 0);
            this.tlpVisualInspection.Controls.Add(this.cmbPowerWires, 1, 2);
            this.tlpVisualInspection.Controls.Add(this.cmbLineLoad, 1, 3);
            this.tlpVisualInspection.Controls.Add(this.cmbTerminatioMode, 1, 4);
            this.tlpVisualInspection.Controls.Add(this.lblInspectUnitItemsLeftUnit, 0, 0);
            this.tlpVisualInspection.Controls.Add(this.lblPowerWiresAllDrives, 0, 2);
            this.tlpVisualInspection.Controls.Add(this.lblLineLoadCorrectly, 0, 3);
            this.tlpVisualInspection.Controls.Add(this.lblAllTerminationsProperConnectors, 0, 4);
            this.tlpVisualInspection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpVisualInspection.Location = new System.Drawing.Point(3, 16);
            this.tlpVisualInspection.Name = "tlpVisualInspection";
            this.tlpVisualInspection.RowCount = 6;
            this.tlpVisualInspection.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspection.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspection.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspection.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspection.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspection.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpVisualInspection.Size = new System.Drawing.Size(363, 109);
            this.tlpVisualInspection.TabIndex = 0;
            // 
            // cmbInspectUnit
            // 
            this.cmbInspectUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbInspectUnit.FormattingEnabled = true;
            this.cmbInspectUnit.Location = new System.Drawing.Point(288, 3);
            this.cmbInspectUnit.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbInspectUnit.MaxLength = 1;
            this.cmbInspectUnit.Name = "cmbInspectUnit";
            this.cmbInspectUnit.Size = new System.Drawing.Size(60, 21);
            this.cmbInspectUnit.TabIndex = 0;
            this.cmbInspectUnit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbPowerWires
            // 
            this.cmbPowerWires.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPowerWires.FormattingEnabled = true;
            this.cmbPowerWires.Location = new System.Drawing.Point(288, 30);
            this.cmbPowerWires.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbPowerWires.MaxLength = 1;
            this.cmbPowerWires.Name = "cmbPowerWires";
            this.cmbPowerWires.Size = new System.Drawing.Size(60, 21);
            this.cmbPowerWires.TabIndex = 2;
            this.cmbPowerWires.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbLineLoad
            // 
            this.cmbLineLoad.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbLineLoad.FormattingEnabled = true;
            this.cmbLineLoad.Location = new System.Drawing.Point(288, 57);
            this.cmbLineLoad.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbLineLoad.MaxLength = 1;
            this.cmbLineLoad.Name = "cmbLineLoad";
            this.cmbLineLoad.Size = new System.Drawing.Size(60, 21);
            this.cmbLineLoad.TabIndex = 3;
            this.cmbLineLoad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbTerminatioMode
            // 
            this.cmbTerminatioMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTerminatioMode.FormattingEnabled = true;
            this.cmbTerminatioMode.Location = new System.Drawing.Point(288, 84);
            this.cmbTerminatioMode.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbTerminatioMode.MaxLength = 1;
            this.cmbTerminatioMode.Name = "cmbTerminatioMode";
            this.cmbTerminatioMode.Size = new System.Drawing.Size(60, 21);
            this.cmbTerminatioMode.TabIndex = 4;
            this.cmbTerminatioMode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblInspectUnitItemsLeftUnit
            // 
            this.lblInspectUnitItemsLeftUnit.AutoSize = true;
            this.lblInspectUnitItemsLeftUnit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInspectUnitItemsLeftUnit.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectUnitItemsLeftUnit.Location = new System.Drawing.Point(3, 0);
            this.lblInspectUnitItemsLeftUnit.Name = "lblInspectUnitItemsLeftUnit";
            this.lblInspectUnitItemsLeftUnit.Size = new System.Drawing.Size(279, 27);
            this.lblInspectUnitItemsLeftUnit.TabIndex = 32;
            this.lblInspectUnitItemsLeftUnit.Text = "Inspect unit for items left in unit (debris)";
            this.lblInspectUnitItemsLeftUnit.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPowerWiresAllDrives
            // 
            this.lblPowerWiresAllDrives.AutoSize = true;
            this.lblPowerWiresAllDrives.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPowerWiresAllDrives.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPowerWiresAllDrives.Location = new System.Drawing.Point(3, 27);
            this.lblPowerWiresAllDrives.Name = "lblPowerWiresAllDrives";
            this.lblPowerWiresAllDrives.Size = new System.Drawing.Size(279, 27);
            this.lblPowerWiresAllDrives.TabIndex = 34;
            this.lblPowerWiresAllDrives.Text = "Power wires to all drives";
            this.lblPowerWiresAllDrives.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblLineLoadCorrectly
            // 
            this.lblLineLoadCorrectly.AutoSize = true;
            this.lblLineLoadCorrectly.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLineLoadCorrectly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLineLoadCorrectly.Location = new System.Drawing.Point(3, 54);
            this.lblLineLoadCorrectly.Name = "lblLineLoadCorrectly";
            this.lblLineLoadCorrectly.Size = new System.Drawing.Size(279, 27);
            this.lblLineLoadCorrectly.TabIndex = 35;
            this.lblLineLoadCorrectly.Text = "Line & Load are terminated correctly";
            this.lblLineLoadCorrectly.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblAllTerminationsProperConnectors
            // 
            this.lblAllTerminationsProperConnectors.AutoSize = true;
            this.lblAllTerminationsProperConnectors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAllTerminationsProperConnectors.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllTerminationsProperConnectors.Location = new System.Drawing.Point(3, 81);
            this.lblAllTerminationsProperConnectors.Name = "lblAllTerminationsProperConnectors";
            this.lblAllTerminationsProperConnectors.Size = new System.Drawing.Size(279, 27);
            this.lblAllTerminationsProperConnectors.TabIndex = 36;
            this.lblAllTerminationsProperConnectors.Text = "All terminations made with proper connectors";
            this.lblAllTerminationsProperConnectors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbPreChecks
            // 
            this.gbPreChecks.Controls.Add(this.tlpPreChecksContainer);
            this.gbPreChecks.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPreChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPreChecks.Location = new System.Drawing.Point(0, 0);
            this.gbPreChecks.Name = "gbPreChecks";
            this.gbPreChecks.Size = new System.Drawing.Size(369, 75);
            this.gbPreChecks.TabIndex = 0;
            this.gbPreChecks.TabStop = false;
            this.gbPreChecks.Text = "PRE-CHECKS";
            this.gbPreChecks.Enter += new System.EventHandler(this.gbPreChecks_Enter);
            // 
            // tlpPreChecksContainer
            // 
            this.tlpPreChecksContainer.ColumnCount = 2;
            this.tlpPreChecksContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPreChecksContainer.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpPreChecksContainer.Controls.Add(this.cmbSubmittalUnitMatch, 1, 0);
            this.tlpPreChecksContainer.Controls.Add(this.cmbUnitBuiltCCS, 1, 1);
            this.tlpPreChecksContainer.Controls.Add(this.lblVerifySubmittalUnitMatch, 0, 0);
            this.tlpPreChecksContainer.Controls.Add(this.lblUnitBuiltComplianceCCS, 0, 1);
            this.tlpPreChecksContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPreChecksContainer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tlpPreChecksContainer.Location = new System.Drawing.Point(3, 16);
            this.tlpPreChecksContainer.Name = "tlpPreChecksContainer";
            this.tlpPreChecksContainer.RowCount = 2;
            this.tlpPreChecksContainer.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPreChecksContainer.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPreChecksContainer.Size = new System.Drawing.Size(363, 56);
            this.tlpPreChecksContainer.TabIndex = 0;
            // 
            // cmbSubmittalUnitMatch
            // 
            this.cmbSubmittalUnitMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSubmittalUnitMatch.FormattingEnabled = true;
            this.cmbSubmittalUnitMatch.Location = new System.Drawing.Point(288, 3);
            this.cmbSubmittalUnitMatch.MaxLength = 1;
            this.cmbSubmittalUnitMatch.Name = "cmbSubmittalUnitMatch";
            this.cmbSubmittalUnitMatch.Size = new System.Drawing.Size(60, 21);
            this.cmbSubmittalUnitMatch.TabIndex = 0;
            this.cmbSubmittalUnitMatch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbUnitBuiltCCS
            // 
            this.cmbUnitBuiltCCS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnitBuiltCCS.FormattingEnabled = true;
            this.cmbUnitBuiltCCS.Location = new System.Drawing.Point(288, 30);
            this.cmbUnitBuiltCCS.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbUnitBuiltCCS.MaxLength = 1;
            this.cmbUnitBuiltCCS.Name = "cmbUnitBuiltCCS";
            this.cmbUnitBuiltCCS.Size = new System.Drawing.Size(60, 21);
            this.cmbUnitBuiltCCS.TabIndex = 1;
            this.cmbUnitBuiltCCS.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblVerifySubmittalUnitMatch
            // 
            this.lblVerifySubmittalUnitMatch.AutoSize = true;
            this.lblVerifySubmittalUnitMatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVerifySubmittalUnitMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVerifySubmittalUnitMatch.Location = new System.Drawing.Point(3, 0);
            this.lblVerifySubmittalUnitMatch.Name = "lblVerifySubmittalUnitMatch";
            this.lblVerifySubmittalUnitMatch.Size = new System.Drawing.Size(279, 27);
            this.lblVerifySubmittalUnitMatch.TabIndex = 6;
            this.lblVerifySubmittalUnitMatch.Text = "Verify Submittal & unit match";
            this.lblVerifySubmittalUnitMatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnitBuiltComplianceCCS
            // 
            this.lblUnitBuiltComplianceCCS.AutoSize = true;
            this.lblUnitBuiltComplianceCCS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnitBuiltComplianceCCS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnitBuiltComplianceCCS.Location = new System.Drawing.Point(3, 27);
            this.lblUnitBuiltComplianceCCS.Name = "lblUnitBuiltComplianceCCS";
            this.lblUnitBuiltComplianceCCS.Size = new System.Drawing.Size(279, 29);
            this.lblUnitBuiltComplianceCCS.TabIndex = 7;
            this.lblUnitBuiltComplianceCCS.Text = "Unit built in compliance with CCS";
            this.lblUnitBuiltComplianceCCS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlPowerChecks
            // 
            this.pnlPowerChecks.Controls.Add(this.pnlElectricHeat);
            this.pnlPowerChecks.Controls.Add(this.gbPrePowerChecks);
            this.pnlPowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlPowerChecks.Location = new System.Drawing.Point(378, 3);
            this.pnlPowerChecks.Name = "pnlPowerChecks";
            this.pnlPowerChecks.Size = new System.Drawing.Size(431, 594);
            this.pnlPowerChecks.TabIndex = 1;
            // 
            // pnlElectricHeat
            // 
            this.pnlElectricHeat.Controls.Add(this.gbElectricHeat);
            this.pnlElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlElectricHeat.Location = new System.Drawing.Point(0, 210);
            this.pnlElectricHeat.Name = "pnlElectricHeat";
            this.pnlElectricHeat.Size = new System.Drawing.Size(431, 384);
            this.pnlElectricHeat.TabIndex = 1;
            // 
            // gbElectricHeat
            // 
            this.gbElectricHeat.Controls.Add(this.tlpElectricHeat);
            this.gbElectricHeat.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbElectricHeat.Location = new System.Drawing.Point(0, 0);
            this.gbElectricHeat.Name = "gbElectricHeat";
            this.gbElectricHeat.Size = new System.Drawing.Size(431, 75);
            this.gbElectricHeat.TabIndex = 0;
            this.gbElectricHeat.TabStop = false;
            this.gbElectricHeat.Text = "ELECTRIC HEAT";
            // 
            // tlpElectricHeat
            // 
            this.tlpElectricHeat.ColumnCount = 2;
            this.tlpElectricHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpElectricHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpElectricHeat.Controls.Add(this.cmbInspectElementsPrePowerChecks, 1, 1);
            this.tlpElectricHeat.Controls.Add(this.cmbCheckPowerTerminationPrePowerChecks, 1, 0);
            this.tlpElectricHeat.Controls.Add(this.lblCheckPowerTerminationElectricHeat, 0, 0);
            this.tlpElectricHeat.Controls.Add(this.lblInspectElementHeaterElectricHeat, 0, 1);
            this.tlpElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpElectricHeat.Location = new System.Drawing.Point(3, 16);
            this.tlpElectricHeat.Name = "tlpElectricHeat";
            this.tlpElectricHeat.RowCount = 2;
            this.tlpElectricHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpElectricHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpElectricHeat.Size = new System.Drawing.Size(425, 56);
            this.tlpElectricHeat.TabIndex = 0;
            // 
            // cmbInspectElementsPrePowerChecks
            // 
            this.cmbInspectElementsPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbInspectElementsPrePowerChecks.FormattingEnabled = true;
            this.cmbInspectElementsPrePowerChecks.Location = new System.Drawing.Point(350, 30);
            this.cmbInspectElementsPrePowerChecks.MaxLength = 1;
            this.cmbInspectElementsPrePowerChecks.Name = "cmbInspectElementsPrePowerChecks";
            this.cmbInspectElementsPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbInspectElementsPrePowerChecks.TabIndex = 2;
            this.cmbInspectElementsPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbInspectElementsPrePowerChecks_SelectedIndexChanged);
            this.cmbInspectElementsPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbCheckPowerTerminationPrePowerChecks
            // 
            this.cmbCheckPowerTerminationPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCheckPowerTerminationPrePowerChecks.FormattingEnabled = true;
            this.cmbCheckPowerTerminationPrePowerChecks.Location = new System.Drawing.Point(350, 3);
            this.cmbCheckPowerTerminationPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbCheckPowerTerminationPrePowerChecks.MaxLength = 1;
            this.cmbCheckPowerTerminationPrePowerChecks.Name = "cmbCheckPowerTerminationPrePowerChecks";
            this.cmbCheckPowerTerminationPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbCheckPowerTerminationPrePowerChecks.TabIndex = 1;
            this.cmbCheckPowerTerminationPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbCheckPowerTerminationPrePowerChecks_SelectedIndexChanged);
            this.cmbCheckPowerTerminationPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCheckPowerTerminationElectricHeat
            // 
            this.lblCheckPowerTerminationElectricHeat.AutoSize = true;
            this.lblCheckPowerTerminationElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckPowerTerminationElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckPowerTerminationElectricHeat.Location = new System.Drawing.Point(3, 0);
            this.lblCheckPowerTerminationElectricHeat.Name = "lblCheckPowerTerminationElectricHeat";
            this.lblCheckPowerTerminationElectricHeat.Size = new System.Drawing.Size(341, 27);
            this.lblCheckPowerTerminationElectricHeat.TabIndex = 2;
            this.lblCheckPowerTerminationElectricHeat.Text = "Check power terminations on both sides of the terminal block";
            this.lblCheckPowerTerminationElectricHeat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblInspectElementHeaterElectricHeat
            // 
            this.lblInspectElementHeaterElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblInspectElementHeaterElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInspectElementHeaterElectricHeat.Location = new System.Drawing.Point(3, 27);
            this.lblInspectElementHeaterElectricHeat.Name = "lblInspectElementHeaterElectricHeat";
            this.lblInspectElementHeaterElectricHeat.Size = new System.Drawing.Size(341, 29);
            this.lblInspectElementHeaterElectricHeat.TabIndex = 3;
            this.lblInspectElementHeaterElectricHeat.Text = "Inspect elements of heater for debris/damage";
            this.lblInspectElementHeaterElectricHeat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbPrePowerChecks
            // 
            this.gbPrePowerChecks.Controls.Add(this.tlpPrePowerChecks);
            this.gbPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPrePowerChecks.Location = new System.Drawing.Point(0, 0);
            this.gbPrePowerChecks.Name = "gbPrePowerChecks";
            this.gbPrePowerChecks.Size = new System.Drawing.Size(431, 210);
            this.gbPrePowerChecks.TabIndex = 0;
            this.gbPrePowerChecks.TabStop = false;
            this.gbPrePowerChecks.Text = "PRE-POWER CHECKS";
            // 
            // tlpPrePowerChecks
            // 
            this.tlpPrePowerChecks.ColumnCount = 2;
            this.tlpPrePowerChecks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrePowerChecks.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpPrePowerChecks.Controls.Add(this.cmbCoreSenseSensorPrePowerChecks, 1, 6);
            this.tlpPrePowerChecks.Controls.Add(this.lblConnectPowerHeads, 0, 0);
            this.tlpPrePowerChecks.Controls.Add(this.lblRemoveAll3HighPrePowerChecks, 0, 6);
            this.tlpPrePowerChecks.Controls.Add(this.cmbPowerWhipPrePowerChecks, 1, 0);
            this.tlpPrePowerChecks.Controls.Add(this.lblContinuityHighVoltageTermination, 0, 1);
            this.tlpPrePowerChecks.Controls.Add(this.cmbHiPotECMPrePowerChecks, 1, 7);
            this.tlpPrePowerChecks.Controls.Add(this.cmbFanMotorsPrePowerChecks, 1, 5);
            this.tlpPrePowerChecks.Controls.Add(this.cmbCorrectSizeWhipPrePowerChecks, 1, 1);
            this.tlpPrePowerChecks.Controls.Add(this.lblAllFanMotorsPrePowerChecks, 0, 5);
            this.tlpPrePowerChecks.Controls.Add(this.cmbConnectPowerPrePowerChecks, 1, 3);
            this.tlpPrePowerChecks.Controls.Add(this.lblCheckContinuityPrePowerChecks, 0, 4);
            this.tlpPrePowerChecks.Controls.Add(this.cmbCheckContinuityPrePower, 1, 4);
            this.tlpPrePowerChecks.Controls.Add(this.lblHiPotUnitPrePowerChecks, 0, 7);
            this.tlpPrePowerChecks.Controls.Add(this.lblNoPowerUnitPrePowerChecks, 0, 3);
            this.tlpPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPrePowerChecks.Location = new System.Drawing.Point(3, 16);
            this.tlpPrePowerChecks.Name = "tlpPrePowerChecks";
            this.tlpPrePowerChecks.RowCount = 9;
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerChecks.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrePowerChecks.Size = new System.Drawing.Size(425, 191);
            this.tlpPrePowerChecks.TabIndex = 17;
            // 
            // cmbCoreSenseSensorPrePowerChecks
            // 
            this.cmbCoreSenseSensorPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCoreSenseSensorPrePowerChecks.FormattingEnabled = true;
            this.cmbCoreSenseSensorPrePowerChecks.Location = new System.Drawing.Point(350, 138);
            this.cmbCoreSenseSensorPrePowerChecks.MaxLength = 1;
            this.cmbCoreSenseSensorPrePowerChecks.Name = "cmbCoreSenseSensorPrePowerChecks";
            this.cmbCoreSenseSensorPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbCoreSenseSensorPrePowerChecks.TabIndex = 5;
            this.cmbCoreSenseSensorPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblConnectPowerHeads
            // 
            this.lblConnectPowerHeads.AutoSize = true;
            this.lblConnectPowerHeads.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConnectPowerHeads.Location = new System.Drawing.Point(3, 0);
            this.lblConnectPowerHeads.Name = "lblConnectPowerHeads";
            this.lblConnectPowerHeads.Size = new System.Drawing.Size(304, 13);
            this.lblConnectPowerHeads.TabIndex = 1;
            this.lblConnectPowerHeads.Text = "Power Whip is not overly tight and has some strain relief for test";
            // 
            // lblRemoveAll3HighPrePowerChecks
            // 
            this.lblRemoveAll3HighPrePowerChecks.AutoSize = true;
            this.lblRemoveAll3HighPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRemoveAll3HighPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRemoveAll3HighPrePowerChecks.Location = new System.Drawing.Point(3, 135);
            this.lblRemoveAll3HighPrePowerChecks.Name = "lblRemoveAll3HighPrePowerChecks";
            this.lblRemoveAll3HighPrePowerChecks.Size = new System.Drawing.Size(341, 27);
            this.lblRemoveAll3HighPrePowerChecks.TabIndex = 15;
            this.lblRemoveAll3HighPrePowerChecks.Text = "Remove all 3 high voltage connectors from the CoreSense sensor";
            // 
            // cmbPowerWhipPrePowerChecks
            // 
            this.cmbPowerWhipPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPowerWhipPrePowerChecks.FormattingEnabled = true;
            this.cmbPowerWhipPrePowerChecks.Location = new System.Drawing.Point(350, 3);
            this.cmbPowerWhipPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbPowerWhipPrePowerChecks.MaxLength = 1;
            this.cmbPowerWhipPrePowerChecks.Name = "cmbPowerWhipPrePowerChecks";
            this.cmbPowerWhipPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbPowerWhipPrePowerChecks.TabIndex = 0;
            this.cmbPowerWhipPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblContinuityHighVoltageTermination
            // 
            this.lblContinuityHighVoltageTermination.AutoSize = true;
            this.lblContinuityHighVoltageTermination.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblContinuityHighVoltageTermination.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContinuityHighVoltageTermination.Location = new System.Drawing.Point(3, 27);
            this.lblContinuityHighVoltageTermination.Name = "lblContinuityHighVoltageTermination";
            this.lblContinuityHighVoltageTermination.Size = new System.Drawing.Size(341, 27);
            this.lblContinuityHighVoltageTermination.TabIndex = 4;
            this.lblContinuityHighVoltageTermination.Text = "Correct size Whip for the unit";
            this.lblContinuityHighVoltageTermination.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbHiPotECMPrePowerChecks
            // 
            this.cmbHiPotECMPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHiPotECMPrePowerChecks.FormattingEnabled = true;
            this.cmbHiPotECMPrePowerChecks.Location = new System.Drawing.Point(350, 165);
            this.cmbHiPotECMPrePowerChecks.MaxLength = 1;
            this.cmbHiPotECMPrePowerChecks.Name = "cmbHiPotECMPrePowerChecks";
            this.cmbHiPotECMPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbHiPotECMPrePowerChecks.TabIndex = 6;
            this.cmbHiPotECMPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbFanMotorsPrePowerChecks
            // 
            this.cmbFanMotorsPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFanMotorsPrePowerChecks.FormattingEnabled = true;
            this.cmbFanMotorsPrePowerChecks.Location = new System.Drawing.Point(350, 111);
            this.cmbFanMotorsPrePowerChecks.MaxLength = 1;
            this.cmbFanMotorsPrePowerChecks.Name = "cmbFanMotorsPrePowerChecks";
            this.cmbFanMotorsPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbFanMotorsPrePowerChecks.TabIndex = 4;
            this.cmbFanMotorsPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbCorrectSizeWhipPrePowerChecks
            // 
            this.cmbCorrectSizeWhipPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCorrectSizeWhipPrePowerChecks.FormattingEnabled = true;
            this.cmbCorrectSizeWhipPrePowerChecks.Location = new System.Drawing.Point(350, 30);
            this.cmbCorrectSizeWhipPrePowerChecks.MaxLength = 1;
            this.cmbCorrectSizeWhipPrePowerChecks.Name = "cmbCorrectSizeWhipPrePowerChecks";
            this.cmbCorrectSizeWhipPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbCorrectSizeWhipPrePowerChecks.TabIndex = 1;
            this.cmbCorrectSizeWhipPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblAllFanMotorsPrePowerChecks
            // 
            this.lblAllFanMotorsPrePowerChecks.AutoSize = true;
            this.lblAllFanMotorsPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAllFanMotorsPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAllFanMotorsPrePowerChecks.Location = new System.Drawing.Point(3, 108);
            this.lblAllFanMotorsPrePowerChecks.Name = "lblAllFanMotorsPrePowerChecks";
            this.lblAllFanMotorsPrePowerChecks.Size = new System.Drawing.Size(341, 27);
            this.lblAllFanMotorsPrePowerChecks.TabIndex = 13;
            this.lblAllFanMotorsPrePowerChecks.Text = "Check Continunity between phases on all fan motors";
            // 
            // cmbConnectPowerPrePowerChecks
            // 
            this.cmbConnectPowerPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbConnectPowerPrePowerChecks.FormattingEnabled = true;
            this.cmbConnectPowerPrePowerChecks.Location = new System.Drawing.Point(350, 57);
            this.cmbConnectPowerPrePowerChecks.MaxLength = 1;
            this.cmbConnectPowerPrePowerChecks.Name = "cmbConnectPowerPrePowerChecks";
            this.cmbConnectPowerPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbConnectPowerPrePowerChecks.TabIndex = 2;
            this.cmbConnectPowerPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCheckContinuityPrePowerChecks
            // 
            this.lblCheckContinuityPrePowerChecks.AutoSize = true;
            this.lblCheckContinuityPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckContinuityPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckContinuityPrePowerChecks.Location = new System.Drawing.Point(3, 81);
            this.lblCheckContinuityPrePowerChecks.Name = "lblCheckContinuityPrePowerChecks";
            this.lblCheckContinuityPrePowerChecks.Size = new System.Drawing.Size(341, 27);
            this.lblCheckContinuityPrePowerChecks.TabIndex = 0;
            this.lblCheckContinuityPrePowerChecks.Text = "Check Continuity to ground at every high voltage termination";
            // 
            // cmbCheckContinuityPrePower
            // 
            this.cmbCheckContinuityPrePower.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbCheckContinuityPrePower.FormattingEnabled = true;
            this.cmbCheckContinuityPrePower.Location = new System.Drawing.Point(350, 84);
            this.cmbCheckContinuityPrePower.MaxLength = 1;
            this.cmbCheckContinuityPrePower.Name = "cmbCheckContinuityPrePower";
            this.cmbCheckContinuityPrePower.Size = new System.Drawing.Size(60, 21);
            this.cmbCheckContinuityPrePower.TabIndex = 3;
            this.cmbCheckContinuityPrePower.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblHiPotUnitPrePowerChecks
            // 
            this.lblHiPotUnitPrePowerChecks.AutoSize = true;
            this.lblHiPotUnitPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHiPotUnitPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHiPotUnitPrePowerChecks.Location = new System.Drawing.Point(3, 162);
            this.lblHiPotUnitPrePowerChecks.Name = "lblHiPotUnitPrePowerChecks";
            this.lblHiPotUnitPrePowerChecks.Size = new System.Drawing.Size(341, 27);
            this.lblHiPotUnitPrePowerChecks.TabIndex = 17;
            this.lblHiPotUnitPrePowerChecks.Text = "Hi-Pot the unit (DO NOT HI POT ROSENBERG  FANS)";
            // 
            // lblNoPowerUnitPrePowerChecks
            // 
            this.lblNoPowerUnitPrePowerChecks.AutoSize = true;
            this.lblNoPowerUnitPrePowerChecks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNoPowerUnitPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoPowerUnitPrePowerChecks.Location = new System.Drawing.Point(3, 54);
            this.lblNoPowerUnitPrePowerChecks.Name = "lblNoPowerUnitPrePowerChecks";
            this.lblNoPowerUnitPrePowerChecks.Size = new System.Drawing.Size(341, 27);
            this.lblNoPowerUnitPrePowerChecks.TabIndex = 2;
            this.lblNoPowerUnitPrePowerChecks.Text = "Connect power leads to power whip. For grounding purposes only. No power to unit";
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.panel1);
            this.panel5.Controls.Add(this.gbMiscNotes);
            this.panel5.Controls.Add(this.panel6);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(815, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(432, 594);
            this.panel5.TabIndex = 147;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.gbGasHeat);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(432, 472);
            this.panel1.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.gbGasLeakTest);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 75);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(432, 397);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.gbPrePowerFinalSteps);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 102);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(432, 295);
            this.panel2.TabIndex = 1;
            // 
            // gbPrePowerFinalSteps
            // 
            this.gbPrePowerFinalSteps.Controls.Add(this.tlpPrePowerFinalSteps);
            this.gbPrePowerFinalSteps.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbPrePowerFinalSteps.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbPrePowerFinalSteps.Location = new System.Drawing.Point(0, 0);
            this.gbPrePowerFinalSteps.Name = "gbPrePowerFinalSteps";
            this.gbPrePowerFinalSteps.Size = new System.Drawing.Size(432, 75);
            this.gbPrePowerFinalSteps.TabIndex = 0;
            this.gbPrePowerFinalSteps.TabStop = false;
            this.gbPrePowerFinalSteps.Text = "PRE-POWER FINAL CHECKS";
            // 
            // tlpPrePowerFinalSteps
            // 
            this.tlpPrePowerFinalSteps.ColumnCount = 2;
            this.tlpPrePowerFinalSteps.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpPrePowerFinalSteps.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpPrePowerFinalSteps.Controls.Add(this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks, 1, 5);
            this.tlpPrePowerFinalSteps.Controls.Add(this.cmbSetPhaseMonitorPrePowerChecks, 1, 3);
            this.tlpPrePowerFinalSteps.Controls.Add(this.lblSetPhaseMonitor, 0, 3);
            this.tlpPrePowerFinalSteps.Controls.Add(this.lblUnlockCorrectVoltageDisconnect, 0, 5);
            this.tlpPrePowerFinalSteps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpPrePowerFinalSteps.Location = new System.Drawing.Point(3, 16);
            this.tlpPrePowerFinalSteps.Name = "tlpPrePowerFinalSteps";
            this.tlpPrePowerFinalSteps.RowCount = 6;
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpPrePowerFinalSteps.Size = new System.Drawing.Size(426, 56);
            this.tlpPrePowerFinalSteps.TabIndex = 14;
            // 
            // cmbUnlockCorrectVoltageDisconnectPrePowerChecks
            // 
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.FormattingEnabled = true;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.IntegralHeight = false;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Location = new System.Drawing.Point(351, 30);
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.MaxLength = 1;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Name = "cmbUnlockCorrectVoltageDisconnectPrePowerChecks";
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.TabIndex = 2;
            this.cmbUnlockCorrectVoltageDisconnectPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbSetPhaseMonitorPrePowerChecks
            // 
            this.cmbSetPhaseMonitorPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSetPhaseMonitorPrePowerChecks.FormattingEnabled = true;
            this.cmbSetPhaseMonitorPrePowerChecks.Location = new System.Drawing.Point(351, 3);
            this.cmbSetPhaseMonitorPrePowerChecks.MaxLength = 1;
            this.cmbSetPhaseMonitorPrePowerChecks.Name = "cmbSetPhaseMonitorPrePowerChecks";
            this.cmbSetPhaseMonitorPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbSetPhaseMonitorPrePowerChecks.TabIndex = 1;
            this.cmbSetPhaseMonitorPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblSetPhaseMonitor
            // 
            this.lblSetPhaseMonitor.AutoSize = true;
            this.lblSetPhaseMonitor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSetPhaseMonitor.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSetPhaseMonitor.Location = new System.Drawing.Point(3, 0);
            this.lblSetPhaseMonitor.Name = "lblSetPhaseMonitor";
            this.lblSetPhaseMonitor.Size = new System.Drawing.Size(342, 27);
            this.lblSetPhaseMonitor.TabIndex = 15;
            this.lblSetPhaseMonitor.Text = "Set phase monitor";
            this.lblSetPhaseMonitor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUnlockCorrectVoltageDisconnect
            // 
            this.lblUnlockCorrectVoltageDisconnect.AutoSize = true;
            this.lblUnlockCorrectVoltageDisconnect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUnlockCorrectVoltageDisconnect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnlockCorrectVoltageDisconnect.Location = new System.Drawing.Point(3, 27);
            this.lblUnlockCorrectVoltageDisconnect.Name = "lblUnlockCorrectVoltageDisconnect";
            this.lblUnlockCorrectVoltageDisconnect.Size = new System.Drawing.Size(342, 29);
            this.lblUnlockCorrectVoltageDisconnect.TabIndex = 0;
            this.lblUnlockCorrectVoltageDisconnect.Text = "Unlock - correct voltage disconnect";
            this.lblUnlockCorrectVoltageDisconnect.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbGasLeakTest
            // 
            this.gbGasLeakTest.Controls.Add(this.tlpGasLeakTest);
            this.gbGasLeakTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbGasLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGasLeakTest.Location = new System.Drawing.Point(0, 0);
            this.gbGasLeakTest.Name = "gbGasLeakTest";
            this.gbGasLeakTest.Size = new System.Drawing.Size(432, 102);
            this.gbGasLeakTest.TabIndex = 0;
            this.gbGasLeakTest.TabStop = false;
            this.gbGasLeakTest.Text = "GAS LEAK TEST";
            // 
            // tlpGasLeakTest
            // 
            this.tlpGasLeakTest.ColumnCount = 2;
            this.tlpGasLeakTest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpGasLeakTest.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpGasLeakTest.Controls.Add(this.lblPlugsAreMissingLeakTest, 0, 4);
            this.tlpGasLeakTest.Controls.Add(this.cmbPlugMissingPrePowerChecks, 1, 4);
            this.tlpGasLeakTest.Controls.Add(this.cmbFieldGasPortPrePowerChecks, 1, 3);
            this.tlpGasLeakTest.Controls.Add(this.lblEnsureFieldGasLeakTest, 0, 3);
            this.tlpGasLeakTest.Controls.Add(this.lblTurnOffGasLeakTest, 0, 5);
            this.tlpGasLeakTest.Controls.Add(this.cmbTurnOffGasLeakTestPrePowerChecks, 1, 5);
            this.tlpGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpGasLeakTest.Location = new System.Drawing.Point(3, 16);
            this.tlpGasLeakTest.Name = "tlpGasLeakTest";
            this.tlpGasLeakTest.RowCount = 6;
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasLeakTest.Size = new System.Drawing.Size(426, 83);
            this.tlpGasLeakTest.TabIndex = 11;
            // 
            // lblPlugsAreMissingLeakTest
            // 
            this.lblPlugsAreMissingLeakTest.AutoSize = true;
            this.lblPlugsAreMissingLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPlugsAreMissingLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlugsAreMissingLeakTest.Location = new System.Drawing.Point(3, 27);
            this.lblPlugsAreMissingLeakTest.Name = "lblPlugsAreMissingLeakTest";
            this.lblPlugsAreMissingLeakTest.Size = new System.Drawing.Size(342, 27);
            this.lblPlugsAreMissingLeakTest.TabIndex = 1;
            this.lblPlugsAreMissingLeakTest.Text = "Do not continue if Iniitals or Plugs are Missing";
            this.lblPlugsAreMissingLeakTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbPlugMissingPrePowerChecks
            // 
            this.cmbPlugMissingPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPlugMissingPrePowerChecks.FormattingEnabled = true;
            this.cmbPlugMissingPrePowerChecks.Location = new System.Drawing.Point(351, 30);
            this.cmbPlugMissingPrePowerChecks.MaxLength = 1;
            this.cmbPlugMissingPrePowerChecks.Name = "cmbPlugMissingPrePowerChecks";
            this.cmbPlugMissingPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbPlugMissingPrePowerChecks.TabIndex = 4;
            this.cmbPlugMissingPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbFieldGasPortPrePowerChecks
            // 
            this.cmbFieldGasPortPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFieldGasPortPrePowerChecks.FormattingEnabled = true;
            this.cmbFieldGasPortPrePowerChecks.Location = new System.Drawing.Point(351, 3);
            this.cmbFieldGasPortPrePowerChecks.MaxLength = 1;
            this.cmbFieldGasPortPrePowerChecks.Name = "cmbFieldGasPortPrePowerChecks";
            this.cmbFieldGasPortPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbFieldGasPortPrePowerChecks.TabIndex = 3;
            this.cmbFieldGasPortPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblEnsureFieldGasLeakTest
            // 
            this.lblEnsureFieldGasLeakTest.AutoSize = true;
            this.lblEnsureFieldGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEnsureFieldGasLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnsureFieldGasLeakTest.Location = new System.Drawing.Point(3, 0);
            this.lblEnsureFieldGasLeakTest.Name = "lblEnsureFieldGasLeakTest";
            this.lblEnsureFieldGasLeakTest.Size = new System.Drawing.Size(342, 27);
            this.lblEnsureFieldGasLeakTest.TabIndex = 12;
            this.lblEnsureFieldGasLeakTest.Text = "Ensure field gas port plug is present";
            this.lblEnsureFieldGasLeakTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTurnOffGasLeakTest
            // 
            this.lblTurnOffGasLeakTest.AutoSize = true;
            this.lblTurnOffGasLeakTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTurnOffGasLeakTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTurnOffGasLeakTest.Location = new System.Drawing.Point(3, 54);
            this.lblTurnOffGasLeakTest.Name = "lblTurnOffGasLeakTest";
            this.lblTurnOffGasLeakTest.Size = new System.Drawing.Size(342, 29);
            this.lblTurnOffGasLeakTest.TabIndex = 13;
            this.lblTurnOffGasLeakTest.Text = "Turn off gas once leak test is complete & passes inspection";
            this.lblTurnOffGasLeakTest.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmbTurnOffGasLeakTestPrePowerChecks
            // 
            this.cmbTurnOffGasLeakTestPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTurnOffGasLeakTestPrePowerChecks.FormattingEnabled = true;
            this.cmbTurnOffGasLeakTestPrePowerChecks.Location = new System.Drawing.Point(351, 57);
            this.cmbTurnOffGasLeakTestPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbTurnOffGasLeakTestPrePowerChecks.MaxLength = 1;
            this.cmbTurnOffGasLeakTestPrePowerChecks.Name = "cmbTurnOffGasLeakTestPrePowerChecks";
            this.cmbTurnOffGasLeakTestPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbTurnOffGasLeakTestPrePowerChecks.TabIndex = 5;
            this.cmbTurnOffGasLeakTestPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // gbGasHeat
            // 
            this.gbGasHeat.Controls.Add(this.tlpGasHeat);
            this.gbGasHeat.Dock = System.Windows.Forms.DockStyle.Top;
            this.gbGasHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGasHeat.Location = new System.Drawing.Point(0, 0);
            this.gbGasHeat.Name = "gbGasHeat";
            this.gbGasHeat.Size = new System.Drawing.Size(432, 75);
            this.gbGasHeat.TabIndex = 0;
            this.gbGasHeat.TabStop = false;
            this.gbGasHeat.Text = "GAS HEAT";
            // 
            // tlpGasHeat
            // 
            this.tlpGasHeat.AutoSize = true;
            this.tlpGasHeat.ColumnCount = 2;
            this.tlpGasHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpGasHeat.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tlpGasHeat.Controls.Add(this.cmbVoltageOnTransformerPrePowerChecks, 1, 1);
            this.tlpGasHeat.Controls.Add(this.cmbHeaterControlBoardPrePowerChecks, 1, 0);
            this.tlpGasHeat.Controls.Add(this.lblCheckHighVoltageElectricHeat, 0, 0);
            this.tlpGasHeat.Controls.Add(this.lblCorrectVoltageGasHeat, 0, 1);
            this.tlpGasHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpGasHeat.Location = new System.Drawing.Point(3, 16);
            this.tlpGasHeat.Name = "tlpGasHeat";
            this.tlpGasHeat.RowCount = 2;
            this.tlpGasHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasHeat.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tlpGasHeat.Size = new System.Drawing.Size(426, 56);
            this.tlpGasHeat.TabIndex = 0;
            // 
            // cmbVoltageOnTransformerPrePowerChecks
            // 
            this.cmbVoltageOnTransformerPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbVoltageOnTransformerPrePowerChecks.FormattingEnabled = true;
            this.cmbVoltageOnTransformerPrePowerChecks.Location = new System.Drawing.Point(351, 30);
            this.cmbVoltageOnTransformerPrePowerChecks.MaxLength = 1;
            this.cmbVoltageOnTransformerPrePowerChecks.Name = "cmbVoltageOnTransformerPrePowerChecks";
            this.cmbVoltageOnTransformerPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbVoltageOnTransformerPrePowerChecks.TabIndex = 3;
            this.cmbVoltageOnTransformerPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbVoltageOnTransformerPrePowerChecks_SelectedIndexChanged);
            this.cmbVoltageOnTransformerPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // cmbHeaterControlBoardPrePowerChecks
            // 
            this.cmbHeaterControlBoardPrePowerChecks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbHeaterControlBoardPrePowerChecks.FormattingEnabled = true;
            this.cmbHeaterControlBoardPrePowerChecks.Location = new System.Drawing.Point(351, 3);
            this.cmbHeaterControlBoardPrePowerChecks.Margin = new System.Windows.Forms.Padding(3, 3, 15, 3);
            this.cmbHeaterControlBoardPrePowerChecks.MaxLength = 1;
            this.cmbHeaterControlBoardPrePowerChecks.Name = "cmbHeaterControlBoardPrePowerChecks";
            this.cmbHeaterControlBoardPrePowerChecks.Size = new System.Drawing.Size(60, 21);
            this.cmbHeaterControlBoardPrePowerChecks.TabIndex = 2;
            this.cmbHeaterControlBoardPrePowerChecks.SelectedIndexChanged += new System.EventHandler(this.cmbHeaterControlBoardPrePowerChecks_SelectedIndexChanged);
            this.cmbHeaterControlBoardPrePowerChecks.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ComboBox_KeyPress);
            // 
            // lblCheckHighVoltageElectricHeat
            // 
            this.lblCheckHighVoltageElectricHeat.AutoSize = true;
            this.lblCheckHighVoltageElectricHeat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCheckHighVoltageElectricHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCheckHighVoltageElectricHeat.Location = new System.Drawing.Point(3, 0);
            this.lblCheckHighVoltageElectricHeat.Name = "lblCheckHighVoltageElectricHeat";
            this.lblCheckHighVoltageElectricHeat.Size = new System.Drawing.Size(342, 27);
            this.lblCheckHighVoltageElectricHeat.TabIndex = 1;
            this.lblCheckHighVoltageElectricHeat.Text = "Check high voltage from the transformer is landed correctly on heater control boa" +
    "rd";
            // 
            // lblCorrectVoltageGasHeat
            // 
            this.lblCorrectVoltageGasHeat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorrectVoltageGasHeat.Location = new System.Drawing.Point(3, 27);
            this.lblCorrectVoltageGasHeat.Name = "lblCorrectVoltageGasHeat";
            this.lblCorrectVoltageGasHeat.Size = new System.Drawing.Size(110, 23);
            this.lblCorrectVoltageGasHeat.TabIndex = 0;
            this.lblCorrectVoltageGasHeat.Text = "Correct voltage written on transformer";
            this.lblCorrectVoltageGasHeat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // gbMiscNotes
            // 
            this.gbMiscNotes.Controls.Add(this.tbPreChecksMiscNotes);
            this.gbMiscNotes.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.gbMiscNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbMiscNotes.Location = new System.Drawing.Point(0, 472);
            this.gbMiscNotes.Name = "gbMiscNotes";
            this.gbMiscNotes.Size = new System.Drawing.Size(432, 88);
            this.gbMiscNotes.TabIndex = 1;
            this.gbMiscNotes.TabStop = false;
            this.gbMiscNotes.Text = "Misc. Notes";
            // 
            // tbPreChecksMiscNotes
            // 
            this.tbPreChecksMiscNotes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbPreChecksMiscNotes.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPreChecksMiscNotes.Location = new System.Drawing.Point(3, 16);
            this.tbPreChecksMiscNotes.Multiline = true;
            this.tbPreChecksMiscNotes.Name = "tbPreChecksMiscNotes";
            this.tbPreChecksMiscNotes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbPreChecksMiscNotes.Size = new System.Drawing.Size(426, 69);
            this.tbPreChecksMiscNotes.TabIndex = 0;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.btnPrintPreChecks);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel6.Location = new System.Drawing.Point(0, 560);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(432, 34);
            this.panel6.TabIndex = 2;
            // 
            // btnPrintPreChecks
            // 
            this.btnPrintPreChecks.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnPrintPreChecks.Location = new System.Drawing.Point(357, 0);
            this.btnPrintPreChecks.Name = "btnPrintPreChecks";
            this.btnPrintPreChecks.Size = new System.Drawing.Size(75, 34);
            this.btnPrintPreChecks.TabIndex = 0;
            this.btnPrintPreChecks.Text = "Print";
            this.btnPrintPreChecks.UseVisualStyleBackColor = true;
            this.btnPrintPreChecks.Click += new System.EventHandler(this.btnPrintPreChecks_Click);
            // 
            // errorProviderApp
            // 
            this.errorProviderApp.ContainerControl = this;
            // 
            // PreChecksControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tlpgrpControls);
            this.Controls.Add(this.tlpPreChecks);
            this.Name = "PreChecksControl";
            this.Size = new System.Drawing.Size(1250, 664);
            this.Load += new System.EventHandler(this.PreChecksControl_Load);
            this.tlpPreChecks.ResumeLayout(false);
            this.tlpPreChecks.PerformLayout();
            this.tlpgrpControls.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.pnlVisualInspection.ResumeLayout(false);
            this.pnlCrankcase.ResumeLayout(false);
            this.gbVisualInspectionCompressorsCrankcase.ResumeLayout(false);
            this.tlpVisualInspectionCompressorsCrankcase.ResumeLayout(false);
            this.tlpVisualInspectionCompressorsCrankcase.PerformLayout();
            this.gbVisualInspection.ResumeLayout(false);
            this.tlpVisualInspection.ResumeLayout(false);
            this.tlpVisualInspection.PerformLayout();
            this.gbPreChecks.ResumeLayout(false);
            this.tlpPreChecksContainer.ResumeLayout(false);
            this.tlpPreChecksContainer.PerformLayout();
            this.pnlPowerChecks.ResumeLayout(false);
            this.pnlElectricHeat.ResumeLayout(false);
            this.gbElectricHeat.ResumeLayout(false);
            this.tlpElectricHeat.ResumeLayout(false);
            this.tlpElectricHeat.PerformLayout();
            this.gbPrePowerChecks.ResumeLayout(false);
            this.tlpPrePowerChecks.ResumeLayout(false);
            this.tlpPrePowerChecks.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.gbPrePowerFinalSteps.ResumeLayout(false);
            this.tlpPrePowerFinalSteps.ResumeLayout(false);
            this.tlpPrePowerFinalSteps.PerformLayout();
            this.gbGasLeakTest.ResumeLayout(false);
            this.tlpGasLeakTest.ResumeLayout(false);
            this.tlpGasLeakTest.PerformLayout();
            this.gbGasHeat.ResumeLayout(false);
            this.gbGasHeat.PerformLayout();
            this.tlpGasHeat.ResumeLayout(false);
            this.tlpGasHeat.PerformLayout();
            this.gbMiscNotes.ResumeLayout(false);
            this.gbMiscNotes.PerformLayout();
            this.panel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProviderApp)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpPreChecks;
        private System.Windows.Forms.Label label150;
        private System.Windows.Forms.Label label151;
        private System.Windows.Forms.Label label152;
        private System.Windows.Forms.Label label153;
        private System.Windows.Forms.TableLayoutPanel tlpgrpControls;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.GroupBox gbMiscNotes;
        public System.Windows.Forms.TextBox tbPreChecksMiscNotes;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnPrintPreChecks;
        private System.Windows.Forms.GroupBox gbPreChecks;
        private System.Windows.Forms.TableLayoutPanel tlpPreChecksContainer;
        private System.Windows.Forms.ComboBox cmbUnitBuiltCCS;
        private System.Windows.Forms.ComboBox cmbSubmittalUnitMatch;
        private System.Windows.Forms.Label lblVerifySubmittalUnitMatch;
        private System.Windows.Forms.Label lblUnitBuiltComplianceCCS;
        private System.Windows.Forms.GroupBox gbVisualInspection;
        private System.Windows.Forms.TableLayoutPanel tlpVisualInspection;
        private System.Windows.Forms.ComboBox cmbInspectUnit;
        private System.Windows.Forms.ComboBox cmbPowerWires;
        private System.Windows.Forms.ComboBox cmbLineLoad;
        private System.Windows.Forms.ComboBox cmbTerminatioMode;
        private System.Windows.Forms.Label lblInspectUnitItemsLeftUnit;
        private System.Windows.Forms.Label lblPowerWiresAllDrives;
        private System.Windows.Forms.Label lblLineLoadCorrectly;
        private System.Windows.Forms.Label lblAllTerminationsProperConnectors;
        private System.Windows.Forms.GroupBox gbVisualInspectionCompressorsCrankcase;
        private System.Windows.Forms.TableLayoutPanel tlpVisualInspectionCompressorsCrankcase;
        private System.Windows.Forms.Label lblCrankcaseCorrectVoltage;
        private System.Windows.Forms.ComboBox cmbCrankcaseVoltage;
        private System.Windows.Forms.ComboBox cmbVerifyPlugHarness;
        private System.Windows.Forms.Label lblVerifyPlugHarnessCorrect;
        private System.Windows.Forms.ErrorProvider errorProviderApp;
        private System.Windows.Forms.Panel pnlVisualInspection;
        private System.Windows.Forms.Panel pnlCrankcase;
        private System.Windows.Forms.Panel pnlPowerChecks;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbPrePowerFinalSteps;
        private System.Windows.Forms.TableLayoutPanel tlpPrePowerFinalSteps;
        private System.Windows.Forms.ComboBox cmbUnlockCorrectVoltageDisconnectPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbSetPhaseMonitorPrePowerChecks;
        private System.Windows.Forms.Label lblSetPhaseMonitor;
        private System.Windows.Forms.Label lblUnlockCorrectVoltageDisconnect;
        private System.Windows.Forms.GroupBox gbPrePowerChecks;
        private System.Windows.Forms.TableLayoutPanel tlpPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCoreSenseSensorPrePowerChecks;
        private System.Windows.Forms.Label lblConnectPowerHeads;
        private System.Windows.Forms.Label lblRemoveAll3HighPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbPowerWhipPrePowerChecks;
        private System.Windows.Forms.Label lblContinuityHighVoltageTermination;
        private System.Windows.Forms.ComboBox cmbHiPotECMPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbFanMotorsPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCorrectSizeWhipPrePowerChecks;
        private System.Windows.Forms.Label lblAllFanMotorsPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbConnectPowerPrePowerChecks;
        private System.Windows.Forms.Label lblCheckContinuityPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCheckContinuityPrePower;
        private System.Windows.Forms.Label lblHiPotUnitPrePowerChecks;
        private System.Windows.Forms.Label lblNoPowerUnitPrePowerChecks;
        private System.Windows.Forms.Panel pnlElectricHeat;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox gbGasLeakTest;
        private System.Windows.Forms.TableLayoutPanel tlpGasLeakTest;
        private System.Windows.Forms.Label lblPlugsAreMissingLeakTest;
        private System.Windows.Forms.ComboBox cmbPlugMissingPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbFieldGasPortPrePowerChecks;
        private System.Windows.Forms.Label lblEnsureFieldGasLeakTest;
        private System.Windows.Forms.Label lblTurnOffGasLeakTest;
        private System.Windows.Forms.ComboBox cmbTurnOffGasLeakTestPrePowerChecks;
        private System.Windows.Forms.GroupBox gbGasHeat;
        private System.Windows.Forms.TableLayoutPanel tlpGasHeat;
        private System.Windows.Forms.ComboBox cmbVoltageOnTransformerPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbHeaterControlBoardPrePowerChecks;
        private System.Windows.Forms.Label lblCheckHighVoltageElectricHeat;
        private System.Windows.Forms.Label lblCorrectVoltageGasHeat;
        private System.Windows.Forms.GroupBox gbElectricHeat;
        private System.Windows.Forms.TableLayoutPanel tlpElectricHeat;
        private System.Windows.Forms.ComboBox cmbInspectElementsPrePowerChecks;
        private System.Windows.Forms.ComboBox cmbCheckPowerTerminationPrePowerChecks;
        private System.Windows.Forms.Label lblCheckPowerTerminationElectricHeat;
        private System.Windows.Forms.Label lblInspectElementHeaterElectricHeat;
        private System.Windows.Forms.Panel panel4;
    }
}
