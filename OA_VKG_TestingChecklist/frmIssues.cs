﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace OA_VKG_TestingChecklist
{
    public partial class frmIssues : Form
    {
        MainBO objMain = new MainBO();
        

        public frmIssues()
        {
            InitializeComponent();
        }

        private void butSubmitIssue_Click(object sender, EventArgs e)
        {           
            string errors = validInputs();
            
            int totMinInt = 0;

            if (errors.Length == 0)
            {
                totMinInt = Int32.Parse(tbIssueImpactTimeMinutes.Text);

                objMain.HeadID = Int32.Parse(lbHeadID.Text);
                objMain.JobNum = lbJobNum.Text;
                objMain.IssueTypeCode = (cbIssueType.SelectedIndex + 1).ToString();
                objMain.IssueSource = tbIssueSource.Text;
                objMain.IssueDescription = tbIssueDescription.Text;
                objMain.HorizonType = cbHorizonType.Text;
                objMain.Line = cbLinenum.Text;
                objMain.Notified = "";
                objMain.ImpactTime = totMinInt.ToString();
                objMain.IssueCodes = cbIssueType.Text;

                if (butSubmitIssue.Text == "Submit")
                {                    
                    try
                    {                                                                      
                        objMain.InsertIssues();
                        MessageBox.Show("Issue successfully added to Issues table.");
                        tbIssueDescription.Text = "";
                        tbIssueSource.Text = "";                          
                        cbIssueType.SelectedIndex = 0;
                        DataTable dt = objMain.GetJobIssues();
                        dgvIssues.DataSource = dt;
                        dgvIssues.Refresh();                                                
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR -- Inserting into OAUT_TC_Issues table : " + ex);
                    }
                }
                else
                {
                    objMain.ID = Int32.Parse(lbID.Text);
                    try
                    {
                        objMain.UpdateIssues();
                        MessageBox.Show("Issue successfully updated in the Issues table.");
                        tbIssueDescription.Text = "";
                        tbIssueSource.Text = "";                                       
                        tbIssueImpactTimeMinutes.Text = "";
                        cbIssueType.Text = "";
                        DataTable dt = objMain.GetJobIssues();
                        dgvIssues.DataSource = dt;
                        dgvIssues.Refresh();
                        butSubmitIssue.Text = "Submit";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("ERROR -- Updating into OAUT_TC_Issues table : " + ex);
                    }
                }
            }
            else
            {
                MessageBox.Show(errors);
            }
            //this.Hide();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private string validInputs()
        {
            string errors = "";
            
            int minInt = 0;

            if (cbLinenum.Text.Length == 0)
            {
                errors = "ERROR - Line is a required field. Please select a Line from drop down.\n";
            }

            if (cbHorizonType.Text.Length == 0)
            {
                errors += "ERROR - Unit Type is a required field. Please select a Unit Type from drop down.\n";
            }         

            if (cbIssueType.Text.Length == 0)
            {
                errors += "ERROR - Issue Type is a required field. Please select the appropriate issue type from the Issue Type drop down.\n";
            }

            if (tbIssueImpactTimeMinutes.Text.Length > 0)
            {
                try
                {
                    minInt = Int32.Parse(tbIssueImpactTimeMinutes.Text);
                }
                catch
                {
                    errors += "ERROR - Issue Impact Time Minutes contains an invalid value. Please input the valid integer number of minutes of Impact Time. \n";
                }
            }
            else
            {
                errors += "ERROR - Issue Impact Time Mins is a required field. Please input the integer number of Minutes of Impact Time. \n";

            }

            if (tbIssueSource.Text.Length == 0)
            {
                errors += "ERROR - Issue Source is a required field. Please enter the source of this issue.\n";
            }

            if (tbIssueDescription.Text.Length == 0)
            {
                errors += "ERROR - Issue Description is a required field. Please enter the descprition for this issue.\n";
            }

            return errors;
        }

        private void dgvIssues_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            int rowIdx = dgvIssues.CurrentRow.Index;

            lbID.Text = dgvIssues.Rows[rowIdx].Cells["ID"].Value.ToString();            
            cbIssueType.Text = dgvIssues.Rows[rowIdx].Cells["IssueCodes"].Value.ToString();
            tbIssueImpactTimeMinutes.Text = dgvIssues.Rows[rowIdx].Cells["ImpactTime"].Value.ToString();
            tbIssueSource.Text = dgvIssues.Rows[rowIdx].Cells["IssueSource"].Value.ToString();
            tbIssueDescription.Text = dgvIssues.Rows[rowIdx].Cells["IssueDescription"].Value.ToString();
            cbHorizonType.Text = dgvIssues.Rows[rowIdx].Cells["HorizonType"].Value.ToString();
            cbLinenum.Text = dgvIssues.Rows[rowIdx].Cells["Line"].Value.ToString();

            butSubmitIssue.Text = "Update";
        }

        private void frmIssues_Load(object sender, EventArgs e)
        {
           
        }
    }
}
