﻿namespace OA_VKG_TestingChecklist
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvJobListOA = new System.Windows.Forms.DataGridView();
            this.btnExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtJobNum = new System.Windows.Forms.TextBox();
            this.butSearch = new System.Windows.Forms.Button();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPageOA = new System.Windows.Forms.TabPage();
            this.tabPageVkg = new System.Windows.Forms.TabPage();
            this.dgvJobListVkg = new System.Windows.Forms.DataGridView();
            this.lbEnvironmentStr = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobListOA)).BeginInit();
            this.tabMain.SuspendLayout();
            this.tabPageOA.SuspendLayout();
            this.tabPageVkg.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobListVkg)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvJobListOA
            // 
            this.dgvJobListOA.AllowUserToAddRows = false;
            this.dgvJobListOA.AllowUserToDeleteRows = false;
            this.dgvJobListOA.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobListOA.Location = new System.Drawing.Point(6, 6);
            this.dgvJobListOA.Name = "dgvJobListOA";
            this.dgvJobListOA.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobListOA.Size = new System.Drawing.Size(967, 478);
            this.dgvJobListOA.TabIndex = 0;
            this.dgvJobListOA.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvJobListOA_CellMouseDoubleClick);
            // 
            // btnExit
            // 
            this.btnExit.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnExit.ForeColor = System.Drawing.Color.Red;
            this.btnExit.Location = new System.Drawing.Point(879, 7);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(103, 25);
            this.btnExit.TabIndex = 1;
            this.btnExit.Text = "Exit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(18, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 23);
            this.label1.TabIndex = 2;
            this.label1.Text = "Search Job Num:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtJobNum
            // 
            this.txtJobNum.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtJobNum.Location = new System.Drawing.Point(151, 7);
            this.txtJobNum.Name = "txtJobNum";
            this.txtJobNum.Size = new System.Drawing.Size(120, 22);
            this.txtJobNum.TabIndex = 3;
            this.txtJobNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtJobNum_KeyDown);
            // 
            // butSearch
            // 
            this.butSearch.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.butSearch.ForeColor = System.Drawing.Color.Green;
            this.butSearch.Location = new System.Drawing.Point(283, 7);
            this.butSearch.Name = "butSearch";
            this.butSearch.Size = new System.Drawing.Size(114, 25);
            this.butSearch.TabIndex = 4;
            this.butSearch.Text = "Search Database";
            this.butSearch.UseVisualStyleBackColor = true;
            this.butSearch.Click += new System.EventHandler(this.butSearch_Click);
            // 
            // tabMain
            // 
            this.tabMain.Controls.Add(this.tabPageOA);
            this.tabMain.Controls.Add(this.tabPageVkg);
            this.tabMain.Location = new System.Drawing.Point(8, 33);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(987, 516);
            this.tabMain.TabIndex = 5;
            // 
            // tabPageOA
            // 
            this.tabPageOA.Controls.Add(this.dgvJobListOA);
            this.tabPageOA.Location = new System.Drawing.Point(4, 22);
            this.tabPageOA.Name = "tabPageOA";
            this.tabPageOA.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageOA.Size = new System.Drawing.Size(979, 490);
            this.tabPageOA.TabIndex = 0;
            this.tabPageOA.Text = "OAU";
            this.tabPageOA.UseVisualStyleBackColor = true;
            // 
            // tabPageVkg
            // 
            this.tabPageVkg.Controls.Add(this.dgvJobListVkg);
            this.tabPageVkg.Location = new System.Drawing.Point(4, 22);
            this.tabPageVkg.Name = "tabPageVkg";
            this.tabPageVkg.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageVkg.Size = new System.Drawing.Size(979, 490);
            this.tabPageVkg.TabIndex = 1;
            this.tabPageVkg.Text = "Viking";
            this.tabPageVkg.UseVisualStyleBackColor = true;
            // 
            // dgvJobListVkg
            // 
            this.dgvJobListVkg.AllowUserToAddRows = false;
            this.dgvJobListVkg.AllowUserToDeleteRows = false;
            this.dgvJobListVkg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvJobListVkg.Location = new System.Drawing.Point(8, 6);
            this.dgvJobListVkg.Name = "dgvJobListVkg";
            this.dgvJobListVkg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvJobListVkg.Size = new System.Drawing.Size(963, 478);
            this.dgvJobListVkg.TabIndex = 1;
            this.dgvJobListVkg.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvJobListVkg_CellMouseDoubleClick);
            // 
            // lbEnvironmentStr
            // 
            this.lbEnvironmentStr.Font = new System.Drawing.Font("Arial", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnvironmentStr.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.lbEnvironmentStr.Location = new System.Drawing.Point(404, 5);
            this.lbEnvironmentStr.Name = "lbEnvironmentStr";
            this.lbEnvironmentStr.Size = new System.Drawing.Size(416, 30);
            this.lbEnvironmentStr.TabIndex = 6;
            this.lbEnvironmentStr.Text = "Production Environment";
            this.lbEnvironmentStr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1003, 556);
            this.Controls.Add(this.lbEnvironmentStr);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.butSearch);
            this.Controls.Add(this.txtJobNum);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnExit);
            this.MinimizeBox = false;
            this.Name = "frmMain";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "OA & Viking Testing Checklist";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobListOA)).EndInit();
            this.tabMain.ResumeLayout(false);
            this.tabPageOA.ResumeLayout(false);
            this.tabPageVkg.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvJobListVkg)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvJobListOA;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtJobNum;
        private System.Windows.Forms.Button butSearch;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.TabPage tabPageOA;
        private System.Windows.Forms.TabPage tabPageVkg;
        private System.Windows.Forms.DataGridView dgvJobListVkg;
        private System.Windows.Forms.Label lbEnvironmentStr;
    }
}

