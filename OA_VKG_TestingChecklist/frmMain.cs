﻿using OA_VKG_TestingChecklist.UserControlObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OA_VKG_TestingChecklist
{
    public partial class frmMain : Form
    {
        MainBO objMain = new MainBO();

        bool IsNew = false;

        public string EnvironmentStr { get; set; }
        public Color EnvColor { get; set; }

        public frmMain()
        {
            InitializeComponent();
           
            EnvironmentStr = "Production Environment";
            EnvColor = Color.Snow;
           
#if DEBUG
            try           // If in the debug environment then set the Database connection string to the test database.
            {             
                EnvironmentStr = "Test2 Environment";
                EnvColor = Color.SpringGreen;
                Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                //config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=Kccwvtepicsql01;Initial Catalog=KCC;Persist Security Info=true; user id=msid1; password=mS1d-202!";
                config.ConnectionStrings.ConnectionStrings["OAU_AssemblySystem.Properties.Settings.KCC905ConnectionString"].ConnectionString = "Data Source=Kccwvtepicsql01;Initial Catalog=KCC;Integrated Security=True";
                config.Save(ConfigurationSaveMode.Modified);

                ConfigurationManager.RefreshSection("connectionStrings");
            }
            catch (Exception ex)
            {
                // MessageBox.Show(ConfigurationManager.ConnectionStrings["con"].ToString() + ". This is invalid connection", "Incorrect server/Database - " + ex);
            }
#endif
        }

        #region Events
        private void frmMain_Load(object sender, EventArgs e)
        {            
            DataTable dtJobs = objMain.GetOA_VKG_JobNumbers("OA");
            populateJobList(dgvJobListOA, dtJobs);
            
            dtJobs = objMain.GetOA_VKG_JobNumbers("VKG");
            populateJobList(dgvJobListVkg, dtJobs);
            
            lbEnvironmentStr.Text = EnvironmentStr;
            this.BackColor = EnvColor;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void butSearch_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (txtJobNum.Text.Length > 0)
            {
                objMain.JobNum = txtJobNum.Text;
                DataTable dtJob = objMain.SearchJobHead();

                if (dtJob.Rows.Count > 0)
                {
                    DataRow drJob = dtJob.Rows[0];

                    frmOAUTestingChecklist frmOA = new frmOAUTestingChecklist();
                    frmOA.lbHeadID.Text = "0";
                    bindDataToForm(frmOA, drJob);
                    if (IsNew == true)
                    {
                        frmOA.butIssues.Enabled = true;
                    }

                    frmOA.ShowDialog();

                    DataTable dtJobs = objMain.GetOA_VKG_JobNumbers("OA");
                    populateJobList(dgvJobListOA, dtJobs);

                    dtJobs = objMain.GetOA_VKG_JobNumbers("VKG");
                    populateJobList(dgvJobListVkg, dtJobs);
                }
            }
            else
            {
                MessageBox.Show("ERROR - JobNum required!");
            }
        }

        private void dgvJobListOA_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int rowIdx = dgvJobListOA.CurrentRow.Index;

            objMain.JobNum = dgvJobListOA.Rows[rowIdx].Cells["JobNum"].Value.ToString();

            DataTable dtJob = objMain.SearchJobHead();

            if (dtJob.Rows.Count > 0)
            {
                DataRow drJob = dtJob.Rows[0];

                frmOAUTestingChecklist frmOA = new frmOAUTestingChecklist();
                frmOA.lbHeadID.Text = "0";

                bindDataToForm(frmOA, drJob);

                if (IsNew == true)
                {
                    frmOA.butIssues.Enabled = true;
                }


                frmOA.ShowDialog();
            }
            DataTable dtJobs = objMain.GetOA_VKG_JobNumbers("OA");
            populateJobList(dgvJobListOA, dtJobs);
        }

        private void dgvJobListVkg_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            int rowIdx = dgvJobListVkg.CurrentRow.Index;

            objMain.JobNum = dgvJobListVkg.Rows[rowIdx].Cells["JobNum"].Value.ToString();

            DataTable dtJob = objMain.SearchJobHead();

            if (dtJob.Rows.Count > 0)
            {
                DataRow drJob = dtJob.Rows[0];

                frmOAUTestingChecklist frmOA = new frmOAUTestingChecklist();
                frmOA.lbHeadID.Text = "0";

                bindDataToForm(frmOA, drJob);

                if (IsNew == true)
                {
                    frmOA.butIssues.Enabled = true;
                }
                frmOA.ShowDialog();
            }
            DataTable dtJobs = objMain.GetOA_VKG_JobNumbers("VKG");
            populateJobList(dgvJobListVkg, dtJobs);
        }

        private void txtJobNum_KeyDown(object sender, KeyEventArgs e)
        {
            string tmpStr = "";

            int rowIdx = 0;

            // KeyValue 35 -> End Key        KeyValue 36 -> Home Key          KeyValue 37 -> Left Arrow Key
            // KeyValue 38 -> Up Arrow Key   KeyValue 39 -> Right Arrow Key   KeyValue 40 -> Down Arrow Key
            // KeyValue 46 -> Delete Key     KeyValue 144 -> Num Lock Key
            if ((e.KeyValue > 34 && e.KeyValue < 41) || (e.KeyValue == 46) || (e.KeyValue == 144))
            {
                return;
            }
            // KeyValue 0 -> 48 --- KeyValue 9 -> 57 OR Numeric KeyPad KeyValue 0 -> 96 --- KeyValue 9 -> 105 
            else if ((e.KeyValue > 47 && e.KeyValue < 58) || (e.KeyValue > 95 && e.KeyValue < 106) ||
                     (e.KeyValue == 8) || e.KeyValue == 109 || e.KeyValue == 189)
            {
                try
                {
                    // Because the key value has not been added to the StartAt textbox
                    // at this point it must be added in order to search properly.
                    tmpStr = this.txtJobNum.Text + convertKeyValue(e.KeyValue);

                    if ((e.KeyValue == 8) && (tmpStr.Length > 0))
                    {
                        tmpStr = tmpStr.Substring(1, (tmpStr.Length - 1));
                    }


                    foreach (DataGridViewRow dgr in dgvJobListOA.Rows)
                    {
                        if (dgr.Cells["JobNum"].Value != null)
                        {
                            if (dgr.Cells["JobNum"].Value.ToString().StartsWith(tmpStr) || dgr.Cells["JobNum"].Value.ToString() == tmpStr)
                            {
                                rowIdx = dgr.Index;
                                dgvJobListOA.ClearSelection();  // This code clears the current selected row and then select the first meeting the search criteria and scrolls forward to that row.
                                dgvJobListOA.Rows[rowIdx].Selected = true;
                                dgvJobListOA.FirstDisplayedScrollingRowIndex = rowIdx;
                                break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("ERROR - " + ex);
                }
            }
            else //Any other KeyValue will trigger an Error Message.
            {
                tmpStr = this.txtJobNum.Text;
                //MessageBox.Show("ERROR - Invalid value.");
                this.txtJobNum.Text = tmpStr;
                this.txtJobNum.SelectionStart = tmpStr.Length;
            }
        }
        #endregion

        #region OtherMethods
        private void populateMainJobList()
        {

        }

        private void populateJobList(DataGridView dgvJobList, DataTable dtJobs)
        {            
            dgvJobList.DataSource = dtJobs;
            dgvJobList.Columns["JobNum"].Width = 80;
            dgvJobList.Columns["JobNum"].HeaderText = "Job Num";
            dgvJobList.Columns["JobNum"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobList.Columns["JobNum"].DisplayIndex = 0;
            dgvJobList.Columns["JobName"].Width = 250;
            dgvJobList.Columns["JobName"].HeaderText = "Job name";
            dgvJobList.Columns["JobName"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobList.Columns["JobName"].DisplayIndex = 1;
            dgvJobList.Columns["ModelNo"].Width = 450;
            dgvJobList.Columns["ModelNo"].HeaderText = "ModelNo";
            dgvJobList.Columns["ModelNo"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobList.Columns["ModelNo"].DisplayIndex = 2;
            dgvJobList.Columns["DateTested"].Width = 110;
            dgvJobList.Columns["DateTested"].HeaderText = "DateTested";
            dgvJobList.Columns["DateTested"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
            dgvJobList.Columns["DateTested"].DefaultCellStyle.Format = "MM/dd/yyyy";
            dgvJobList.Columns["DateTested"].DisplayIndex = 3;
            dgvJobList.Columns["Line"].Width = 80;
            dgvJobList.Columns["Line"].HeaderText = "Line";
            dgvJobList.Columns["Line"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobList.Columns["Line"].DisplayIndex = 5;
            dgvJobList.Columns["Line"].Visible = true;
            dgvJobList.Columns["HorizonType"].Width = 80;
            dgvJobList.Columns["HorizonType"].HeaderText = "HorizonType";
            dgvJobList.Columns["HorizonType"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvJobList.Columns["HorizonType"].DisplayIndex = 4;
            dgvJobList.Columns["HorizonType"].Visible = true;


        }

        private string convertKeyValue(int keyValue)
        {
            // This function converts the integer KeyValue passed into it to its string equivalent.

            if ((keyValue == 48) || (keyValue == 96))
            {
                return "0";
            }
            else if ((keyValue == 49) || (keyValue == 97))
            {
                return "1";
            }
            else if ((keyValue == 50) || (keyValue == 98))
            {
                return "2";
            }
            else if ((keyValue == 51) || (keyValue == 99))
            {
                return "3";
            }
            else if ((keyValue == 52) || (keyValue == 100))
            {
                return "4";
            }
            else if ((keyValue == 53) || (keyValue == 101))
            {
                return "5";
            }
            else if ((keyValue == 54) || (keyValue == 102))
            {
                return "6";
            }
            else if ((keyValue == 55) || (keyValue == 103))
            {
                return "7";
            }
            else if ((keyValue == 56) || (keyValue == 104))
            {
                return "8";
            }
            else if ((keyValue == 57) || (keyValue == 105))
            {
                return "9";
            }

            return "";
        }

        //private void bindDataToForm(frmOAUTestingChecklist frmOA, DataGridViewRow dgvRow)
        //{
        //    string unitTypeStr = "";

        //    frmOA.tbJobNum.Text = dgvRow.Cells["JobNum"].Value.ToString();
        //    frmOA.tbJobName.Text = dgvRow.Cells["JobName"].Value.ToString();
        //    frmOA.tbSerialnum.Text = "OA" + dgvRow.Cells["JobNum"].Value.ToString();
        //    frmOA.tbModelNum.Text = dgvRow.Cells["ModelNo"].Value.ToString();

        //    unitTypeStr = dgvRow.Cells["ModelNo"].Value.ToString().Substring(0, 3);

        //    if (unitTypeStr == "HAE")
        //    {
        //        frmOA.Text = "Viking Testing Checklist";
        //    }

        //    if (dgvRow.Cells["HeadID"].Value != DBNull.Value)
        //    {
        //        objMain.HeadID = (int)dgvRow.Cells["HeadID"].Value;
        //        frmOA.lbHeadID.Text = objMain.HeadID.ToString();
        //        IsNew = true;
        //        frmOA.butSave.Text = "Update";

        //        frmOA.tbAuditedBy.Text = dgvRow.Cells["AuditedBy"].Value.ToString();

        //        if (dgvRow.Cells["DateTested"].Value != DBNull.Value)
        //        {
        //            frmOA.dtpDate.Value = DateTime.Parse(dgvRow.Cells["DateTested"].Value.ToString());
        //        }

        //        decimal acHumTemp = (decimal)dgvRow.Cells["AC_Temp"].Value;
        //        frmOA.nudTemperature.Value = Decimal.ToInt32(acHumTemp);

        //        acHumTemp = (decimal)dgvRow.Cells["AC_Humidity"].Value;
        //        frmOA.nudHumidity.Value = Decimal.ToInt32(acHumTemp);
        //        frmOA.nudCircuit1Charge.Value = (decimal)dgvRow.Cells["Charge_Circuit_1"].Value;
        //        frmOA.tbCircuit1ChargeVariance.Text = dgvRow.Cells["Charge_Circuit_1_var"].Value.ToString();
        //        //frmOA.nudCircuit2Charge.Value = (decimal)dgvRow.Cells["Charge_Circuit_2"].Value;
        //        //frmOA.tbCircuit2ChargeVariance.Text = dgvRow.Cells["Charge_Circuit_2_var"].Value.ToString();
        //        frmOA.cbDisconnectType.Text = dgvRow.Cells["Disconnect_Type"].Value.ToString();
        //        frmOA.tbDisconnectSize.Text = dgvRow.Cells["Disconnect_Size"].Value.ToString();
        //        frmOA.tbERV_ModelNo.Text = dgvRow.Cells["ERV_ModelNo"].Value.ToString();
        //        frmOA.tbERV_SerialNo.Text = dgvRow.Cells["ERV_SerialNo"].Value.ToString();
        //        frmOA.tbERVRatedAmps.Text = dgvRow.Cells["ERV_Rated_Amp"].Value.ToString();
        //        frmOA.tbERVRunningAmps.Text = dgvRow.Cells["ERV_Running_Amp"].Value.ToString();
        //        if (dgvRow.Cells["ERV_Proper_Rotation"].Value.ToString() == "True")
        //        {
        //            frmOA.chkbERVProperRotation.Checked = true;
        //        }
        //        frmOA.tbMiscNotes.Text = dgvRow.Cells["MiscNotes"].Value.ToString();

        //        populateBlowerData(frmOA);
        //        populateCircuitData(frmOA);
        //        populateCompressorData(frmOA);
        //        populateCondenserData(frmOA);
        //        populateFilterData(frmOA);
        //        populateHeatData(frmOA);
        //        populateOptionsData(frmOA, unitTypeStr);
        //        populateVoltageData(frmOA);
        //    }
        //    else
        //    {
        //        if (unitTypeStr == "HAE")
        //        {
        //            frmOA.gbStdEquipViking.Size = new Size(366, 152);
        //            frmOA.gbStdEquipViking.Location = new Point(267, 299);
        //            frmOA.gbOptionsViking.Size = new Size(255, 402);
        //            frmOA.gbOptionsViking.Location = new Point(638, 49);
        //            frmOA.gbOptions.Visible = false;
        //            frmOA.gbStandardEquipment.Visible = false;
        //        }
        //        else
        //        {
        //            frmOA.gbStandardEquipment.Size = new Size(366, 152);
        //            frmOA.gbOptions.Size = new Size(255, 402);
        //            frmOA.gbOptionsViking.Visible = false;
        //            frmOA.gbStdEquipViking.Visible = false;
        //            frmOA.gbStandardEquipment.BringToFront();
        //        }
        //    }
        //}

        private void bindDataToForm(frmOAUTestingChecklist frmOA, DataRow row)
        {
            string unitTypeStr = "";

            frmOA.tbJobNum.Text = row["JobNum"].ToString();
            frmOA.tbJobName.Text = row["JobName"].ToString();
            frmOA.tbSerialnum.Text = "OA" + row["JobNum"].ToString();
            frmOA.tbModelNum.Text = row["ModelNo"].ToString();
            frmOA.cbLinenum.Text = row["Line"].ToString();
            frmOA.cbHorizonType.Text = row["HorizonType"].ToString();
            frmOA.tbTestedBy.Text = row["TestedBy"].ToString();


            unitTypeStr = row["ModelNo"].ToString().Substring(0, 3);


            if (unitTypeStr == "HAE")
            {
                //frmOA.Text = "Viking Testing Checklist";
                frmOA.Text = "MAU Testing Checklist";
                frmOA.lblSuperHeat.Text = "Super Heat:\r\n(range 10 - 20)";
                frmOA.label39.Text = "Subcooling:\r\n(range 5 - 15)";
                frmOA.tbSerialnum.Text = row["Serialnum"].ToString();
                FieldVisibility(frmOA);
            }

            if (row["ModelNo"].ToString().Substring(0, 2) == "OA")
            {
                frmOA.Text = "OAU Testing Checklist";
            }

            if (row["HeadID"] != DBNull.Value)
            {
                objMain.HeadID = (int)row["HeadID"];
                frmOA.lbHeadID.Text = objMain.HeadID.ToString();
                objMain.JobNum = row["JobNum"].ToString();
                IsNew = true;
                frmOA.butSave.Text = "Update";

                frmOA.tbAuditedBy.Text = row["AuditedBy"].ToString();

                if (row["DateTested"] != DBNull.Value)
                {
                    DateTime dateTested = DateTime.Parse(row["DateTested"].ToString());
                    frmOA.dtpDate.Value = dateTested;
                    if(dateTested <DateTime.Today)
                    {
                        
                        frmOA.SkipPreChecksValidation = true;
                    }
                }

                StringBuilder errors = new StringBuilder();
                //Reset the temperature and humidity value to minimum value if invalid data found
                decimal acHumTemp = (decimal)row["AC_Temp"];
                if (acHumTemp <= -100 || acHumTemp >= 200)
                {
                    errors.AppendLine("Ambient Condition Temperature should be greater than -100 and less than 200.");
                    acHumTemp = -99;
                }
                frmOA.nudTemperature.Value = Decimal.ToInt32(acHumTemp);

                acHumTemp = (decimal)row["AC_Humidity"];
                if (acHumTemp <= 0 || acHumTemp >= 100)
                {
                    acHumTemp = 1;
                    errors.AppendLine("Ambient Condition Humidity should be greater than 0 and less than 100.");
                }
                if(errors.Length>0)
                {
                    errors.Insert(0, $"Below errors found in the data and values are reset to the minimum allowed value:{Environment.NewLine}");
                    //MessageBox.Show(this, errors.ToString());
                }
                frmOA.nudHumidity.Value = Decimal.ToInt32(acHumTemp);
                frmOA.nudCircuit1Charge.Value = (decimal)row["Charge_Circuit_1"];
                frmOA.tbCircuit1ChargeVariance.Text = row["Charge_Circuit_1_var"].ToString();
                frmOA.nudCircuit2Charge.Value = (decimal)row["Charge_Circuit_2"];
                frmOA.tbCircuit2ChargeVariance.Text = row["Charge_Circuit_2_var"].ToString();
                frmOA.cbDisconnectType.Text = row["Disconnect_Type"].ToString();
                frmOA.tbDisconnectSize.Text = row["Disconnect_Size"].ToString();
                frmOA.tbERV_ModelNo.Text = row["ERV_ModelNo"].ToString();
                frmOA.tbERV_SerialNo.Text = row["ERV_SerialNo"].ToString();
                frmOA.tbERVRatedAmps.Text = row["ERV_Rated_Amp"].ToString();
                frmOA.tbERVRunningAmps.Text = row["ERV_Running_Amp"].ToString();
                if (row["ERV_Proper_Rotation"].ToString() == "1")
                {
                    frmOA.chkbERVProperRotation.Checked = true;
                }
                frmOA.tbMiscNotes.Text = row["MiscNotes"].ToString();
                PopulateMeasurePressureData(frmOA);
                populateBlowerData(frmOA);
                populateCircuitData(frmOA);
                populateCompressorData(frmOA);
                populateCondenserData(frmOA);
                populateFilterData(frmOA);
                populateHeatData(frmOA);
                populateOptionsData(frmOA, unitTypeStr);
                populateVoltageData(frmOA);
            }
            else
            {
                if (unitTypeStr == "HAE")
                {
                    frmOA.gbOptions.Visible = false;
                    frmOA.gbStandardEquipment.Visible = false;

                }
                else
                {
                    frmOA.gbOptionsViking.Visible = false;
                    frmOA.gbStdEquipViking.Visible = false;
                    frmOA.gbStandardEquipment.BringToFront();
                }
            }
        }
        private void PopulateMeasurePressureData(frmOAUTestingChecklist frmOAU)
        {
            DataTable dtPressure = objMain.GetMeasurePressureData();
            if (dtPressure.Rows.Count > 0)
            {
                DataRow drPress = dtPressure.Rows[0];
                frmOAU.txtInletModulationPresure.Text = drPress["InletModulationPressure"].ToString();
                frmOAU.txtLowFire.Text = drPress["LowFire"].ToString();
                frmOAU.txtHighFire.Text = drPress["HighFire"].ToString();
            }
        }

        private void populateBlowerData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtBCH = objMain.GetBlowerData();

            foreach (DataRow drBCH in dtBCH.Rows)
            {
                if (drBCH["BlowerType"].ToString() == "Indoor")
                {
                    frmOA.tbHpIndoor.Text = drBCH["HP"].ToString();
                    frmOA.tbRatedAmpsIndoor.Text = drBCH["RatedAmp"].ToString();
                    frmOA.tbRunningAmpsIndoor.Text = drBCH["RunningAmp"].ToString();
                    frmOA.tbWheelSizeIndoor.Text = drBCH["WheelSize"].ToString();
                    frmOA.tbMaxHzIndoor.Text = drBCH["MaxHz"].ToString();
                    frmOA.tbMinHzIndoor.Text = drBCH["MinHz"].ToString();
                    frmOA.tbVoltageOutputIndoor.Text = drBCH["VoltageOutput"].ToString();
                    frmOA.tbModelNumIndoor.Text = drBCH["ModelNum"].ToString();
                    if (drBCH["DoubleBlower"].ToString() == "1")
                    {
                        frmOA.chkbDoubleBlowerIndoor.Checked = true;
                    }
                }
                else
                {
                    frmOA.tbHpExhaust.Text = drBCH["HP"].ToString();
                    frmOA.tbRatedAmpsExhaust.Text = drBCH["RatedAmp"].ToString();
                    frmOA.tbRunningAmpsExhaust.Text = drBCH["RunningAmp"].ToString();
                    frmOA.tbWheelSizeExhaust.Text = drBCH["WheelSize"].ToString();
                    frmOA.tbMaxHzExhaust.Text = drBCH["MaxHz"].ToString();
                    frmOA.tbMinHzExhaust.Text = drBCH["MinHz"].ToString();
                    frmOA.tbVoltageOutputExhaust.Text = drBCH["VoltageOutput"].ToString();
                    frmOA.tbModelNumExhaust.Text = drBCH["ModelNum"].ToString();
                    if (drBCH["DoubleBlower"].ToString() == "1")
                    {
                        frmOA.chkbDoubleBlowerExhaust.Checked = true;
                    }
                }
            }
        }

        private void populateCircuitData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtCirc = objMain.GetCircuitData();

            foreach (DataRow drCirc in dtCirc.Rows)
            {
                if (drCirc["CircuitValueType"].ToString() == "tbCircuit1SuctPSI")
                {
                    frmOA.tbCircuit1SuctPSI.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1SST")
                {
                    frmOA.tbCircuit1SST.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1SuctTemp")
                {
                    frmOA.tbCircuit1SuctTemp.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "chkbCircuit1APRActive")
                {
                    if (drCirc["CircuitValue"].ToString() == "True")
                    {
                        frmOA.chkbCircuit1APRActive.Checked = true;
                    }
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1LiqPSI")
                {
                    frmOA.tbCircuit1LiqPSI.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1SLT")
                {
                    frmOA.tbCircuit1SLT.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1LiqTemp")
                {
                    frmOA.tbCircuit1LiqTemp.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1Subcooling")
                {
                    frmOA.tbCircuit1Subcooling.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "txtSuperHeat")
                {
                    frmOA.txtSuperHeat.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1DisPSI")
                {
                    frmOA.tbCircuit1DisPSI.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1SDT")
                {
                    frmOA.tbCircuit1SDT.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit1DisTemp")
                {
                    frmOA.tbCircuit1DisTemp.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2SuctPSI")
                {
                    frmOA.tbCircuit2SuctPSI.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2SST")
                {
                    frmOA.tbCircuit2SST.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2SuctTemp")
                {
                    frmOA.tbCircuit2SuctTemp.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2LiqPSI")
                {
                    frmOA.tbCircuit2LiqPSI.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2SLT")
                {
                    frmOA.tbCircuit2SLT.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2LiqTemp")
                {
                    frmOA.tbCircuit2LiqTemp.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2Subcooling")
                {
                    frmOA.tbCircuit2Subcooling.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2SuperHeat")
                {
                    frmOA.tbCircuit2SuperHeat.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2DisPSI")
                {
                    frmOA.tbCircuit2DisPSI.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2SDT")
                {
                    frmOA.tbCircuit2SDT.Text = drCirc["CircuitValue"].ToString();
                }
                else if (drCirc["CircuitValueType"].ToString() == "tbCircuit2DisTemp")
                {
                    frmOA.tbCircuit2DisTemp.Text = drCirc["CircuitValue"].ToString();
                }
            }
        }

        private void populateCompressorData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtComp = objMain.GetCompressorData();
            foreach (DataRow drComp in dtComp.Rows)
            {
                if (drComp["CompressorNum"].ToString() == "Comp1")
                {
                    frmOA.tbCompressor1ModelNum.Text = drComp["ModelNum"].ToString();
                    frmOA.tbCompressor1SerialNum.Text = drComp["SerialNum"].ToString();
                    frmOA.tbAMACompressorRunningAmps1.Text = drComp["RunningAmps"].ToString();
                    frmOA.tbAMACompressorRatedAmps1.Text = drComp["RatedAmps"].ToString();
                }
                else if (drComp["CompressorNum"].ToString() == "Comp2")
                {
                    frmOA.tbCompressor2ModelNum.Text = drComp["ModelNum"].ToString();
                    frmOA.tbCompressor2SerialNum.Text = drComp["SerialNum"].ToString();
                    frmOA.tbAMACompressorRunningAmps2.Text = drComp["RunningAmps"].ToString();
                    frmOA.tbAMACompressorRatedAmps2.Text = drComp["RatedAmps"].ToString();
                }
                else if (drComp["CompressorNum"].ToString() == "Comp3")
                {
                    frmOA.tbCompressor3ModelNum.Text = drComp["ModelNum"].ToString();
                    frmOA.tbCompressor3SerialNum.Text = drComp["SerialNum"].ToString();
                    frmOA.tbAMACompressorRunningAmps3.Text = drComp["RunningAmps"].ToString();
                    frmOA.tbAMACompressorRatedAmps3.Text = drComp["RatedAmps"].ToString();
                }
                else if (drComp["CompressorNum"].ToString() == "Comp4")
                {
                    frmOA.tbCompressor4ModelNum.Text = drComp["ModelNum"].ToString();
                    frmOA.tbCompressor4SerialNum.Text = drComp["SerialNum"].ToString();
                    frmOA.tbAMACompressorRunningAmps4.Text = drComp["RunningAmps"].ToString();
                    frmOA.tbAMACompressorRatedAmps4.Text = drComp["RatedAmps"].ToString();
                }
                else if (drComp["CompressorNum"].ToString() == "Comp5")
                {
                    frmOA.tbCompressor5ModelNum.Text = drComp["ModelNum"].ToString();
                    frmOA.tbCompressor5SerialNum.Text = drComp["SerialNum"].ToString();
                    frmOA.tbAMACompressorRunningAmps5.Text = drComp["RunningAmps"].ToString();
                    frmOA.tbAMACompressorRatedAmps5.Text = drComp["RatedAmps"].ToString();
                }
                else if (drComp["CompressorNum"].ToString() == "Comp6")
                {
                    frmOA.tbCompressor6ModelNum.Text = drComp["ModelNum"].ToString();
                    frmOA.tbCompressor6SerialNum.Text = drComp["SerialNum"].ToString();
                    frmOA.tbAMACompressorRunningAmps6.Text = drComp["RunningAmps"].ToString();
                    frmOA.tbAMACompressorRatedAmps6.Text = drComp["RatedAmps"].ToString();
                }
            }
        }

        private void populateCondenserData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtCon = objMain.GetCondenserData();

            foreach (DataRow drCon in dtCon.Rows)
            {
                if (drCon["CondenserNum"].ToString() == "Cond1")
                {
                    frmOA.tbCondenserFanRunningAmps1.Text = drCon["RunningAmps"].ToString();
                    frmOA.tbCondenserFanRatedAmps1.Text = drCon["RatedAmps"].ToString();
                }
                else if (drCon["CondenserNum"].ToString() == "Cond2")
                {
                    frmOA.tbCondenserFanRunningAmps2.Text = drCon["RunningAmps"].ToString();
                    frmOA.tbCondenserFanRatedAmps2.Text = drCon["RatedAmps"].ToString();
                }
                else if (drCon["CondenserNum"].ToString() == "Cond3")
                {
                    frmOA.tbCondenserFanRunningAmps3.Text = drCon["RunningAmps"].ToString();
                    frmOA.tbCondenserFanRatedAmps3.Text = drCon["RatedAmps"].ToString();
                }
                else if (drCon["CondenserNum"].ToString() == "Cond4")
                {
                    frmOA.tbCondenserFanRunningAmps4.Text = drCon["RunningAmps"].ToString();
                    frmOA.tbCondenserFanRatedAmps4.Text = drCon["RatedAmps"].ToString();
                }
                else if (drCon["CondenserNum"].ToString() == "Cond5")
                {
                    frmOA.tbCondenserFanRunningAmps5.Text = drCon["RunningAmps"].ToString();
                    frmOA.tbCondenserFanRatedAmps5.Text = drCon["RatedAmps"].ToString();
                }
                else if (drCon["CondenserNum"].ToString() == "Cond6")
                {
                    frmOA.tbCondenserFanRunningAmps6.Text = drCon["RunningAmps"].ToString();
                    frmOA.tbCondenserFanRatedAmps6.Text = drCon["RatedAmps"].ToString();
                }
            }
        }

        private void populateFilterData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtFilt = objMain.GetFilterData();


            //yourDataGridView.DataSource = yourDataTable;

            if (dtFilt.Rows.Count > 0)
            {
                string qtyStr = "";

                foreach (DataRow dr in dtFilt.Rows)
                {
                    qtyStr = dr["Qty"].ToString();
                    qtyStr = qtyStr.Substring(0, qtyStr.IndexOf("."));
                    frmOA.dgvFilters.Rows.Add(dr["FilterSize"].ToString(), dr["FilterStyle"].ToString(), Convert.ToInt32(qtyStr));
                }
            }
            else
            {

                //frmOA.dgvFilters.DataSource = dtFilt;

                //frmOA.dgvFilters.Columns["FilterSize"].Width = 100;
                //frmOA.dgvFilters.Columns["FilterSize"].HeaderText = "FilterSize";
                //frmOA.dgvFilters.Columns["FilterSize"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                //frmOA.dgvFilters.Columns["FilterSize"].DisplayIndex = 0;
                //frmOA.dgvFilters.Columns["FilterStyle"].Width = 125;
                //frmOA.dgvFilters.Columns["FilterStyle"].HeaderText = "FilterStyle";
                //frmOA.dgvFilters.Columns["FilterStyle"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                //frmOA.dgvFilters.Columns["FilterStyle"].DisplayIndex = 1;
                //frmOA.dgvFilters.Columns["Qty"].Width = 80;
                //frmOA.dgvFilters.Columns["Qty"].HeaderText = "Qty";
                //frmOA.dgvFilters.Columns["Qty"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                //frmOA.dgvFilters.Columns["Qty"].DisplayIndex = 2;
                //frmOA.dgvFilters.Columns["PartNum"].Visible = false;
            }
        }



        private void populateHeatData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtHeat = objMain.GetHeatData();

            if (dtHeat.Rows.Count > 0)
            {
                DataRow drHeat = dtHeat.Rows[0];
                frmOA.cbHeatPrimary.Text = drHeat["Heat_Primary"].ToString();
                frmOA.cbHeatPosition.Text = drHeat["TopBottom"].ToString();
                frmOA.tbHeatModelNumTop.Text = drHeat["Heat_model_num_Top"].ToString();
                frmOA.tbHeatSerialNumTop.Text = drHeat["Heat_Serial_Top"].ToString();

                frmOA.tbHeatItemNum.Text = drHeat["Heat_Item_num"].ToString();
                frmOA.tbHeatPONum.Text = drHeat["Heat_PO_num"].ToString();
                frmOA.tbHeatPartNumTop.Text = drHeat["Heat_Partnum_Top"].ToString();
                frmOA.tbHeatSizeTop.Text = drHeat["Heat_Size_Top"].ToString();

                frmOA.tbHeatRatedAmps.Text = drHeat["Heat_Rated_Amp"].ToString();
                frmOA.tbHeatRunningAmps.Text = drHeat["Heat_Running_Amp"].ToString();

                frmOA.tbHeatModelNumBottom.Text = drHeat["Heat_model_num_Bottom"].ToString();
                frmOA.tbHeatSerialNumBottom.Text = drHeat["Heat_Serial_Bottom"].ToString();
                frmOA.tbHeatPartNumBottom.Text = drHeat["Heat_Partnum_Bottom"].ToString();
                frmOA.tbHeatSizeBottom.Text = drHeat["Heat_Size_Bottom"].ToString();

                frmOA.tbPreHeatModelNo.Text = drHeat["Heat_model_num_PreHeat"].ToString();
                frmOA.tbPreHeatSerialNo.Text = drHeat["Heat_Serial_PreHeat"].ToString();
                frmOA.tbPreHeatRunningAmps.Text = drHeat["Heat_RunningAmp_PreHeat"].ToString();
                frmOA.tbPreHeatRatedAmps.Text = drHeat["Heat_RatedAmp_PreHeat"].ToString();
                frmOA.tbPreHeatSize.Text = drHeat["Heat_Size_PreHeat"].ToString();
                frmOA.cbHeatSectionFuelType.Text = drHeat["Heat_FuelType"].ToString();
            }
        }



        private void populateOptionsData(frmOAUTestingChecklist frmOA, string unitTypeStr)
        {
            if (unitTypeStr == "HAE")
            {
                frmOA.gbOptions.Visible = false;
                frmOA.gbStandardEquipment.Visible = false;
                frmOA.gbOptionsViking.Visible = true;
                frmOA.gbStdEquipViking.Visible = true;
                populateOptionsDataViking(frmOA);
            }
            else
            {
                //frmOA.gbDisconnect.Location = new Point(258,208);                           
                frmOA.gbOptionsViking.Visible = false;
                frmOA.gbStdEquipViking.Visible = false;

                DataTable dtOpt = objMain.GetOptionsData();

                foreach (DataRow drOpt in dtOpt.Rows)
                {
                    if (drOpt["OptionName"].ToString() == "cbPreHeatTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbPreHeatTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbPreHeatInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbPreHeatInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbHotGasReheatTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbHotGasReheatTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbHotGasReheatInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbHotGasReheatInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbReversingValvesTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbReversingValvesTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbReversingValvesInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbReversingValvesInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbAuxiliaryLightsTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbAuxiliaryLightsTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbAuxiliaryLightsInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbAuxiliaryLightsInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbProtoNodeTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbProtoNodeTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbProtoNodeInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbProtoNodeInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbGreenTrolTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbGreenTrolTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbGreenTrolInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbGreenTrolInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbExhaustPiezoTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbExhaustPiezoTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbExhaustPiezoInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbExhaustPiezoInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbSupplyPiezoTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbSupplyPiezoTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbSupplyPiezoInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbSupplyPiezoInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbConvenienceOutletTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbConvenienceOutletTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbConvenienceOutletInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbConvenienceOutletInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbBypassDampersTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbBypassDampersTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbBypassDampersInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbBypassDampersInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbERVTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbERVTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbERVInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbERVInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbPoweredExhaustTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbPoweredExhaustTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbPoweredExhaustInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbPoweredExhaustInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbHeaterTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbHeaterTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbHeaterInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbHeaterInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbReturnSmokeDetectorTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbReturnSmokeDetectorTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbReturnSmokeDetectorInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbReturnSmokeDetectorInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbSupplySmokeDectectorTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbSupplySmokeDectectorTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbSupplySmokeDectectorInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbSupplySmokeDectectorInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbHailguardInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbHailguradInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbHailguardTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbHailguardTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbLEDServiceInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbLEDServiceInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbLEDServiceTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbLEDServiceTested.Checked = true;
                        }
                    }

                    else if (drOpt["OptionName"].ToString() == "cbAPRTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbAPRTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbAPRInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbAPRInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbDigitalScrollTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbDigitalScrollTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbDigitalScrollInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbDigitalScrollInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbRADampersTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbRADampersTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbRADampersInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbRADampersInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbOADampersTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbOADampersTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbOADampersInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbOADampersInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbFilterStatusTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbFilterStatusTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbFilterStatusInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbFilterStatusInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbFanFailureTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbFanFailureTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbFanFailureInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbFanFailureInstalled.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbCCHeaterTested")
                    {
                        if (drOpt["OptionTested"].ToString() == "1")
                        {
                            frmOA.cbCCHeaterTested.Checked = true;
                        }
                    }
                    else if (drOpt["OptionName"].ToString() == "cbCCHeaterInstalled")
                    {
                        if (drOpt["OptionInstalled"].ToString() == "1")
                        {
                            frmOA.cbCCHeaterInstalled.Checked = true;
                        }
                    }
                }
            }
        }

        private void populateOptionsDataViking(frmOAUTestingChecklist frmOA)
        {
            DataTable dtOpt = objMain.GetOptionsData();

            foreach (DataRow drOpt in dtOpt.Rows)
            {
                if (drOpt["OptionName"].ToString() == "cbPreHeatTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbPreHeatTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbPreHeatInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbPreHeatInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbHGRHTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbHGRHTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbHGRHInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbHGRHInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbRevValvesTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbHailguardTested.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbRevValvesInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbHailguradInstalled.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbAuxLightsTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbAuxLightsTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbAuxLightsInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbAuxLightsInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbProtoNodeTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbLEDServiceTested.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbProtoNodeInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbLEDServiceInstalled.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbExhPiezoTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbExhPiezoTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbExhPiezoInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbExhPiezoInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbSupplyPiezoTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbSupplyPiezoTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbSupplyPiezoInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbSupplyPiezoInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbConvOutletTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbConvOutletTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbConvOutletInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbConvOutletInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbBypDampTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbBypDampTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbBypDampInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbBypDampInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbERVTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbERVTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbERVInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbERVInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbPwrExhTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbPwrExhTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbPwrExhInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbPwrExhInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbHeaterTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbHeaterTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbHeaterInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbHeaterInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbDigScrollTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbFSSTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbDigScrollInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbFSSInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbRADampTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbRADampTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbRADampInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbRADampInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbOADampTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbOADampTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbOADampInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbOADampInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbHailguardVkgTested")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbHailguardVkgTested.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbHailguardVkgInstalled")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbHailguardVkgInstalled.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbLEDServiceVkgTested")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbLEDServiceVkgTested.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbLEDServiceInstalled")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbLEDServiceVKgInstalled.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbFiltStatSwitchTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbFiltStatSwitchTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbFiltStatSwitchInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbFiltStatSwitchInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbFanFailSwitchTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbFanFailSwitchTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbFanFailSwitchInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbFanFailSwitchInstalledVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbOADamperTestedVkg")
                {
                    if (drOpt["OptionTested"].ToString() == "1")
                    {
                        frmOA.cbOADamperTestedVkg.Checked = true;
                    }
                }
                else if (drOpt["OptionName"].ToString() == "cbOADamperInstalledVkg")
                {
                    if (drOpt["OptionInstalled"].ToString() == "1")
                    {
                        frmOA.cbOADamperInstalledVkg.Checked = true;
                    }
                }
            }
        }

        private void populateVoltageData(frmOAUTestingChecklist frmOA)
        {
            DataTable dtHeat = objMain.GetVoltageData();

            foreach (DataRow drHeat in dtHeat.Rows)
            {
                if (drHeat["Voltage"].ToString() == "tbVoltageL1L2")
                {
                    frmOA.tbVoltageL1L2.Text = drHeat["Voltage_Value"].ToString();
                }
                else if (drHeat["Voltage"].ToString() == "tbVoltageL2L3")
                {
                    frmOA.tbVoltageL2L3.Text = drHeat["Voltage_Value"].ToString();
                }
                else if (drHeat["Voltage"].ToString() == "tbVoltageL1L3")
                {
                    frmOA.tbVoltageL1L3.Text = drHeat["Voltage_Value"].ToString();
                }
                else if (drHeat["Voltage"].ToString() == "tbVoltageL1G")
                {
                    frmOA.tbVoltageL1G.Text = drHeat["Voltage_Value"].ToString();
                }
                else if (drHeat["Voltage"].ToString() == "tbVoltageL2G")
                {
                    frmOA.tbVoltageL2G.Text = drHeat["Voltage_Value"].ToString();
                }
                else if (drHeat["Voltage"].ToString() == "tbVoltageL3G")
                {
                    frmOA.tbVoltageL3G.Text = drHeat["Voltage_Value"].ToString();
                }
                else if (drHeat["Voltage"].ToString() == "rbVoltage208")
                {
                    frmOA.rbVoltage208.Checked = true;
                }
                else if (drHeat["Voltage"].ToString() == "rbVoltage460")
                {
                    frmOA.rbVoltage460.Checked = true;
                }
                else if (drHeat["Voltage"].ToString() == "rbVoltage575")
                {
                    frmOA.rbVoltage575.Checked = true;
                }
            }
        }

        #endregion

        private void FieldVisibility(frmOAUTestingChecklist frmOA)
        {
            frmOA.nudCircuit2Charge.Visible = false;
            frmOA.tbCircuit2ChargeVariance.Visible = false;
            frmOA.gbCharge.Size = new Size(365, 40);
            frmOA.gbCompressorData.Size = new Size(597, 75);
            frmOA.gbCondensers.Size = new Size(280, 75);
            frmOA.gbDisconnect.Location = new Point(258, 185);
            frmOA.gbCircuit2.Visible = false;
            frmOA.tbAMACompressorRatedAmps3.Visible = false;
            frmOA.tbAMACompressorRatedAmps4.Visible = false;
            frmOA.tbAMACompressorRatedAmps5.Visible = false;
            frmOA.tbAMACompressorRatedAmps6.Visible = false;
            frmOA.tbAMACompressorRunningAmps3.Visible = false;
            frmOA.tbAMACompressorRunningAmps4.Visible = false;
            frmOA.tbAMACompressorRunningAmps5.Visible = false;
            frmOA.tbAMACompressorRunningAmps6.Visible = false;
            frmOA.tbCompressor3ModelNum.Visible = false;
            frmOA.tbCompressor3SerialNum.Visible = false;
            frmOA.tbCompressor4ModelNum.Visible = false;
            frmOA.tbCompressor4SerialNum.Visible = false;
            frmOA.tbCompressor5ModelNum.Visible = false;
            frmOA.tbCompressor5SerialNum.Visible = false;
            frmOA.tbCompressor6ModelNum.Visible = false;
            frmOA.tbCompressor6SerialNum.Visible = false;
            frmOA.tbCondenserFanRatedAmps3.Visible = false;
            frmOA.tbCondenserFanRatedAmps4.Visible = false;
            frmOA.tbCondenserFanRatedAmps5.Visible = false;
            frmOA.tbCondenserFanRatedAmps6.Visible = false;
            frmOA.tbCondenserFanRunningAmps3.Visible = false;
            frmOA.tbCondenserFanRunningAmps4.Visible = false;
            frmOA.tbCondenserFanRunningAmps5.Visible = false;
            frmOA.tbCondenserFanRunningAmps6.Visible = false;
        }

    }
}
