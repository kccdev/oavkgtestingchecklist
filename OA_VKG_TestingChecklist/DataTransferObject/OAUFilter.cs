﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.DataTransferObject
{
    class OAUFilter
    {
        public string FiltersSize { get; set; }
        public string FiltersStyle { get; set; }
        public decimal Qnty { get; set; }
    }
}
