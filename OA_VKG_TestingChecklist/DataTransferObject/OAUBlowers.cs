﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.DataTransferObject
{
    class OAUBlowers
    {
        public string BlowerProperty { get; set; }
        public decimal IndoorVal { get; set; }
        public decimal ExhaustVal { get; set; }
    }
}
