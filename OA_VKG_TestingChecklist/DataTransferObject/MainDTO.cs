﻿using OA_VKG_TestingChecklist.Common.Enums;
using OA_VKG_TestingChecklist.DataTransferObject;
using OA_VKG_TestingChecklist.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist
{
    class MainDTO
    {        
        public ProductCode ProductType { get; set; }
        public OAUPreChecklist PreChecklist { get; set; }
        public OAUVoltage Voltages { get; set; }
        public List<OAUOption> Options { get; set; }
        public List<OAUOption> StandardEquipments { get; set; }
        public OAUCircuit1 Circuit1 { get; set; }
        public OAUCircuit2 Circuit2 { get; set; }
        public List<OAUFilter> Filters { get; set; }
        public List<OAUBlowers> Blowers { get; set; }
        public List<OAUCondenser> Condensers { get; set; }
        public List<OAUCompressor> Compressors { get; set; }
        public OptionValue IndoorBlowerType { get; set; }
        public OptionValue ExhaustBlowerType { get; set; }
        public OptionValue APRActive { get; set; }
        public OAUTPrePowerCheckslist OAUTPrePower { get; set; }
        public int HeadID { get; set; }
        public int ID { get; set; }
        public int OldHeadID { get; set; }
        public string IndoorBlowerModel { get; set; }
        public string ExhaustBlowerModel { get; set; }

        #region Blower Properties        

        private string _BlowerType;
        public string BlowerType
        {
            get
            {
                return this._BlowerType;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._BlowerType = value;
                }
                else
                {
                    throw new Exception("BlowerType cannot be more than 50 characters.");
                }
            }
        }

        private decimal _BlwHP;
        public decimal BlwHP
        {
            get
            {
                return this._BlwHP;
            }
            set
            {
                this._BlwHP = value;
            }
        }

        private decimal _BlwRatedAmp;
        public decimal BlwRatedAmp
        {
            get
            {
                return this._BlwRatedAmp;
            }
            set
            {
                this._BlwRatedAmp = value;
            }
        }

        private decimal _BlwRunningAmp;
        public decimal BlwRunningAmp
        {
            get
            {
                return this._BlwRunningAmp;
            }
            set
            {
                this._BlwRunningAmp = value;
            }
        }

        private decimal _BlwWheelSize;
        public decimal BlwWheelSize
        {
            get
            {
                return this._BlwWheelSize;
            }
            set
            {
                this._BlwWheelSize = value;
            }
        }

        private decimal _BlwMaxHz;
        public decimal BlwMaxHz
        {
            get
            {
                return this._BlwMaxHz;
            }
            set
            {
                this._BlwMaxHz = value;
            }
        }

        private decimal _BlwMinHz;
        public decimal BlwMinHz
        {
            get
            {
                return this._BlwMinHz;
            }
            set
            {
                this._BlwMinHz = value;
            }
        }

        private decimal _BlwVoltageOutput;
        public decimal BlwVoltageOutput
        {
            get
            {
                return this._BlwVoltageOutput;
            }
            set
            {
                this._BlwVoltageOutput = value;
            }
        }

        private string _BlwModelNum;
        public string BlwModelNum
        {
            get
            {
                return this._BlwModelNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._BlwModelNum = value;
                }
                else
                {
                    throw new Exception("ModelNum cannot be more than 50 characters.");
                }
            }
        }

        private string _BlwSerialnum;
        public string BlwSerialnum
        {
            get
            {
                return this._BlwSerialnum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._BlwSerialnum = value;
                }
                else
                {
                    throw new Exception("Serialnum cannot be more than 50 characters.");
                }
            }
        }

        private byte _BlwDoubleBlower = 0;
        public byte BlwDoubleBlower
        {
            get
            {
                return this._BlwDoubleBlower;
            }
            set
            {
                this._BlwDoubleBlower = value;
            }
        }

        public Boolean IsDoubleBlower
        {
            get
            {
                if (this._BlwDoubleBlower == 1)
                    return true;
                else
                    return false;
            }
        }

        #endregion

        #region Circuit Properties

        private string _CircuitNum;
        public string CircuitNum
        {
            get
            {
                return this._CircuitNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CircuitNum = value;
                }
                else
                {
                    throw new Exception("CircuitNum cannot be more than 50 characters.");
                }
            }
        }

        private string _CircuitValue;
        public string CircuitValue
        {
            get
            {
                return this._CircuitValue;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CircuitValue = value;
                }
                else
                {
                    throw new Exception("CircuitValue cannot be more than 50 characters.");
                }
            }
        }

        private string _CircuitValueType;
        public string CircuitValueType
        {
            get
            {
                return this._CircuitValueType;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CircuitValueType = value;
                }
                else
                {
                    throw new Exception("CircuitValueType cannot be more than 50 characters.");
                }
            }
        }
        #endregion

        #region Compressor Properties

        private string _CompNum;
        public string CompNum
        {
            get
            {
                return this._CompNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CompNum = value;
                }
                else
                {
                    throw new Exception("Compressornum cannot be more than 50 characters.");
                }
            }
        }

        private string _CompModelNum;
        public string CompModelNum
        {
            get
            {
                return this._CompModelNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CompModelNum = value;
                }
                else
                {
                    throw new Exception("CircuitNum cannot be more than 50 characters.");
                }
            }
        }

        private string _CompSerialNum;
        public string CompSerialNum
        {
            get
            {
                return this._CompSerialNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CompSerialNum = value;
                }
                else
                {
                    throw new Exception("CircuitNum cannot be more than 50 characters.");
                }
            }
        }

        private decimal _CompRatedAmps;
        public decimal CompRatedAmps
        {
            get
            {
                return this._CompRatedAmps;
            }
            set
            {

                this._CompRatedAmps = value;

            }
        }

        private decimal _CompRunningAmps;
        public decimal CompRunningAmps
        {
            get
            {
                return this._CompRunningAmps;
            }
            set
            {
                this._CompRunningAmps = value;

            }
        }
        #endregion

        #region Condenser Properties

        private string _CondNum;
        public string CondNum
        {
            get
            {
                return this._CondNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CondNum = value;
                }
                else
                {
                    throw new Exception("_CondNum cannot be more than 50 characters.");
                }
            }
        }

        private string _CondModelNum;
        public string CondModelNum
        {
            get
            {
                return this._CondModelNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CondModelNum = value;
                }
                else
                {
                    throw new Exception("CircuitNum cannot be more than 50 characters.");
                }
            }
        }

        private string _CondSerialNum;
        public string CondSerialNum
        {
            get
            {
                return this._CondSerialNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._CondSerialNum = value;
                }
                else
                {
                    throw new Exception("SerialNum cannot be more than 50 characters.");
                }
            }
        }

        private decimal _CondRatedAmps;
        public decimal CondRatedAmps
        {
            get
            {
                return this._CondRatedAmps;
            }
            set
            {

                this._CondRatedAmps = value;

            }
        }

        private decimal _CondRunningAmps;
        public decimal CondRunningAmps
        {
            get
            {
                return this._CondRunningAmps;
            }
            set
            {
                this._CondRunningAmps = value;

            }
        }
        #endregion

        #region Filter Properties
        private string _FilterSize;

        public string FilterSize
        {
            get
            {
                return this._FilterSize;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._FilterSize = value;
                }
                else
                {
                    throw new Exception("FilterSize cannot be more than 50 characters.");
                }
            }
        }
        private string _FilterStyle;
        public string FilterStyle
        {
            get
            {
                return this._FilterStyle;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._FilterStyle = value;
                }
                else
                {
                    throw new Exception("FilterStyle cannot be more than 50 characters.");
                }
            }
        }
        private decimal _Qty;
        public decimal Qty
        {
            get
            {
                return this._Qty;
            }
            set
            {
                this._Qty = value;

            }
        }
        private string _FilterPartNum;
        public string FilterPartNum
        {
            get
            {
                return this._FilterPartNum;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._FilterPartNum = value;
                }
                else
                {
                    throw new Exception("PartNum cannot be more than 50 characters.");
                }
            }
        }
        #endregion

        #region Measure Properties
        private decimal _InletModulationPressure;
        public decimal InletModulationPressure
        {
            get
            {
                return this._InletModulationPressure;
            }
            set
            {             
                this._InletModulationPressure = value;

            }
        }
        private decimal _LowFire;
        public decimal LowFire
        {
            get
            {
                return this._LowFire;
            }
            set
            {
               this._LowFire = value;
            }
        }

        private decimal _HighFire;
        public decimal HighFire
        {
            get
            {
                return this._HighFire;
            }
            set
            {        
                
                    this._HighFire = value;
            }
        }
        #endregion

        #region Head Properties

        public string JobNum {get; set;}


        private string _name;
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                if (value.Length <= 100)
                {
                    this._name = value;
                }
                else
                {
                    throw new Exception("Name cannot be more than 100 characters.");
                }
            }
        }

        private string _Serialnum;
        public string Serialnum
        {
            get
            {
                return this._Serialnum;
            }
            set
            {
                if (value.Length <= 100)
                {
                    this._Serialnum = value;
                }
                else
                {
                    throw new Exception("Serialnum cannot be more than 100 characters.");
                }
            }
        }

        private string _Modelnum;
        public string Modelnum
        {
            get
            {
                return this._Modelnum;
            }
            set
            {
                if (value.Length <= 100)
                {
                    this._Modelnum = value;
                }
                else
                {
                    throw new Exception("Modelnum cannot be more than 100 characters.");
                }
            }
        }
        private DateTime _DateTested;
        public DateTime DateTested
        {
            get
            {
                return this._DateTested;
            }
            set
            {
                this._DateTested = value;
            }
        }

        private string _TestedBy;
        public string TestedBy
        {
            get
            {
                return this._TestedBy;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._TestedBy = value;
                }
                else
                {
                    throw new Exception("TestedBy cannot be more than 50 characters.");
                }
            }
        }

        private decimal _AC_Temp;
        public decimal AC_Temp
        {
            get
            {
                return this._AC_Temp;
            }
            set
            {
                this._AC_Temp = value;
            }
        }

        private decimal _AC_Humidity;
        public decimal AC_Humidity
        {
            get
            {
                return this._AC_Humidity;
            }
            set
            {
                this._AC_Humidity = value;
            }
        }

        private decimal _Charge_Circuit_1;
        public decimal Charge_Circuit_1
        {
            get
            {
                return this._Charge_Circuit_1;
            }
            set
            {
                this._Charge_Circuit_1 = value;
            }
        }

        private decimal _Charge_Circuit_2;
        public decimal Charge_Circuit_2
        {
            get
            {
                return this._Charge_Circuit_2;
            }
            set
            {
                this._Charge_Circuit_2 = value;
            }
        }

        private string _Charge_Circuit_1_Var;
        public string Charge_Circuit_1_Var
        {
            get
            {
                return this._Charge_Circuit_1_Var;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Charge_Circuit_1_Var = value;
                }
                else
                {
                    throw new Exception("Charge_Circuit_1_Var cannot be more than 50 characters.");
                }
            }
        }

        private string _Charge_Circuit_2_Var;
        public string Charge_Circuit_2_Var
        {
            get
            {
                return this._Charge_Circuit_2_Var;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Charge_Circuit_2_Var = value;
                }
                else
                {
                    throw new Exception("Charge_Circuit_2_Var cannot be more than 50 characters.");
                }
            }
        }

        private string _Disconnect_Type;
        public string Disconnect_Type
        {
            get
            {
                return this._Disconnect_Type;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Disconnect_Type = value;
                }
                else
                {
                    throw new Exception("Disconnect_Type cannot be more than 50 characters.");
                }
            }
        }

        private string _Disconnect_Size;
        public string Disconnect_Size
        {
            get
            {
                return this._Disconnect_Size;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Disconnect_Size = value;
                }
                else
                {
                    throw new Exception("Disconnect_Size cannot be more than 50 characters.");
                }
            }
        }

        private string _ERV_Rated_Amp;
        public string ERV_Rated_Amp
        {
            get
            {
                return this._ERV_Rated_Amp;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._ERV_Rated_Amp = value;
                }
                else
                {
                    throw new Exception("Heat_Rated_Amp cannot be more than 50 characters.");
                }
            }
        }

        private string _ERV_Running_Amp;
        public string ERV_Running_Amp
        {
            get
            {
                return this._ERV_Running_Amp;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._ERV_Running_Amp = value;
                }
                else
                {
                    throw new Exception("Heat_Rated_Amp cannot be more than 50 characters.");
                }
            }
        }

        private byte _ERV_Proper_Rotation = 0;
        public byte ERV_Proper_Rotation
        {
            get
            {
                return this._ERV_Proper_Rotation;
            }
            set
            {
                this._ERV_Proper_Rotation = value;
            }
        }

        public Boolean IsERV_Proper_Rotation
        {
            get
            {
                if (this._ERV_Proper_Rotation == 1)
                    return true;
                else
                    return false;
            }
        }

        private string _ERV_ModelNo;
        public string ERV_ModelNo
        {
            get
            {
                return this._ERV_ModelNo;
            }
            set
            {
                this._ERV_ModelNo = value;
            }
        }

        private string _ERV_SerialNo;
        public string ERV_SerialNo
        {
            get
            {
                return this._ERV_SerialNo;
            }
            set
            {
                this._ERV_SerialNo = value;
            }
        }
        private string _MiscNotes;
        public string MiscNotes
        {
            get
            {
                return this._MiscNotes;
            }
            set
            {

                this._MiscNotes = value;

            }
        }

        private string _AuditedBy;
        public string AuditedBy
        {
            get
            {
                return this._AuditedBy;
            }
            set
            {

                this._AuditedBy = value;

            }
        }

        #endregion

        #region Heat Properties
      
        private string _Heat_Primary;
        public string Heat_Primary
        {
            get
            {
                return this._Heat_Primary;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Primary = value;
                }
                else
                {
                    throw new Exception("Heat_Primary cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_FuelType;
        public string Heat_FuelType
        {
            get
            {
                return this._Heat_FuelType;
            }
            set
            {     
                    this._Heat_FuelType = value;
            }
        }

        private string _Heat_Item_num;
        public string Heat_Item_num
        {
            get
            {
                return this._Heat_Item_num;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Item_num = value;
                }
                else
                {
                    throw new Exception("Heat_Item_num cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_PO_num;
        public string Heat_PO_num
        {
            get
            {
                return this._Heat_PO_num;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_PO_num = value;
                }
                else
                {
                    throw new Exception("Heat_PO_num cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_model_num_Top;
        public string Heat_model_num_Top
        {
            get
            {
                return this._Heat_model_num_Top;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_model_num_Top = value;
                }
                else
                {
                    throw new Exception("Heat_model_num cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Serial_Top;
        public string Heat_Serial_Top
        {
            get
            {
                return this._Heat_Serial_Top;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Serial_Top = value;
                }
                else
                {
                    throw new Exception("Heat_Serial cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Partnum_Top;
        public string Heat_Partnum_Top
        {
            get
            {
                return this._Heat_Partnum_Top;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Partnum_Top = value;
                }
                else
                {
                    throw new Exception("Heat_Partnum cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Size_Top;
        public string Heat_Size_Top
        {
            get
            {
                return this._Heat_Size_Top;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Size_Top = value;
                }
                else
                {
                    throw new Exception("Heat_Size cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_model_num_Bottom;
        public string Heat_model_num_Bottom
        {
            get
            {
                return this._Heat_model_num_Bottom;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_model_num_Bottom = value;
                }
                else
                {
                    throw new Exception("Heat_model_num cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Serial_Bottom;
        public string Heat_Serial_Bottom
        {
            get
            {
                return this._Heat_Serial_Bottom;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Serial_Bottom = value;
                }
                else
                {
                    throw new Exception("Heat_Serial cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Partnum_Bottom;
        public string Heat_Partnum_Bottom
        {
            get
            {
                return this._Heat_Partnum_Bottom;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Partnum_Bottom = value;
                }
                else
                {
                    throw new Exception("Heat_Partnum cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Size_Bottom;
        public string Heat_Size_Bottom
        {
            get
            {
                return this._Heat_Size_Bottom;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Size_Bottom = value;
                }
                else
                {
                    throw new Exception("Heat_Size cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Rated_Amp;
        public string Heat_Rated_Amp
        {
            get
            {
                return this._Heat_Rated_Amp;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Rated_Amp = value;
                }
                else
                {
                    throw new Exception("Heat_Rated_Amp cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_Running_Amp;
        public string Heat_Running_Amp
        {
            get
            {
                return this._Heat_Running_Amp;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Running_Amp = value;
                }
                else
                {
                    throw new Exception("Heat_Rated_Amp cannot be more than 50 characters.");
                }
            }
        }

        private string _TopBottom;
        public string TopBottom
        {
            get
            {
                return this._TopBottom;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._TopBottom = value;
                }
                else
                {
                    throw new Exception("TopBottom cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_model_num_PreHeat;
        public string Heat_model_num_PreHeat
        {
            get
            {
                return this._Heat_model_num_PreHeat;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_model_num_PreHeat = value;
                }
                else
                {
                    throw new Exception("_Heat_model_num_PreHeat cannot be more than 50 characters.");
                }
            }
        }

        public string _Heat_Serial_PreHeat;
        public string Heat_Serial_PreHeat
        {
            get
            {
                return this._Heat_Serial_PreHeat;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._Heat_Serial_PreHeat = value;
                }
                else
                {
                    throw new Exception("Heat_Serial_PreHeat cannot be more than 50 characters.");
                }
            }
        }

        private string _Heat_RatedAmp_PreHeat;
        public string Heat_RatedAmp_PreHeat
        {
            get
            {
                return this._Heat_RatedAmp_PreHeat;
            }
            set
            {
                if (value.Length <= 20)
                {
                    this._Heat_RatedAmp_PreHeat = value;
                }
                else
                {
                    throw new Exception("Heat_RatedAmp_PreHeat cannot be more than 20 characters.");
                }
            }
        }

        private string _Heat_RunningAmp_PreHeat;
        public string Heat_RunningAmp_PreHeat
        {
            get
            {
                return this._Heat_RunningAmp_PreHeat;
            }
            set
            {
                if (value.Length <= 20)
                {
                    this._Heat_RunningAmp_PreHeat = value;
                }
                else
                {
                    throw new Exception("Heat_RunningAmp_PreHeat cannot be more than 20 characters.");
                }
            }
        }

        private string _Heat_Size_PreHeat;
        public string Heat_Size_PreHeat
        {
            get
            {
                return this._Heat_Size_PreHeat;
            }
            set
            {
                if (value.Length <= 20)
                {
                    this._Heat_Size_PreHeat = value;
                }
                else
                {
                    throw new Exception("Heat_Size_PreHeat cannot be more than 20 characters.");
                }
            }
        }
        #endregion

        #region Issue Properties
        private string IJobNum { get; set; } 

        private byte _issueCheck;
        public byte IssueCheck
        {
            get
            {
                return this._issueCheck;
            }
            set
            {
                this._issueCheck = value;
            }

        }

        private string _issueTypeCode;
        public string IssueTypeCode
        {
            get
            {
                return this._issueTypeCode;
            }
            set
            {
                this._issueTypeCode = value;
            }

        }

        private string _issueCodeDesc;
        public string IssueCodeDesc
        {
            get
            {
                return this._issueCodeDesc;
            }
            set
            {
                this._issueCodeDesc = value;
            }

        }

        private string _issueSource;
        public string IssueSource
        {
            get
            {
                return this._issueSource;
            }
            set
            {
                this._issueSource = value;
            }

        }
        private string _issueDescription;
        public string IssueDescription
        {
            get
            {
                return this._issueDescription;
            }
            set
            {
                this._issueDescription = value;
            }

        }
        private string _horizonType;
        public string HorizonType
        {
            get
            {
                return this._horizonType;
            }
            set
            {
                this._horizonType = value;
            }

        }
        private string _line;
        public string Line
        {
            get
            {
                return this._line;
            }
            set
            {
                this._line = value;
            }

        }
        private string _notified;
        public string Notified
        {
            get
            {
                return this._notified;
            }
            set
            {
                this._notified = value;
            }

        }
        private byte _qcSigned;
        public byte QCsigned
        {
            get
            {
                return this._qcSigned;
            }
            set
            {
                this._qcSigned = value;
            }

        }
        private byte _impact;
        public byte Impact
        {
            get
            {
                return this._impact;
            }
            set
            {
                this._impact = value;
            }

        }
        private string _impactTime;
        public string ImpactTime
        {
            get
            {
                return this._impactTime;
            }
            set
            {
                this._impactTime = value;
            }

        }

        private string _assemblyimpactTime;
        public string AssemblyImpactTime
        {
            get
            {
                return this._assemblyimpactTime;
            }
            set
            {
                this._assemblyimpactTime = value;
            }

        }

        private string _electricBoardimpactTime;
        public string ElectricBoardImpactTime
        {
            get
            {
                return this._electricBoardimpactTime;
            }
            set
            {
                this._electricBoardimpactTime = value;
            }

        }
        private string _electricLineimpactTime;
        public string ElectricLineImpactTime
        {
            get
            {
                return this._electricLineimpactTime;
            }
            set
            {
                this._electricLineimpactTime = value;
            }

        }
        private string _engineeringimpactTime;
        public string EngineeringImpactTime
        {
            get
            {
                return this._engineeringimpactTime;
            }
            set
            {
                this._engineeringimpactTime = value;
            }

        }
        private string _manufacturerimpactTime;
        public string ManufacturerImpactTime
        {
            get
            {
                return this._manufacturerimpactTime;
            }
            set
            {
                this._manufacturerimpactTime = value;
            }

        }
        private string _mechanicalimpactTime;
        public string MechanicalImpactTime
        {
            get
            {
                return this._mechanicalimpactTime;
            }
            set
            {
                this._mechanicalimpactTime = value;
            }

        }
        private string _refrigerationimpactTime;
        public string RefrigerationImpactTime
        {
            get
            {
                return this._refrigerationimpactTime;
            }
            set
            {
                this._refrigerationimpactTime = value;
            }

        }
        private string _issueCodes;
        public string IssueCodes
        {
            get
            {
                return this._issueCodes;
            }
            set
            {
                this._issueCodes = value;
            }

        }
        #endregion

        #region Option Properties
        private string _OptionName;
        public string OptionName
        {
            get
            {
                return this._OptionName;
            }
            set
            {
                if (value.Length <= 50)
                {
                    this._OptionName = value;
                }
                else
                {
                    throw new Exception("OptionName cannot be more than 50 characters.");
                }
            }
        }


        private byte _OptionInstalled = 0;
        public byte OptionInstalled
        {
            get
            {
                return this._OptionInstalled;
            }
            set
            {
                this._OptionInstalled = value;
            }
        }

        public Boolean IsOptionInstalled
        {
            get
            {
                if (this._OptionInstalled == 1)
                    return true;
                else
                    return false;
            }
        }

        private byte _OptionTested = 0;
        public byte OptionTested
        {
            get
            {
                return this._OptionTested;
            }
            set
            {
                this._OptionTested = value;
            }
        }

        public Boolean IsOptionTested
        {
            get
            {
                if (this._OptionTested == 1)
                    return true;
                else
                    return false;
            }
        }


        #endregion

        #region Voltage Properties
        private string _Voltage;
        public string Voltage
        {
            get
            {
                return this._Voltage;
            }
            set
            {

                this._Voltage = value;

            }
        }

        private string _Voltage_Value;
        public string Voltage_Value
        {
            get
            {
                return this._Voltage_Value;
            }
            set
            {

                this._Voltage_Value = value;

            }
        }
        #endregion

       
    }
}
