﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.DataTransferObject
{
    class OAUCircuit2
    {
        public string SuctPSIVal { get; set; }
        public string SSTVal { get; set; }
        public string SuctTempVal { get; set; }
        public string LiqPSIVal { get; set; }
        public string SLT { get; set; }
        public string LiqTempVal { get; set; }
        public string DisPSIVal { get; set; }
        public string SDTVal { get; set; }
        public string DisTempVal { get; set; }
        public string Subcooling { get; set; }
        public string SuperHeat { get; set; }
    }
}
