﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.DataTransferObject
{
    class OAUOption
    {
        public string Optionlabel { get; set; }
        public Boolean InstalledType { get; set; }
        public Boolean TestedType { get; set; }

    }
}
