﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.DataTransferObject
{
    class OAUCondenser
    {
        public string CondenserNum { get; set; }
        public decimal CondenserRatedAmps { get; set; }
        public decimal ConenserdRunningAmps { get; set; }
    }
}
