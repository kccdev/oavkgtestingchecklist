﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.DataTransferObject
{
    class OAUCompressor
    {
        public string CompressorNum { get; set; }
        public string CompressorModelNum { get; set; }
        public string CompressorSerialNum { get; set; }
        public decimal CompressorRunningAmps { get; set; }
        public decimal CompressorRatedAmps { get; set; }
    }
}
