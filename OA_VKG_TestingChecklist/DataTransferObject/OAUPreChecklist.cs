﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OA_VKG_TestingChecklist.Services
{
    class OAUPreChecklist
    {
       
        private string _Verify_sub_un_match;
        public string Verify_sub_un_match
        {
            get
            {
                return this._Verify_sub_un_match;
            }
            set
            {
                this._Verify_sub_un_match = value;
            }
        }

        private string _Unit_built_compl_CCS;
        public string Unit_built_compl_CCS
        {
            get
            {
                return this._Unit_built_compl_CCS;
            }
            set
            {
                this._Unit_built_compl_CCS = value;
            }
        }

        private string _Inspect_unit_items;
        public string Inspect_unit_items
        {
            get
            {
                return this._Inspect_unit_items;
            }
            set
            {
                this._Inspect_unit_items = value;
            }
        }

        private string _Wires_circuit_break;
        public string Wires_circuit_break
        {
            get
            {
                return this._Wires_circuit_break;
            }
            set
            {
                this._Wires_circuit_break = value;
            }
        }

        private string _Power_wire_driver;
        public string Power_wire_driver
        {
            get
            {
                return this._Power_wire_driver;
            }
            set
            {
                this._Power_wire_driver = value;
            }
        }


        private string _Line_Load_term_Correctly;
        public string Line_Load_term_Correctly
        {
            get
            {
                return this._Line_Load_term_Correctly;
            }
            set
            {
                this._Line_Load_term_Correctly = value;
            }
        }

        private string _Vis_term_prop_connector;
        public string Vis_term_prop_connector
        {
            get
            {
                return this._Vis_term_prop_connector;
            }
            set
            {
                this._Vis_term_prop_connector = value;
            }
        }

        private string _Check_fuses_correct_amp;
        public string Check_fuses_correct_amp
        {
            get
            {
                return this._Check_fuses_correct_amp;
            }
            set
            {
                this._Check_fuses_correct_amp = value;
            }
        }


        private string _Comp_power_wires_comp;
        public string Comp_power_wires_comp
        {
            get
            {
                return this._Comp_power_wires_comp;
            }
            set
            {
                this._Comp_power_wires_comp = value;
            }
        }

        private string _Verify_plug_harn_correct;
        public string Verify_plug_harn_correct
        {
            get
            {
                return this._Verify_plug_harn_correct;
            }
            set
            {
                this._Verify_plug_harn_correct = value;
            }
        }

        private string _Co_power_wire_contactor;
        public string Co_power_wire_contactor
        {
            get
            {
                return this._Co_power_wire_contactor;
            }
            set
            {
                this._Co_power_wire_contactor = value;
            }
        }

        private string _Tight_term_angel_hairs;
        public string Tight_term_angel_hairs
        {
            get
            {
                return this._Tight_term_angel_hairs;
            }
            set
            {
                this._Tight_term_angel_hairs = value;
            }
        }

        private string _Wire_crank_angel_hairs;
        public string Wire_crank_angel_hairs
        {
            get
            {
                return this._Wire_crank_angel_hairs;
            }
            set
            {
                this._Wire_crank_angel_hairs = value;
            }
        }

        private string _Crankcase_correct_volt;
        public string Crankcase_correct_volt
        {
            get
            {
                return this._Crankcase_correct_volt;
            }
            set
            {
                this._Crankcase_correct_volt = value;
            }
        }

        private string _Wires_switch_prop_term;
        public string Wires_switch_prop_term
        {
            get
            {
                return this._Wires_switch_prop_term;
            }
            set
            {
                this._Wires_switch_prop_term = value;
            }
        }

        private string _Transform_correct_volt;
        public string Transform_correct_volt
        {
            get
            {
                return this._Transform_correct_volt;
            }
            set
            {
                this._Transform_correct_volt = value;
            }
        }

        private string _Con_out_assembler_instal;
        public string Con_out_assembler_instal
        {
            get
            {
                return this._Con_out_assembler_instal;
            }
            set
            {
                this._Con_out_assembler_instal = value;
            }
        }

        private string _PreChecksMiscNotes;
        public string PreChecksMiscNotes
        {
            get
            {
                return this._PreChecksMiscNotes;
            }
            set
            {

                this._PreChecksMiscNotes = value;

            }
        }
        private string _Power_whip_over_tight;
        public string Power_whip_over_tight
        {
            get
            {
                return this._Power_whip_over_tight;
            }
            set
            {
                this._Power_whip_over_tight = value;
            }
        }

        private string _Correct_size_whip_unit;
        public string Correct_size_whip_unit
        {
            get
            {
                return this._Correct_size_whip_unit;
            }
            set
            {
                this._Correct_size_whip_unit = value;
            }
        }

        private string _Attach_guages_unit;
        public string Attach_guages_unit
        {
            get
            {
                return this._Attach_guages_unit;
            }
            set
            {
                this._Attach_guages_unit = value;
            }
        }

        private string _Co_Power_Ground_purp;
        public string Co_Power_Ground_purp
        {
            get
            {
                return this._Co_Power_Ground_purp;
            }
            set
            {
                this._Co_Power_Ground_purp = value;
            }
        }

        private string _Cont_High_volt_term;
        public string Cont_High_volt_term
        {
            get
            {
                return this._Cont_High_volt_term;
            }
            set
            {
                this._Cont_High_volt_term = value;
            }
        }

        private string _Check_Phases_fan_motor;
        public string Check_Phases_fan_motor
        {
            get
            {
                return this._Check_Phases_fan_motor;
            }
            set
            {
                this._Check_Phases_fan_motor = value;
            }
        }

        private string _Remove_volt_Sen_Sensor;
        public string Remove_volt_Sen_Sensor
        {
            get
            {
                return this._Remove_volt_Sen_Sensor;
            }
            set
            {
                this._Remove_volt_Sen_Sensor = value;
            }
        }

        private string _Hi_pot_unit_fans;
        public string Hi_pot_unit_fans
        {
            get
            {
                return this._Hi_pot_unit_fans;
            }
            set
            {
                this._Hi_pot_unit_fans = value;
            }
        }

        private string _Inspect_ARC_flash;
        public string Inspect_ARC_flash
        {
            get
            {
                return this._Inspect_ARC_flash;
            }
            set
            {
                this._Inspect_ARC_flash = value;
            }
        }

        private string _Check_Power_term_block;
        public string Check_Power_term_block
        {
            get
            {
                return this._Check_Power_term_block;
            }
            set
            {
                this._Check_Power_term_block = value;
            }
        }

        private string _Inspect_element_heater;
        public string Inspect_element_heater
        {
            get
            {
                return this._Inspect_element_heater;
            }
            set
            {
                this._Inspect_element_heater = value;
            }
        }

        private string _Check_heater_cont_board;
        public string Check_heater_cont_board
        {
            get
            {
                return this._Check_heater_cont_board;
            }
            set
            {
                this._Check_heater_cont_board = value;
            }
        }

        private string _Co_volt_writtn_transform;
        public string Co_volt_writtn_transform
        {
            get
            {
                return this._Co_volt_writtn_transform;
            }
            set
            {
                this._Co_volt_writtn_transform = value;
            }
        }

        private string _Connect_gas_manifold;
        public string Connect_gas_manifold
        {
            get
            {
                return this._Connect_gas_manifold;
            }
            set
            {
                this._Connect_gas_manifold = value;
            }
        }

        private string _Unlock_gas_valves;
        public string Unlock_gas_valves
        {
            get
            {
                return this._Unlock_gas_valves;
            }
            set
            {
                this._Unlock_gas_valves = value;
            }
        }

        private string _Turn_on_gas;
        public string Turn_on_gas
        {
            get
            {
                return this._Turn_on_gas;
            }
            set
            {
                this._Turn_on_gas = value;
            }
        }

        private string _En_field_gas_port;
        public string En_field_gas_port
        {
            get
            {
                return this._En_field_gas_port;
            }
            set
            {
                this._En_field_gas_port = value;
            }
        }

        private string _In_Plug_are_Missing;
        public string In_Plug_are_Missing
        {
            get
            {
                return this._In_Plug_are_Missing;
            }
            set
            {
                this._In_Plug_are_Missing = value;
            }
        }

        private string _Turn_off_gas_inspection;
        public string Turn_off_gas_inspection
        {
            get
            {
                return this._Turn_off_gas_inspection;
            }
            set
            {
                this._Turn_off_gas_inspection = value;
            }
        }

        private string _Bac_Air_proving_switches;
        public string Bac_Air_proving_switches
        {
            get
            {
                return this._Bac_Air_proving_switches;
            }
            set
            {
                this._Bac_Air_proving_switches = value;
            }
        }

        private string _Install_TD7;
        public string Install_TD7
        {
            get
            {
                return this._Install_TD7;
            }
            set
            {
                this._Install_TD7 = value;
            }
        }

        private string _Program_UC600;
        public string Program_UC600
        {
            get
            {
                return this._Program_UC600;
            }
            set
            {
                this._Program_UC600 = value;
            }
        }

        private string _Set_Phase_Monitor;
        public string Set_Phase_Monitor
        {
            get
            {
                return this._Set_Phase_Monitor;
            }
            set
            {
                this._Set_Phase_Monitor = value;
            }
        }

        private string _Unlock_disconnect_k_box;
        public string Unlock_disconnect_k_box
        {
            get
            {
                return this._Unlock_disconnect_k_box;
            }
            set
            {
                this._Unlock_disconnect_k_box = value;
            }
        }

        private string _Un_Correct_volt_disconn;
        public string Un_Correct_volt_disconn
        {
            get
            {
                return this._Un_Correct_volt_disconn;
            }
            set
            {
                this._Un_Correct_volt_disconn = value;
            }
        }

    }
}
