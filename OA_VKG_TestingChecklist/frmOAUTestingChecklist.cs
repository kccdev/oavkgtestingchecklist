﻿//using CrystalDecisions.CrystalReports.Engine;
//using CrystalDecisions.Shared;
using OA_VKG_TestingChecklist.Common.Enums;
using OA_VKG_TestingChecklist.DataTransferObject;
using OA_VKG_TestingChecklist.Extensions;
using OA_VKG_TestingChecklist.Services;
using OA_VKG_TestingChecklist.UserControlObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OA_VKG_TestingChecklist
{
    public partial class frmOAUTestingChecklist : Form
    {        
        #region "Data Members"
        MainBO objMain = new MainBO();
        bool IsNew = true;
        bool IsValidated = true;
        /// <summary>
        /// Temporary to allow users to do work on production checklist for previous day jobs
        /// </summary>
        public bool SkipPreChecksValidation { get; set; }
        #endregion
        public frmOAUTestingChecklist()
        {
            InitializeComponent();

        }
        #region Events
        private void frmOAUTestingChecklist_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(tbModelNum.Text))
            {
                objMain.ProductType = tbModelNum.Text.Substring(0, 2) == "OA" ? ProductCode.Horizon : ProductCode.MixedAir;
            }
            if (lbHeadID.Text != "0")
            {
                IsNew = false;
                objMain.HeadID = Int32.Parse(lbHeadID.Text);
            }

            //if (objMain.ProductType == ProductCode.MixedAir && string.IsNullOrWhiteSpace(cbHorizonType.Text))
            //{
            //    cbLinenum.Text = "MAULineA";
            //    cbHorizonType.Text = "HAE (Viking)";
            //}
            ResizeCheckContainer();
            BindHeaderDataAtPrintEvent();
            BindOAUTestingChecklistAtPrintEvent();

            //if (cbLinenum.Text == "Viking Line A")  // 08/19/2021 - TonyT - No longer needed, now set in stored procedure.
            //{
            //    cbLinenum.Text = "MAULineA";
            //}
            GetUnitLineForProduct();
            preChecksControl1.SetDependencies(this.objMain);
            preChecksControl1.ConfigureAndPopulateControls();
            preChecksControl1.LoadData(objMain.HeadID);

            preChecksControl1.BindPreChecksDataAtPrintEvent();

            //prePowerChecksControl1.SetDependencies(this.objMain);
            //prePowerChecksControl1.ConfigureAndPopulateControls();
            //prePowerChecksControl1.LoadPrePowerData(objMain.HeadID);
            //prePowerChecksControl1.BindPrePowerChecksDataAtPrintEvent();

            butIssues.Visible = false;
            btnExportAll.Visible = true;
            btnPrintProduction.Visible = true;
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            string errors = "";
            Cursor.Current = Cursors.WaitCursor;
            errors = validateChecklistData();

            if (errors.Length > 0)
            {
                MessageBox.Show(errors);
            }
            else
            {
                if (IsNew == false)
                {
                    objMain.DeleteChecklistByHeadID();
                    IsNew = true;
                }

                insertNewChecklist();

                if (butSave.Text == "Save")
                {
                    MessageBox.Show("Checklist saved!");
                }
                else
                {
                    MessageBox.Show("Checklist updated!");
                }
                butIssues.Enabled = true;
                butSave.Text = "Update";
            }
        }

        //        private void butPrint_Click(object sender, EventArgs e)
        //        {
        //            string jobNum = tbJobNum.Text;

        //            frmPrint frmPrt = new frmPrint();

        //            ReportDocument cryRpt = new ReportDocument();

        //            ParameterValues curPV = new ParameterValues();

        //            ParameterFields paramFields = new ParameterFields();

        //            ParameterField pf1 = new ParameterField();
        //            ParameterDiscreteValue pdv1 = new ParameterDiscreteValue();
        //            pf1.Name = "@JobNum";
        //            pdv1.Value = jobNum;
        //            pf1.CurrentValues.Add(pdv1);

        //            paramFields.Add(pf1);

        //            frmPrt.crystalReportViewer1.ParameterFieldInfo = paramFields;

        //#if DEBUG            
        //            cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\RefrigerationComponent_TestVersion.rpt");
        //#else
        //            cryRpt.Load(@"\\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\RefrigerationComponent.rpt");
        //#endif

        //            frmPrt.crystalReportViewer1.ReportSource = cryRpt;
        //            frmPrt.ShowDialog();
        //            frmPrt.crystalReportViewer1.Refresh();


        //        }

        private void ButExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void butIssues_Click(object sender, EventArgs e)
        {
            if (objMain.HeadID == 0)
            {
                MessageBox.Show("Please Save the Checklist before marking the issue");
            }
            else if (cbHorizonType.Text == "" || cbLinenum.Text == "")
            {
                MessageBox.Show("Please select the Line and unit type before marking the issue!");
            }
            else
            {
                frmIssues frmIs = new frmIssues();
                frmIs.lbJobNum.Text = tbJobNum.Text;
                objMain.JobNum = tbJobNum.Text;
                frmIs.lbHeadID.Text = objMain.HeadID.ToString();

                //DataTable dt = objMain.GetJobIssues();
                if (objMain.HorizonType == null)
                {
                    objMain.HorizonType = cbHorizonType.Text;
                }
                if (objMain.Line == null)
                {
                    objMain.Line = cbLinenum.Text;
                }
                //if (dt.Rows.Count > 0)
                //{
                //    DataRow dr = dt.Rows[0];
                //    frmIs.cbLinenum.Text = dr["Line"].ToString();
                //    frmIs.cbHorizonType.Text = dr["HorizonType"].ToString();                
                //}

                frmIs.cbLinenum.Text = objMain.Line;
                frmIs.cbHorizonType.Text = objMain.HorizonType;
                frmIs.tbIssueImpactTimeMinutes.Text = "";
                frmIs.cbIssueType.Text = "";
                frmIs.tbIssueDescription.Text = "";
                frmIs.tbIssueSource.Text = "";

                if (frmIs.cbLinenum.Text != "")
                {
                    frmIs.cbLinenum.Enabled = false;

                }
                if (frmIs.cbHorizonType.Text != "")
                {
                    frmIs.cbHorizonType.Enabled = false;
                }
                DataTable dt = objMain.GetJobIssues();
                if (dt.Rows.Count > 0)
                {
                    frmIs.dgvIssues.DataSource = dt;

                    frmIs.dgvIssues.Columns["IssueSource"].Width = 405;
                    frmIs.dgvIssues.Columns["IssueSource"].HeaderText = "IssueSource";
                    frmIs.dgvIssues.Columns["IssueSource"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    frmIs.dgvIssues.Columns["IssueSource"].DisplayIndex = 0;
                    frmIs.dgvIssues.Columns["IssueDescription"].Width = 425;
                    frmIs.dgvIssues.Columns["IssueDescription"].HeaderText = "IssueDescription";
                    frmIs.dgvIssues.Columns["IssueDescription"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    frmIs.dgvIssues.Columns["IssueDescription"].DisplayIndex = 1;
                    frmIs.dgvIssues.Columns["ImpactTime"].Width = 100;
                    frmIs.dgvIssues.Columns["ImpactTime"].HeaderText = "ImpactTime(Mins)";
                    frmIs.dgvIssues.Columns["ImpactTime"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                    frmIs.dgvIssues.Columns["ImpactTime"].DisplayIndex = 2;
                    frmIs.dgvIssues.Columns["IssueCodes"].Width = 125;
                    frmIs.dgvIssues.Columns["IssueCodes"].HeaderText = "Issue Type";
                    frmIs.dgvIssues.Columns["IssueCodes"].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
                    frmIs.dgvIssues.Columns["IssueCodes"].DisplayIndex = 3;
                    frmIs.dgvIssues.Columns["IssueTypeCode"].Visible = false;
                    frmIs.dgvIssues.Columns["Line"].Visible = false;
                    frmIs.dgvIssues.Columns["HeadID"].Visible = false;
                    frmIs.dgvIssues.Columns["HorizonType"].Visible = false;
                    frmIs.dgvIssues.Columns["ID"].Visible = false;

                    // this.OAUTestTimeIssues.tbHorizonType.Text = tbModelNum.Text.Remove(4);

                }

                frmIs.ShowDialog();
            }
        }

        private void cbHeatPrimary_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cbHeatPrimary.Text == "Gas")
            {
                cbHeatPosition.Text = "Top";
                cbHeatPosition.Items.Clear();
                cbHeatPosition.Items.Add("Top");
                cbHeatPosition.Items.Add("Top/Bottom");
                tbHeatModelNumTop.Enabled = true;
                tbHeatSerialNumTop.Enabled = true;
                tbHeatModelNumTop.Enabled = true;
                tbHeatSizeTop.Enabled = true;
                tbHeatPartNumTop.Enabled = true;

                tbHeatItemNum.Enabled = true;
                tbHeatPONum.Enabled = true;
                tbHeatRatedAmps.Enabled = true;
                tbHeatRunningAmps.Enabled = true;


            }
            else if (this.cbHeatPrimary.Text == "Electric")
            {
                cbHeatPosition.Text = "Top";
                cbHeatPosition.Items.Clear();
                cbHeatPosition.Items.Add("Top");
                tbHeatSerialNumTop.Enabled = true;
                tbHeatModelNumTop.Enabled = true;
                tbHeatSizeTop.Enabled = true;

                tbHeatPartNumTop.Enabled = true;
                tbHeatItemNum.Enabled = true;
                tbHeatPONum.Enabled = true;
                tbHeatRatedAmps.Enabled = true;
                tbHeatRunningAmps.Enabled = true;
            }
            else if (this.cbHeatPrimary.Text == "None")
            {
                cbHeatPosition.Text = "";
                cbHeatPosition.Items.Clear();
                tbHeatModelNumTop.Enabled = false;
                tbHeatModelNumTop.Clear();
                tbHeatSerialNumTop.Enabled = false;
                tbHeatSerialNumTop.Clear();
                tbHeatModelNumTop.Enabled = false;
                tbHeatSizeTop.Enabled = false;
                tbHeatSizeTop.Clear();
                errorProviderApp.SetError(cbHeatPosition, "");
                errorProviderApp.SetError(cbHeatSectionFuelType, "");
                errorProviderApp.SetError(tbHeatModelNumTop, "");
                errorProviderApp.SetError(tbHeatSerialNumTop, "");

                tbHeatPartNumTop.Enabled = false;
                tbHeatPartNumTop.Clear();
                tbHeatItemNum.Enabled = false;
                tbHeatItemNum.Clear();
                tbHeatPONum.Enabled = false;
                tbHeatPONum.Clear();
                tbHeatRatedAmps.Enabled = false;
                tbHeatRatedAmps.Clear();
                tbHeatRunningAmps.Enabled = false;
                tbHeatRunningAmps.Clear();
            }
            else
            {
                cbHeatPosition.Text = "Top";
                cbHeatPosition.Items.Clear();
                cbHeatPosition.Items.Add("Top");
                tbHeatModelNumTop.Enabled = true;
                tbHeatSerialNumTop.Enabled = true;
                tbHeatModelNumTop.Enabled = true;
                tbHeatSizeTop.Enabled = true;
                tbHeatPartNumTop.Enabled = true;

                tbHeatItemNum.Enabled = true;
                tbHeatPONum.Enabled = true;
                tbHeatRatedAmps.Enabled = true;
                tbHeatRunningAmps.Enabled = true;
            }
        }

        private void cbHeatPosition_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbHeatPosition.Text == "Top")
            {
                tbHeatModelNumBottom.Enabled = false;
                tbHeatSerialNumBottom.Enabled = false;
                tbHeatPartNumBottom.Enabled = false;
                tbHeatSizeBottom.Enabled = false;
                tbHeatModelNumBottom.Text = "";
                tbHeatSerialNumBottom.Text = "";
                tbHeatPartNumBottom.Text = "";
                tbHeatSizeBottom.Text = "";
            }
            else
            {
                tbHeatModelNumBottom.Enabled = true;
                tbHeatSerialNumBottom.Enabled = true;
                tbHeatPartNumBottom.Enabled = true;
                tbHeatSizeBottom.Enabled = true;
            }
        }

        private void tcTestingChecklist_Selecting(object sender, TabControlCancelEventArgs e)
        {

            if (tcTestingChecklist.SelectedTab == tabPage3)
            {
                ResizeCheckContainer();
                butIssues.Visible = true;
                butIssues.Enabled = true;

                string tabValidations = ValidateTabs(sender, e);
                if (!string.IsNullOrEmpty(tabValidations))
                {
                    MessageBox.Show(tabValidations, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            else if (tcTestingChecklist.SelectedTab == tpPreChecks)
            {
                butIssues.Visible = false;

            }
            //else if (tcTestingChecklist.SelectedTab == tpPrePowerChecks)
            //{
            //    butIssues.Visible = false;
            //    if (!this.SkipPreChecksValidation)
            //    {
            //        string tabValidations = ValidatePreChecksTabs(sender, e);
            //        if (!string.IsNullOrEmpty(tabValidations))
            //        {
            //            MessageBox.Show(tabValidations, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //        }
            //    }
            //}
        }

        private void cbLinenum_SelectedIndexChanged(object sender, EventArgs e)
        {
            objMain.Line = cbLinenum.Text;
            errorProviderApp.Clear();
        }

        private void cbHorizonType_SelectedIndexChanged(object sender, EventArgs e)
        {

            objMain.HorizonType = cbHorizonType.Text;
            errorProviderApp.Clear();
        }

        private void tbTestedBy_TextChanged(object sender, EventArgs e)
        {
            objMain.TestedBy = tbTestedBy.Text;
            errorProviderApp.Clear();
        }
        private void btnPrintProduction_Click(object sender, EventArgs e)
        {
            string unitTypeStr = tbModelNum.Text.Substring(0, 2);

            BindOAUTestingChecklistAtPrintEvent();
            ExportServices exportServices = new ExportServices();
            try
            {
                string reportsPath = $"{Application.StartupPath}\\Reports";
                if (unitTypeStr == "HA")
                {
                    string filePath = $"{Application.StartupPath}\\Reports\\VkgTestingChecklist_{Guid.NewGuid().ToString()}.pdf";
                    if (!System.IO.Directory.Exists(reportsPath))
                    {
                        System.IO.Directory.CreateDirectory(reportsPath);
                    }
                    exportServices.ExportOAUTestingChecklist(objMain, filePath);
                    System.Diagnostics.Process.Start(filePath);
                }
                else
                {
                    string filePath = $"{Application.StartupPath}\\Reports\\OAUTestingChecklist_{Guid.NewGuid().ToString()}.pdf";
                    if (!System.IO.Directory.Exists(reportsPath))
                    {
                        System.IO.Directory.CreateDirectory(reportsPath);
                    }
                    exportServices.ExportOAUTestingChecklist(objMain, filePath);
                    System.Diagnostics.Process.Start(filePath);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void btnExportAll_Click_1(object sender, EventArgs e)
        {
            BindHeaderDataAtPrintEvent();
            //preChecks.BindPreChecksDataAtPrintEvent();
            //prePowerChecksControl1.BindPrePowerChecksDataAtPrintEvent();
            BindOAUTestingChecklistAtPrintEvent();

            ExportServices exportServices = new ExportServices();
            try
            {
                string reportsPath = $"{Application.StartupPath}\\Reports";
                string filePath = $"{Application.StartupPath}\\Reports\\TestingChecklists_{Guid.NewGuid().ToString()}.pdf";
                if (!System.IO.Directory.Exists(reportsPath))
                {
                    System.IO.Directory.CreateDirectory(reportsPath);
                }
                exportServices.ExportTestingChecklist(objMain, filePath);
                System.Diagnostics.Process.Start(filePath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }

        private void frmOAUTestingChecklist_Resize(object sender, EventArgs e)
        {
            ResizeCheckContainer();
        }

        public string ValidatePreChecksTabs(object sender, CancelEventArgs e)
        {
            List<string> errors = new List<string>();


            try
            {
                string validateHeaderResults = validateHeaderFields();
                if (!String.IsNullOrEmpty(validateHeaderResults))
                {
                    errors.Add(validateHeaderResults);
                    e.Cancel = true;
                }
            }
            catch
            {
                errors.Add("You must complete ALL mandatory checks before saving!");
            }

            string preChecksValidation = preChecksControl1.ValidateCondtions();
            if (!string.IsNullOrEmpty(preChecksValidation))
            {

                errors.Add(preChecksValidation);
                e.Cancel = true;
            }
            StringBuilder msgBuilder = new StringBuilder();
            if (errors.Count > 0)
            {
                msgBuilder.AppendLine("Please complete the Pre-Checks Checklist error before moving to Testing Checklist.");
                msgBuilder.AppendLine();
                errors.ForEach(m => msgBuilder.AppendLine(m));
            }
            return msgBuilder.ToString();
        }

        public string ValidateTabs(object sender, CancelEventArgs e)
        {
            List<string> errors = new List<string>();

            try
            {
                if (!string.IsNullOrWhiteSpace(tbTestedBy.Text))
                {
                    string validateHeaderResults = validateHeaderFields();

                    if (!String.IsNullOrEmpty(validateHeaderResults))
                    {
                        errors.Add(validateHeaderResults);
                        e.Cancel = true;
                    }
                }
            }
            catch
            {
                errors.Add("You must complete ALL mandatory checks before saving!");
            }
            if (!this.SkipPreChecksValidation)
            {
                string preChecksValidation = preChecksControl1.ValidateCondtions();
                if (!string.IsNullOrEmpty(preChecksValidation))
                {

                    errors.Add(preChecksValidation);
                    e.Cancel = true;
                }

                //string prePowerChecksValidation = preChecksControl1.ValidateConditions();
                //if (!string.IsNullOrEmpty(prePowerChecksValidation))
                //{
                //    errors.Add(prePowerChecksValidation);
                //    e.Cancel = true;
                //}
            }
            StringBuilder msgBuilder = new StringBuilder();
            if (errors.Count > 0)
            {
                msgBuilder.AppendLine("Please complete the Pre-Checks Checklist error before moving to Testing Checklist.");
                msgBuilder.AppendLine();
                errors.ForEach(m => msgBuilder.AppendLine(m));
            }
            return msgBuilder.ToString();
        }
        #endregion

        #region "Public Methods"
        public void GetUnitLineForProduct()
        {
            if (!string.IsNullOrWhiteSpace(tbModelNum.Text))
            {
                objMain.ProductType = tbModelNum.Text.Substring(0, 2) == "OA" ? ProductCode.Horizon : ProductCode.MixedAir;
            }


            if (objMain.ProductType == ProductCode.MixedAir)
            {
                cbLinenum.Items.Add("MAULineA");
                cbHorizonType.Items.Add("HAE (Viking)");
                cbHorizonType.Items.Add("HAE w/Aux");
            }
            else if (objMain.ProductType == ProductCode.Horizon)
            {

                cbLinenum.Items.Add("LineA");
                cbLinenum.Items.Add("LineB");
                cbLinenum.Items.Add("LineC");
                cbLinenum.Items.Add("LineD");
                cbHorizonType.Items.Add("OAB");
                cbHorizonType.Items.Add("OAD");
                cbHorizonType.Items.Add("OAG");
                cbHorizonType.Items.Add("OAK");
                cbHorizonType.Items.Add("OAN");
                cbHorizonType.Items.Add("OAB w/Aux");
                cbHorizonType.Items.Add("OAD w/Aux");
                cbHorizonType.Items.Add("OAG w/Aux");
                cbHorizonType.Items.Add("OAK w/Aux");
                cbHorizonType.Items.Add("OAN w/Aux");
            }

        }

        public void BindHeaderDataAtPrintEvent()
        {

            objMain.JobNum = tbJobNum.Text;
            objMain.Name = tbJobName.Text;
            objMain.Serialnum = tbSerialnum.Text;
            objMain.Modelnum = tbModelNum.Text;
            objMain.Line = cbLinenum.Text;
            objMain.HorizonType = cbHorizonType.Text;
            objMain.TestedBy = tbTestedBy.Text;
            objMain.DateTested = dtpDate.Value;
            objMain.AuditedBy = tbAuditedBy.Text;
        }

        public string validateHeaderFields()
        {
            StringBuilder errors = new StringBuilder();


            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(tbTestedBy.Text))
                {
                    tbTestedBy.Focus();
                    errorProviderApp.SetError(tbTestedBy, "Required!");
                    IsValidated = false;
                }
                if (!IsValidated)
                {
                    errors.AppendLine("TESTED BY:");
                    errors.AppendLine("You must enter your name in the TESTED BY field");
                }
            }
            catch
            {
                errors.AppendLine("You must enter your name in the TESTED BY field");
            }

            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(cbLinenum.Text))
                {
                    cbLinenum.Focus();
                    errorProviderApp.SetError(cbLinenum, "Required!");
                    IsValidated = false;
                }
                if (!IsValidated)
                {
                    errors.AppendLine("LINE:");
                    errors.AppendLine("Please select the Line before saving!");
                }
            }
            catch
            {
                errors.AppendLine("Please select the Line before saving!");
            }

            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(cbHorizonType.Text))
                {
                    cbHorizonType.Focus();
                    errorProviderApp.SetError(cbHorizonType, "Required!");
                    IsValidated = false;
                }
                if (!IsValidated)
                {
                    errors.AppendLine("UNIT TYPE:");
                    errors.AppendLine("Please select the unit type before saving!");
                }
            }
            catch
            {
                errors.AppendLine("Please select the unit type before saving!");
            }
            return errors.ToString();
        }
        public string validateChecklistData()
        {
            List<string> errors = new List<string>();
            try
            {
                string validateHeaderResults = validateHeaderFields();
                if (!String.IsNullOrEmpty(validateHeaderResults))
                {
                    errors.Add(validateHeaderResults);
                }
            }
            catch
            {
                errors.Add("You must complete ALL mandatory checks before saving!");
            }

            if (!this.SkipPreChecksValidation)
            {
                try
                {
                    string preChecksValidationResult = preChecksControl1.ValidateCondtions();
                    if (!String.IsNullOrEmpty(preChecksValidationResult))
                    {
                        errors.Add(preChecksValidationResult);
                    }
                }
                catch
                {
                    errors.Add("You must complete ALL mandatory checks before saving!");
                }

                try
                {
                    //string prePowerChecksValidationResult = prePowerChecksControl1.ValidateConditions();
                    //if (!String.IsNullOrEmpty(prePowerChecksValidationResult))
                    //{
                    //    errors.Add(prePowerChecksValidationResult);
                    //}

                }
                catch
                {
                    errors.Add("You must complete mandatory checks before saving!");
                }
            }

            try
            {
                string producttestingChecklistValidationResult = ValidateCondition();
                if (!String.IsNullOrWhiteSpace(producttestingChecklistValidationResult))
                {
                    errors.Add("ProductTestingChecklist:");
                    errors.Add(producttestingChecklistValidationResult);
                }

            }
            catch
            {
                errors.Add("You must complete mandatory checks before saving!");
            }
            StringBuilder msgBuilder = new StringBuilder();
            if (errors.Count > 0)
            {
                msgBuilder.AppendLine("Please correct the below errors before saving the changes.");
                msgBuilder.AppendLine();
                errors.ForEach(m => msgBuilder.AppendLine(m));
            }
            return msgBuilder.ToString();
        }
        public string ValidateCondition()
        {
            StringBuilder errors = new StringBuilder();
            string unitTypeStr = tbModelNum.Text.Substring(0, 2);
            decimal tempDec = 0;
            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(nudTemperature.Text))
                {
                    nudTemperature.Focus();
                    errorProviderApp.SetError(nudTemperature, "Required!");
                    IsValidated = false;
                }
                else if (nudTemperature.Value <= -100 || nudTemperature.Value >= 200)
                {
                    nudTemperature.Focus();
                    errorProviderApp.SetError(nudTemperature, "Specify a numeric value greater than -100 and less than 200.");
                    IsValidated = false;
                }
                else { errorProviderApp.SetError(nudTemperature, ""); }
                if (!IsValidated)
                {
                    errors.AppendLine(string.IsNullOrWhiteSpace(nudTemperature.Text) ? "Please fill the required field - Ambient Condition Temperature" :
                        "Ambient Condition Temperature - Specify a numeric value greater than -100 and less than 200.");
                }

            }
            catch
            {
                errors.AppendLine("Please fill the required field - Ambient Condition Temperature");
            }
            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(nudHumidity.Text))
                {
                    nudHumidity.Focus();
                    errorProviderApp.SetError(nudHumidity, "Required!");
                    IsValidated = false;
                }
                else if (nudHumidity.Value <= 0 || nudHumidity.Value >= 100)
                {
                    nudHumidity.Focus();
                    errorProviderApp.SetError(nudHumidity, "Specify a numeric value greater than 0 and less than 100.");
                    IsValidated = false;
                }
                else { errorProviderApp.SetError(nudHumidity, ""); }
                if (!IsValidated)
                {
                    errors.AppendLine(string.IsNullOrWhiteSpace(nudHumidity.Text) ? "Please fill the required field - Ambient Condition Humidity" :
                        "Ambient Condition Humidity - Specify a numeric value greater than 0 and less than 100.");
                }
            }
            catch
            {
                errors.AppendLine("Please fill the required field - Ambient Condition Humidity");
            }
            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(nudCircuit1Charge.Text))
                {
                    nudCircuit1Charge.Focus();
                    errorProviderApp.SetError(nudCircuit1Charge, "Required!");
                    IsValidated = false;
                }
                else if (nudCircuit1Charge.Value <= 0 || nudCircuit1Charge.Value >= 100)
                {
                    nudCircuit1Charge.Focus();
                    errorProviderApp.SetError(nudCircuit1Charge, "Specify a numeric value greater than 0 and less than 100.");
                    IsValidated = false;
                }
                else { errorProviderApp.SetError(nudCircuit1Charge, ""); }
                if (!IsValidated)
                {
                    errors.AppendLine(string.IsNullOrWhiteSpace(nudCircuit1Charge.Text) ? "Please fill the required field - Charge Data  Circuit#1 " :
                        "Charge Data  Circuit#1 - Specify a numeric value greater than 0 and less than 100.");
                }

            }
            catch
            {
                errors.AppendLine("Please fill the required field -Charge Data Circuit#1");
            }
            if (unitTypeStr == "OA")
            {
                try
                {

                    IsValidated = true;
                    if (nudCircuit2Charge.Value <= 0 || nudCircuit2Charge.Value >= 100 || string.IsNullOrWhiteSpace(nudCircuit2Charge.Text))
                    {
                        nudCircuit2Charge.Focus();
                        errorProviderApp.SetError(nudCircuit2Charge, "Required!");
                        IsValidated = false;
                    }
                    else { errorProviderApp.SetError(nudCircuit2Charge, ""); }
                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required field - Charge Data  Circuit#2");
                    }

                }

                catch
                {
                    errors.AppendLine("Please fill the required field -Charge Data Circuit#2");
                }
            }

            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(tbDisconnectSize.Text))
                {
                    tbDisconnectSize.Focus();
                    errorProviderApp.SetError(tbDisconnectSize, "Required!");
                    IsValidated = false;
                }
                else { errorProviderApp.SetError(tbDisconnectSize, ""); }
                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the required field - Disconnect Size");
                }
            }
            catch
            {
                errors.AppendLine("Please fill the required field - Disconnect Size");
            }
            try
            {
                IsValidated = true;
                if (string.IsNullOrWhiteSpace(tbModelNumIndoor.Text))
                {
                    tbModelNumIndoor.Focus();
                    errorProviderApp.SetError(tbModelNumIndoor, "Required!");
                    IsValidated = false;
                }
                else { errorProviderApp.SetError(tbModelNumIndoor, ""); }
                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the required field - Blower Model Number Indoor");
                }

            }
            catch
            {
                errors.AppendLine("Please fill the required field - Blower Model Number Indoor");
            }
            try
            {
                if (tbERVRunningAmps.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbERVRunningAmps.Text);
                }
            }
            catch
            {
                errors.AppendLine("ERV Running Amp is a numeric field");
            }

            try
            {
                if (tbERVRatedAmps.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbERVRatedAmps.Text);
                }
            }
            catch
            {
                errors.AppendLine("ERV Rated Amp is a numeric field");
            }

            try
            {
                if (tbHpIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbHpIndoor.Text);
                    errorProviderApp.SetError(tbHpIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbHpIndoor.Text))
                    {
                        tbHpIndoor.Focus();
                        errorProviderApp.SetError(tbHpIndoor, "Required!");
                        IsValidated = false;
                    }

                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower HP Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor Blower HP is a numeric field");
            }

            try
            {
                if (tbRatedAmpsIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbRatedAmpsIndoor.Text);
                    errorProviderApp.SetError(tbRatedAmpsIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbRatedAmpsIndoor.Text))
                    {
                        tbRatedAmpsIndoor.Focus();
                        errorProviderApp.SetError(tbRatedAmpsIndoor, "Required!");
                        IsValidated = false;
                    }



                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower Rated Amps Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor Rated Amps is a numeric field");
            }

            try
            {
                if (tbRunningAmpsIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbRunningAmpsIndoor.Text);
                    errorProviderApp.SetError(tbRunningAmpsIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbRunningAmpsIndoor.Text))
                    {
                        tbRunningAmpsIndoor.Focus();
                        errorProviderApp.SetError(tbRunningAmpsIndoor, "Required!");
                        IsValidated = false;
                    }

                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower Running Amps Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor Running Amps is a numeric field");
            }

            try
            {
                if (tbWheelSizeIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbWheelSizeIndoor.Text);
                    errorProviderApp.SetError(tbWheelSizeIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbWheelSizeIndoor.Text))
                    {
                        tbWheelSizeIndoor.Focus();
                        errorProviderApp.SetError(tbWheelSizeIndoor, "Required!");
                        IsValidated = false;
                    }

                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower Wheel Size Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor Wheel Size is a numeric field");
            }

            try
            {
                if (tbMaxHzIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbMaxHzIndoor.Text);
                    errorProviderApp.SetError(tbMaxHzIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbMaxHzIndoor.Text))
                    {
                        tbMaxHzIndoor.Focus();
                        errorProviderApp.SetError(tbMaxHzIndoor, "Required!");
                        IsValidated = false;
                    }

                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower MaxHz Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor MaxHz is a numeric field");
            }

            try
            {
                if (tbMinHzIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbMinHzIndoor.Text);
                    errorProviderApp.SetError(tbMinHzIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbMinHzIndoor.Text))
                    {
                        tbMinHzIndoor.Focus();
                        errorProviderApp.SetError(tbMinHzIndoor, "Required!");
                        IsValidated = false;
                    }

                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower MinHz Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor MinHz is a numeric field");
            }

            try
            {
                if (tbVoltageOutputIndoor.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbVoltageOutputIndoor.Text);
                    errorProviderApp.SetError(tbVoltageOutputIndoor, "");
                }
                else
                {
                    IsValidated = true;
                    if (string.IsNullOrWhiteSpace(tbVoltageOutputIndoor.Text))
                    {
                        tbVoltageOutputIndoor.Focus();
                        errorProviderApp.SetError(tbVoltageOutputIndoor, "Required!");
                        IsValidated = false;
                    }

                    if (!IsValidated)
                    {
                        errors.AppendLine("Please fill the required numeric field - Blower Voltage Output Indoor");
                    }
                }
            }
            catch
            {
                errors.AppendLine("Indoor Voltage Output is a numeric field");
            }

            try
            {
                if (tbHpExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbHpExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust Blower HP is a numeric field");
            }

            try
            {
                if (tbRatedAmpsExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbRatedAmpsExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust Rated Amps is a numeric field");
            }

            try
            {
                if (tbRunningAmpsExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbRunningAmpsExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust Running Amps is a numeric field");
            }

            try
            {
                if (tbWheelSizeExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbWheelSizeExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust Wheel Size is a numeric field");
            }

            try
            {
                if (tbMaxHzExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbMaxHzExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust MaxHz is a numeric field");
            }

            try
            {
                if (tbMinHzExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbMinHzExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust MinHz is a numeric field");
            }

            try
            {
                if (tbVoltageOutputExhaust.Text.Length > 0)
                {
                    tempDec = decimal.Parse(tbVoltageOutputExhaust.Text);
                }
            }
            catch
            {
                errors.AppendLine("Exhaust Voltage Output is a numeric field");
            }
            try
            {
                if (txtInletModulationPresure.Text.Length > 0)
                {
                    tempDec = decimal.Parse(txtInletModulationPresure.Text);
                }
            }
            catch
            {
                errors.AppendLine("InletModulation Pressure is a numeric field");
            }

            try
            {
                if (txtLowFire.Text.Length > 0)
                {
                    tempDec = decimal.Parse(txtLowFire.Text);
                }
            }
            catch
            {
                errors.AppendLine("Low Fire Measure Pressure is a numeric field");
            }

            try
            {
                if (txtHighFire.Text.Length > 0)
                {
                    tempDec = decimal.Parse(txtHighFire.Text);
                }
            }
            catch
            {
                errors.AppendLine("High Fire Measure Pressure is a numeric field");
            }

            //int num = 0;
            //try
            //{
            //    if (txtSuperHeat.Text != "")
            //    {
            //        num = int.Parse(txtSuperHeat.Text);

            //        if (unitTypeStr == "OA")
            //        {
            //            if (num >= 10 && num <= 25)
            //            {
            //                tempDec = decimal.Parse(txtSuperHeat.Text);
            //            }
            //            else
            //            {
            //                return "Super Heat value lied between 10 to 25";
            //            }
            //        }
            //        else
            //        {
            //            if (num >= 10 && num <= 20)
            //            {
            //                tempDec = decimal.Parse(txtSuperHeat.Text);
            //            }
            //            else
            //            {
            //                return "Super Heat value lied between 10 to 20";
            //            }
            //        }
            //    }
            //}
            //catch
            //{
            //    errors.AppendLine("Super Heat value lied between 10 to 25");
            //}
            //if (unitTypeStr == "OA" && tbCircuit2SuperHeat.Text != "")
            //{
            //    int numCircuit2 = int.Parse(tbCircuit2SuperHeat.Text);
            //    if (tbCircuit2SuperHeat.Text.Length > 0)
            //    {
            //        try
            //        {
            //            if (numCircuit2 >= 10 && numCircuit2 <= 25)
            //            {
            //                tempDec = decimal.Parse(tbCircuit2SuperHeat.Text);
            //            }
            //            else
            //            {
            //                return "Super Heat value lied between 10 to 25";
            //            }
            //        }
            //        catch
            //        {
            //            errors.AppendLine("Super Heat value lied between 10 to 25");
            //        }
            //    }
            //}
            int compNum = 1;
            foreach (var Textboxvar in this.gbCompressorData.Controls.OfType<TextBox>())
            {
                if (Textboxvar.Name.Contains("Rated"))
                {
                    try
                    {
                        if (Textboxvar.Text.Length > 0)
                        {
                            tempDec = decimal.Parse(Textboxvar.Text);
                        }
                    }
                    catch
                    {
                        errors.AppendLine("Comp" + compNum + "Rated Amps is a numeric field");
                    }
                }

                if (Textboxvar.Name.Contains("Running"))
                {
                    try
                    {
                        if (Textboxvar.Text.Length > 0)
                        {
                            tempDec = decimal.Parse(Textboxvar.Text);
                        }
                    }
                    catch
                    {
                        errors.AppendLine("Comp" + compNum + "\n Running Amps is a numeric field");
                    }
                }
                ++compNum;
            }
            if (unitTypeStr == "HA")
            {
                foreach (var Checkboxvar in this.gbOptionsViking.Controls.OfType<CheckBox>())
                {
                    if (Checkboxvar.Name.Contains("Installed"))
                    {
                        if (Checkboxvar.Checked)
                        {
                            var str = Checkboxvar.Name.Substring(0, Checkboxvar.Name.Length - 12);
                            var flag = false;
                            foreach (var obj in this.gbOptionsViking.Controls.OfType<CheckBox>())
                            {
                                if (obj.Name.Contains("Tested") && obj.Name.Contains(str))
                                {
                                    if (!obj.Checked)
                                    {
                                        errors.AppendLine("Please test the installed optional equipment/components.");
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                            if (flag)
                            {
                                break;
                            }

                        }
                    }

                }
                foreach (var Checkboxvar in this.gbStdEquipViking.Controls.OfType<CheckBox>())
                {
                    if (Checkboxvar.Name.Contains("Installed"))
                    {
                        if (Checkboxvar.Checked)
                        {
                            var str = Checkboxvar.Name.Substring(0, Checkboxvar.Name.Length - 12);
                            var flag = false;
                            foreach (var obj in this.gbStdEquipViking.Controls.OfType<CheckBox>())
                            {
                                if (obj.Name.Contains("Tested") && obj.Name.Contains(str))
                                {
                                    if (!obj.Checked)
                                    {
                                        errors.AppendLine("Please test the installed standard equipment/components.");
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                            if (flag)
                            {
                                break;
                            }

                        }
                    }

                }
            }
            else
            {
                foreach (var Checkboxvar in this.gbOptions.Controls.OfType<CheckBox>())
                {
                    if (Checkboxvar.Name.Contains("Installed"))
                    {
                        if (Checkboxvar.Checked)
                        {
                            var str = Checkboxvar.Name.Substring(0, Checkboxvar.Name.Length - 9);
                            var flag = false;
                            foreach (var obj in this.gbOptions.Controls.OfType<CheckBox>())
                            {
                                if (obj.Name.Contains("Tested") && obj.Name.Contains(str))
                                {
                                    if (!obj.Checked)
                                    {
                                        errors.AppendLine("Please test the installed optional equipment/components.");
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                            if (flag)
                            {
                                break;
                            }

                        }
                    }

                }
                foreach (var Checkboxvar in this.gbStandardEquipment.Controls.OfType<CheckBox>())
                {
                    if (Checkboxvar.Name.Contains("Installed"))
                    {
                        if (Checkboxvar.Checked)
                        {
                            var str = Checkboxvar.Name.Substring(0, Checkboxvar.Name.Length - 9);
                            var flag = false;
                            foreach (var obj in this.gbStandardEquipment.Controls.OfType<CheckBox>())
                            {
                                if (obj.Name.Contains("Tested") && obj.Name.Contains(str))
                                {
                                    if (!obj.Checked)
                                    {
                                        errors.AppendLine("Please test the installed standard equipment/components.");
                                        flag = true;
                                        break;
                                    }
                                }
                            }
                            if (flag)
                            {
                                break;
                            }

                        }
                    }

                }

            }

            int condNum = 1;
            foreach (var Textboxvar in this.gbCondensers.Controls.OfType<TextBox>())
            {
                if (Textboxvar.Name.Contains("Rated"))
                {
                    try
                    {
                        if (Textboxvar.Text.Length > 0)
                        {
                            tempDec = decimal.Parse(Textboxvar.Text);
                        }
                    }
                    catch
                    {
                        errors.AppendLine("Condenser" + condNum + "\n Rated Amps is a numeric field");
                    }
                }

                if (Textboxvar.Name.Contains("Running"))
                {
                    try
                    {
                        if (Textboxvar.Text.Length > 0)
                        {
                            tempDec = decimal.Parse(Textboxvar.Text);
                        }
                    }
                    catch
                    {
                        errors.AppendLine("Condenser" + condNum + "\n Running Amps is a numeric field");
                    }
                }
                ++condNum;
            }

            string filtQty = "";
            int tempInt = 0;
            foreach (DataGridViewRow row in dgvFilters.Rows)
            {
                if (row.Cells["FilterSize"].Value != null)
                {
                    if (row.Cells["Qty"].Value != null)
                    {
                        filtQty = row.Cells["Qty"].Value.ToString();

                        try
                        {
                            tempInt = Int32.Parse(filtQty);
                        }
                        catch
                        {
                            errors.AppendLine("Filter Qty is an integer field");
                        }
                    }

                }
            }

            IsValidated = true;
            if (!rbVoltage208.Checked && !rbVoltage460.Checked && !rbVoltage575.Checked)
            {

                errorProviderApp.SetError(rbVoltage460, "Required!");
                errorProviderApp.SetError(rbVoltage208, "Required!");
                errorProviderApp.SetError(rbVoltage575, "Required!");
                IsValidated = false;

                if (!IsValidated)
                {
                    errors.AppendLine("Please select the required field");
                }
                else
                {
                    errors.Clear();
                }
            }
            else
            {
                errorProviderApp.SetError(rbVoltage460, "");
                errorProviderApp.SetError(rbVoltage208, "");
                errorProviderApp.SetError(rbVoltage575, "");
            }

            IsValidated = true;

            if (cbHeatPrimary.SelectedIndex == 0)
            {
                if (string.IsNullOrWhiteSpace(tbHeatModelNumTop.Text) && tbHeatModelNumTop.Enabled == true)
                {
                    errorProviderApp.SetError(tbHeatModelNumTop, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbHeatModelNumTop, "");
                }
                if (string.IsNullOrWhiteSpace(tbHeatSerialNumTop.Text) && tbHeatSerialNumTop.Enabled == true)
                {
                    errorProviderApp.SetError(tbHeatSerialNumTop, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbHeatSerialNumTop, "");
                }
                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the Heat Top mandatory fields");
                }
               
            }
            else
            {
                errorProviderApp.SetError(tbHeatModelNumTop, "");
                errorProviderApp.SetError(tbHeatSerialNumTop, "");
            }


            IsValidated = true;
            if (cbHeatPrimary.SelectedIndex==1)
            {
                if (string.IsNullOrWhiteSpace(tbHeatSizeTop.Text) && tbHeatSizeTop.Enabled == true)
                {
                    errorProviderApp.SetError(tbHeatSizeTop, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbHeatSizeTop, "");
                }
                if (string.IsNullOrWhiteSpace(tbHeatPartNumTop.Text) && tbHeatPartNumTop.Enabled == true)
                {
                    errorProviderApp.SetError(tbHeatPartNumTop, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbHeatPartNumTop, "");
                }
                if (string.IsNullOrWhiteSpace(tbHeatPONum.Text) && tbHeatPONum.Enabled == true)
                {
                    errorProviderApp.SetError(tbHeatPONum, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbHeatPONum, "");
                }
                if (string.IsNullOrWhiteSpace(tbHeatItemNum.Text) && tbHeatItemNum.Enabled == true)
                {
                    errorProviderApp.SetError(tbHeatItemNum, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbHeatItemNum, "");
                }
                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the Heat Top mandatory fields");
                }
                
            }
            else
            {
                errorProviderApp.SetError(tbHeatPartNumTop, "");
                errorProviderApp.SetError(tbHeatSizeTop, "");
                errorProviderApp.SetError(tbHeatItemNum, "");
                errorProviderApp.SetError(tbHeatPONum, "");
            }




            IsValidated = true;
            if (cbHeaterInstalled.Checked || cbHeaterInstalledVkg.Checked)
            {
                if (cbHeatPrimary.SelectedIndex == 2)
                {
                    errorProviderApp.SetError(cbHeatPrimary, "Required!");

                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(cbHeatPrimary, "");
                }
                if (string.IsNullOrWhiteSpace(cbHeatPosition.Text))
                {
                    errorProviderApp.SetError(cbHeatPosition, "Required!");

                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(cbHeatPosition, "");
                }
                if (cbHeatSectionFuelType.SelectedItem == null)
                {
                    errorProviderApp.SetError(cbHeatSectionFuelType, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(cbHeatSectionFuelType, "");
                }
                //if (string.IsNullOrWhiteSpace(tbHeatModelNumTop.Text) && tbHeatModelNumTop.Enabled == true)
                //{
                //    errorProviderApp.SetError(tbHeatModelNumTop, "Required!");
                //    IsValidated = false;
                //}
                //else
                //{
                //    errorProviderApp.SetError(tbHeatModelNumTop, "");
                //}
                //if (string.IsNullOrWhiteSpace(tbHeatSerialNumTop.Text) && tbHeatSerialNumTop.Enabled == true)
                //{
                //    errorProviderApp.SetError(tbHeatSerialNumTop, "Required!");
                //    IsValidated = false;
                //}
                //else
                //{
                //    errorProviderApp.SetError(tbHeatSerialNumTop, "");
                //}


                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the Heat mandatory fields");
                }

            }
            else
            {
                errorProviderApp.SetError(cbHeatPrimary, "");
                errorProviderApp.SetError(cbHeatPosition, "");
                errorProviderApp.SetError(cbHeatSectionFuelType, "");
                //errorProviderApp.SetError(tbHeatModelNumTop, "");
                //errorProviderApp.SetError(tbHeatSerialNumTop, "");
            }

            IsValidated = true;
            if (cbERVInstalled.Checked || cbERVInstalledVkg.Checked)
            {
                if (string.IsNullOrWhiteSpace(tbERV_ModelNo.Text))
                {
                    errorProviderApp.SetError(tbERV_ModelNo, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbERV_ModelNo, "");
                }
                if (string.IsNullOrWhiteSpace(tbERV_SerialNo.Text))
                {
                    errorProviderApp.SetError(tbERV_SerialNo, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbERV_SerialNo, "");
                }
                if (string.IsNullOrWhiteSpace(tbERVRunningAmps.Text))
                {
                    errorProviderApp.SetError(tbERVRunningAmps, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbERVRunningAmps, "");
                }
                if (string.IsNullOrWhiteSpace(tbERVRatedAmps.Text))
                {
                    errorProviderApp.SetError(tbERVRatedAmps, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbERVRatedAmps, "");
                }
                if (!chkbERVProperRotation.Checked)
                {
                    errorProviderApp.SetError(chkbERVProperRotation, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(chkbERVProperRotation, "");
                }


                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the ERV mandatory fields");
                }

            }
            else
            {
                errorProviderApp.SetError(tbERV_ModelNo, "");
                errorProviderApp.SetError(tbERV_SerialNo, "");
                errorProviderApp.SetError(tbERVRunningAmps, "");
                errorProviderApp.SetError(tbERVRatedAmps, "");
                errorProviderApp.SetError(chkbERVProperRotation, "");
            }


            IsValidated = true;
            if (cbPreHeatInstalled.Checked || cbPreHeatInstalledVkg.Checked)
            {
                if (string.IsNullOrWhiteSpace(tbPreHeatModelNo.Text))
                {
                    errorProviderApp.SetError(tbPreHeatModelNo, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbPreHeatModelNo, "");
                }
                if (string.IsNullOrWhiteSpace(tbPreHeatSerialNo.Text))
                {
                    errorProviderApp.SetError(tbPreHeatSerialNo, "Required!");
                    IsValidated = false;
                }
                else
                {
                    errorProviderApp.SetError(tbPreHeatSerialNo, "");
                }


                if (!IsValidated)
                {
                    errors.AppendLine("Please fill the Pre-heat mandatory fields");
                }

            }
            else
            {
                errorProviderApp.SetError(tbPreHeatModelNo, "");
                errorProviderApp.SetError(tbPreHeatSerialNo, "");
            }
            return errors.ToString();
        }

        private void bindFormDataToModelAndSave()
        {
            string headIdStr = "";
            string unitTypeStr = tbModelNum.Text.Substring(0, 2);


            #region OAUT_TC_Head Data

            objMain.JobNum = tbJobNum.Text;
            objMain.Name = tbJobName.Text;
            objMain.Serialnum = tbSerialnum.Text;
            objMain.Modelnum = tbModelNum.Text;
            objMain.Line = cbLinenum.Text;
            objMain.HorizonType = cbHorizonType.Text;
            objMain.TestedBy = tbTestedBy.Text;
            objMain.DateTested = dtpDate.Value;
            objMain.AuditedBy = tbAuditedBy.Text;
            objMain.AC_Temp = nudTemperature.Value;
            objMain.AC_Humidity = nudTemperature.Value;
            objMain.Charge_Circuit_1 = nudCircuit1Charge.Value;
            objMain.Charge_Circuit_2 = nudCircuit2Charge.Value;
            objMain.Charge_Circuit_1_Var = tbCircuit1ChargeVariance.Text;
            objMain.Charge_Circuit_2_Var = tbCircuit2ChargeVariance.Text;
            objMain.Disconnect_Type = cbDisconnectType.Text;
            objMain.Disconnect_Size = tbDisconnectSize.Text;
            objMain.ERV_ModelNo = tbERV_ModelNo.Text;
            objMain.ERV_SerialNo = tbERV_SerialNo.Text;
            objMain.ERV_Running_Amp = tbERVRunningAmps.Text;
            objMain.ERV_Rated_Amp = tbERVRatedAmps.Text;
            objMain.MiscNotes = tbMiscNotes.Text;
            if (chkbERVProperRotation.Checked == true)
            {
                objMain.ERV_Proper_Rotation = 1;
            }
            else
            {
                objMain.ERV_Proper_Rotation = 0;
            }

            try
            {
                objMain.OldHeadID = objMain.HeadID;
                headIdStr = objMain.InsertJobHead();
                if (headIdStr != "-1")
                {
                    DataTable dtIssue = objMain.GetJobIssues();
                    objMain.HeadID = Int32.Parse(headIdStr);

                    foreach (DataRow drow in dtIssue.Rows)
                    {
                        objMain.ID = (int)drow["ID"];
                        objMain.UpdateIssuesHeadID();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_Head - " + ex);
            }

            #endregion            

            #region OAUT_TC_Blower Data
            objMain.BlowerType = "Indoor";
            objMain.BlwHP = 0;
            if (tbHpIndoor.Text.Length > 0)
            {
                objMain.BlwHP = decimal.Parse(tbHpIndoor.Text);
            }
            objMain.BlwRatedAmp = 0;
            if (tbRatedAmpsIndoor.Text.Length > 0)
            {
                objMain.BlwRatedAmp = decimal.Parse(tbRatedAmpsIndoor.Text);
            }

            objMain.BlwRunningAmp = 0;
            if (tbRunningAmpsIndoor.Text.Length > 0)
            {
                objMain.BlwRunningAmp = decimal.Parse(tbRunningAmpsIndoor.Text);
            }

            objMain.BlwWheelSize = 0;
            if (tbWheelSizeIndoor.Text.Length > 0)
            {
                objMain.BlwWheelSize = decimal.Parse(tbWheelSizeIndoor.Text);
            }

            objMain.BlwMaxHz = 0;
            if (tbMaxHzIndoor.Text.Length > 0)
            {
                objMain.BlwMaxHz = decimal.Parse(tbMaxHzIndoor.Text);
            }

            objMain.BlwMinHz = 0;
            if (tbMinHzIndoor.Text.Length > 0)
            {
                objMain.BlwMinHz = decimal.Parse(tbMinHzIndoor.Text);
            }

            objMain.BlwVoltageOutput = 0;
            if (tbVoltageOutputIndoor.Text.Length > 0)
            {
                objMain.BlwVoltageOutput = decimal.Parse(tbVoltageOutputIndoor.Text);
            }

            objMain.BlwModelNum = tbModelNumIndoor.Text;

            if (chkbDoubleBlowerIndoor.Checked == true)
            {
                objMain.BlwDoubleBlower = 1;
            }
            else
            {
                objMain.BlwDoubleBlower = 0;
            }

            try
            {
                objMain.InsertBlowerData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving Indoor OAUT_TC_Blower - " + ex);
            }

            objMain.BlowerType = "Exhaust" +
                " ";
            objMain.BlwHP = 0;
            if (tbHpExhaust.Text.Length > 0)
            {
                objMain.BlwHP = decimal.Parse(tbHpExhaust.Text);
            }
            objMain.BlwRatedAmp = 0;
            if (tbRatedAmpsExhaust.Text.Length > 0)
            {
                objMain.BlwRatedAmp = decimal.Parse(tbRatedAmpsExhaust.Text);
            }

            objMain.BlwRunningAmp = 0;
            if (tbRunningAmpsExhaust.Text.Length > 0)
            {
                objMain.BlwRunningAmp = decimal.Parse(tbRunningAmpsExhaust.Text);
            }

            objMain.BlwWheelSize = 0;
            if (tbWheelSizeExhaust.Text.Length > 0)
            {
                objMain.BlwWheelSize = decimal.Parse(tbWheelSizeExhaust.Text);
            }

            objMain.BlwMaxHz = 0;
            if (tbMaxHzExhaust.Text.Length > 0)
            {
                objMain.BlwMaxHz = decimal.Parse(tbMaxHzExhaust.Text);
            }

            objMain.BlwMinHz = 0;
            if (tbMinHzExhaust.Text.Length > 0)
            {
                objMain.BlwMinHz = decimal.Parse(tbMinHzExhaust.Text);
            }

            objMain.BlwVoltageOutput = 0;
            if (tbVoltageOutputExhaust.Text.Length > 0)
            {
                objMain.BlwVoltageOutput = decimal.Parse(tbVoltageOutputExhaust.Text);
            }

            objMain.BlwModelNum = tbModelNumExhaust.Text;

            if (chkbDoubleBlowerExhaust.Checked == true)
            {
                objMain.BlwDoubleBlower = 1;
            }
            else
            {
                objMain.BlwDoubleBlower = 0;
            }

            try
            {
                objMain.InsertBlowerData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving Exhaust OAUT_TC_Blower - " + ex);
            }
            #endregion

            #region OAUT_TC_CircuitData

            try
            {
                objMain.CircuitNum = "Circuit1";

                foreach (var Textboxvar in this.gbCircuit1.Controls.OfType<TextBox>())
                {
                    objMain.CircuitValueType = Textboxvar.Name;
                    objMain.CircuitValue = Textboxvar.Text;

                    objMain.InsertCircuitData();
                }

                objMain.CircuitValueType = "chkbCircuit1APRActive";
                objMain.CircuitValue = "False";

                if (chkbCircuit1APRActive.Checked == true)
                {
                    objMain.CircuitValue = "True";
                }
                objMain.InsertCircuitData();
                if (unitTypeStr == "OA")
                {
                    objMain.CircuitNum = "Circuit2";

                    foreach (var Textboxvar in this.gbCircuit2.Controls.OfType<TextBox>())
                    {
                        objMain.CircuitValueType = Textboxvar.Name;
                        objMain.CircuitValue = Textboxvar.Text;

                        objMain.InsertCircuitData();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_CircuitData - " + ex);
            }

            #endregion

            #region OAUT_TC_Compressor

            try
            {
                objMain.CompNum = "Comp1";

                objMain.CompModelNum = tbCompressor1ModelNum.Text;
                objMain.CompSerialNum = tbCompressor1SerialNum.Text;
                objMain.CompRunningAmps = 0;
                if (tbAMACompressorRunningAmps1.Text.Length > 0)
                {
                    objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps1.Text);
                }

                objMain.CompRatedAmps = 0;
                if (tbAMACompressorRatedAmps1.Text.Length > 0)
                {
                    objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps1.Text);
                }
                objMain.InsertCompressorData();

                objMain.CompNum = "Comp2";

                objMain.CompModelNum = tbCompressor2ModelNum.Text;
                objMain.CompSerialNum = tbCompressor2SerialNum.Text;
                objMain.CompRunningAmps = 0;
                if (tbAMACompressorRunningAmps2.Text.Length > 0)
                {
                    objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps2.Text);
                }

                objMain.CompRatedAmps = 0;
                if (tbAMACompressorRatedAmps2.Text.Length > 0)
                {
                    objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps2.Text);
                }
                objMain.InsertCompressorData();
                if (unitTypeStr == "OA")
                {
                    objMain.CompNum = "Comp3";

                    objMain.CompModelNum = tbCompressor3ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor3SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps3.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps3.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps3.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps3.Text);
                    }
                    objMain.InsertCompressorData();

                    objMain.CompNum = "Comp4";

                    objMain.CompModelNum = tbCompressor4ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor4SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps4.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps4.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps4.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps4.Text);
                    }
                    objMain.InsertCompressorData();

                    objMain.CompNum = "Comp5";

                    objMain.CompModelNum = tbCompressor5ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor5SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps5.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps5.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps5.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps5.Text);
                    }
                    objMain.InsertCompressorData();

                    objMain.CompNum = "Comp6";

                    objMain.CompModelNum = tbCompressor6ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor6SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps6.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps6.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps6.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps6.Text);
                    }
                    objMain.InsertCompressorData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_Compressor - " + ex);
            }

            #endregion

            #region OAUT_TC_Condenser

            try
            {
                objMain.CondNum = "Cond1";

                objMain.CondModelNum = "";
                objMain.CondSerialNum = "";
                objMain.CondRunningAmps = 0;
                if (tbCondenserFanRunningAmps1.Text.Length > 0)
                {
                    objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps1.Text);
                }

                objMain.CondRatedAmps = 0;
                if (tbCondenserFanRatedAmps1.Text.Length > 0)
                {
                    objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps1.Text);
                }
                objMain.InsertCondenserData();

                objMain.CondNum = "Cond2";
                objMain.CondRunningAmps = 0;
                if (tbCondenserFanRunningAmps2.Text.Length > 0)
                {
                    objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps2.Text);
                }

                objMain.CondRatedAmps = 0;
                if (tbCondenserFanRatedAmps2.Text.Length > 0)
                {
                    objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps2.Text);
                }
                objMain.InsertCondenserData();

                if (unitTypeStr == "OA")
                {
                    objMain.CondNum = "Cond3";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps3.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps3.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps3.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps3.Text);
                    }
                    objMain.InsertCondenserData();

                    objMain.CondNum = "Cond4";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps4.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps4.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps4.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps4.Text);
                    }
                    objMain.InsertCondenserData();

                    objMain.CondNum = "Cond5";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps5.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps5.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps5.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps5.Text);
                    }
                    objMain.InsertCondenserData();

                    objMain.CondNum = "Cond6";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps6.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps6.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps6.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps6.Text);
                    }
                    objMain.InsertCondenserData();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_Compressor - " + ex);
            }
            #endregion

            #region OAUT_TC_Filter Insert
            string filtQty = "";
            dgvFilters.CurrentCell = null;
            foreach (DataGridViewRow row in dgvFilters.Rows)
            {
                if (row.Cells["FilterSize"].Value != null)
                {
                    objMain.FilterSize = row.Cells["FilterSize"].Value.ToString();
                    objMain.FilterStyle = row.Cells["FilterStyle"].Value.ToString();
                    objMain.Qty = 0;
                    if (row.Cells["Qty"].Value != null)
                    {
                        filtQty = row.Cells["Qty"].Value.ToString();
                        objMain.Qty = Convert.ToDecimal(filtQty);
                        //if (row.Cells["PartNum"].Value != null)
                        //{
                        //    objMain.FilterPartNum = row.Cells["PartNum"].Value.ToString();
                        //}

                    }
                    objMain.InsertFilterData();
                }
            }

            #endregion

            #region OAUT_TC_Pre Checks
            preChecksControl1.BindPreChecksDataFormToModel();
            #endregion

            #region OAUT_TC_PrePower Checks
           // prePowerChecksControl1.BindPreChecksDataFormToModel();
            #endregion
            #region OAUT_TC_Heat Insert

            objMain.Heat_FuelType = cbHeatSectionFuelType.Text;
            objMain.TopBottom = cbHeatPosition.Text;
            objMain.Heat_Primary = cbHeatPrimary.Text;
            objMain.Heat_Item_num = tbHeatItemNum.Text;
            objMain.Heat_PO_num = tbHeatPONum.Text;
            objMain.Heat_model_num_Top = tbHeatModelNumTop.Text;
            objMain.Heat_Serial_Top = tbHeatSerialNumTop.Text;
            objMain.Heat_Partnum_Top = tbHeatPartNumTop.Text;
            objMain.Heat_Size_Top = tbHeatSizeTop.Text;
            objMain.Heat_Rated_Amp = tbHeatRatedAmps.Text;
            objMain.Heat_Running_Amp = tbHeatRunningAmps.Text;

            objMain.Heat_model_num_Bottom = tbHeatModelNumBottom.Text;
            objMain.Heat_Serial_Bottom = tbHeatSerialNumBottom.Text;
            objMain.Heat_Partnum_Bottom = tbHeatPartNumBottom.Text;
            objMain.Heat_Size_Bottom = tbHeatSizeBottom.Text;
            objMain.Heat_model_num_PreHeat = tbPreHeatModelNo.Text;
            objMain.Heat_Serial_PreHeat = tbPreHeatSerialNo.Text;
            objMain.Heat_RatedAmp_PreHeat = tbPreHeatRatedAmps.Text;
            objMain.Heat_RunningAmp_PreHeat = tbPreHeatRunningAmps.Text;
            objMain.Heat_Size_PreHeat = tbPreHeatSize.Text;

            objMain.InsertHeatData();

            #endregion

            #region OAUT_TC_Options Insert          

            if (unitTypeStr == "HA")
            {
                foreach (var Checkboxvar in this.gbOptionsViking.Controls.OfType<CheckBox>())
                {
                    objMain.OptionName = Checkboxvar.Name;

                    if (objMain.OptionName.Contains("Installed"))
                    {
                        objMain.OptionInstalled = Convert.ToByte(Checkboxvar.Checked);
                        objMain.OptionTested = 0;
                    }
                    else if (objMain.OptionName.Contains("Tested"))
                    {
                        objMain.OptionInstalled = 0;
                        objMain.OptionTested = Convert.ToByte(Checkboxvar.Checked);
                    }

                    objMain.InsertOptionsData();
                }
                foreach (var Checkboxvar in this.gbStdEquipViking.Controls.OfType<CheckBox>())
                {
                    objMain.OptionName = Checkboxvar.Name;
                    if (objMain.OptionName.Contains("Installed"))
                    {
                        objMain.OptionInstalled = Convert.ToByte(Checkboxvar.Checked);
                        objMain.OptionTested = 0;
                    }
                    else if (objMain.OptionName.Contains("Tested"))
                    {
                        objMain.OptionInstalled = 0;
                        objMain.OptionTested = Convert.ToByte(Checkboxvar.Checked);
                    }

                    objMain.InsertOptionsData();
                }
            }
            else
            {
                foreach (var Checkboxvar in this.gbOptions.Controls.OfType<CheckBox>())
                {
                    objMain.OptionName = Checkboxvar.Name;

                    if (objMain.OptionName.Contains("Installed"))
                    {
                        objMain.OptionInstalled = Convert.ToByte(Checkboxvar.Checked);
                        objMain.OptionTested = 0;
                    }
                    else if (objMain.OptionName.Contains("Tested"))
                    {
                        objMain.OptionInstalled = 0;
                        objMain.OptionTested = Convert.ToByte(Checkboxvar.Checked);
                    }

                    objMain.InsertOptionsData();
                }
                foreach (var Checkboxvar in this.gbStandardEquipment.Controls.OfType<CheckBox>())
                {
                    objMain.OptionName = Checkboxvar.Name;
                    if (objMain.OptionName.Contains("Installed"))
                    {
                        objMain.OptionInstalled = Convert.ToByte(Checkboxvar.Checked);
                        objMain.OptionTested = 0;
                    }
                    else if (objMain.OptionName.Contains("Tested"))
                    {
                        objMain.OptionInstalled = 0;
                        objMain.OptionTested = Convert.ToByte(Checkboxvar.Checked);
                    }

                    objMain.InsertOptionsData();
                }
            }
            #endregion

            #region OAUT_TC_Voltage Insert          
            byte checkedVal = 0;

            foreach (var Textboxvar in this.gbVoltage.Controls.OfType<TextBox>())
            {
                objMain.Voltage = Textboxvar.Name;
                objMain.Voltage_Value = Textboxvar.Text;

                objMain.InsertVoltageData();
            }
            foreach (var Radiobtnvar in this.gbVoltage.Controls.OfType<RadioButton>())
            {
                checkedVal = Convert.ToByte(Radiobtnvar.Checked);
                if (checkedVal == 1)
                {
                    objMain.OptionName = Radiobtnvar.Name;
                    if (objMain.OptionName.Contains("208"))
                    {
                        objMain.Voltage_Value = "208";
                    }
                    else if (objMain.OptionName.Contains("460"))
                    {
                        objMain.Voltage_Value = "460";
                    }
                    else
                    {
                        objMain.Voltage_Value = "575";
                    }
                    objMain.Voltage = Radiobtnvar.Name;
                    objMain.InsertVoltageData();
                }

                IsNew = false;
            }

            #endregion

            #region OAUT_TC_MeasurePressure Insert
            try
            {
                if (txtInletModulationPresure.Text.Length > 0)
                {
                    objMain.InletModulationPressure = decimal.Parse(txtInletModulationPresure.Text);
                }
                if (txtLowFire.Text.Length > 0)
                {
                    objMain.LowFire = decimal.Parse(txtLowFire.Text);
                }
                if (txtHighFire.Text.Length > 0)
                {
                    objMain.HighFire = decimal.Parse(txtHighFire.Text);
                }

                objMain.InsertMeasurePressure();
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_MeasurePressure - " + ex);
            }

            #endregion

        }

        public void ResizeCheckContainer()
        {
            pnlProductCheckListContainer.Height = tabPage3.ClientRectangle.Height - 5;
            pnlProductCheckListContainer.Width = tabPage3.ClientRectangle.Width;
        }
        #endregion    

        #region "Private Methods"

        private void BindOAUTestingChecklistAtPrintEvent()
        {
            string unitTypeStr = tbModelNum.Text.Substring(0, 2);

            #region OAUT_TC_Head Data

            objMain.AC_Temp = nudTemperature.Value;
            objMain.AC_Humidity = nudTemperature.Value;
            objMain.Charge_Circuit_1 = nudCircuit1Charge.Value;
            objMain.Charge_Circuit_2 = nudCircuit2Charge.Value;
            objMain.Charge_Circuit_1_Var = tbCircuit1ChargeVariance.Text;
            objMain.Charge_Circuit_2_Var = tbCircuit2ChargeVariance.Text;
            objMain.Disconnect_Type = cbDisconnectType.Text;
            objMain.Disconnect_Size = tbDisconnectSize.Text;
            objMain.ERV_ModelNo = tbERV_ModelNo.Text;
            objMain.ERV_SerialNo = tbERV_SerialNo.Text;
            objMain.ERV_Running_Amp = tbERVRunningAmps.Text;
            objMain.ERV_Rated_Amp = tbERVRatedAmps.Text;

            if (chkbERVProperRotation.Checked == true)
            {
                objMain.ERV_Proper_Rotation = 1;
            }
            else
            {
                objMain.ERV_Proper_Rotation = 0;
            }

            #endregion            

            #region OAUT_TC_Blower Data

            if (objMain.Blowers == null)
            {
                objMain.Blowers = new List<OAUBlowers>();
            }

            objMain.Blowers.Clear();

            objMain.Blowers.Add(CreateBlowerInfo(label10.Text, tbHpIndoor.Text, tbHpExhaust.Text));

            objMain.Blowers.Add(CreateBlowerInfo(label11.Text, tbRatedAmpsIndoor.Text, tbRatedAmpsExhaust.Text));

            objMain.Blowers.Add(CreateBlowerInfo(label12.Text, tbRunningAmpsIndoor.Text, tbRunningAmpsExhaust.Text));

            objMain.Blowers.Add(CreateBlowerInfo(label13.Text, tbWheelSizeIndoor.Text, tbWheelSizeExhaust.Text));

            objMain.Blowers.Add(CreateBlowerInfo(label14.Text, tbMaxHzIndoor.Text, tbMaxHzExhaust.Text));

            objMain.Blowers.Add(CreateBlowerInfo(label15.Text, tbMinHzIndoor.Text, tbMinHzExhaust.Text));

            objMain.Blowers.Add(CreateBlowerInfo(label16.Text, tbVoltageOutputIndoor.Text, tbVoltageOutputExhaust.Text));

            objMain.IndoorBlowerModel = tbModelNumIndoor.Text;

            objMain.IndoorBlowerType = chkbDoubleBlowerIndoor.Checked ? OptionValue.Yes : OptionValue.No;

            objMain.ExhaustBlowerModel = tbModelNumExhaust.Text;

            objMain.ExhaustBlowerType = chkbDoubleBlowerExhaust.Checked == true ? OptionValue.Yes : OptionValue.No;

            #endregion

            #region OAUT_TC_CircuitData

            try
            {

                objMain.CircuitNum = "Circuit1";

                objMain.Circuit1 = new OAUCircuit1();

                objMain.Circuit1.SuctPSIVal = tbCircuit1SuctPSI.Text;
                objMain.Circuit1.SSTVal = tbCircuit1SST.Text;
                objMain.Circuit1.SuctTempVal = tbCircuit1SuctTemp.Text;
                objMain.Circuit1.LiqPSIVal = tbCircuit1LiqPSI.Text;
                objMain.Circuit1.SLT = tbCircuit1SLT.Text;
                objMain.Circuit1.LiqTempVal = tbCircuit1LiqTemp.Text;
                objMain.Circuit1.DisPSIVal = tbCircuit1DisPSI.Text;
                objMain.Circuit1.SDTVal = tbCircuit1SDT.Text;
                objMain.Circuit1.DisTempVal = tbCircuit1DisTemp.Text;
                objMain.Circuit1.Subcooling = tbCircuit1Subcooling.Text;
                objMain.Circuit1.SuperHeat = txtSuperHeat.Text;

                objMain.CircuitValueType = "chkbCircuit1APRActive";
                objMain.CircuitValue = "False";

                objMain.APRActive = chkbCircuit1APRActive.Checked ? OptionValue.Yes : OptionValue.No;

                objMain.CircuitNum = "Circuit2";

                objMain.Circuit2 = new OAUCircuit2();

                objMain.Circuit2.SuctPSIVal = tbCircuit2SuctPSI.Text;
                objMain.Circuit2.SSTVal = tbCircuit2SST.Text;
                objMain.Circuit2.SuctTempVal = tbCircuit2SuctTemp.Text;
                objMain.Circuit2.LiqPSIVal = tbCircuit2LiqPSI.Text;
                objMain.Circuit2.SLT = tbCircuit2SLT.Text;
                objMain.Circuit2.LiqTempVal = tbCircuit2LiqTemp.Text;
                objMain.Circuit2.DisPSIVal = tbCircuit2DisPSI.Text;
                objMain.Circuit2.SDTVal = tbCircuit2SDT.Text;
                objMain.Circuit2.DisTempVal = tbCircuit2DisTemp.Text;
                objMain.Circuit2.Subcooling = tbCircuit2Subcooling.Text;
                objMain.Circuit2.SuperHeat = tbCircuit2SuperHeat.Text;

            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_CircuitData - " + ex);
            }

            #endregion

            #region OAUT_TC_Compressor

            try
            {
                if (objMain.Compressors == null)
                {
                    objMain.Compressors = new List<OAUCompressor>();
                }
                objMain.CompNum = "Comp1";

                objMain.Compressors.Clear();
                objMain.CompModelNum = tbCompressor1ModelNum.Text;
                objMain.CompSerialNum = tbCompressor1SerialNum.Text;
                objMain.CompRunningAmps = 0;
                if (tbAMACompressorRunningAmps1.Text.Length > 0)
                {
                    objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps1.Text);
                }

                objMain.CompRatedAmps = 0;
                if (tbAMACompressorRatedAmps1.Text.Length > 0)
                {
                    objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps1.Text);
                }
                objMain.Compressors.Add(new OAUCompressor()
                {
                    CompressorNum = "Compressor Fan #1",
                    CompressorModelNum = objMain.CompModelNum,
                    CompressorSerialNum = objMain.CompSerialNum,
                    CompressorRunningAmps = objMain.CompRunningAmps,
                    CompressorRatedAmps = objMain.CompRatedAmps
                });

                objMain.CompNum = "Comp2";

                objMain.CompModelNum = tbCompressor2ModelNum.Text;
                objMain.CompSerialNum = tbCompressor2SerialNum.Text;
                objMain.CompRunningAmps = 0;
                if (tbAMACompressorRunningAmps2.Text.Length > 0)
                {
                    objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps2.Text);
                }

                objMain.CompRatedAmps = 0;
                if (tbAMACompressorRatedAmps2.Text.Length > 0)
                {
                    objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps2.Text);
                }
                objMain.Compressors.Add(new OAUCompressor()
                {
                    CompressorNum = "Compressor Fan #2",
                    CompressorModelNum = objMain.CompModelNum,
                    CompressorSerialNum = objMain.CompSerialNum,
                    CompressorRunningAmps = objMain.CompRunningAmps,
                    CompressorRatedAmps = objMain.CompRatedAmps
                });

                if (unitTypeStr == "OA")
                {
                    objMain.CompNum = "Comp3";

                    objMain.CompModelNum = tbCompressor3ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor3SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps3.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps3.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps3.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps3.Text);
                    }
                    objMain.Compressors.Add(new OAUCompressor()
                    {
                        CompressorNum = "Compressor Fan #3",
                        CompressorModelNum = objMain.CompModelNum,
                        CompressorSerialNum = objMain.CompSerialNum,
                        CompressorRunningAmps = objMain.CompRunningAmps,
                        CompressorRatedAmps = objMain.CompRatedAmps
                    });

                    objMain.CompNum = "Comp4";

                    objMain.CompModelNum = tbCompressor4ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor4SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps4.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps4.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps4.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps4.Text);
                    }
                    objMain.Compressors.Add(new OAUCompressor()
                    {
                        CompressorNum = "Compressor Fan #4",
                        CompressorModelNum = objMain.CompModelNum,
                        CompressorSerialNum = objMain.CompSerialNum,
                        CompressorRunningAmps = objMain.CompRunningAmps,
                        CompressorRatedAmps = objMain.CompRatedAmps
                    });

                    objMain.CompNum = "Comp5";

                    objMain.CompModelNum = tbCompressor5ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor5SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps5.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps5.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps5.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps5.Text);
                    }
                    objMain.Compressors.Add(new OAUCompressor()
                    {
                        CompressorNum = "Compressor Fan #5",
                        CompressorModelNum = objMain.CompModelNum,
                        CompressorSerialNum = objMain.CompSerialNum,
                        CompressorRunningAmps = objMain.CompRunningAmps,
                        CompressorRatedAmps = objMain.CompRatedAmps
                    });

                    objMain.CompNum = "Comp6";

                    objMain.CompModelNum = tbCompressor6ModelNum.Text;
                    objMain.CompSerialNum = tbCompressor6SerialNum.Text;
                    objMain.CompRunningAmps = 0;
                    if (tbAMACompressorRunningAmps6.Text.Length > 0)
                    {
                        objMain.CompRunningAmps = decimal.Parse(tbAMACompressorRunningAmps6.Text);
                    }

                    objMain.CompRatedAmps = 0;
                    if (tbAMACompressorRatedAmps6.Text.Length > 0)
                    {
                        objMain.CompRatedAmps = decimal.Parse(tbAMACompressorRatedAmps6.Text);
                    }
                    objMain.Compressors.Add(new OAUCompressor()
                    {
                        CompressorNum = "Compressor Fan #6",
                        CompressorModelNum = objMain.CompModelNum,
                        CompressorSerialNum = objMain.CompSerialNum,
                        CompressorRunningAmps = objMain.CompRunningAmps,
                        CompressorRatedAmps = objMain.CompRatedAmps
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_Compressor - " + ex);
            }

            #endregion

            #region OAUT_TC_Condenser

            try
            {

                if (objMain.Condensers == null)
                {
                    objMain.Condensers = new List<OAUCondenser>();
                }
                objMain.Condensers.Clear();

                objMain.CondNum = "Cond1";

                objMain.CondModelNum = "";
                objMain.CondSerialNum = "";
                objMain.CondRunningAmps = 0;
                if (tbCondenserFanRunningAmps1.Text.Length > 0)
                {
                    objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps1.Text);
                }

                objMain.CondRatedAmps = 0;
                if (tbCondenserFanRatedAmps1.Text.Length > 0)
                {
                    objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps1.Text);
                }
                objMain.Condensers.Add(new OAUCondenser()
                {
                    CondenserNum = "Condenser Fan #1",
                    CondenserRatedAmps = objMain.CondRatedAmps,
                    ConenserdRunningAmps = objMain.CondRunningAmps
                });
                objMain.CondNum = "Cond2";
                objMain.CondRunningAmps = 0;
                if (tbCondenserFanRunningAmps2.Text.Length > 0)
                {
                    objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps2.Text);
                }

                objMain.CondRatedAmps = 0;
                if (tbCondenserFanRatedAmps2.Text.Length > 0)
                {
                    objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps2.Text);
                }
                objMain.Condensers.Add(new OAUCondenser()
                {
                    CondenserNum = "Condenser Fan #2",
                    CondenserRatedAmps = objMain.CondRatedAmps,
                    ConenserdRunningAmps = objMain.CondRunningAmps
                });
                if (unitTypeStr == "OA")
                {
                    objMain.CondNum = "Cond3";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps3.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps3.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps3.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps3.Text);
                    }
                    objMain.Condensers.Add(new OAUCondenser()
                    {
                        CondenserNum = "Condenser Fan #3",
                        CondenserRatedAmps = objMain.CondRatedAmps,
                        ConenserdRunningAmps = objMain.CondRunningAmps
                    });
                    objMain.CondNum = "Cond4";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps4.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps4.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps4.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps4.Text);
                    }
                    objMain.Condensers.Add(new OAUCondenser()
                    {
                        CondenserNum = "Condenser Fan #4",
                        CondenserRatedAmps = objMain.CondRatedAmps,
                        ConenserdRunningAmps = objMain.CondRunningAmps
                    });
                    objMain.CondNum = "Cond5";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps5.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps5.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps5.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps5.Text);
                    }
                    objMain.Condensers.Add(new OAUCondenser()
                    {
                        CondenserNum = "Condenser Fan #5",
                        CondenserRatedAmps = objMain.CondRatedAmps,
                        ConenserdRunningAmps = objMain.CondRunningAmps
                    });

                    objMain.CondNum = "Cond6";

                    objMain.CondRunningAmps = 0;
                    if (tbCondenserFanRunningAmps6.Text.Length > 0)
                    {
                        objMain.CondRunningAmps = decimal.Parse(tbCondenserFanRunningAmps6.Text);
                    }

                    objMain.CondRatedAmps = 0;
                    if (tbCondenserFanRatedAmps6.Text.Length > 0)
                    {
                        objMain.CondRatedAmps = decimal.Parse(tbCondenserFanRatedAmps6.Text);
                    }
                    objMain.Condensers.Add(new OAUCondenser()
                    {
                        CondenserNum = "Condenser Fan #6",
                        CondenserRatedAmps = objMain.CondRatedAmps,
                        ConenserdRunningAmps = objMain.CondRunningAmps
                    });
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("ERROR -- Saving OAUT_TC_Compressor - " + ex);
            }


            #endregion

            #region OAUT_TC_Filter Insert
            string filtQty = "";

            if (objMain.Filters == null)
            {
                objMain.Filters = new List<OAUFilter>();
            }

            objMain.Filters.Clear();
            foreach (DataGridViewRow row in dgvFilters.Rows)
            {
                if (row.Cells["FilterSize"].Value == null)
                {
                    continue;
                }

                objMain.FilterSize = row.Cells["FilterSize"].Value != null ? row.Cells["FilterSize"].Value.ToString() : "";
                objMain.FilterStyle = row.Cells["FilterStyle"].Value != null ? row.Cells["FilterStyle"].Value.ToString() : "";

                filtQty = row.Cells["Qty"].Value != null ? row.Cells["Qty"].Value.ToString() : "0";
                objMain.Qty = Convert.ToDecimal(filtQty);
                //if (row.Cells["PartNum"].Value != null)
                //{
                //    objMain.FilterPartNum = row.Cells["PartNum"].Value.ToString();
                //}

                objMain.Filters.Add(new OAUFilter()
                {
                    FiltersSize = objMain.FilterSize,
                    FiltersStyle = objMain.FilterStyle,
                    Qnty = objMain.Qty
                });
            }



            #endregion

            #region OAUT_TC_Heat Insert

            objMain.TopBottom = cbHeatPosition.Text;
            objMain.Heat_Primary = cbHeatPrimary.Text;

            objMain.Heat_FuelType = cbHeatSectionFuelType.Text;
            objMain.Heat_PO_num = tbHeatPONum.Text;
            objMain.Heat_Item_num = tbHeatItemNum.Text;
            objMain.Heat_model_num_Top = tbHeatModelNumTop.Text;
            objMain.Heat_Serial_Top = tbHeatSerialNumTop.Text;
            objMain.Heat_Partnum_Top = tbHeatPartNumTop.Text;
            objMain.Heat_Size_Top = tbHeatSizeTop.Text;
            objMain.Heat_Rated_Amp = tbHeatRatedAmps.Text;
            objMain.Heat_Running_Amp = tbHeatRunningAmps.Text;

            objMain.Heat_model_num_Bottom = tbHeatModelNumBottom.Text;
            objMain.Heat_Serial_Bottom = tbHeatSerialNumBottom.Text;
            objMain.Heat_Partnum_Bottom = tbHeatPartNumBottom.Text;
            objMain.Heat_Size_Bottom = tbHeatSizeBottom.Text;

            objMain.Heat_model_num_PreHeat = tbPreHeatModelNo.Text;
            objMain.Heat_Serial_PreHeat = tbPreHeatSerialNo.Text;
            objMain.Heat_RatedAmp_PreHeat = tbPreHeatRatedAmps.Text;
            objMain.Heat_RunningAmp_PreHeat = tbPreHeatRunningAmps.Text;
            objMain.Heat_Size_PreHeat = tbPreHeatSize.Text;

            #endregion

            #region OAUT_TC_Options Insert          

            if (objMain.Options == null)
            {
                objMain.Options = new List<OAUOption>();
            }

            if (objMain.StandardEquipments == null)
            {
                objMain.StandardEquipments = new List<OAUOption>();
            }
            objMain.Options.Clear();
            objMain.StandardEquipments.Clear();

            if (unitTypeStr == "HA")
            {     ////////////////////////Options checkboxes for Viking
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label147.Text,
                    InstalledType = cbHeaterInstalledVkg.Checked,
                    TestedType = cbHeaterTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label146.Text,
                    InstalledType = cbPwrExhInstalledVkg.Checked,
                    TestedType = cbPwrExhTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label145.Text,
                    InstalledType = cbERVInstalledVkg.Checked,
                    TestedType = cbERVTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label144.Text,
                    InstalledType = cbBypDampInstalledVkg.Checked,
                    TestedType = cbBypDampTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label143.Text,
                    InstalledType = cbConvOutletInstalledVkg.Checked,
                    TestedType = cbConvOutletTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label142.Text,
                    InstalledType = cbSupplyPiezoInstalledVkg.Checked,
                    TestedType = cbSupplyPiezoTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label140.Text,
                    InstalledType = cbExhPiezoInstalledVkg.Checked,
                    TestedType = cbExhPiezoTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label77.Text,
                    InstalledType = cbLEDServiceVKgInstalled.Checked,
                    TestedType = cbLEDServiceVkgTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label138.Text,
                    InstalledType = cbAuxLightsInstalledVkg.Checked,
                    TestedType = cbAuxLightsTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label80.Text,
                    InstalledType = cbHailguardVkgInstalled.Checked,
                    TestedType = cbHailguardVkgTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label136.Text,
                    InstalledType = cbHGRHInstalledVkg.Checked,
                    TestedType = cbHGRHTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label135.Text,
                    InstalledType = cbPreHeatInstalledVkg.Checked,
                    TestedType = cbPreHeatTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label141.Text,
                    InstalledType = cbFiltStatSwitchInstalledVkg.Checked,
                    TestedType = cbFiltStatSwitchTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label133.Text,
                    InstalledType = cbOADampInstalledVkg.Checked,
                    TestedType = cbOADampTestedVkg.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label132.Text,
                    InstalledType = cbRADampInstalledVkg.Checked,
                    TestedType = cbRADampTestedVkg.Checked
                });
                ////////////////////////StandardEquipment checkboxes for Viking
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label134.Text,
                    InstalledType = cbFanFailSwitchInstalledVkg.Checked,
                    TestedType = cbFanFailSwitchTestedVkg.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label131.Text,
                    InstalledType = cbFSSInstalledVkg.Checked,
                    TestedType = cbFSSTestedVkg.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label127.Text,
                    InstalledType = cbOADamperInstalledVkg.Checked,
                    TestedType = cbOADamperTestedVkg.Checked
                });
            }
            else
            {    ////////////////////////Options checkboxes for OAU
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label85.Text,
                    InstalledType = cbSupplySmokeDectectorInstalled.Checked,
                    TestedType = cbSupplySmokeDectectorTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label86.Text,
                    InstalledType = cbReturnSmokeDetectorInstalled.Checked,
                    TestedType = cbReturnSmokeDetectorTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label97.Text,
                    InstalledType = cbHeaterInstalled.Checked,
                    TestedType = cbHeaterTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label98.Text,
                    InstalledType = cbPoweredExhaustInstalled.Checked,
                    TestedType = cbPoweredExhaustTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label99.Text,
                    InstalledType = cbERVInstalled.Checked,
                    TestedType = cbERVTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label100.Text,
                    InstalledType = cbBypassDampersInstalled.Checked,
                    TestedType = cbBypassDampersTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label101.Text,
                    InstalledType = cbConvenienceOutletInstalled.Checked,
                    TestedType = cbConvenienceOutletTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label102.Text,
                    InstalledType = cbSupplyPiezoInstalled.Checked,
                    TestedType = cbSupplyPiezoTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label103.Text,
                    InstalledType = cbExhaustPiezoInstalled.Checked,
                    TestedType = cbExhaustPiezoTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label139.Text,
                    InstalledType = cbProtoNodeInstalled.Checked,
                    TestedType = cbProtoNodeTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label106.Text,
                    InstalledType = cbAuxiliaryLightsInstalled.Checked,
                    TestedType = cbAuxiliaryLightsTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label137.Text,
                    InstalledType = cbReversingValvesInstalled.Checked,
                    TestedType = cbReversingValvesTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label108.Text,
                    InstalledType = cbHotGasReheatInstalled.Checked,
                    TestedType = cbHotGasReheatTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label109.Text,
                    InstalledType = cbPreHeatInstalled.Checked,
                    TestedType = cbPreHeatTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label23.Text,
                    InstalledType = cbIsolationDamperoption.Checked,
                    TestedType = cbIsolationDamperTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label105.Text,
                    InstalledType = cbLEDServiceInstalled.Checked,
                    TestedType = cbLEDServiceTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = label107.Text,
                    InstalledType = cbHailguradInstalled.Checked,
                    TestedType = cbHailguardTested.Checked
                });
                objMain.Options.Add(new OAUOption()
                {
                    Optionlabel = lblGreenTrol.Text,
                    InstalledType = cbGreenTrolInstalled.Checked,
                    TestedType = cbGreenTrolTested.Checked
                });

                ////////////////////standardEquipment checkboxes for OAU
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label58.Text,
                    InstalledType = cbFanFailureInstalled.Checked,
                    TestedType = cbFanFailureTested.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label87.Text,
                    InstalledType = cbFilterStatusInstalled.Checked,
                    TestedType = cbFilterStatusTested.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label88.Text,
                    InstalledType = cbOADampersInstalled.Checked,
                    TestedType = cbOADampersTested.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label116.Text,
                    InstalledType = cbCCHeaterInstalled.Checked,
                    TestedType = cbCCHeaterTested.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label81.Text,
                    InstalledType = cbDigitalScrollInstalled.Checked,
                    TestedType = cbDigitalScrollTested.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label83.Text,
                    InstalledType = cbAPRInstalled.Checked,
                    TestedType = cbAPRTested.Checked
                });
                objMain.StandardEquipments.Add(new OAUOption()
                {
                    Optionlabel = label84.Text,
                    InstalledType = cbRADampersInstalled.Checked,
                    TestedType = cbRADampersTested.Checked
                });
            }

            #endregion

            #region OAUT_TC_Voltage Insert          
            byte checkedVal = 0;

            objMain.Voltages = new OAUVoltage();

            objMain.Voltages.VoltageL1L2 = tbVoltageL1L2.Text;
            objMain.Voltages.VoltageL2L3 = tbVoltageL2L3.Text;
            objMain.Voltages.VoltageL1L3 = tbVoltageL1L3.Text;
            objMain.Voltages.VoltageL1G = tbVoltageL1G.Text;
            objMain.Voltages.VoltageL2G = tbVoltageL2G.Text;
            objMain.Voltages.VoltageL3G = tbVoltageL3G.Text;

            foreach (var Radiobtnvar in this.gbVoltage.Controls.OfType<RadioButton>())
            {
                checkedVal = Convert.ToByte(Radiobtnvar.Checked);
                if (checkedVal == 1)
                {
                    objMain.OptionName = Radiobtnvar.Name;
                    if (objMain.OptionName.Contains("208"))
                    {
                        objMain.Voltages.Voltage = "208";
                    }
                    else if (objMain.OptionName.Contains("460"))
                    {
                        objMain.Voltages.Voltage = "460";
                    }
                    else if (objMain.OptionName.Contains("575"))
                    {
                        objMain.Voltages.Voltage = "575";
                    }
                    break;
                }
                else
                {
                    objMain.Voltages.Voltage = "";
                }

                IsNew = false;
            }

            #endregion

            #region OAUT_Measured_Pressure
            if (txtInletModulationPresure.Text.Length > 0)
            {
                objMain.InletModulationPressure = decimal.Parse(txtInletModulationPresure.Text);
            }
            if (txtLowFire.Text.Length > 0)
            {
                objMain.LowFire = decimal.Parse(txtLowFire.Text);
            }
            if (txtHighFire.Text.Length > 0)
            {
                objMain.HighFire = decimal.Parse(txtHighFire.Text);
            }
            #endregion
        }

        private OAUBlowers CreateBlowerInfo(string label, string indoorValue, string exhautValue)
        {
            OAUBlowers blowerData = new OAUBlowers()
            {
                BlowerProperty = label
            };

            decimal indoorVal = 0m; decimal exhaustVal = 0m;
            decimal.TryParse(indoorValue, out indoorVal);
            decimal.TryParse(exhautValue, out exhaustVal);
            blowerData.IndoorVal = indoorVal;
            blowerData.ExhaustVal = exhaustVal;

            return blowerData;
        }

        private void insertNewChecklist()
        {

            bindFormDataToModelAndSave();

        }
        #endregion


    }
}
