﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OA_VKG_TestingChecklist.Extensions
{
   public static class ControlExtensions
    {

        /// <summary>
        /// Find controls by type in the container
        /// </summary>
        /// <param name="ctrlContainer"></param>
        /// <param name="controlType"></param>
        /// <returns></returns>
        public static List<Control> FindControls(this Control ctrlContainer, Type controlType)
        {
            List<Control> controls = new List<Control>();
            foreach (Control ctrl in ctrlContainer.Controls)
            {
                if (ctrl.GetType() == controlType)
                {
                    controls.Add(ctrl);
                }
                if (ctrl.HasChildren)
                    controls.AddRange(FindControls(ctrl, controlType));
            }
            return controls;
        }
    }
}
