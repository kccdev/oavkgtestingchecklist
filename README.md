# OA VKG Testing Checklist

This windows forms application is a tool used by testers to record all data when testing a unit.

## Project Structure

### OA_VKG_TestingChecklist
This project is a .NET Framework 4.5 windows application project.

## Data Access
All data access to the KCC database is done via ADO.NET and stored procedures. The following stored procedures are used:

* OAUT_TC_GetOA_VKG_JobList
* OAUT_TC_GetOA_VKG_SearchJobHead
* OAUT_TC_GetBlowerData
* OAUT_TC_GetCondenserData
* OAUT_TC_GetCompressorData
* OAUT_TC_GetFilterData
* OAUT_TC_GetHeatData
* OAUT_TC_GetOptionsData
* OAUT_TC_GetVoltageData
* OAUT_TC_GetCircuitData
* OAUT_TC_InsertJobHead
* OAUT_TC_InsertBlowerData
* OAUT_TC_InsertCircuitData
* OAUT_TC_InsertCompressorData
* OAUT_TC_InsertCondenserData
* OAUT_TC_InsertFilterData
* OAUT_TC_InsertHeatData
* OAUT_TC_InsertOptionsData
* OAUT_TC_InsertVoltageData
* OAUT_TC_DeleteChecklistByHeadID
* OAUT_TC_GetJobIssues
* OAUT_TC_InsertIssues
* OAUT_TC_UpdateIssues
* OAUT_TC_UpdateIssuesHeadID

## Reports
The following Crystal Reports are used (reports not stored on epicor905app server are used for debugging purposes):

* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\RefrigerationComponent_TestVersion.rpt
* \\EPICOR905APP\Apps\OAU\OAUConfiguratorcrystalreports\RefrigerationComponent.rpt
* rassdk://\\\\EPICOR905APP\\Apps\\OAU\\OAUConfiguratorcrystalreports\\R6_OA_NoJobNumJobTraveler.rpt

## How to run locally
Assuming a VPN connection, make sure solution is ready to run in Debug configuration. Change the connection strings in frmMain.cs #if Debug sections to point to the appropriate local or dev KCC database.
Make sure Crystal Reports for Visual Studio is installed, and fix all broken references to point to the installed Crystal Reports .dlls. Disable manifest signing. Build the solution and run it. 

## How to deploy
One-click publish: [https://docs.microsoft.com/en-us/visualstudio/deployment/how-to-publish-a-clickonce-application-using-the-publish-wizard?view=vs-2019]

## Author/Devs
Tony Thoman